-- MySQL dump 10.10
--
-- Host: localhost    Database: cantine2
-- ------------------------------------------------------
-- Server version	5.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `classes`
--

DROP TABLE IF EXISTS `classes`;
CREATE TABLE `classes` (
  `id_classe` int(2) NOT NULL auto_increment,
  `classe` varchar(15) NOT NULL,
  `categorie` varchar(10) NOT NULL,
  PRIMARY KEY  (`id_classe`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classes`
--


/*!40000 ALTER TABLE `classes` DISABLE KEYS */;
LOCK TABLES `classes` WRITE;
INSERT INTO `classes` VALUES (1,'Touts petits','maternelle'),(2,'Petits','maternelle'),(3,'Moyens','maternelle'),(4,'Grands','maternelle'),(5,'CP','primaire'),(6,'CE1','primaire'),(7,'CE2','primaire'),(8,'CM1','primaire'),(9,'CM2','primaire'),(10,'Adulte','adultes');
UNLOCK TABLES;
/*!40000 ALTER TABLE `classes` ENABLE KEYS */;

--
-- Table structure for table `conges`
--

DROP TABLE IF EXISTS `conges`;
CREATE TABLE `conges` (
  `id_conge` int(11) NOT NULL,
  `annee` varchar(9) default NULL,
  `nom` varchar(25) default NULL,
  `debut` date default NULL,
  `fin` date default NULL,
  PRIMARY KEY  (`id_conge`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `conges`
--


/*!40000 ALTER TABLE `conges` DISABLE KEYS */;
LOCK TABLES `conges` WRITE;
INSERT INTO `conges` VALUES (1,'2005-2006','Toussaint','2005-11-01','2005-11-01'),(2,'2005-2006','Toussaint','2005-10-22','2005-11-03'),(3,'2005-2006','Armistice 18','2005-11-11','2005-11-11'),(4,'2005-2006','Noël','2005-12-17','2006-01-03'),(5,'2005-2006','Noël','2005-12-25','2005-12-25'),(6,'2005-2006','Jour de l\'\'an','2006-01-01','2006-01-01'),(7,'2005-2006','Hiver','2006-02-11','2006-02-27'),(8,'2005-2006','Lundi de pâques','2006-04-17','2006-04-17'),(9,'2005-2006','Printemps','2006-04-15','2006-05-02'),(10,'2005-2006','Fête du travail','2006-05-01','2006-05-01'),(11,'2005-2006','Victoire 45','2006-05-08','2005-05-08'),(12,'2005-2006','Ascension','2006-05-25','2006-05-25'),(13,'2005-2006','Lundi de pentecôte','2006-06-05','2006-06-05'),(14,'2005-2006','Été','2006-07-04','2006-09-04'),(15,'2006-2007','Fête nationale','2006-07-14','2006-07-14'),(16,'2006-2007','Assomption','2006-08-15','2006-08-15'),(17,'2006-2007','Toussaint','2006-10-25','2006-11-06'),(18,'2006-2007','Assomption','2006-11-01','2006-11-01'),(19,'2006-2007','Armistice 18','2006-11-11','2006-11-11'),(20,'2006-2007','Noël','2006-12-23','2007-01-08'),(21,'2006-2007','Noël','2006-12-25','2006-12-25'),(22,'2006-2007','Jour de l\'an','2007-01-01','2007-01-01'),(23,'2006-2007','Hiver','2007-02-24','2007-03-12'),(24,'2006-2007','Lundi de pâques','2007-04-09','2007-04-09'),(25,'2006-2007','Fête du travail','2007-05-01','2007-05-01'),(26,'2006-2007','Victoire 45','2007-05-08','2007-05-08'),(27,'2006-2007','Ascension','2007-05-17','2007-05-17'),(28,'2006-2007','Lundi de pentecôte','2007-05-28','2007-05-28'),(29,'2006-2007','Printemps','2007-04-14','2007-05-02'),(30,'2006-2007','Été','2007-07-04','2007-09-04');
UNLOCK TABLES;
/*!40000 ALTER TABLE `conges` ENABLE KEYS */;

--
-- Table structure for table `details_facture`
--

DROP TABLE IF EXISTS `details_facture`;
CREATE TABLE `details_facture` (
  `id_facture` int(11) NOT NULL default '0',
  `id_eleve` int(11) NOT NULL default '0',
  `quantite` int(11) NOT NULL default '0',
  `prix` float NOT NULL default '0',
  PRIMARY KEY  (`id_facture`,`id_eleve`),
  CONSTRAINT `fk_detail_eleve` FOREIGN KEY (`id_eleve`) REFERENCES `eleves` (`id_eleve`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_detail_facture` FOREIGN KEY (`id_facture`) REFERENCES `factures` (`id_facture`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `eleves`
--

DROP TABLE IF EXISTS `eleves`;
CREATE TABLE `eleves` (
  `id_eleve` int(11) NOT NULL default '0',
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `sexe` char(1) NOT NULL,
  `date_naissance` date default NULL,
  `lieu_naissance` varchar(100) default NULL,
  `id_classe` int(11) default NULL,
  `cantine` tinyint(1) default NULL,
  `centre_aere` tinyint(1) default NULL,
  `observations` longtext,
  `allergies` longtext,
  `id_famille` int(11) default NULL,
  PRIMARY KEY  (`id_eleve`),
  CONSTRAINT `fk_eleves_classe` FOREIGN KEY (`id_classe`) REFERENCES `classes` (`id_classe`) ON UPDATE CASCADE,
  CONSTRAINT `fk_eleves_famille` FOREIGN KEY (`id_famille`) REFERENCES `familles` (`id_famille`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `factures`
--

DROP TABLE IF EXISTS `factures`;
CREATE TABLE `factures` (
  `id_facture` int(11) NOT NULL default '0',
  `date` date NOT NULL default '0000-00-00',
  `id_famille` int(11) default NULL,
  `ht` decimal(10,2) default NULL,
  `acompte` float NOT NULL default '0',
  `deduire` float NOT NULL default '0',
  `regle` tinyint(1) default NULL,
  PRIMARY KEY  (`id_facture`),
  CONSTRAINT `fk_factures_famille` FOREIGN KEY (`id_famille`) REFERENCES `familles` (`id_famille`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `familles`
--

DROP TABLE IF EXISTS `familles`;
CREATE TABLE `familles` (
  `id_famille` int(11) NOT NULL default '0',
  `nom` varchar(100) default NULL,
  `adresse` varchar(200) default NULL,
  `cp` varchar(5) default NULL,
  `ville` varchar(100) default NULL,
  `telephone` varchar(10) default NULL,
  `tel_portable` varchar(10) default NULL,
  `mail` varchar(50) default NULL,
  `id_medecin` int(11) default NULL,
  `id_hopital` int(11) default NULL,
  `adulte` tinyint(1) default NULL,
  `assurance` varchar(50) default NULL,
  `num_police` varchar(20) default NULL,
  PRIMARY KEY  (`id_famille`),
  CONSTRAINT `fk_familles_hopitaux` FOREIGN KEY (`id_hopital`) REFERENCES `hopitaux` (`id_hopital`) ON UPDATE CASCADE,
  CONSTRAINT `fk_familles_medecins` FOREIGN KEY (`id_medecin`) REFERENCES `medecins` (`id_medecin`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `hopitaux`
--

DROP TABLE IF EXISTS `hopitaux`;
CREATE TABLE `hopitaux` (
  `id_hopital` int(11) NOT NULL default '0',
  `nom` varchar(50) NOT NULL,
  `adresse` varchar(200) NOT NULL,
  `cp` varchar(5) NOT NULL,
  `ville` varchar(50) NOT NULL,
  `tel` varchar(10) default NULL,
  `fax` varchar(10) default NULL,
  `tel_portable` varchar(10) default NULL,
  `mail` varchar(50) default NULL,
  `commentaires` longtext,
  PRIMARY KEY  (`id_hopital`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `medecins`
--

DROP TABLE IF EXISTS `medecins`;
CREATE TABLE `medecins` (
  `id_medecin` int(11) NOT NULL default '0',
  `nom` varchar(50) NOT NULL,
  `adresse` varchar(200) NOT NULL,
  `cp` varchar(5) NOT NULL,
  `ville` varchar(50) NOT NULL,
  `tel` varchar(10) default NULL,
  `fax` varchar(10) default NULL,
  `tel_portable` varchar(10) default NULL,
  `mail` varchar(50) default NULL,
  `commentaires` longtext,
  PRIMARY KEY  (`id_medecin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `opt_mairie`
--

DROP TABLE IF EXISTS `opt_mairie`;
CREATE TABLE `opt_mairie` (
  `id_mairie` int(11) NOT NULL,
  `nom` varchar(100) default NULL,
  `adresse` varchar(100) default NULL,
  `cp` varchar(5) default NULL,
  `ville` varchar(50) default NULL,
  `tel` varchar(10) default NULL,
  `fax` varchar(10) default NULL,
  `mail` varchar(50) default NULL,
  `site` varchar(100) default NULL,
  `logo` varchar(200) default NULL,
  `signataire` varchar(50) default NULL,
  PRIMARY KEY  (`id_mairie`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opt_mairie`
--


/*!40000 ALTER TABLE `opt_mairie` DISABLE KEYS */;
LOCK TABLES `opt_mairie` WRITE;
INSERT INTO `opt_mairie` VALUES (1,'','','','','','','','','','');
UNLOCK TABLES;
/*!40000 ALTER TABLE `opt_mairie` ENABLE KEYS */;

--
-- Table structure for table `opt_pointages`
--

DROP TABLE IF EXISTS `opt_pointages`;
CREATE TABLE `opt_pointages` (
  `lundi` tinyint(1) NOT NULL,
  `mardi` tinyint(1) NOT NULL,
  `mercredi` tinyint(1) NOT NULL,
  `jeudi` tinyint(1) NOT NULL,
  `vendredi` tinyint(1) NOT NULL,
  `samedi` tinyint(1) NOT NULL,
  `dimanche` tinyint(1) NOT NULL,
  `hide_closed` tinyint(1) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opt_pointages`
--


/*!40000 ALTER TABLE `opt_pointages` DISABLE KEYS */;
LOCK TABLES `opt_pointages` WRITE;
INSERT INTO `opt_pointages` VALUES (0,0,0,0,0,0,1,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `opt_pointages` ENABLE KEYS */;

--
-- Table structure for table `opt_tarifs`
--

DROP TABLE IF EXISTS `opt_tarifs`;
CREATE TABLE `opt_tarifs` (
  `id_tarif` int(11) NOT NULL,
  `nombre` int(11) NOT NULL,
  `prix` float NOT NULL,
  PRIMARY KEY  (`id_tarif`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opt_tarifs`
--


/*!40000 ALTER TABLE `opt_tarifs` DISABLE KEYS */;
LOCK TABLES `opt_tarifs` WRITE;
INSERT INTO `opt_tarifs` VALUES (1,1,2.77),(2,2,2.53),(3,3,2.23);
UNLOCK TABLES;
/*!40000 ALTER TABLE `opt_tarifs` ENABLE KEYS */;

--
-- Table structure for table `opt_traiteur`
--

DROP TABLE IF EXISTS `opt_traiteur`;
CREATE TABLE `opt_traiteur` (
  `id_traiteur` int(11) NOT NULL,
  `raison_sociale` varchar(50) default NULL,
  `adresse` varchar(100) default NULL,
  `cp` varchar(5) default NULL,
  `ville` varchar(50) default NULL,
  `telephone` varchar(10) default NULL,
  `fax` varchar(10) default NULL,
  `mail` varchar(50) default NULL,
  PRIMARY KEY  (`id_traiteur`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opt_traiteur`
--


/*!40000 ALTER TABLE `opt_traiteur` DISABLE KEYS */;
LOCK TABLES `opt_traiteur` WRITE;
INSERT INTO `opt_traiteur` VALUES (1,'','','','','','','');
UNLOCK TABLES;
/*!40000 ALTER TABLE `opt_traiteur` ENABLE KEYS */;

--
-- Table structure for table `pointages`
--

DROP TABLE IF EXISTS `pointages`;
CREATE TABLE `pointages` (
  `date` date NOT NULL default '0000-00-00',
  `id_eleve` int(11) NOT NULL default '0',
  `type` char(1) NOT NULL,
  `id_type` int(11) NOT NULL default '0',
  PRIMARY KEY  (`date`,`id_eleve`,`type`),
  CONSTRAINT `fk_pointages_eleve` FOREIGN KEY (`id_eleve`) REFERENCES `eleves` (`id_eleve`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_pointages_type` FOREIGN KEY (`id_type`) REFERENCES `types_pointages` (`id_type`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `presence`
--

DROP TABLE IF EXISTS `presence`;
CREATE TABLE `presence` (
  `id_eleve` int(11) NOT NULL default '0',
  `type` char(1) NOT NULL,
  `lundi` int(1) default NULL,
  `mardi` int(1) default NULL,
  `mercredi` int(1) default NULL,
  `jeudi` int(1) default NULL,
  `vendredi` int(1) default NULL,
  `samedi` int(1) default NULL,
  `dimanche` int(1) default NULL,
  PRIMARY KEY  (`id_eleve`,`type`),
  CONSTRAINT `fk_presence_eleve` FOREIGN KEY (`id_eleve`) REFERENCES `eleves` (`id_eleve`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id_role` int(11) NOT NULL default '0',
  `role` varchar(50) NOT NULL,
  PRIMARY KEY  (`id_role`),
  UNIQUE KEY `role` (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--


/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
LOCK TABLES `roles` WRITE;
INSERT INTO `roles` VALUES (1,'Père'),(2,'Mère');
UNLOCK TABLES;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

--
-- Table structure for table `tiers`
--

DROP TABLE IF EXISTS `tiers`;
CREATE TABLE `tiers` (
  `id_tiers` int(11) NOT NULL default '0',
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `tel_portable` varchar(10) default NULL,
  `mail` varchar(50) default NULL,
  `employeur` varchar(50) default NULL,
  `profession` varchar(50) default NULL,
  `tel_travail` varchar(10) default NULL,
  `poste_tel_travail` varchar(7) default NULL,
  `commentaires` longtext,
  `id_role` int(11) default NULL,
  `id_famille` int(11) default NULL,
  PRIMARY KEY  (`id_tiers`),
  CONSTRAINT `fk_tiers_famille` FOREIGN KEY (`id_famille`) REFERENCES `familles` (`id_famille`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tiers_role` FOREIGN KEY (`id_role`) REFERENCES `roles` (`id_role`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `types_pointages`
--

DROP TABLE IF EXISTS `types_pointages`;
CREATE TABLE `types_pointages` (
  `id_type` int(11) NOT NULL default '0',
  `type` varchar(30) NOT NULL,
  `couleur` varchar(11) NOT NULL,
  PRIMARY KEY  (`id_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `types_pointages`
--


/*!40000 ALTER TABLE `types_pointages` DISABLE KEYS */;
LOCK TABLES `types_pointages` WRITE;
INSERT INTO `types_pointages` VALUES (1,'a facturer','255,255,255'),(2,'déjà  déduit','0,255,255'),(3,'a rembourser','0,255,0'),(4,'non justifié','0,0,255'),(5,'en attente','255,0,0'),(6,'non prevue','255,255,0'),(7,'prépayé','255,153,0');
UNLOCK TABLES;
/*!40000 ALTER TABLE `types_pointages` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

