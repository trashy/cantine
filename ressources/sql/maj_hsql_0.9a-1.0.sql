--
-- Update cantine tables from 0.9a to 1.0
-- using hsqldb 1.8.0
--

-- Ajout acompte pour factures
ALTER TABLE factures ADD COLUMN acompte float DEFAULT 0 not null BEFORE regle;
-- Ajout deduire pour factures
ALTER TABLE factures ADD COLUMN deduire float DEFAULT 0 not null BEFORE regle;
-- Ajout signataire pour mairie
ALTER TABLE opt_mairie ADD COLUMN signataire VARCHAR(50);
-- Ajout site pour mairie
ALTER TABLE opt_mairie ADD COLUMN site VARCHAR(100) BEFORE logo;
-- Mise à niveau données mairie
UPDATE opt_mairie SET signataire='', site='';
-- Ajout cacher fermeture pour pointages
ALTER TABLE opt_pointages ADD COLUMN hide_closed boolean;
-- Mise à jour couleur pointages 'à facturer'
UPDATE types_pointages SET couleur='255,255,255' WHERE id_type=1;
-- Mise à jour couleur pointage 'déjà déduit'
UPDATE types_pointages SET couleur='0,255,255' WHERE id_type=2;
-- Mise à jour couleur pointage 'à rembourser'
UPDATE types_pointages SET couleur='0,255,0' WHERE id_type=3;
-- Mise à jour couleur pointage 'non justifié'
UPDATE types_pointages SET couleur='0,0,255' WHERE id_type=4;
-- Mise à jour couleur pointage 'en attente'
UPDATE types_pointages SET couleur='255,0,0' WHERE id_type=5;
-- Mise à jour couleur pointage 'non prévu'
UPDATE types_pointages SET couleur='255,255,0' WHERE id_type=6;
-- Mise à jour couleur pointage 'prépayé'
UPDATE types_pointages SET couleur='255,153,0' WHERE id_type=7;
