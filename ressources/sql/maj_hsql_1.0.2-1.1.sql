--
-- Compliance with MySQL auto_increment
-- For hsql, the first default identity value is 0 ; and for mysql the first 
-- auto_increment is 1
-- 
-- for usage with "cantine" v 1.x
-- using hsqldb 1.8.0
-- using mysql 5.0

-- we need first to drop and create again bad constraints
-- seems that hsql doesn't permit altering constraints
-- note that user must be sa !! otherwise the drop ans add constraints 
-- will fail as well as the updates

-- contraintes table élèves
ALTER TABLE eleves DROP CONSTRAINT SYS_FK_1266;
ALTER TABLE eleves DROP CONSTRAINT SYS_FK_1265;
ALTER TABLE eleves ADD CONSTRAINT fk_eleves_famille FOREIGN KEY(id_famille) REFERENCES familles(id_famille) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE eleves ADD CONSTRAINT fk_eleves_classe FOREIGN KEY(id_classe) REFERENCES classes(id_classe) ON DELETE RESTRICT ON UPDATE CASCADE;
-- contraintes table famille
ALTER TABLE familles DROP CONSTRAINT SYS_FK_1257;
ALTER TABLE familles DROP CONSTRAINT SYS_FK_1258;
ALTER TABLE familles ADD CONSTRAINT fk_familles_medecins FOREIGN KEY(id_medecin) REFERENCES medecins(id_medecin) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE familles ADD CONSTRAINT fk_familles_hopitaux FOREIGN KEY(id_hopital) REFERENCES hopitaux(id_hopital) ON DELETE RESTRICT ON UPDATE CASCADE;
-- contraintes table presence
ALTER TABLE presence DROP CONSTRAINT SYS_FK_1272;
ALTER TABLE presence ADD CONSTRAINT fk_presence_eleve FOREIGN KEY (id_eleve) REFERENCES eleves(id_eleve) ON DELETE CASCADE ON UPDATE CASCADE;
--contraintes table tiers
ALTER TABLE tiers DROP CONSTRAINT SYS_FK_1279;
ALTER TABLE tiers DROP CONSTRAINT SYS_FK_1280;
ALTER TABLE tiers ADD CONSTRAINT fk_tiers_role FOREIGN KEY (id_role) REFERENCES roles (id_role) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE tiers ADD CONSTRAINT fk_tiers_famille FOREIGN KEY (id_famille) REFERENCES familles (id_famille) ON DELETE CASCADE ON UPDATE CASCADE;
-- contraintes de la table factures
ALTER TABLE factures DROP CONSTRAINT SYS_FK_1286;
ALTER TABLE factures ADD CONSTRAINT fk_factures_famille FOREIGN KEY (id_famille) REFERENCES familles(id_famille) ON DELETE CASCADE ON UPDATE CASCADE;
-- contraintes de la table détails facture
ALTER TABLE details_facture DROP CONSTRAINT SYS_FK_1308;
ALTER TABLE details_facture DROP CONSTRAINT SYS_FK_1309;
ALTER TABLE details_facture ADD CONSTRAINT fk_detail_facture FOREIGN KEY (id_facture) REFERENCES factures (id_facture) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE details_facture ADD CONSTRAINT fk_detail_eleve FOREIGN KEY (id_eleve) REFERENCES eleves (id_eleve) ON DELETE CASCADE ON UPDATE CASCADE;
-- contraintes de la table pointages
ALTER TABLE pointages DROP CONSTRAINT SYS_FK_1291;
ALTER TABLE pointages DROP CONSTRAINT SYS_FK_1292;
ALTER TABLE pointages ADD CONSTRAINT fk_pointages_eleve FOREIGN KEY (id_eleve) REFERENCES eleves (id_eleve) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE pointages ADD CONSTRAINT fk_pointages_type FOREIGN KEY (id_type) REFERENCES types_pointages (id_type) ON DELETE RESTRICT ON UPDATE CASCADE;



UPDATE classes SET id_classe = id_classe+1;
UPDATE medecins SET id_medecin=id_medecin+1;
UPDATE hopitaux SET id_hopital=id_hopital+1;
UPDATE roles SET id_role=id_role+1;
UPDATE types_pointages SET id_type=id_type+1;
UPDATE familles SET id_famille=id_famille+1;
UPDATE factures SET id_facture=id_facture+1;
UPDATE eleves SET id_eleve=id_eleve+1;

-- tables sans relations
UPDATE conges SET id_conge=id_conge+1;
UPDATE opt_mairie SET id_mairie=id_mairie+1;
UPDATE opt_tarifs SET id_tarif=id_tarif+1;
UPDATE opt_traiteur SET id_traiteur=id_traiteur+1;
UPDATE tiers SET id_tiers=id_tiers+1;
