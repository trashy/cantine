#!/bin/sh
#creation des repertoires
mkdir windows
#mkdir windows/datas/
mkdir windows/libs/
mkdir windows/.config/
mkdir windows/.logs/
mkdir windows/files/
mkdir windows/sources/
mkdir windows/sources/cantine
mkdir windows/sources/xtnd-commons
#copie des fichiers
cp cantine.jar windows/
cp changelog.txt windows/
cp license.txt windows/
cp gpl.html windows/
cp icone.ico windows/
cp updater_icon.ico windows/
cp cantine.nsi windows/
cp ressources/installer-files/cantine.bat windows/
cp -r ressources/docs/ windows/
cp ../xtnd_database_update/xtnd_db_updater.jar windows/
cp ressources/sql/*.xml windows/files
cp ressources/sql/*.sql windows/files
cp .config/* windows/.config/
#cp datas/* windows/datas/
#cp ressources/datas-empty.tar.bz2 windows/
#cd windows && tar jxvf datas-empty.tar.bz2
#cd .. && rm windows/datas-empty.tar.bz2
cp ../commons-libs/xtnd/be.xtnd.commons-0.0.1.jar windows/libs/
cp ../commons-libs/xtnd/LICENSE.xtnd-commons.txt windows/libs/
cp ../commons-libs/bouncy/bcpg-jdk15-128.jar windows/libs/
cp ../commons-libs/bouncy/bcprov-jdk15-128.jar windows/libs/
cp ../commons-libs/bouncy/LICENSE.bouncy.txt windows/libs/
cp ../commons-libs/hsqldb/hsqldb.jar windows/libs/
cp ../commons-libs/hsqldb/LICENSE.hsqldb.txt windows/libs/
cp ../commons-libs/mysql/mysql-connector-java-3.1.12-bin.jar windows/libs/
cp ../commons-libs/mysql/LICENSE.mysql-connector.txt windows/libs/
cp ../commons-libs/jasper/jdt-compiler.jar windows/libs/
cp ../commons-libs/jasper/LICENSE.jdt-compiler.html windows/libs/
cp ../commons-libs/javahelp/jhall.jar windows/libs/
cp ../commons-libs/javahelp/LICENSE.javahelp.html windows/libs/
cp ../commons-libs/JGoodies/looks-1.3.1.jar windows/libs/
cp ../commons-libs/JGoodies/LICENSE.looks.txt windows/libs/
#sources cantine
cp -r src/ windows/sources/cantine
tar jcvf windows/sources/cantine-src.tar.bz2 windows/sources/cantine/
rm -rf windows/sources/cantine
#
cp -r ../xtnd-commons/src windows/sources/xtnd-commons/
tar jcvf windows/sources/xtnd-commons-0.0.1-src.tar.bz2 windows/sources/xtnd-commons
rm -rf windows/sources/xtnd-commons
