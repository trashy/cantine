/*
 * Eleve.java, 8 août 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               	Eleve.java
 * Author's email :     	johan@x-tnd.be
 * Author's Website :   	http://cantine.sourceforge.net
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import be.xtnd.cantine.gui.Launcher;
import be.xtnd.commons.db.Database;


/**
 * Gestion des élèves
 * 
 * @author Johan Cwiklinski
 */
public class Eleve {
	private String nom, prenom, lieu_naissance, observations, allergies, strdate;
	private Classe classe;
	private Presence presence_cantine/*, presence_centre*/;
    private Date date_naissance;
    private char sexe;
    private Integer id_eleve, id_famille;
    private boolean cantine/*, centre_aere*/;
	private static Database connexion;
	private ArrayList<Date> arConges;
	/** Nom de la table élèves */
	public static final String TABLE = "eleves";
	/** Clé primaire de la table élèves */
	public static final String PK = "id_eleve";
	/** Elève garçon */
	public static final char GARCON = 'G';
	/** Elève fille */
	public static final char FILLE = 'F'; 
	static Logger logger = Logger.getLogger(Eleve.class.getName());
    
	/**
	 * Constructeur par défaut
	 *
	 */
    public Eleve(){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/cantine/log4j.properties"));
    	classe = new Classe();
    	id_eleve = -1;
    	presence_cantine = new Presence();
    	//presence_centre = new Presence(Presence.CENTRE);
    }
    
    /**
     * Renseigne les champs de classe en fonction
     * du contenu de la base de données
     * 
     * @param id l'identifiant de l'élève
     */
    public Eleve(Integer id){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/cantine/log4j.properties"));
    	this.id_eleve = id;
		String strSql="SELECT * FROM "+TABLE+" WHERE "+PK+"="+id;
		connexion =  Launcher.db;

		try {
			ResultSet rsEleve = connexion.execQuery(strSql);
			rsEleve.first();
			this.nom = rsEleve.getString("nom");
			this.prenom = rsEleve.getString("prenom");
			this.sexe = rsEleve.getString("sexe").charAt(0);
			this.date_naissance = rsEleve.getDate("date_naissance");
			this.lieu_naissance = rsEleve.getString("lieu_naissance");
			this.cantine = rsEleve.getBoolean("cantine");
			//this.centre_aere = rsEleve.getBoolean("centre_aere");
			this.observations = rsEleve.getString("observations");
			this.allergies = rsEleve.getString("allergies");

			int id_classe = new Integer(rsEleve.getInt(Classe.PK));			
			this.classe = new Classe(id_classe);
			this.presence_cantine = new Presence(this.id_eleve);
		} catch (SQLException e) {
			logger.error("SQLExeption Eleve(Integer id) : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			e.printStackTrace();
		}
    }

    
	/**
	 * Tableau d'eleves par famille
	 * 
	 * @param famille : l'identifiant de la famille
	 * @return Renvoie liste d'eleves
	 */
	public static ArrayList<Eleve> elevesFamille(Integer famille){
		ArrayList<Eleve> array = null;
		
		String strSql="SELECT * FROM "+TABLE+" WHERE  "+Famille.PK+"="+famille;
		connexion =  Launcher.db;

		try {
			array = new ArrayList<Eleve>(3);
			
			ResultSet rsEleves = connexion.execQuery(strSql);
			rsEleves.beforeFirst();
						
			ArrayList<Integer> list = new ArrayList<Integer>();
			while(rsEleves.next())
				list.add(rsEleves.getInt(PK));
			
			Iterator<Integer> it = list.iterator();
			while(it.hasNext()){
				Integer current = (Integer) it.next();
				array.add(new Eleve(current));
			}			
		} catch (SQLException e) {
			logger.error("SQLException elevesFamille(Integer famille) : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			e.printStackTrace();
		}
		array.trimToSize();
		return array;
	}    
	
	/**
	 * Construit les lignes du tableau à fournir au TableModel
	 * pour les pointages.
	 * 
	 * @param month mois sélectionné
	 * @param year année sélectionnée
	 * @param type type de réservation (adulte, maternelle, primaire)
	 * @return Object[][]
	 */
	public Object[][] presenceELeves(int month, int year, String type){
		Object[][] tabloPresence = null;
		
		//String champ_reserv = new String();
		//if(reserv==Presence.CANTINE) champ_reserv = "cantine";
		//if(reserv==Presence.CENTRE) champ_reserv = "centre_aere";
				
		connexion = Launcher.db;
		
		//récupération des jours de congés
		getConges(month, year);		

		String strSql="SELECT * FROM " + TABLE + " INNER JOIN " + Classe.TABLE +
			" ON("+TABLE+"." + Classe.PK + "=" + Classe.TABLE + "." + Classe.PK + ") WHERE "
			+ Classe.TABLE + ".categorie='" + type + "' ORDER BY nom, prenom";

		String sqlCount = "SELECT COUNT("+PK+") As compte FROM "+TABLE+" INNER JOIN "+
			Classe.TABLE + " ON("+TABLE+"."+Classe.PK+"="+Classe.TABLE+"."+Classe.PK+") " +
			"WHERE "+Classe.TABLE+".categorie='"+type+"'";
		
		try {
			ResultSet count = connexion.execQuery(sqlCount);
			while(count.next()){
				tabloPresence = new Object[count.getInt("compte")][];
				logger.debug("Count from category " + type + " : " + count.getInt("compte"));
			}
			ResultSet rs = connexion.execQuery(strSql);
			int nbre = 0;
			
			ArrayList<Object> eleves = new ArrayList<Object>(); 
			while(rs.next()){
				Integer pk = rs.getInt(PK);
				String id_eleve = String.valueOf(rs.getInt("id_eleve"));
				String nom = rs.getString("nom");
				String prenom= rs.getString("prenom");

				Object[] o = { pk, id_eleve, nom, prenom};
				eleves.add(o);
			}

			Iterator<Object> it = eleves.iterator();
			
			while(it.hasNext()){
				Object[] o = (Object[]) it.next();
				//TODO compatibilité pour centre aéré.
				Presence presence = new Presence(Integer.valueOf(o[1].toString()));
				//Presence presence = (reserv == Presence.CANTINE)?presence_cantine:presence_centre;
				Planning pointage = new Planning((Integer) o[0]);
				ArrayList<Object[]> pointages = pointage.getValues(month, year);
				String[] listOfDays = getDays(
						presence, 
						month, 
						year,
						pointages);
				listOfDays[0] = (String) o[1];
				listOfDays[1] = (String) o[2]+" "+(String) o[3];
				tabloPresence[nbre]=listOfDays;
				nbre++;
			}
		} catch (SQLException e) {
			logger.error("SQLException presenceEleves(...) : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			e.printStackTrace();
		}
		return tabloPresence;
	}
	
	/**
	 * Construit les colonnes du tableau à fournir au TableModel
	 * pour les pointages.
	 * 
	 * @param presence jours de présence prévus
	 * @param month mois sélectionné
	 * @param year année selectionnée
	 * @param pointages ArrayList des pointages existants
	 * @return String[]
	 */
	public String[] getDays(Presence presence, int month, int year, ArrayList<Object[]> pointages){
		String[] listOfDays;
		
		Calendar currentDate = Calendar.getInstance();
		currentDate.set(Calendar.DAY_OF_MONTH, 1);
		
	    Calendar cal = new GregorianCalendar(
	    		year, 
				month, 
				1);
	    
		boolean preceding = (cal.before(currentDate))?true:false;

	    int presenceCount = 0;
	    
		int days = cal.getActualMaximum(Calendar.DAY_OF_MONTH); 
		listOfDays = new String[days+2];
		int pointagesCount = 0;

	    for(int c = 0; c < pointages.size() ; c++){
			Object[] o = (Object[]) pointages.get(c);
	    	if(new Integer(o[2].toString()).intValue()==1) presenceCount++;
	    }
	    preceding = (presenceCount>0 && preceding)?false:true;
		int congesCount = 0;
		boolean inConges = false;
		boolean jourFerie = false;
		Date d = null;
		if(arConges.size()>0){
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				d = sdf.parse(arConges.get(congesCount).toString());
			} catch (ParseException e) {e.printStackTrace();}
			if(d.before(cal.getTime())){ //si la date de début du premier congé est antérieure (mois précédent)
				congesCount++;
				inConges = true;
			}
		}
		
		for(int i=0; i<days+2; i++){
			boolean pointed = false;
	    	if(i==0 || i==1){listOfDays[i]=" ";}else{
	    		int jour = cal.get(Calendar.DAY_OF_WEEK);
	    		/* CONGES */
	    		if(arConges.size()>0){
		    		if(arConges.contains(cal.getTime())){
		    			int first = arConges.indexOf(cal.getTime());
		    			int last = arConges.lastIndexOf(cal.getTime());
		    			jourFerie = (!inConges && first!=last)?true:false;
		    			inConges = (first!=0 && first%2==1)?false:true;
		    		}
	    		}
	    		/* FIN CONGES */
	    		if(inConges)
	    			listOfDays[i] = String.valueOf(Presence.WEEKEND);
	    			
	    		if(pointagesCount<pointages.size() && !inConges){
	    			Object[] o = (Object[]) pointages.get(pointagesCount);
		    		if(new Integer(o[0].toString()).intValue()==cal.get(Calendar.DATE)){
		    			listOfDays[i] = o[2].toString();
		    			pointed = true;
		    			pointagesCount++;
		    		}
	    		} else {pointed = false;}
	    		if(!pointed && !inConges){
		    		switch(jour){
		    			case 2:
		    				listOfDays[i] = (Launcher.options.getActiveDays()[0])?
		    							String.valueOf(Presence.WEEKEND):
		    							(presence.isLundi() && preceding)?
		    									String.valueOf(Presence.PRESENT):
		    									String.valueOf(Presence.NON);
		    	    		break;
		    	    	case 3:
		    	    		listOfDays[i] = (Launcher.options.getActiveDays()[1])?
		    	    					String.valueOf(Presence.WEEKEND):
		    	    					(presence.isMardi() && preceding)?
		    	    							String.valueOf(Presence.PRESENT):
		    	    							String.valueOf(Presence.NON);
		    	    		break;
		    	    	case 4:
		    	    		listOfDays[i] = (Launcher.options.getActiveDays()[2])?
		    	    					String.valueOf(Presence.WEEKEND):
		    	    					(presence.isMercredi() && preceding)?
		    	    							String.valueOf(Presence.PRESENT):
		    	    							String.valueOf(Presence.NON);
		    	    		break;
		    	    	case 5:
		    	    		listOfDays[i] = (Launcher.options.getActiveDays()[3])?
		    	    					String.valueOf(Presence.WEEKEND):
		    	    					(presence.isJeudi() && preceding)?
		    	    							String.valueOf(Presence.PRESENT):
		    	    							String.valueOf(Presence.NON);
		    	    		break;
		    	    	case 6:
		    	    		listOfDays[i] = (Launcher.options.getActiveDays()[4])?
		    	    					String.valueOf(Presence.WEEKEND):
		    	    					(presence.isVendredi() && preceding)?
		    	    							String.valueOf(Presence.PRESENT):
		    	    							String.valueOf(Presence.NON);
		    	    		break;
		    	    	case 7:
		    	    		listOfDays[i] = (Launcher.options.getActiveDays()[5])?
		    	    					String.valueOf(Presence.WEEKEND):
		    	    					(presence.isSamedi() && preceding)?
		    	    							String.valueOf(Presence.PRESENT):
		    	    							String.valueOf(Presence.NON);
		    	    		break;
		    	    	case 1:
		    	    		listOfDays[i] = (Launcher.options.getActiveDays()[6])?
		    	    					String.valueOf(Presence.WEEKEND):
		    	    					(presence.isDimanche() && preceding)?
		    	    							String.valueOf(Presence.PRESENT):
		    	    							String.valueOf(Presence.NON);
		    				break;
		    			default:
		    				listOfDays[i] = String.valueOf(-2); //cas improbable !
		    		}
	    		}
	    		cal.add(Calendar.DAY_OF_MONTH, 1);
	    		if(jourFerie){inConges=false;jourFerie=false;}
	    	}
	    }
	    return listOfDays;
	}

	private void getConges(int month, int year){
	    Calendar calConges = new GregorianCalendar(
	    		year,
	    		month,
	    		1
	    );
	    
	    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	    String bDate = df.format(calConges.getTime());
	    calConges.set(Calendar.DAY_OF_MONTH, calConges.getActualMaximum(Calendar.DAY_OF_MONTH));
	    String eDate = df.format(calConges.getTime());
		String sqlConges = "SELECT * FROM conges WHERE debut BETWEEN '"+bDate+"' AND '"+eDate+"'"+
			" OR fin BETWEEN '"+bDate+"' AND '"+eDate+"' ORDER BY debut, fin";
		ArrayList<Date> arConges = new ArrayList<Date>(20);
		try {
			ResultSet rsConges = connexion.execQuery(sqlConges);
			while(rsConges.next()){
				arConges.add(rsConges.getDate("debut"));
				arConges.add(rsConges.getDate("fin"));				
			}
			arConges.trimToSize();
		} catch (SQLException e) {
			logger.error("SQLException getting holidays : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			e.printStackTrace();
		}
		/* CONGES */
		this.arConges = arConges;
	} 
	
	private void formatteDonnees(){
    	this.nom = Database.replace(this.nom,"\'","\'\'");
    	this.prenom = Database.replace(this.prenom,"\'","\'\'");
    	/* TRAITEMENT DATE DE NAISSANCE */
    	Date currentdate = Calendar.getInstance().getTime();
    	strdate = new String();
		String strcurrentdate = new String();
		Date enterdate = date_naissance;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		strdate = dateFormat.format(enterdate);
		strcurrentdate = dateFormat.format(currentdate);
		if(strdate.equalsIgnoreCase(strcurrentdate )){
			strdate = null; //"0000-00-00";
		}else{
			strdate = "'"+strdate+"'";
		}
		/* FIN TRAITEMENT DATE NAISSANCE*/
		this.lieu_naissance = "'" + Database.replace(this.lieu_naissance,"\'","\'\'") + "'";
		this.observations = Database.replace(this.observations,"\'","\'\'");
		this.allergies = "'" + Database.replace(this.allergies,"\'","\'\'") + "'";			
	}
	
	/**
	 * Enregistre les données de l'objet Eleve dans la base
	 *
	 */
    public void storeEleve(){
    	formatteDonnees();
    	
		String strSql="INSERT INTO "+TABLE+" (nom, prenom, sexe, date_naissance, lieu_naissance, id_classe, " +
				"cantine, observations, allergies, id_famille)" +
				"VALUES('"+nom+"', '"+prenom+"', '"+sexe+"', "+strdate+", "+lieu_naissance+", "+
				classe.getId_classe()+", "+cantine+", '"+observations+"', "+allergies+", "+id_famille+")";
		connexion = Launcher.db;
    	try {
    		connexion.execUpdate(strSql);
    		ResultSet rs = (Launcher.conf.getProvider_name().equalsIgnoreCase("hsqldb"))?
    				connexion.execQuery("CALL IDENTITY()"):
    					connexion.execQuery("SELECT LAST_INSERT_ID();");
  	    	rs.first();
  	    	this.id_eleve = new Integer(rs.getInt(1));
  	    	this.presence_cantine.setId_eleve(this.id_eleve);
  	    	this.presence_cantine.setPresence();
    	} catch (SQLException e) {
    		logger.error("SQLException while storeEleve() : "+e.getMessage()+" (code "+e.getErrorCode()+")");
    		e.printStackTrace();
    	}
    }
    
    /**
     * Met à jour les données de l'élève dans la base
     * 
     * @param id identifiant de l'élève à modifier
     */
    public void modifyEleve(Integer id){
    	if(this.id_eleve==-1){ storeEleve(); return; }
    	formatteDonnees();
    	
		String strSql="UPDATE "+TABLE+"  SET "+
				"nom='"+this.nom+"', prenom='"+this.prenom+"', sexe='"+this.sexe+"', "+
				"date_naissance="+strdate+", "+"lieu_naissance="+this.lieu_naissance+", "+
				"id_classe="+this.classe.getId_classe()+", cantine="+this.cantine+
				", observations='"+this.observations+"', allergies="+this.allergies+", id_famille="+this.id_famille + 
				" WHERE "+PK+"=" + id;
		connexion =  Launcher.db;
    	try {
    		connexion.execUpdate(strSql);
  	    	this.presence_cantine.setPresence();
    	} catch (SQLException e) {
    		logger.error("SQLException while modifyEleve(Integer id) : "+e.getMessage()+" (code "+e.getErrorCode());
    		e.printStackTrace();
    	}
    }
    
	/**
	 * Supprime un élève de la base de données
	 * 
	 * @param id identifiant de l'élève à supprimer
	 */
    public void deleteEleve(Integer id){
		String strSql="DELETE FROM "+TABLE+" WHERE "+PK+"="+id;
		connexion = Launcher.db;	
		try {connexion.execUpdate(strSql);} catch (SQLException e) {
    		logger.error("SQLException while deleteEleve(Integer id) : "+e.getMessage()+" (code "+e.getErrorCode());
			e.printStackTrace();
		}
    }
		
	/**
	 * @return Renvoie allergies.
	 */
	public String getAllergies() {
		return allergies;
	}
	/**
	 * @param allergies allergies à définir.
	 */
	public void setAllergies(String allergies) {
		this.allergies = allergies;
	}
	/**
	 * @return Renvoie cantine.
	 */
	public boolean isCantine() {
		return cantine;
	}
	/**
	 * @param cantine cantine à définir.
	 */
	public void setCantine(boolean cantine) {
		this.cantine = cantine;
	}
	/**
	 * @return Renvoie classe.
	 */
	public Classe getClasse() {
		return this.classe;
	}
	/**
	 * @param classe classe à définir.
	 */
	public void setClasse(Classe classe) {
		this.classe = classe;
	}
	/**
	 * @return Renvoie date_naissance.
	 */
	public Date getDate_naissance() {
		Date the_date = new Date();
		Calendar calendar = Calendar.getInstance(); 
		the_date = (date_naissance!=null)?date_naissance:calendar.getTime();
		return the_date;
	}
	/**
	 * @param date_naissance date_naissance à définir.
	 */
	public void setDate_naissance(Date date_naissance) {
		this.date_naissance = date_naissance;
	}
	/**
	 * @return Renvoie id_eleve.
	 */
	public Integer getId_eleve() {
		return id_eleve;
	}
	/**
	 * @param id_eleve id_eleve à définir.
	 */
	public void setId_eleve(Integer id_eleve) {
		this.id_eleve = id_eleve;
	}
	/**
	 * @return Renvoie lieu_naissance.
	 */
	public String getLieu_naissance() {
		return lieu_naissance;
	}
	/**
	 * @param lieu_naissance lieu_naissance à définir.
	 */
	public void setLieu_naissance(String lieu_naissance) {
		this.lieu_naissance = lieu_naissance;
	}
	/**
	 * @return Renvoie nom.
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom nom à définir.
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return Renvoie observations.
	 */
	public String getObservations() {
		return observations;
	}
	/**
	 * @param observations observations à définir.
	 */
	public void setObservations(String observations) {
		this.observations = observations;
	}
	/**
	 * @return Renvoie prenom.
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * @param prenom prenom à définir.
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	/**
	 * @return Renvoie sexe.
	 */
	public char getSexe() {
		return sexe;
	}
	/**
	 * @param sexe sexe à définir.
	 */
	public void setSexe(char sexe) {
		this.sexe = sexe;
	}
	/**
	 * @return Renvoie id_famille.
	 */
	public Integer getId_famille() {
		return id_famille;
	}
	/**
	 * @param id_famille id_famille à définir.
	 */
	public void setId_famille(Integer id_famille) {
		this.id_famille = id_famille;
	}

	/**
	 * @return Renvoie presence_cantine.
	 */
	public Presence getPresence_cantine() {
		return presence_cantine;
	}

	/**
	 * @param presence_cantine presence_cantine à définir.
	 */
	public void setPresence_cantine(Presence presence_cantine) {
		this.presence_cantine = presence_cantine;
	}
}