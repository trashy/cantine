/*
 * Classe.java, 1 sept. 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               	Classe.java
 * Author's email :     	johan@x-tnd.be
 * Author's Website :   	http://cantine.sourceforge.net
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import be.xtnd.cantine.gui.Launcher;
import be.xtnd.commons.db.Database;

/**
 * 
 * 
 * @author Johan Cwiklinski
 *
 */
public class Classe {
	/** Classe adulte */
	public static final int ADULTE =10;
	/** Nom de la table des classes */
	public static final String TABLE = "classes";
	/** Clé primaire de la table des classes */
	public static final String PK = "id_classe";
	private static Database connexion;
	private String classe;
	private Integer id_classe;
	private String categorie;
	static Logger logger = Logger.getLogger(Classe.class.getName());


	/**
	 * Initialise une nouvelle classe 
	 *
	 */
	public Classe(){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/cantine/log4j.properties"));
	}
	
	/**
	 * Initialise une classe en fonction de l'id spécifiée
	 * @param id id de la classe à initialiser
	 */
	public Classe(Integer id){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/cantine/log4j.properties"));
		this.id_classe = id;
		connexion = Launcher.db;
		try {
			ResultSet rs = connexion.execQuery("SELECT * FROM "+TABLE+" WHERE "+PK+"="+id);
			while(rs.next()){
				this.classe = rs.getString("classe");
				this.categorie = rs.getString("categorie");
			}
		} catch (SQLException e) {
			logger.error("SQLException Classe(Integer id) : "+e.getMessage()+" (code "+e.getErrorCode()+")");
		}
	}

	/**
	 * Initialise une classe en fonction du nom spécifié
	 * @param classe nom de la classe à initialiser
	 */
	public Classe(String classe){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/cantine/log4j.properties"));
		this.classe = classe;
		connexion = Launcher.db;
		try {
			ResultSet rs = connexion.execQuery("SELECT * FROM "+TABLE+" WHERE classe='"+classe+"'");
			while(rs.next()){
				this.id_classe = new Integer(rs.getInt("id_classe"));
				this.categorie = rs.getString("categorie");
			}
		} catch (SQLException e) {
			logger.error("SQLException Classe(String classe) : "+e.getMessage()+" (code "+e.getErrorCode()+")");
		}
	}
	
	/**
	 * Liste des classes hors adultes 
	 * @return ArrayList 
	 */
	public static ArrayList<String> listing(){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/cantine/log4j.properties"));
		ArrayList<String> list = new ArrayList<String>(5);
		String query = "SELECT * FROM "+TABLE+" WHERE "+PK+"!="+ADULTE;
		try{
			connexion =  Launcher.db;	
			ResultSet rs = connexion.execQuery(query);
			rs.beforeFirst();
			while(rs.next()){
				list.add(rs.getString("classe"));
			}
		}catch(Exception e){
			logger.error("SQLException initializing listing() : "+e.getMessage());
		}
		list.trimToSize();
		return list;
	}

	/**
	 * Récupère les catégories de classes existantes
	 * @return ArrayList des catégories
	 */
	public ArrayList<String> getCategories(){
		ArrayList<String> list = new ArrayList<String>(5);
		String query = "SELECT DISTINCT categorie FROM "+TABLE+" ORDER BY categorie";
		try{
			connexion =  Launcher.db;	
			ResultSet rs = connexion.execQuery(query);
			rs.beforeFirst();
			while(rs.next()){
				list.add(rs.getString("categorie"));
			}
		}catch(Exception e){
			logger.error("SQLException getCategories() : "+e.getMessage());
			e.printStackTrace();
		}
		list.trimToSize();
		return list;
	}
	
	/**
	 * @return Renvoie categorie.
	 */
	public String getCategorie() {
		return categorie;
	}
	/**
	 * @param categorie categorie à définir.
	 */
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}
	/**
	 * @return Renvoie classe.
	 */
	public String getClasse() {
		connexion = Launcher.db;
		String strSql = "SELECT * FROM "+TABLE+" WHERE "+PK+"="+id_classe;
		try {
			ResultSet rs = connexion.execQuery(strSql);
			while(rs.next()){
				classe = rs.getString("classe");
			}
		} catch (SQLException e) {
			logger.error("SQLException getClasse() : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			e.printStackTrace();
		}
		return classe;
	}
	/**
	 * @param id_classe classe à définir.
	 */
	public void setClasse(Integer id_classe) {
		this.id_classe = id_classe;
	}
	/**
	 * @return Renvoie id_classe.
	 */
	public Integer getId_classe() {
		return id_classe;
	}
}
