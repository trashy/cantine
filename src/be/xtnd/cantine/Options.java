/*
 * Options.java, 23 sept. 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               Options.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://www.x-tnd.be/
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine;

import java.awt.Color;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import be.xtnd.cantine.gui.Launcher;
import be.xtnd.commons.db.Database;

/**
 * 
 * 
 * @author Johan Cwiklinski
 */
public class Options{
	private boolean[]	activeDays;
	private Object[][]	tarifs;
	private Object[][] conges;
	private Object[]	traiteur, mairie;
	private Database	db;
	private Object[]	couleurs_pointages;
    private String anneeCourante;
    private boolean hide_closed;
	static Logger logger = Logger.getLogger(Options.class.getName());

    /**
     * Initialise les options
     *
     */
	public Options(){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/cantine/log4j.properties"));
		db = Launcher.db;
		/* jours actifs */
		String strDays = "SELECT * FROM opt_pointages";
		try{
			ResultSet rs = db.execQuery(strDays);
			this.activeDays = new boolean[8];
			while (rs.next()){
				this.activeDays[0] = rs.getBoolean("lundi");
				this.activeDays[1] = rs.getBoolean("mardi");
				this.activeDays[2] = rs.getBoolean("mercredi");
				this.activeDays[3] = rs.getBoolean("jeudi");
				this.activeDays[4] = rs.getBoolean("vendredi");
				this.activeDays[5] = rs.getBoolean("samedi");
				this.activeDays[6] = rs.getBoolean("dimanche");
				this.hide_closed  =rs.getBoolean("hide_closed");
			}
		}catch (SQLException e){
			logger.fatal("SQLException loading options : "+e.getMessage()+"(code "+e.getErrorCode()+") "+e);
		}

		/* tarifs */
		loadTarifs();
		// traiteur
		loadTraiteur();
		// mairie
		this.mairie = getMairie();
		// vacances année en cours
		Calendar cal = Calendar.getInstance();
		int month = cal.get(Calendar.MONTH);
		if (month < 8)
			cal.add(Calendar.YEAR, -1);
		int actualYear = cal.get(Calendar.YEAR);
		cal.add(Calendar.YEAR, 1);
		int nextYear = cal.get(Calendar.YEAR);
		anneeCourante = actualYear + "-" + nextYear;
		loadConges(anneeCourante);
		/* couleurs tableau pointage */
		String sqlColors = "SELECT * FROM types_pointages ORDER BY id_type";
		try {
			ResultSet rsColors = db.execQuery(sqlColors);
			rsColors.last();
			couleurs_pointages = new Object[rsColors.getRow()+1];
			rsColors.beforeFirst();
			couleurs_pointages[0] = Color.BLACK;
			int cpteColors = 1;
			while(rsColors.next()){
				String parseColor = rsColors.getString("couleur");
				String[] parsedColor = parseColor.split(",");
				couleurs_pointages[cpteColors] = new Color(
						new Integer(parsedColor[0]).intValue(),
						new Integer(parsedColor[1]).intValue(),
						new Integer(parsedColor[2]).intValue()
				);
				cpteColors++;
			}
		} catch (SQLException e) {
			logger.fatal("SQLException retrieving types_pointages : "+e.getMessage()+"(code "+e.getErrorCode()+") "+e);
		}
	}

	/**
	 * @return Renvoie activeDays.
	 */
	public boolean[] getActiveDays(){
		return this.activeDays;
	}

	/**
	 * @param days days à définir.
	 */
	public void setActivedays(boolean[] days){
		String query;
		query = "UPDATE opt_pointages SET  lundi = " + days[0] + ", mardi = "
				+ days[1] + ", mercredi = " + days[2] + ", jeudi = " + days[3]
				+ ", vendredi = " + days[4] + ", samedi = " + days[5]
				+ ", dimanche = " + days[6];
		try{
			db.execUpdate(query);
		}catch (SQLException e){
			logger.fatal("SQLException storing active days : "+e.getMessage()+"(code "+e.getErrorCode()+") "+e);
		}

		this.activeDays[0] = days[0];
		this.activeDays[1] = days[1];
		this.activeDays[2] = days[2];
		this.activeDays[3] = days[3];
		this.activeDays[4] = days[4];
		this.activeDays[5] = days[5];
		this.activeDays[6] = days[6];
	}

	private void loadTraiteur(){
		String strTraiteur = "SELECT * FROM opt_traiteur";
		this.traiteur = new Object[7];
		try{
			ResultSet rs = db.execQuery(strTraiteur);
			while (rs.next()){
				this.traiteur[0] = rs.getString("raison_sociale");
				this.traiteur[1] = rs.getString("adresse");
				this.traiteur[2] = rs.getString("cp");
				this.traiteur[3] = rs.getString("ville");
				this.traiteur[4] = rs.getString("telephone");
				this.traiteur[5] = rs.getString("fax");
				this.traiteur[6] = rs.getString("mail");
			}
		}catch (SQLException e){
			logger.fatal("SQLException loading traiteur : "+e.getMessage()+"(code "+e.getErrorCode()+") "+e);
		}		
	}
	
	/**
	 * @return Renvoie traiteur.
	 */
	public Object[] getTraiteur(){
		return this.traiteur;
	}

	private void loadTarifs(){
		String strTarifs = "SELECT * FROM opt_tarifs";
		try{
			ResultSet rs = db.execQuery(strTarifs);
			rs.last();
			int countTarifs = rs.getRow();
			tarifs = new Object[countTarifs][];
			rs.beforeFirst();
			int i = 0;
			while (rs.next()){
				Object[] o = {
						new Integer(rs.getInt("id_tarif")),
						new Integer(rs.getInt("nombre")),
						new Float(rs.getFloat("prix")) };
				tarifs[i] = o;
				i++;
			}
		}catch (SQLException e){
			logger.fatal("SQLException loading tarifs : "+e.getMessage()+"(code "+e.getErrorCode()+") "+e);
		}
	}
	
	/**
	 * Pour l'enregistrement d'une nouvelle année dans les congés,
	 * vérifie que l'année n'existe pas dans la base
	 * @param year année à enregistrer
	 * @return true si l'année n'existe pas encore, flase sinon
	 */
	public boolean isNewYear(String year){
		boolean isIt = false;
		String qry = "SELECT COUNT(DISTINCT annee) AS cpte FROM conges WHERE annee='"+year+"'";
		try {
			ResultSet rs = db.execQuery(qry);
			rs.first();
			isIt = (rs.getInt("cpte")>0)?false:true;
		} catch (SQLException e) {
			logger.fatal("SQLEception checking school year exists : "+e.getMessage()+"(code "+e.getErrorCode()+") "+e);
		}
		return isIt;
	}
	
	/**
	 * @return Renvoie tarifs.
	 */
	public Object[][] getTarifs(){
		return tarifs;
	}

	/**
	 * @param tarifs tarifs à définir.
	 */
	public void setTarifs(Object[][] tarifs){
		String query;
		for (int i = 0; i < tarifs.length; i++){
			if(tarifs[i][0] == "-1"){//cas nouveau tarif
				query = "INSERT INTO opt_tarifs VALUES (null, "+tarifs[i][1]+", "+tarifs[i][2]+")";
			}else{//cas modification tarif
				query = "UPDATE opt_tarifs SET prix=" + tarifs[i][2] + ", nombre=" + tarifs[i][1] + 
					" WHERE id_tarif = " + tarifs[i][0];
			}
			try{
				db.execUpdate(query);
			}catch (SQLException e){
				logger.fatal("SQLException storing tarifs : "+e.getMessage()+"(code "+e.getErrorCode()+") "+e);
			}
		}
		this.tarifs = tarifs;
	}

	/**
	 * Supprime un tarif de la base
	 * @param id id du tarif à supprimer 
	 */
	public void supprimeTarif(Integer id){
		String query = "DELETE FROM opt_tarifs WHERE id_tarif = " + id;
		try {
			db.execUpdate(query);
		} catch (SQLException e) {
			logger.fatal("SQLException deleting tarif : "+e.getMessage()+"(code "+e.getErrorCode()+") "+e);
		}
		loadTarifs();
	}
	
	/**
	 * 
	 * @return Renvoie mairie
	 */
	public Object[] getMairie(){
		String strMairie = "SELECT * FROM opt_mairie";
		this.mairie = new Object[10];
		try{
			ResultSet rs = db.execQuery(strMairie);
			while (rs.next()){
				this.mairie[0] = rs.getString("nom");
				this.mairie[1] = rs.getString("adresse");
				this.mairie[2] = rs.getString("cp");
				this.mairie[3] = rs.getString("ville");
				this.mairie[4] = rs.getString("tel");
				this.mairie[5] = rs.getString("fax");
				this.mairie[6] = rs.getString("mail");
				this.mairie[7] = rs.getString("site");
				this.mairie[8] = rs.getString("logo");
				this.mairie[9] = rs.getString("signataire");
			}
		}catch (SQLException e){
			logger.fatal("SQLEception loading mairie : "+e.getMessage()+"(code "+e.getErrorCode()+") "+e);
		}
		return this.mairie;
	}

	private void loadConges(String annee){
		String query = "SELECT * FROM conges WHERE annee = '" + annee + "'";
		
		try {
			ResultSet rs = db.execQuery(query);
			rs.last();			
			int nbr_lignes = rs.getRow();
			rs.beforeFirst();		
			conges = new Object[nbr_lignes][];
			int ligne = 0;
			while(rs.next()){
				conges[ligne] = new Object[5];
				conges[ligne][0] = new Integer(rs.getString(1));
				conges[ligne][1] = rs.getString("annee");
				conges[ligne][2] = rs.getString("nom");
				conges[ligne][3] = rs.getDate("debut");
				conges[ligne][4]  = rs.getDate("fin");
				++ligne;
			}
		}catch (SQLException e) {
			logger.fatal("SQLEception loading conges : "+e.getMessage()+"(code "+e.getErrorCode()+") "+e);
		}
	}
	
	/**
	 * Object[][] pour construction JTable dans la fenêtre options
	 * 
	 * @return Renvoie conges.
	 */
	public Object[][] getConges(){
		return this.conges;
	}

	/**
	 * enregistre ou insère un enregistrement dans la table des congés
	 * 
	 * @param conge conges à définir.
	 */
	public void setConges(Object[][] conge){
		String query;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		for(int i = 0; i < conge.length; i++){
			if(conge[i][0] == "-1"){
				query = "INSERT INTO conges (annee,nom,debut,fin) VALUES ('"  
					+ conge[i][1] + "','"  + Database.replace(conge[i][2].toString(), "\'", "\'\'") + "','"
 			        + df.format(conge[i][3]) + "','" + df.format(conge[i][4]) + "' )";
			}else{
				query = "UPDATE conges SET annee = '" + conge[i][1] + "',nom = '" 
				  + Database.replace(conge[i][2].toString(), "\'", "\'\'") + "', debut = '" + df.format(conge[i][3]) + "', fin = '" 
				  + df.format(conge[i][4]) + "' WHERE id_conge = " + conge[i][0];
			}
				
			try {
				db.execUpdate(query);
			} catch (SQLException e) {
				logger.fatal("SQLEception storing conges : "+e.getMessage()+"(code "+e.getErrorCode()+") "+e);
			}
		
		}
	}
	
	/**
	 * Supprime une date de congés.
	 * @param id id du congé à supprimer
	 */
	public void supprimeConges(Integer id){
		String query = "DELETE FROM conges WHERE id_conge = " + id;
		try {
			db.execUpdate(query);
		} catch (SQLException e) {
			logger.fatal("SQLException deleting conge : "+e.getMessage()+"(code "+e.getErrorCode()+") "+e);
		}
		loadConges(getAnneeCourante().toString());
	}
	
	/**
	 * @param mairie mairie à définir.
	 */
	public void setMairie(Object[] mairie){
		this.mairie[0] = mairie[0];
		this.mairie[1] = mairie[1];
		this.mairie[2] = mairie[2];
		this.mairie[3] = mairie[3];
		this.mairie[4] = mairie[4];
		this.mairie[5] = mairie[5];
		this.mairie[6] = mairie[6];
		this.mairie[7] = mairie[7];
		this.mairie[8] = mairie[8];
		this.mairie[9] = mairie[9];

		mairie[0] = Database.replace(mairie[0].toString(), "\'", "\'\'");
		mairie[1] = Database.replace(mairie[1].toString(), "\'", "\'\'");
		mairie[2] = Database.replace(mairie[2].toString(), " ", "");
		mairie[3] = Database.replace(mairie[3].toString(), "\'", "\'\'");
		mairie[4] = Database.replace(mairie[4].toString(), " ", "");
		mairie[5] = Database.replace(mairie[5].toString(), " ", "");
		mairie[6] = Database.replace(mairie[6].toString(), "\'", "");
		mairie[7] = Database.replace(mairie[7].toString(), "\'", "");
		mairie[8] = Database.replace(mairie[8].toString(), "\\", "\\\\"); //double les antislashes poour windows
		mairie[9] = Database.replace(mairie[9].toString(), "\'", "\'\'");

		String query;
		query = "UPDATE opt_mairie SET nom = '" + mairie[0] + "' , "
				+ "adresse = '" + mairie[1] + "' , " + "cp = '" + mairie[2]
				+ "' , " + "ville = '" + mairie[3] + "' , " + "tel = '"
				+ mairie[4] + "' , " + "fax = '" + mairie[5] + "' , "
				+ "mail = '" + mairie[6] + "', site='"+mairie[7]+"' , logo = '" + mairie[8]
				+ "', signataire='" + mairie[9] + "' WHERE id_mairie = 1";
		try{
			db.execUpdate(query);
		}catch (SQLException e){
			logger.fatal("SQLEception storing mairie : "+e.getMessage()+"(code "+e.getErrorCode()+") "+e);
		}
	}

	/**
	 * @param traiteur traiteur à définir.
	 */
	public void setTraiteur(Object[] traiteur){
		this.traiteur[0] = traiteur[0];
		this.traiteur[1] = traiteur[1];
		this.traiteur[2] = traiteur[2];
		this.traiteur[3] = traiteur[3];
		this.traiteur[4] = traiteur[4];
		this.traiteur[5] = traiteur[5];

		traiteur[0] = Database.replace(traiteur[0].toString(), "\'", "\'\'");
		traiteur[1] = Database.replace(traiteur[1].toString(), "\'", "\'\'");
		traiteur[2] = Database.replace(traiteur[2].toString(), " ", "");
		traiteur[3] = Database.replace(traiteur[3].toString(), "\'", "\'\'");
		traiteur[4] = Database.replace(traiteur[4].toString(), " ", "");
		traiteur[5] = Database.replace(traiteur[5].toString(), " ", "");
		traiteur[6] = Database.replace(traiteur[6].toString(), "\'", "");

		String query = "UPDATE opt_traiteur SET raison_sociale = '"
				+ traiteur[0] + "' , " + "adresse = '" + traiteur[1] + "' , "
				+ "cp = '" + traiteur[2] + "' , " + "ville = '" + traiteur[3]
				+ "' , " + "telephone = '" + traiteur[4] + "' , " + "fax = '"
				+ traiteur[5] + "' , " + "mail = '" + traiteur[6]
				+ "' WHERE id_traiteur = 1";
		try{
			db.execUpdate(query);
		}catch (SQLException e){
			logger.fatal("SQLException storing traiteur : "+e.getMessage()+"(code "+e.getErrorCode()+") "+e);
		}
	}
	
	/**
	 * 
	 * @return Renvoie les couleurs des pointages
	 */
	public Object[] getCouleurs_pointages() {
		return couleurs_pointages;
	}
	
	/**
	 * 
	 *  @return Renvoie l'année initialisée
	 */
	public Object getAnneeCourante(){
		return anneeCourante;
	}

	/**
	 * 
	 * @param anneeCourante Initialise l'année courante
	 */
	public void setAnneeCourante(String anneeCourante) {
		this.anneeCourante = anneeCourante;
		loadConges(this.anneeCourante);
	}

	/**
	 * 
	 * @return Renvoie la liste des années
	 */
	public ArrayList<String> getAnnees(){
		ArrayList<String> list = new ArrayList<String>(5);
		String query = "SELECT DISTINCT annee FROM conges";
		try {
			ResultSet rs = db.execQuery(query);
			while(rs.next()){
				list.add(rs.getString("annee"));
			}
		} catch (SQLException e) {
			logger.fatal("SQLException loading years : "+e.getMessage()+"(code "+e.getErrorCode()+") "+e);
		}
		list.trimToSize();
		return list;
	}

	/**
	 * 
	 * @return Les jours de fermeture sont cachés ?
	 */
	public boolean isHide_closed() {
		return hide_closed;
	}

	/**
	 * 
	 * @param hide_closed affiche les jours de congés dans les pointages si true, 
	 * les cache sinon
	 */
	public void setHide_closed(boolean hide_closed) {
		String query = "UPDATE opt_pointages SET hide_closed = " + hide_closed ;
		try {
			db.execUpdate(query);
        } catch (SQLException e) {
        	logger.fatal("SQLException storing hide_closed : "+e.getMessage()+"(code "+e.getErrorCode()+") "+e);
        }
		this.hide_closed = hide_closed;
	}
}