/*
 * ApplicationProperties.java, 2010-03-31
 * 
 * This file is part of cantine.
 *
 * Copyright © 2010 Johan Cwiklinski
 *
 * File :               ApplicationProperties.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine;

import java.util.ResourceBundle;

public class ApplicationProperties extends
		be.xtnd.commons.ApplicationProperties {
    private static final String BUNDLE_NAME = "be.xtnd.cantine.cantine";
    
    /**
	 * Search and return a key value from application properties file
	 * 
	 * @param key key to look for
	 * @return key value in the resource file
     */
    public static String getString(String key){
        return getString(key, BUNDLE_NAME);
    }
    
    /**
     * Get local bundle, mainly to pass it to xtnd-commons classes
     *  
     * @return ResourceBundle application's bundle
     */
    public static ResourceBundle getBundle(){
   	 return getBundle(BUNDLE_NAME);
    }

}
