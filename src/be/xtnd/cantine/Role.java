/*
 * Role.java, 29 août 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               	Role.java
 * Author's email :     	johan@x-tnd.be
 * Author's Website :   	http://cantine.sourceforge.net
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import be.xtnd.cantine.gui.Launcher;
import be.xtnd.commons.db.Database;

/**
 * 
 * 
 * @author Johan Cwiklinski
 *
 */
public class Role {
	/** Rôle père */
	public final static int PERE = 1; 
	/** Rôle mère */
	public final static int MERE = 2;
	/** Nom de la table des rôles */
	public static final String TABLE = "roles";
	/** Clé primaire de la table des rôles */
	public static final String PK = "id_role";
	/** Requête de liste des rôles */
	public static final String LISTQUERY = "SELECT * FROM "+TABLE;
	private static Database connexion;
	private Integer id_role;
	private String role;
	static Logger logger = Logger.getLogger(Role.class.getName());

	/**
	 * Constructeur par défaut
	 *
	 */
	public Role(){}
	
	/**
	 *  Renseigne les champs de classe en fonction
     * du contenu de la base de données
     * 
	 * @param id identifiant du role
	 */
	public Role(Integer id){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/cantine/log4j.properties"));
		this.id_role = id;
		connexion =  Launcher.db;
		try {
			ResultSet rs = connexion.execQuery("SELECT * FROM "+TABLE+" WHERE "+PK+"="+id);
			while(rs.next()){
				this.role = rs.getString("role");
			}
		} catch (SQLException e) {
    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode());
			e.printStackTrace();
		}
	}

	/**
	 * Initialise un rôle en fonction de son nom
	 * @param role nom du rôle à initialiser
	 */
	public Role(String role){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/cantine/log4j.properties"));
		this.role = role;
		connexion =  Launcher.db;
		try {
			ResultSet rs = connexion.execQuery("SELECT * FROM "+TABLE+" WHERE role='"+role+"'");
			while(rs.next()){
				this.id_role = new Integer(rs.getInt("id_role"));
			}
		} catch (SQLException e) {
    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode());
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @return Renvoie la liste des rôles
	 */
	public static ArrayList<String> listing(){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/cantine/log4j.properties"));
		ArrayList<String> list = new ArrayList<String>(5);
		String query = "SELECT * FROM "+TABLE;
		try{
			connexion =  Launcher.db;
			ResultSet rs = connexion.execQuery(query);
			rs.beforeFirst();
			while(rs.next()){
				list.add(rs.getString("role"));
			}
		}catch(Exception e){
    		logger.error("SQLException : "+e.getMessage());
			e.printStackTrace();
		}
		list.trimToSize();
		return list;
	}
	
	/**
	 * Enregistre un nouveau rôle
	 * @return entier résultat
	 */
	public int storeRole(){
		int result = 0;
		String strSql = "INSERT INTO " + TABLE + "(id_role, role) VALUES (null, '"+Database.replace(this.role,"\'", "\'\'")+"')";
		connexion =  Launcher.db;
    	try {
    		result = connexion.execUpdate(strSql);
    	} catch (SQLException e) {
    		if(Launcher.sql_errors.isDuplicate(e.getErrorCode())){ //duplicate entry
    			return -2;
    		}else{
        		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode());
    			e.printStackTrace();
    		}
    	}
    	return result;
	}
	
	/**
	 * Modifie un rôle existant
	 * @param id_role id du rôle à modifier
	 */
	public void modifyRole(Integer id_role){
		String strSql = "UPDATE " + TABLE + " SET role='"+Database.replace(this.role,"\'", "\'\'")+"' WHERE "+PK+"="+id_role;
		connexion =  Launcher.db;
    	try {
    		connexion.execUpdate(strSql);
    	} catch (SQLException e) {
    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode());
    		e.printStackTrace();
    	}
	}
	
	/**
	 * Supprime un rôle.
	 * @param id_role id du rôle à supprimer
	 * @return entier résultat
	 */
	public int deleteRole(Integer id_role){
		int result = 0;
		String strSql = "DELETE FROM "+TABLE+" WHERE "+PK+"="+id_role;
		connexion = Launcher.db;	
		try {
			result = connexion.execUpdate(strSql);
		} catch (SQLException e) {
			if(Launcher.sql_errors.isFkFails(e.getErrorCode())){ //fk fails
				return -2;
			}else{
	    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode());
				e.printStackTrace();
			}
		}
		return result;
	}
	
	/**
	 * @return Renvoie role.
	 */
	public String getRole() {
		return this.role;
	}
	/**
	 * @param role role à définir.
	 */
	public void setRole(String role) {
		this.role = role;
	}
	/**
	 * @param id_role id_role à définir.
	 */
	public void setId_role(Integer id_role) {
		this.id_role = id_role;
	}
	/**
	 * @return Renvoie role.
	 */
	public Integer getId_role() {
		return id_role;
	}
}
