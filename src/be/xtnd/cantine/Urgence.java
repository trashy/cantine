/*
 * Urgence.java, 8 août 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               Urgence.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://cantine.sourceforge.net
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import be.xtnd.cantine.gui.Launcher;
import be.xtnd.commons.db.Database;


/**
 * 
 * 
 * @author Johan Cwiklinski
 */
public class Urgence {
    private String nom, adresse, cp, ville, tel, fax, tel_portable, mail, commentaires;
    private String type, oneType;
    private Integer id;
	private Database connexion;
	/** Type médecin */
	public static final String MEDECIN_TYPE = "medecins";
	/** Type hôpital */
	public static final String HOPITAUX_TYPE = "hopitaux";
	/** Table des médecins */
	public static final String MEDECINS_TABLE = "medecins";
	/** Table des hôpitaux */
	public static final String HOPITAUX_TABLE = "hopitaux";
	/** Clé primaire de la table des médecins*/ 
	public static final String MEDECINS_PK = "id_medecin";
	/** Clé primaire de la table des hôpitaux*/
	public static final String HOPITAUX_PK = "id_hopital";
	static Logger logger = Logger.getLogger(Urgence.class.getName());
    
	/**
	 * Constructeur par défaut 
	 * 
	 * @param type type d'urgence (medecons ou hopitaux)
	 */
    public Urgence(String type){
    	setType(type);
    	this.tel = new String();
    	this.fax = new String();
    	this.tel_portable = new String();
    }

    /**
     * Initialise une urgence 
     * @param type type d'urgence ({@link #MEDECIN_TYPE Medecin} ou {@link #HOPITAUX_TYPE Hôpital})
     * @param id id à initialiser selon le type
     */
    public Urgence(String type, Integer id){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/cantine/log4j.properties"));
		setType(type);
    	this.id = id;
		String strSql="SELECT * FROM "+type+" WHERE id_"+oneType+"='"+id+"'";
		connexion =  Launcher.db;
		
		try {
			ResultSet rsUrgence = connexion.execQuery(strSql);
			rsUrgence.first();
			this.nom = rsUrgence.getString("nom");
			this.adresse = rsUrgence.getString("adresse");
			this.cp = rsUrgence.getString("cp");
			this.ville = rsUrgence.getString("ville");
			this.tel = rsUrgence.getString("tel");
			this.fax = rsUrgence.getString("fax");
			this.tel_portable = rsUrgence.getString("tel_portable");
			this.mail = rsUrgence.getString("mail");
			this.commentaires = rsUrgence.getString("commentaires");
		} catch (SQLException e) {
    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode());
			e.printStackTrace();
		}
    }

    /**
     * Initialise une urgence 
     * @param type type d'urgence ({@link #MEDECIN_TYPE Medecin} ou {@link #HOPITAUX_TYPE Hôpital})
     * @param nom nom à initialiser selon le type
     */
    public Urgence(String type, String nom){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/cantine/log4j.properties"));
    	setType(type);
    	this.nom = nom;
		String strSql="SELECT * FROM "+type+" WHERE nom='"+nom+"'";
		connexion = Launcher.db;	

		try {
			ResultSet rsUrgence = connexion.execQuery(strSql);
			rsUrgence.first();
			this.id = new Integer(rsUrgence.getInt("id_"+oneType));
			this.adresse = rsUrgence.getString("adresse");
			this.cp = rsUrgence.getString("cp");
			this.ville = rsUrgence.getString("ville");
			this.tel = rsUrgence.getString("tel");
			this.fax = rsUrgence.getString("fax");
			this.tel_portable = rsUrgence.getString("tel_portable");
			this.mail = rsUrgence.getString("mail");
			this.commentaires = rsUrgence.getString("commentaires");
		} catch (SQLException e) {
    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode());
			e.printStackTrace();
		}
    }

    private void formatteDonnees(){
    	this.nom = Database.replace(this.nom, "\'", "\'\'");
    	this.adresse = Database.replace(this.adresse, "\'", "\'\'");
    	this.cp = Database.replace(this.cp, "\'", "\'\'");
    	this.ville = Database.replace(this.ville, "\'", "\'\'");
    	this.tel = Database.replace(this.tel, " ", null);
    	this.fax = Database.replace(this.fax, " ", null);
    	this.tel_portable = Database.replace(this.tel_portable, " ", null);
    	this.mail = Database.replace(this.mail, "\'", "\'\'");
    	this.commentaires = Database.replace(this.commentaires, "\'", "\'\'");
    }
    
    /**
     * Enregistre une nouvelle urgence 
     *
     */
    public void storeUrgence(){
    	formatteDonnees();
		String strSql="INSERT INTO "+type+" (nom, adresse, cp, ville, tel, fax, tel_portable, mail, commentaires)" +
				"VALUES('"+nom+"', '"+adresse+"', '"+cp+"', '"+ville+"', '"+tel+"', '"+fax+"', '"+tel_portable+"', '"+
				mail+"', '"+commentaires+"')";
		connexion = Launcher.db;
    	try {connexion.execUpdate(strSql);} catch (SQLException e) {
    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode());
    		e.printStackTrace();
    	}
    }
    
    /**
     * Modifie l'urgence
     * @param id id de l'urgence à modifier
     */
    public void modifyUrgence(Integer id){
    	formatteDonnees();
    	String strSql = "UPDATE "+type+" SET nom='"+nom+"', adresse='"+adresse+"', cp='"+cp+
			"', ville='"+ville+"', tel='"+tel+"', fax='"+fax+"', tel_portable='"+tel_portable+"', mail='"+mail+
			"', commentaires='"+commentaires+"' WHERE id_"+oneType+"="+id;
    	connexion = Launcher.db;
    	try {connexion.execUpdate(strSql);} catch (SQLException e) {
    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode());
    		e.printStackTrace();
    	}
    }
    
    /**
     * Supprime une urgence
     * @param id id de l'urgence à supprimer
     * @return entier résultat
     */
    public int deleteUrgence(Integer id){
    	int result = 0;
		String strSql="DELETE FROM "+type+" WHERE id_"+oneType+"="+id;
		connexion = Launcher.db;	
		try {
			result = connexion.execUpdate(strSql);
		} catch (SQLException e) {
			if(Launcher.sql_errors.isFkFails(e.getErrorCode())){ //fk fails
				fkFamilles(id);
				return -2;
			}else{
	    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode());
				e.printStackTrace();
			}
		}
		return result;
    }

    /**
     * Calcule le nombre de familles auxquelles l'urgence est assignée
     * @param id id de l'urgence
     * @return nombre de familles auxquelles l'urgence est rattachée
     */
    public Integer fkFamilles(Integer id){
    	Integer result = null;
		String strSql="SELECT COUNT("+Famille.PK+") FROM "+Famille.TABLE+" INNER JOIN "+type+
			" ON("+Famille.TABLE+".id_"+oneType+"="+type+".id_"+oneType+") WHERE id_"+oneType+"="+id;
		connexion =  Launcher.db; //new Database(true);
		ResultSet rs;
		try {
			rs = connexion.execQuery(strSql);
			while(rs.next()){
				result = new Integer(rs.getInt(1));
			}
		} catch (SQLException e) {
    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode());
			e.printStackTrace();
		}
		return result;    	
    }
    
    /**
     * Liste les urgences
     * @return ArrayList
     */
	public ArrayList<String> listing(){
		ArrayList<String> list = new ArrayList<String>(5);
		String query = "SELECT * FROM "+type;
		try{
			connexion =  Launcher.db;
			ResultSet rs = connexion.execQuery(query);
			rs.beforeFirst();
			while(rs.next()){
				list.add(rs.getString("nom"));
			}
		}catch(Exception e){
    		logger.error("SQLException : "+e.getMessage());
			e.printStackTrace();
		}
		list.trimToSize();
		return list;
	}
     
    /**
	 * @return Renvoie adresse.
	 */
	public String getAdresse() {
		return adresse;
	}
	/**
	 * @param adresse adresse à définir.
	 */
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	/**
	 * @return Renvoie commentaires.
	 */
	public String getCommentaires() {
		return commentaires;
	}
	/**
	 * @param commentaires commentaires à définir.
	 */
	public void setCommentaires(String commentaires) {
		this.commentaires = commentaires;
	}
	/**
	 * @return Renvoie cp.
	 */
	public String getCp() {
		return cp;
	}
	/**
	 * @param cp cp à définir.
	 */
	public void setCp(String cp) {
		this.cp = cp;
	}
	/**
	 * @return Renvoie fax.
	 */
	public String getFax() {
		return fax;
	}
	/**
	 * @param fax fax à définir.
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}
	/**
	 * @return Renvoie id.
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * Retrouve l'id de l'urgence à partir de son nom
	 * @param name nom de l'urgence à chercher
	 * @return Integer id de l'urgence
	 */
	public Integer getIdFromName(String name){
		String query = "SELECT * FROM "+type+" WHERE nom='"+name+"'";
		Integer id_urgence = null;
		try{
			connexion = Launcher.db;
			ResultSet rs = connexion.execQuery(query);
			rs.first();
			id_urgence = new Integer(rs.getInt("id_"+oneType));
		}catch(SQLException e){
    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode());
			e.printStackTrace();
		}

		return id_urgence;
	}
	/**
	 * @param id id à définir.
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return Renvoie mail.
	 */
	public String getMail() {
		return mail;
	}
	/**
	 * @param mail mail à définir.
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}
	/**
	 * @return Renvoie nom.
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom nom à définir.
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return Renvoie tel.
	 */
	public String getTel() {
		return tel;
	}
	/**
	 * @param tel tel à définir.
	 */
	public void setTel(String tel) {
		this.tel = tel;
	}
	/**
	 * @return Renvoie tel_portable.
	 */
	public String getTel_portable() {
		return tel_portable;
	}
	/**
	 * @param tel_portable tel_portable à définir.
	 */
	public void setTel_portable(String tel_portable) {
		this.tel_portable = tel_portable;
	}
	/**
	 * @param type type à définir.
	 */
	public void setType(String type) {
		this.type = type;
		if(type.equalsIgnoreCase("medecins")){this.oneType = "medecin";
		}else if(type.equalsIgnoreCase("hopitaux")){this.oneType = "hopital";}
	}
	/**
	 * @return Renvoie ville.
	 */
	public String getVille() {
		return ville;
	}
	/**
	 * @param ville ville à définir.
	 */
	public void setVille(String ville) {
		this.ville = ville;
	}
}
