/*
 * Tiers.java, 26 août 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               	Tiers.java
 * Author's email :     	johan@x-tnd.be
 * Author's Website :   	http://cantine.sourceforge.net
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import be.xtnd.cantine.gui.Launcher;
import be.xtnd.commons.db.Database;

/**
 * 
 * 
 * @author Johan Cwiklinski
 *
 */
public class Tiers{
	protected String nom, prenom, tel_portable, mail, employeur, profession, 
		tel_travail, poste_tel_travail, commentaires/*, role*/;
	protected Integer id_tiers;
	protected Integer id_famille/*, id_role*/;
	private static Database connexion;
	protected Role roles;
	/** Nom de la tables des tiers */
	public static final String TABLE = "tiers";
	/** Clé primaire de la table des tiers */
	public static final String PK = "id_tiers";
	static Logger logger = Logger.getLogger(Tiers.class.getName());
	
	/**
	 * Constructeur par défaut
	 *
	 */
	public Tiers(){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/cantine/log4j.properties"));
		roles = new Role();
		this.tel_portable = new String();
		this.tel_travail = new String();
	}
	
	/**
	 * Renseigne les champs de classe en fonction
     * du contenu de la base de données
     * 
	 * @param id identifiant du tiers
	 */
	public Tiers(Integer id){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/cantine/log4j.properties"));
		this.id_tiers = id;
		String strSql="SELECT * FROM "+TABLE+" WHERE "+PK+"="+id;
		connexion =  Launcher.db;
		try {
			ResultSet rsTiers = connexion.execQuery(strSql);
			rsTiers.first();

			this.nom = rsTiers.getString("nom");
			this.prenom = rsTiers.getString("prenom");
			this.tel_portable = rsTiers.getString("tel_portable");
			this.mail = rsTiers.getString("mail");
			this.employeur = rsTiers.getString("employeur");
			this.profession = rsTiers.getString("profession");
			this.tel_travail = rsTiers.getString("tel_travail");
			this.poste_tel_travail = rsTiers.getString("poste_tel_travail");
			this.commentaires = rsTiers.getString("commentaires");
			this.roles = new Role(new Integer(rsTiers.getInt(Role.PK)));
		} catch (SQLException e) {
    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode());
			e.printStackTrace();
		}
	}
	
	/**
	 * Revnvoie les tiers appartenant à une famille
	 * 
	 * @param famille identifiant de la famille 
	 * @return ArrayList<Tiers>
	 */
	public static ArrayList<Tiers> tiersFamille(Integer famille){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/cantine/log4j.properties"));
		ArrayList<Tiers> array = null;
		
		String strSql="SELECT * FROM "+TABLE+" WHERE "+Famille.PK+"="+famille;
		connexion = Launcher.db;	

		try {
			array = new ArrayList<Tiers>(3);
			
			ResultSet rsTiers = connexion.execQuery(strSql);
			rsTiers.beforeFirst();
			
			ArrayList<Integer> list = new ArrayList<Integer>();
			
			while(rsTiers.next())
				list.add(rsTiers.getInt(PK));
			
			Iterator<Integer> it = list.iterator();
			
			while(it.hasNext()){
				Integer current = (Integer) it.next();
				array.add(new Tiers(current));
			}			
		} catch (SQLException e) {
    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode());
			e.printStackTrace();
		}
		array.trimToSize();
		return array;
	}

	private void formatteDonnees(){
    	this.nom = Database.replace(this.nom,"\'","\'\'");
    	this.prenom = Database.replace(this.prenom,"\'","\'\'");
    	this.tel_portable = Database.replace(this.tel_portable," ", null);
    	this.mail = Database.replace(this.mail, "\'", "\'\'");
    	this.profession = Database.replace(this.profession, "\'", "\'\'");
    	this.employeur = Database.replace(this.employeur, "\'", "\'\'");
    	this.tel_travail = Database.replace(this.tel_travail, " ", null);
    	this.commentaires = Database.replace(this.commentaires, "\'", "\'\'");		
	}
	
	/**
	 * Enregistre les données de l'objet Eleve dans la base
	 *
	 */
    public void storeTiers(){
    	formatteDonnees();
    	
    	String strSql = "INSERT INTO " + TABLE + "(nom, prenom, tel_portable, mail, employeur, profession, tel_travail," +
    			"poste_tel_travail, commentaires, id_role, id_famille) VALUES ('" + this.nom + "', '" + this.prenom + "', '" +
				this.tel_portable + "', '" + this.mail + "', '" + this.employeur + "', '" + this.profession + "', '" + this.tel_travail +
				"', '" + this.poste_tel_travail + "', '" + this.commentaires + "', " + this.roles.getId_role() + ", " + this.id_famille +")";
		connexion = Launcher.db; 
    	try {
    		connexion.execUpdate(strSql);
    	} catch (SQLException e) {
    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode());
    		e.printStackTrace();
    	}
    }
    
    /**
     * Met à jour les données de l'élève dans la base
     * 
     * @param id identifiant de l'élève à modifier
     */
    public void modifyTiers(Integer id){
    	formatteDonnees();
    	
    	String strSql = "UPDATE " + TABLE + " SET " + 
			"nom='"+this.nom+"', prenom='"+this.prenom+"', tel_portable='"+this.tel_portable+"', mail='"+this.mail+"', "+
			"employeur='"+this.employeur+"', profession='"+this.profession+"', tel_travail='"+this.tel_travail+"', " + 
			"poste_tel_travail='"+this.poste_tel_travail+"', commentaires='"+this.commentaires+"', id_role="+
			this.roles.getId_role()+", id_famille="+this.id_famille+" WHERE "+PK+"="+this.id_tiers;
		connexion =  Launcher.db;
    	try {connexion.execUpdate(strSql);} catch (SQLException e) {
    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode());
    		e.printStackTrace();
    	}
    }
    
	/**
	 * Supprime un élève de la base de données
	 * 
	 * @param id identifiant de l'élève à supprimer
	 */
    public void deleteTiers(Integer id){
		String strSql="DELETE FROM "+TABLE+" WHERE "+PK+"="+id;
		connexion =  Launcher.db;	
		try {connexion.execUpdate(strSql);} catch (SQLException e) {
    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode());
			e.printStackTrace();
		}
    }
	
	/**
	 * @return Renvoie commentaires.
	 */
	public String getCommentaires() {
		return commentaires;
	}
	/**
	 * @param commentaires commentaires à définir.
	 */
	public void setCommentaires(String commentaires) {
		this.commentaires = commentaires;
	}
	/**
	 * @return Renvoie employeur.
	 */
	public String getEmployeur() {
		return employeur;
	}
	/**
	 * @param employeur employeur à définir.
	 */
	public void setEmployeur(String employeur) {
		this.employeur = employeur;
	}
	/**
	 * @return Renvoie id_famille.
	 */
	public Integer getId_famille() {
		return id_famille;
	}
	/**
	 * @param id_famille id_famille à définir.
	 */
	public void setId_famille(Integer id_famille) {
		this.id_famille = id_famille;
	}
	/**
	 * @return Renvoie mail.
	 */
	public String getMail() {
		return mail;
	}
	/**
	 * @param mail mail à définir.
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}
	/**
	 * @return Renvoie nom.
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom nom à définir.
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return Renvoie poste_tel_travail.
	 */
	public String getPoste_tel_travail() {
		return poste_tel_travail;
	}
	/**
	 * @param poste_tel_travail poste_tel_travail à définir.
	 */
	public void setPoste_tel_travail(String poste_tel_travail) {
		this.poste_tel_travail = poste_tel_travail;
	}
	/**
	 * @return Renvoie prenom.
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * @param prenom prenom à définir.
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	/**
	 * @return Renvoie profession.
	 */
	public String getProfession() {
		return profession;
	}
	/**
	 * @param profession profession à définir.
	 */
	public void setProfession(String profession) {
		this.profession = profession;
	}
	/**
	 * @return Renvoie nom role.
	 */
	public String getRoleName() {
		return roles.getRole();
	}
	/**
	 * @return Renvoie role.
	 */
	public Role getRole() {
		return roles;
	}
	/**
	 * @param role role à définir.
	 */
	public void setRole(int role) {
		this.roles = new Role(new Integer(role));
	}
	/**
	 * @param name nom du role à définir.
	 */
	public void setRole(String name) {
		this.roles = new Role(name);
	}
	/**
	 * @return Renvoie tel_portable.
	 */
	public String getTel_portable() {
		return tel_portable;
	}
	/**
	 * @param tel_portable tel_portable à définir.
	 */
	public void setTel_portable(String tel_portable) {
		this.tel_portable = tel_portable;
	}
	/**
	 * @return Renvoie tel_travail.
	 */
	public String getTel_travail() {
		return tel_travail;
	}
	/**
	 * @param tel_travail tel_travail à définir.
	 */
	public void setTel_travail(String tel_travail) {
		this.tel_travail = tel_travail;
	}
	/**
	 * @return Renvoie id_tiers.
	 */
	public Integer getId_tiers() {
		return id_tiers;
	}
}
