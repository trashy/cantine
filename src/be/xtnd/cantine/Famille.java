/*
 * Familles.java, 8 août 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               Familles.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://cantine.sourceforge.net
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */
package be.xtnd.cantine;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import be.xtnd.cantine.gui.Launcher;
import be.xtnd.commons.db.Database;


/**
 * 
 * 
 * @author Johan Cwiklinski
 */
public class Famille {
	private ArrayList<Tiers> tiers;
	private Pere pere;
	private Mere mere;
    private ArrayList<Eleve> enfants;
    private Integer id_famille, id_medecin, id_hopital;
    private String nom, adresse, cp, ville, telephone, tel_portable, mail, assurance, num_police;
    private boolean adulte;
   	private Database connexion;
   	/** Nom de la table familles */
   	public static final String TABLE = "familles";
   	/** Clé primaire de la table familles */
   	public static final String PK = "id_famille";
   	/** requete de base pour les listes */
   	public static final String BASE_QUERY = "SELECT id_famille, nom, adresse, cp, ville FROM "
   		+TABLE+" WHERE adulte=false ";
   	/** clause order by */
   	public static final String ORDER_BY = " ORDER BY nom ASC";
   	/** Requête de liste des familles, par ordre alphabétique */
   	public static final String QUERY_LIST = BASE_QUERY + ORDER_BY;
	static Logger logger = Logger.getLogger(Famille.class.getName());


   	/**
   	 * Initialise une nouvelle famille
   	 *
   	 */
   	public Famille(){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/cantine/log4j.properties"));
   		this.pere = new Pere();
   		this.mere = new Mere();
   		this.tiers = new ArrayList<Tiers>(2);
   		this.tiers.add(pere);
   		this.tiers.add(mere);
   		this.telephone = new String();
   		this.enfants = new ArrayList<Eleve>(0);
   	}
   	
   	/**
   	 * Initialise une famille en fonction de l'id spécifiée
   	 * @param id id de la famille à initialiser
   	 */
    public Famille(Integer id){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/cantine/log4j.properties"));
    	this.id_famille = id;
		String strSql="SELECT * FROM "+TABLE+" WHERE "+PK+"="+id;
		connexion =  Launcher.db;

		try {
			ResultSet rsFamille = connexion.execQuery(strSql);
			rsFamille.first();
			
			this.nom = rsFamille.getString("nom");
			this.adresse  =rsFamille.getString("adresse");
			this.cp = rsFamille.getString("cp");
			this.ville = rsFamille.getString("ville");
			this.telephone = rsFamille.getString("telephone");
			this.tel_portable = rsFamille.getString("tel_portable");
			this.mail = rsFamille.getString("mail");	
			int medecin = rsFamille.getInt(Urgence.MEDECINS_PK);
			int hopital = rsFamille.getInt(Urgence.HOPITAUX_PK);
			this.adulte = rsFamille.getBoolean("adulte");
			this.assurance = rsFamille.getString("assurance");
			this.num_police = rsFamille.getString("num_police");
			this.tiers = Tiers.tiersFamille(id);
			this.enfants = Eleve.elevesFamille(id);
			
			this.id_medecin = new Integer(medecin);
			this.id_hopital = new Integer(hopital);

		} catch (SQLException e) {
			logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			e.printStackTrace();
		}
    }

	private void formatteDonnees(){
    	this.nom = Database.replace(this.nom, "\'", "\'\'");
    	this.adresse = Database.replace(this.adresse, "\'", "\'\'");
    	this.cp = Database.replace(this.cp, "\'", "\'\'");
    	this.ville = Database.replace(this.ville, "\'", "\'\'");
    	this.telephone = Database.replace(this.telephone, " ", null);
    	this.tel_portable = Database.replace(this.tel_portable, " ", null);
    	this.mail = Database.replace(this.mail, "\'", "\'\'");
		this.assurance = Database.replace(this.assurance, "\'", "\'\'");
		this.num_police = Database.replace(this.num_police, "\'","\'\'");
	}
    
	/**
	 * Enregistre une nouvelle famille
	 *
	 */
	public void storeFamille(){  
    	formatteDonnees();
    	String strSql = "INSERT INTO " + TABLE + " (nom, adresse, cp, ville, telephone, tel_portable, mail, " +
    			"id_medecin, id_hopital, adulte, assurance, num_police) VALUES ('" + 
				this.nom + "', '" + this.adresse + "', '" + this.cp + "', '" + this.ville + "', '" + this.telephone + "', '" +
				this.tel_portable +"', '"+this.mail+ "', " + this.id_medecin + ", " + this.id_hopital + ", " + 
				this.adulte + ", '" + this.assurance + "', '" + this.num_police + "')";
		connexion =  Launcher.db;
    	try {
    		connexion.execUpdate(strSql);
    		ResultSet rs = (Launcher.conf.getProvider_name().equalsIgnoreCase("hsqldb"))?
    				connexion.execQuery("CALL IDENTITY()"):
    					connexion.execQuery("SELECT LAST_INSERT_ID();");
    		rs.first();
    		this.id_famille = new Integer(rs.getInt(1));    		
    	} catch (SQLException e) {
    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode()+")");
    		e.printStackTrace();
    	}
    }
    
	/**
	 * Modifie une famille existante
	 * @param id id de la famille à modifier
	 */
    public void modifyFamille(Integer id){
    	formatteDonnees();
    	String strSql = "UPDATE " + TABLE +" SET " +
		"nom='" + this.nom + "', " +
		"adresse='" + this.adresse + "', " + 
		"cp='" + this.cp +"', " +
		"ville='" + this.ville + "', " +
		"telephone='" + this.telephone + "', " +
		"tel_portable='" + this.tel_portable + "', " +
		"mail='" + this.mail + "', " +
		"id_medecin=" + this.id_medecin + ", " +
		"id_hopital=" + this.id_hopital + ", " +
		"adulte=" + this.adulte + ", " +
		"assurance='" + this.assurance + "', " +
		"num_police='" + this.num_police + "' " + 
		"WHERE "+PK+"="+id;
    	
    	connexion =  Launcher.db;
    	try {connexion.execUpdate(strSql);} catch (SQLException e) {
    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode()+")");
    		e.printStackTrace();
    	}
    }
    
    /**
     * Ajoute le nom à une famille existante. (nom du père / nom de la mère)
     * @param id_famille id de la famille à modifier
     * 
     * @see Pere#nomPere(Integer)
     * @see Mere#nomMere(Integer)
     */
    public void ajouteNom(Integer id_famille){
    	String strSql = "UPDATE "+TABLE+" SET nom='"+Database.replace(this.nom,"\'","\'\'")+"' WHERE "+PK+"="+id_famille;
    	connexion =  Launcher.db;
    	try {connexion.execUpdate(strSql);} catch (SQLException e) {
    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode());
    		e.printStackTrace();
    	}
    }    
    
	/**
	 * Supprime la famille en argument de la base de données
	 * @param id identifiant de la famille à supprimer
	 */
    public void deleteFamille(Integer id){
		String strSql="DELETE FROM "+TABLE+" WHERE "+PK+"="+id;
		connexion = Launcher.db;
		try {connexion.execUpdate(strSql);} catch (SQLException e) {
    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode());
			e.printStackTrace();
		}
    }

    /**
     * 
     * @param t1
     * @return boolean
     */
    public boolean hasOthersParent(Tiers t1){
    	boolean retour = false;
    	Iterator<Tiers> it = tiers.iterator();
    	while(it.hasNext()){
    		Tiers t = (Tiers)it.next();
    		if(t != t1)
    		//if(t.getId_tiers() != id)
    			if(t.getRole().getId_role() == Role.PERE || 
    					t.getRole().getId_role() == Role.MERE)
    				retour = true;
    	}
    	return retour;
    }
    
    /**
	 * @return Renvoie adresse.
	 */
	public String getAdresse() {
		return adresse;
	}
	/**
	 * @param adresse adresse à définir.
	 */
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	/**
	 * @return Renvoie adulte.
	 */
	public boolean isAdulte() {
		return adulte;
	}
	/**
	 * @param adulte adulte à définir.
	 */
	public void setAdulte(boolean adulte) {
		this.adulte = adulte;
	}
	/**
	 * @return Renvoie assurance.
	 */
	public String getAssurance() {
		return assurance;
	}
	/**
	 * @param assurance assurance à définir.
	 */
	public void setAssurance(String assurance) {
		this.assurance = assurance;
	}
	/**
	 * @return Renvoie cp.
	 */
	public String getCp() {
		return cp;
	}
	/**
	 * @param cp cp à définir.
	 */
	public void setCp(String cp) {
		this.cp = cp;
	}
	/**
	 * @return Renvoie mail.
	 */
	public String getMail() {
		return mail;
	}
	/**
	 * @param mail mail à définir.
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}
	/**
	 * @return Renvoie num_police.
	 */
	public String getNum_police() {
		return num_police;
	}
	/**
	 * @param num_police num_police à définir.
	 */
	public void setNum_police(String num_police) {
		this.num_police = num_police;
	}
	/**
	 * @return Renvoie ville.
	 */
	public String getVille() {
		return ville;
	}
	/**
	 * @param ville ville à définir.
	 */
	public void setVille(String ville) {
		this.ville = ville;
	}
	/**
	 * @return Renvoie telephone.
	 */
	public String getTelephone() {
		return telephone;
	}
	/**
	 * @param telephone telephone à définir.
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	/**
	 * @return Renvoie tiers.
	 */
	public ArrayList<Tiers> getTiers() {
		return tiers;
	}
	/**
	 * @param tiers tiers à définir.
	 */
	public void setTiers(ArrayList<Tiers> tiers) {
		this.tiers = tiers;
	}
	/**
	 * @return Renvoie id_hopital.
	 */
	public Integer getId_hopital() {
		return id_hopital;
	}
	/**
	 * @param id_hopital id_hopital à définir.
	 */
	public void setId_hopital(Integer id_hopital) {
		this.id_hopital = id_hopital;
	}
	/**
	 * @return Renvoie id_medecin.
	 */
	public Integer getId_medecin() {
		return id_medecin;
	}
	/**
	 * @param id_medecin id_medecin à définir.
	 */
	public void setId_medecin(Integer id_medecin) {
		this.id_medecin = id_medecin;
	}
	/**
	 * @return Renvoie id_famille.
	 */
	public Integer getId_famille() {
		return id_famille;
	}
	/**
	 * @param id_famille id_famille à définir.
	 */
	public void setId_famille(Integer id_famille) {
		this.id_famille = id_famille;
	}
	/**
	 * @return Renvoie enfants.
	 */
	public ArrayList<Eleve> getEnfants() {
		return enfants;
	}
	/**
	 * @param enfants enfants à définir.
	 */
	public void setEnfants(ArrayList<Eleve> enfants) {
		this.enfants = enfants;
	}
	/**
	 * @return Renvoie nom.
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom nom à définir.
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return Renvoie tel_portable.
	 */
	public String getTel_portable() {
		return tel_portable;
	}
	/**
	 * @param tel_portable tel_portable à définir.
	 */
	public void setTel_portable(String tel_portable) {
		this.tel_portable = tel_portable;
	}
}
