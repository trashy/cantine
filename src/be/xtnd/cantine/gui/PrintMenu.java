/*
 * PrintMenu.java, 2005-10-05
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               	PrintMenu.java
 * Author's email :     	johan@x-tnd.be
 * Author's Website :   	http://cantine.ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import be.xtnd.cantine.CantineI18n;
import be.xtnd.commons.gui.EscapeInternalFrame;
import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.i18n.CommonsI18n;

import com.jeta.forms.components.panel.FormPanel;
import com.jeta.forms.components.separator.TitledSeparator;
import com.jgoodies.looks.plastic.PlasticInternalFrameUI;

/**
 * Print menu
 * 
 * @author Johan Cwiklinski
 * @since 2005-10-05
 */
public class PrintMenu extends EscapeInternalFrame{
	private static final long serialVersionUID = -5735975718978783969L;
	/** Window name */
	public static final String NAME = "menu_impressions";
	
	/**
	 * Default constructor
	 */
	public PrintMenu(){
		super(CantineI18n.tr("Printing"), true, true, true, true);
		super.setName(NAME);
		
        putClientProperty(
            PlasticInternalFrameUI.IS_PALETTE,
            Boolean.TRUE);

		add(new JScrollPane(initMenu()));

		pack();
		setLocation(
				MainGui.centerOnDesktop(this.getSize())
				);
		setMinimumSize(getSize());
		setVisible(true);
		setClosable(true);
		setIconifiable(true);
		setResizable(true);
		MainGui.desktop.add(this);
		
		//check preferences
		Object[] mairie = Launcher.options.getMairie();
		StringBuffer required = new StringBuffer();
		required.append(CantineI18n.tr("Following preferences has not been setted:"));
		boolean opts_ok = true;
		
		if(mairie[0].toString().equals("")){
			required.append("\n- "+CantineI18n.tr("Name"));
			opts_ok = false;
		}
		if(mairie[1].toString().equals("")){
			required.append("\n- "+CantineI18n.tr("Address"));
			opts_ok = false;
		}
		if(mairie[2].toString().equals("")){
			required.append("\n- "+CantineI18n.tr("Zip code"));
			opts_ok = false;
		}
		if(mairie[3].toString().equals("")){
			required.append("\n- "+CantineI18n.tr("Town"));
			opts_ok = false;
		}
		if(mairie[4].toString().equals("")){
			required.append("\n- "+CantineI18n.tr("Phone"));
			opts_ok = false;
		}
		if(mairie[5].toString().equals("")){
			required.append("\n- "+CantineI18n.tr("Fax"));
			opts_ok = false;
		}
		if(mairie[8].toString().equals("")){
			required.append("\n- "+CantineI18n.tr("Logo"));
			opts_ok = false;
		}else{
			File logo = new File(mairie[8].toString()); 
			if(!logo.exists()){
				required.append("\n- "+CantineI18n.tr("Path to logo invlaid"));
				opts_ok = false;				
			}else if(!isImage(logo)){
				required.append("\n- "+CantineI18n.tr("Specified logo is not an image, or its format is not supported"));
				opts_ok = false;								
			}
		}
		if(mairie[9].toString().equals("")){
			required.append("\n- "+CantineI18n.tr("Signatory"));
			opts_ok = false;
		}
		
		required.append("\n\n"+CantineI18n.tr("Would you like to go to the preferences to fix these problems?\nPrinting may not work properly if some errors has occured."));

		if (!opts_ok) {
			int response = JOptionPane.showConfirmDialog(
					MainGui.desktop, 
					required.toString(),
					CommonsI18n.tr("Error!"),
					JOptionPane.YES_NO_OPTION,
					JOptionPane.WARNING_MESSAGE
				);
			if(response == JOptionPane.YES_OPTION){
				if(MainGui.verifUnicite(OptionsGui.NAME, null)){
					new OptionsGui();
				}
			}
			
		}

	}

	/**
	 * Check if object is a valid image
	 * @param o object
	 * @return boolean
	 */
    private static boolean isImage(Object o) {
        try {
            // Create an image input stream on the image
            ImageInputStream iis = ImageIO.createImageInputStream(o);
    
            // Find all image readers that recognize the image format
            Iterator<?> iter = ImageIO.getImageReaders(iis);
            if (!iter.hasNext()) {
                // No readers found
                return false;
            }
    
            // Use the first reader
            //ImageReader reader = (ImageReader)iter.next();
    
            // Close stream
            iis.close();
    
            // Return the format name
            return true;
        } catch (IOException e) {}
        // The image could not be read
        return false;
    }

    /**
     * Initialize menu
     * @return JPanel
     */
	private JPanel initMenu(){
		FormPanel pane = new FormPanel( "be/xtnd/cantine/gui/descriptions/print_menu.jfrm" );
		
		TitledSeparator title = (TitledSeparator)pane.getComponentByName("title");
		title.setText(CantineI18n.tr("Printing"));
		
	    JButton invoices = (JButton) pane.getButton("factures");
	    invoices.addActionListener(new PrintInvoices());
	    invoices.setText(CantineI18n.tr("Invoices"));
	    	    
	    JButton commands = (JButton)pane.getButton("commandes");
	    commands.addActionListener(new PrintCdes());
	    commands.setText(CantineI18n.tr("Commands"));
	    
	    JButton families = (JButton) pane.getButton("familles");
	    families.addActionListener(new PrintFamilies());
	    families.setText(CantineI18n.tr("Families"));
	    
	    JButton adults = (JButton) pane.getButton("adultes");
	    adults.addActionListener(new PrintAdults());
	    adults.setText(CantineI18n.tr("Adults"));
	    
	    JButton pointages = (JButton) pane.getButton("pointages");
	    pointages.addActionListener(new PrintChecks());
	    pointages.setText(CantineI18n.tr("Plannings"));
	     
	    JButton close = (JButton)pane.getButton("fermer");
	    close.setText(CantineI18n.tr("Close"));
	    close.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                dispose();
            }    
	    });
	    
		return pane;
	}

	/**
	 * Action on invoices
	 * @author trasher
	 * @since 2005-10-05
	 */
	class PrintInvoices implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(Launcher.verifUnicite(MoisFacture.NAME,null)){
				new MoisFacture();
			}
		}
	}

	/**
	 * Action on commands
	 * @author trasher
	 * @since 2005-10-05
	 */
	class PrintCdes implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(Launcher.verifUnicite(DateCommande.NAME,null)){
				new DateCommande();
			}
		}
	}
	
	/**
	 * Action on families
	 * @author trasher
	 * @since 2005-10-05
	 */
	class PrintFamilies implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(Launcher.verifUnicite(ImprimeFamilles.NAME,null)){
				new ImprimeFamilles();
			}
		}	
	}
	
	/**
	 * Action on adults
	 * @author trasher
	 * @since 2005-10-05
	 */
	class PrintAdults implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(Launcher.verifUnicite(ImprimeAdultes.NAME,null)){
				new ImprimeAdultes();
			}
		}	
	}
	
	/**
	 * Action on planning
	 * @author trasher
	 * @since 2005-10-05
	 */
	class PrintChecks implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(Launcher.verifUnicite(ImprimePointages.NAME,null)){
				new ImprimePointages();
			}
		}	
	}
}
