/*
 * Messages.java, 19 oct. 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               	Messages.java
 * Author's email :     	johan@x-tnd.be
 * Author's Website :   	http://cantine.sourceforge.net
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine.gui;

import java.util.ResourceBundle;

import be.xtnd.cantine.CantineI18n;
import be.xtnd.commons.AbstractMessages;
import be.xtnd.commons.ApplicationProperties;

/**
 * Récupère les valeurs en fonction d'une clé fournie
 * dans un fichier .properties
 * 
 * @author Johan Cwiklinski
 * @version 1.0
 * @deprecated
 */
public class Messages extends AbstractMessages {
     private static final String BUNDLE_NAME = "be.xtnd.cantine.messages";
  
     /**
      * Récupère la clé en paramètres dans le fichier de ressources
      * @param key la clé à lire
      * @return String valeur de la clé
      * @deprecated Replaced by {@link CantineI18n} for translations 
      * and {@link ApplicationProperties} for bundle properties
      */
     public static String getString(String key){
         return getString(key, BUNDLE_NAME);
     }
     
     /**
      * Renvoie le ResourceBundle utilisé, pour les classes communes (Splash, AboutBox, ...)
      *  
      * @return ResourceBundle le bundle de l'application
      * @see be.xtnd.commons.Splash
      * @see be.xtnd.commons.gui.AboutBox
      * @see be.xtnd.commons.gui.MainGui
      * @deprecated {@link be.xtnd.cantine.ApplicationProperties#getBundle()}
      */
     public static ResourceBundle getBundle(){
    	 return getBundle(BUNDLE_NAME);
     }
}
