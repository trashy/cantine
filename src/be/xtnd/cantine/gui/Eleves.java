/*
 * Eleves.java, 8 août 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :              	 	Eleves.java
 * Author's email :     	johan@x-tnd.be
 * Author's Website :   	http://www.x-tnd.be/
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.MessageFormat;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import be.xtnd.cantine.CantineI18n;
import be.xtnd.cantine.Classe;
import be.xtnd.cantine.Eleve;
import be.xtnd.cantine.Famille;
import be.xtnd.commons.GuiCommons;
import be.xtnd.commons.gui.EscapeInternalFrame;
import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.i18n.CommonsI18n;

import com.jeta.forms.components.border.TitledBorderLabel;
import com.jeta.forms.components.panel.FormPanel;
import com.jeta.forms.components.separator.TitledSeparator;
import com.jgoodies.looks.plastic.PlasticInternalFrameUI;
import com.toedter.calendar.JDateChooser;

/**
 * 
 *
 * @author Johan Cwiklinski
 */
public class Eleves {
	private Eleve eleve;
	private GuiCommons commons = new GuiCommons();
	private JTextField nom_eleve, prenom_eleve, lieu_naissance_eleve;
	private JComboBox classe;
	private JTextArea observations_eleve, allergies_eleve;
	private JRadioButton eleve_g, eleve_f;
	private JCheckBox cantine;
	private JDateChooser date_naissance;
	private JButton presence_eleve;
	private JLabel classe_label, nom_enfant_label, prenom_enfant_label, sexe_enfant_label,
		date_naissance_label, lieu_naissance_label, allergies_label, observations_label;
	private TitledBorderLabel reservation_label;
	private TitledSeparator naissance_label, divers_label;
	/** Nom de la fenêtre */
	public static final String NAME = "nouvel_eleve";
	private Famille famille;
	private Familles familles;
	
	/**
	 * 
	 * @param familles
	 */
	public void nouvelEleve(Familles familles){
		this.famille = familles.famille;
		this.familles = familles;
		new Fenetre();
	}
	
	private void setValues(){
		eleve.setNom(nom_eleve.getText());
		eleve.setPrenom(prenom_eleve.getText());
		if(eleve_g.isSelected()) eleve.setSexe('G');
		if(eleve_f.isSelected()) eleve.setSexe('F');
		eleve.setDate_naissance(date_naissance.getDate());
		eleve.setLieu_naissance(lieu_naissance_eleve.getText());
		eleve.setClasse(new Classe(classe.getSelectedItem().toString()));
		eleve.setCantine(cantine.isSelected());
		//eleve.setCentre_aere(centre_aere.isSelected());
		eleve.setObservations(observations_eleve.getText());
		eleve.setAllergies(allergies_eleve.getText());

		//eleve.storeEleve();
		famille.getEnfants().add(eleve);
	}
	
	private boolean verifEntrees(){
		boolean valid = true;
		StringBuffer required = new StringBuffer();
		required.append(CommonsI18n.tr("The following are required:"));

		if(!eleve_g.isSelected() && !eleve_f.isSelected()){
			required.append("\n- " + Messages.getString("window.eleve.required.sex"));
			valid=false; 
		}
		if(prenom_eleve.getText().trim().equalsIgnoreCase("")){
			required.append("\n- " + Messages.getString("window.eleve.required.surname"));
			prenom_eleve.requestFocus();
			valid = false;
		}
		if(classe.getSelectedIndex()==0){
			required.append("\n- " + Messages.getString("window.eleve.required.class"));
			if(valid) classe.requestFocus();
			valid = false;
		}

		if(!valid){
			JOptionPane.showMessageDialog(
					MainGui.desktop, 
					required.toString(),
					CommonsI18n.tr("Error!"),
					JOptionPane.ERROR_MESSAGE);			
		}
		return valid;
	}

	/**
	 * 
	 * @return FormPanel
	 */
	public FormPanel elevePanel(){
		FormPanel pane = new FormPanel( "be/xtnd/cantine/gui/descriptions/enfants.jfrm" );
		
		reservation_label = (TitledBorderLabel)pane.getComponentByName("reservation_label");
		reservation_label.setText(Messages.getString("window.eleve.labels.reservation"));
		
		naissance_label = (TitledSeparator)pane.getComponentByName("naissance_label");
		naissance_label.setText(Messages.getString("window.eleve.labels.birth"));
		
		divers_label = (TitledSeparator)pane.getComponentByName("divers_label");
		divers_label.setText(Messages.getString("window.eleve.labels.divers"));
		
		nom_enfant_label = pane.getLabel("nom_enfant_label");
		nom_enfant_label.setText(CantineI18n.tr("Name"));
		
		nom_eleve = pane.getTextField("nom_enfant");
		
		prenom_enfant_label = pane.getLabel("prenom_enfant_label");
		prenom_enfant_label.setText(CantineI18n.tr("First name"));
		
		prenom_eleve = pane.getTextField("prenom_enfant");
		
		date_naissance_label = pane.getLabel("date_naissance_label");
		date_naissance_label.setText(Messages.getString("window.eleve.labels.birthdate"));
		
		Calendar calendar = Calendar.getInstance(); 
		date_naissance = (JDateChooser)pane.getComponentByName("date_naissance");
		date_naissance.setDate(calendar.getTime());
		
		lieu_naissance_label = pane.getLabel("lieu_naissance_label");
		lieu_naissance_label.setText(Messages.getString("window.eleve.labels.birthplace"));
		
		lieu_naissance_eleve = pane.getTextField("lieu_naissance");
		
		classe_label = pane.getLabel("classe_label");
		classe_label.setText(Messages.getString("window.eleve.labels.classe"));
		
		classe = pane.getComboBox("classe");
		commons.buildCombo(classe, Classe.listing());
		
		sexe_enfant_label = pane.getLabel("sexe_enfant_label");
		sexe_enfant_label.setText(Messages.getString("window.eleve.labels.sex"));
		
		eleve_g = pane.getRadioButton("garcon_radio");
		eleve_g.setText(Messages.getString("window.eleve.labels.boy"));
		
		eleve_f = pane.getRadioButton("fille_radio");
		eleve_f.setText(Messages.getString("window.eleve.labels.girl"));
		
		cantine = pane.getCheckBox("cantine");
		cantine.setText(Messages.getString("window.eleve.labels.cantine"));
		cantine.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e) {
				eleve.setCantine((e.getStateChange() == ItemEvent.SELECTED)?true:false);
				if(e.getStateChange() == ItemEvent.SELECTED){
					presence_eleve.setEnabled(true);
				}
				if(e.getStateChange() == ItemEvent.DESELECTED){
					presence_eleve.setEnabled(false);
				}
				
			}
		});
			
		presence_eleve = (JButton)pane.getButton("presence");
		presence_eleve.setText(Messages.getString("window.eleve.labels.presence"));
		presence_eleve.setEnabled(false);
        presence_eleve.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
            	Object[] args = {""};
            	if(MainGui.verifUnicite(
            			MessageFormat.format(Messages.getString("window.presence.name"), args),
						null)){
                	new Presences(eleve);	                		
            	}
            }
        });			

        
		observations_label = pane.getLabel("observations_label");
		observations_label.setText(Messages.getString("window.eleve.labels.observations"));
		
		observations_eleve = (JTextArea)pane.getComponentByName("observations");
		
		allergies_label = pane.getLabel("allergies_label");
		allergies_label.setText(Messages.getString("window.eleve.labels.allergies"));
		
		allergies_eleve = (JTextArea)pane.getComponentByName("allergies");	

		return pane;
	} 

	private class Fenetre extends EscapeInternalFrame{
		private static final long serialVersionUID = -3314511411467101238L;

		/**
		 * 
		 */
		public Fenetre(){
			super();		
			eleve = new Eleve();
			eleve.setId_famille(famille.getId_famille());
			
	        putClientProperty(
	                PlasticInternalFrameUI.IS_PALETTE,
	                Boolean.TRUE);
	        
	        FormPanel elevesPane = elevePanel();
			getContentPane().add(elevesPane);
			elevesPane.setBorder(new EmptyBorder(3,3,3,3));
			
			commons.createButtonBar(this, new OkayEvent());
			
			pack();
			setTitle(Messages.getString("window.eleve.titles.new"));
			setName(NAME);

			MainGui.desktop.add(this, JLayeredPane.POPUP_LAYER);
			setLocation(MainGui.centerOnDesktop(getSize()));
			
			setResizable(true);
			setClosable(true);
			setIconifiable(true);
			setMaximizable(true);
			setVisible(true);
		}

		class OkayEvent implements ActionListener{
			/**
			 * @param e
			 */
			public void actionPerformed(ActionEvent e) {
				if(verifEntrees()){
					setValues();
					//rafraichit la fenetre famille
					//familly_frame.dispose();
					//new Familles().modifieFamille(eleve.getId_famille());
					familles.enfantsTab(); //rafraichit le TabbedPane enfants
					dispose();
				}
			}		
		}

	}
}
