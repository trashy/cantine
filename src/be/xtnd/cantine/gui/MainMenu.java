/*
 * MainMenu.java, 2005-08-08
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               MainMenu.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://cantine.ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import be.xtnd.cantine.CantineI18n;
import be.xtnd.commons.gui.EscapeInternalFrame;
import be.xtnd.commons.gui.MainGui;

import com.jeta.forms.components.border.TitledBorderLabel;
import com.jeta.forms.components.panel.FormPanel;
import com.jgoodies.looks.plastic.PlasticInternalFrameUI;

/**
 * Application main menu  
 * @author Johan Cwiklinski
 * @since 2005-08-08
 */
public class MainMenu extends EscapeInternalFrame{
	private static final long serialVersionUID = 979642540309205579L;
	/** Window name */
	public static final String NAME = "main_menu";
	
	/**
	 * Default constructor. Builds and shows up main menu window.
	 */
	public MainMenu(){
		super(CantineI18n.tr("Main menu"), true, true, true, true);
		super.setName(NAME);
		
        putClientProperty(
            PlasticInternalFrameUI.IS_PALETTE,
            Boolean.TRUE);

		add(new JScrollPane(initMenu()));
		
		pack();
		setLocation(
				MainGui.centerOnDesktop(this.getSize())
		);
		setMinimumSize(getSize());
		setVisible(true);
		setClosable(true);
		setIconifiable(true);
		setResizable(true);
		MainGui.desktop.add(this);
		toFront();
		try {
			setSelected(true);
		} catch (PropertyVetoException e) {}
	}

	/**
	 * Builds menu
	 * @return JPanel
	 */
	private JPanel initMenu(){
		FormPanel pane = new FormPanel( "be/xtnd/cantine/gui/descriptions/main_menu.jfrm" );
	    JButton familles = (JButton) pane.getButton("familles");
	    familles.addActionListener(new EditFamilles());
	    familles.setText(CantineI18n.tr("Families edition"));
	    
	    JButton adults = (JButton)pane.getButton("adultes");
	    adults.addActionListener(new EditAdults());
	    adults.setText(CantineI18n.tr("Adults"));
	     
	    JButton impressions = (JButton) pane.getButton("impressions");
	    impressions.setText(CantineI18n.tr("Print reports"));
	    impressions.addActionListener(new Print());
	    
	    JButton factures = (JButton)pane.getButton("factures");
	    factures.setText(CantineI18n.tr("Invoicing"));
	    factures.addActionListener(new EditFactures());
	    
	    JButton pointages = (JButton) pane.getButton("pointages");
	    pointages.setText(CantineI18n.tr("Planning"));
	    pointages.addActionListener(new PlanningMenuAction());
	    
	    TitledBorderLabel urgences_label = (TitledBorderLabel)pane.getComponentByName("urgences_label");
	    urgences_label.setText(CantineI18n.tr("EMERGENCIES"));
	    
	    JButton medecins = (JButton) pane.getButton("medecins");
	    medecins.setText(CantineI18n.tr("Doctors"));
	    medecins.addActionListener(new MedecinsAction());
	    
	    JButton hopitaux = (JButton) pane.getButton("hopitaux");
	    hopitaux.setText(CantineI18n.tr("Hospitals"));
	    hopitaux.addActionListener(new HopitauxAction());
	    
		return pane;
	}
	
	/**
	 * Action on edit families
	 * @author trasher
	 * @since 2005-08-08
	 */
	class EditFamilles implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(Launcher.verifUnicite(Familles.LIST_NAME, null)){		
				new Familles().afficheListe();
			}
		}
	}
	
	/**
	 * Action on adults
	 * @author trasher
	 * @since 2005-08-08
	 */
	class EditAdults implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(Launcher.verifUnicite(Adultes.LIST_NAME, null)){		
				new Adultes().afficheListe();
			}
		}
	}
	
	/**
	 * Action on reports
	 * @author trasher
	 * @since 2005-08-08
	 */
	class Print implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            if(Launcher.verifUnicite(PrintMenu.NAME, null)){
                new PrintMenu();
            }
        }	    
	}
	
	/**
	 * Action on invoicing
	 * @author trasher
	 * @since 2005-08-08
	 */
	class EditFactures implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(Launcher.verifUnicite(Factures.LIST_NAME, null)){		
				new Factures();
			}
		}
	}
	
	/**
	 * Action on absences
	 * @author trasher
	 * @since 2005-08-08
	 */
	class PlanningMenuAction implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if(Launcher.verifUnicite(Plannings.NAME, null)){		
				new Plannings();
			}
		}		
	}
	
	/**
	 * Action on doctors
	 * @author trasher
	 * @since 2005-08-08
	 */
	class MedecinsAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(Launcher.verifUnicite(Urgences.DOCTORS_LIST_NAME, null)){		
				new Urgences("medecins").getList();
			}
		}
	}
	
	/**
	 * Action on hospitals
	 * @author trasher
	 * @since 2005-08-08
	 */
	class HopitauxAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(Launcher.verifUnicite(Urgences.HOSPITALS_LIST_NAME, null)){		
				new Urgences("hopitaux").getList();
			}
		}
	}
	
}
