/*
 * Plannings.java, 18 août 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               	Plannings.java
 * Author's email :     	johan@x-tnd.be
 * Author's Website :   	http://cantine.sourceforge.net
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JRadioButton;
import javax.swing.JRootPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import be.xtnd.cantine.CantineI18n;
import be.xtnd.cantine.Classe;
import be.xtnd.cantine.Eleve;
import be.xtnd.cantine.Planning;
import be.xtnd.cantine.Presence;
import be.xtnd.commons.GuiCommons;
import be.xtnd.commons.gui.EscapeInternalFrame;
import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.i18n.CommonsI18n;

import com.jeta.forms.components.border.TitledBorderLabel;
import com.jeta.forms.components.panel.FormPanel;
import com.jgoodies.looks.plastic.PlasticInternalFrameUI;
import com.toedter.calendar.JMonthChooser;
import com.toedter.calendar.JYearChooser;

/**
 * 
 * 
 * @author Johan Cwiklinski
 *
 */
public class Plannings extends EscapeInternalFrame implements ActionListener{
	private static final long serialVersionUID = 2535364790734288488L;
	private Classe classe = new Classe();
	private GuiCommons commons = new GuiCommons();
	
	private JYearChooser annee;
	private JMonthChooser mois;
    private JTable pointages;
    private JComboBox type/*, reservation*/;
    private JRadioButton deduit, a_rembourser, non_justifie, en_attente, imprevu, prepaye;
    private JLabel label_deduit, label_rembourser, label_non_justifie, label_attente, 
    	label_imprevu, label_prepaye, type_label/*, reservation_label*/, mois_label, annee_label;
    private TitledBorderLabel title_pointage;
    private ButtonGroup btn_group;
    private JButton okay, apply, cancel;
	
	private String[] listOfDays;
	private Object[][] listOfRows, origsRows;
    private boolean DEBUG = true;
    private Border headerBorder = UIManager.getBorder("TableHeader.cellBorder");
    private Color couleur, police_couleur;
    
    private int month;
    private int year;
    //private char reserv;
	static Logger logger = Logger.getLogger(Plannings.class.getName());
    /** Nom de la fenêtre */
    public static final String NAME = "planning";
	
    /**
     * 
     *
     */
	public Plannings(){
		super();
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/cantine/log4j.properties"));
		setName(NAME);

        putClientProperty(
                PlasticInternalFrameUI.IS_PALETTE,
                Boolean.TRUE);

		FormPanel pane = new FormPanel( "be/xtnd/cantine/gui/descriptions/gestion_absences.jfrm" );
		
		type_label = pane.getLabel("type_label");
		type_label.setText(Messages.getString("window.planning.labels.type"));
		
		type = pane.getComboBox("type"); 
		commons.buildCombo(type, classe.getCategories());
		type.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				drawTable();
			}
		});
		
		/*reservation_label = pane.getLabel("reservation_label");
		reservation_label.setText(Messages.getString("window.planning.labels.reservation"));
		
		reservation = pane.getComboBox("reservation");
		reservation.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				drawTable();
			}
		});*/
				
		title_pointage = (TitledBorderLabel)pane.getComponentByName("title_pointage");
		title_pointage.setText(Messages.getString("window.planning.title_pointage"));
		
		Object[] colors = Launcher.options.getCouleurs_pointages();
		
		label_deduit = pane.getLabel("label_deduit");
		label_deduit.setForeground((Color)colors[Presence.DEDUIT]);
		label_deduit.setBackground((Color)colors[Presence.DEDUIT]);
		
		label_rembourser = pane.getLabel("label_rembourser");
		label_rembourser.setForeground((Color)colors[Presence.REMBOURSER]);
		label_rembourser.setBackground((Color)colors[Presence.REMBOURSER]);
		
		label_non_justifie = pane.getLabel("label_non_justifie");
		label_non_justifie.setForeground((Color)colors[Presence.NON_JUSTIFIE]);
		label_non_justifie.setBackground((Color)colors[Presence.NON_JUSTIFIE]);
		
		label_attente = pane.getLabel("label_attente");
		label_attente.setForeground((Color)colors[Presence.ATTENTE]);
		label_attente.setBackground((Color)colors[Presence.ATTENTE]);
		
		label_imprevu = pane.getLabel("label_imprevu");
		label_imprevu.setForeground((Color)colors[Presence.IMPREVU]);
		label_imprevu.setBackground((Color)colors[Presence.IMPREVU]);
		
		label_prepaye = pane.getLabel("label_prepaye");
		label_prepaye.setForeground((Color)colors[Presence.PREPAYE]);
		label_prepaye.setBackground((Color)colors[Presence.PREPAYE]);
		
		deduit = pane.getRadioButton("deduit");
		deduit.setText(Messages.getString("window.planning.deducted"));
		a_rembourser = pane.getRadioButton("a_rembourser");
		a_rembourser.setText(Messages.getString("window.planning.refund"));
		non_justifie = pane.getRadioButton("non_justifie");
		non_justifie.setText(Messages.getString("window.planning.not_explained"));
		en_attente = pane.getRadioButton("en_attente");
		en_attente.setText(Messages.getString("window.planning.waiting"));
		imprevu = pane.getRadioButton("imprevu");
		imprevu.setText(Messages.getString("window.planning.unscheduled"));	
		prepaye = pane.getRadioButton("prepaye");
		prepaye.setText(Messages.getString("window.planning.prepaid"));

		btn_group = new ButtonGroup();
		btn_group.add(deduit);
		btn_group.add(a_rembourser);
		btn_group.add(non_justifie);
		btn_group.add(en_attente);
		btn_group.add(imprevu);
		btn_group.add(prepaye);
		
		pointages = (JTable)pane.getComponentByName("pointages");
		pointages.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		pointages.getTableHeader().setReorderingAllowed(false);
		pointages.setRowSelectionAllowed(false);
		pointages.setColumnSelectionAllowed(false);
		pointages.setCellSelectionEnabled(true);
		pointages.addMouseListener(new MouseAdapter(){
			public void mouseReleased(MouseEvent e){
		  	  	int nModifier = e.getModifiers();
		    	if ((nModifier & InputEvent.BUTTON1_MASK) != 0) {
		    		applyPointages(
		    				pointages.rowAtPoint(e.getPoint()), 
							pointages.columnAtPoint(e.getPoint())
					);
		      	}
			}
		});
		//FIXME implémentation de la sélection multiple
	    //SelectionListener listener = new SelectionListener(pointages);
	    //pointages.getSelectionModel().addListSelectionListener(listener);
	    //pointages.getColumnModel().getSelectionModel().addListSelectionListener(listener);
		
		JTableHeader head = pointages.getTableHeader();
		couleur = head.getBackground();
		police_couleur = head.getForeground();
		
		annee_label = pane.getLabel("annee_label");
		annee_label.setText(Messages.getString("window.planning.labels.year"));
		
		Calendar calendar = Calendar.getInstance(); 
		annee = (JYearChooser)pane.getComponentByName("annee");
		annee.addPropertyChangeListener(new ChangeDateListener());
		annee.setMinimum(2000);
		annee.setMaximum(2500);
		this.year = annee.getYear();

		mois_label = pane.getLabel("mois_label");
		mois_label.setText(Messages.getString("window.planning.labels.month"));
		
		mois = (JMonthChooser)pane.getComponentByName("mois");
		mois.setMonth(calendar.get(Calendar.MONTH));
		mois.setYearChooser(annee);
		this.month = mois.getMonth();

		mois.addPropertyChangeListener(new ChangeDateListener()); //changement de mois
		annee.addPropertyChangeListener(new ChangeDateListener()); //changement d'année
		
		okay = (JButton)pane.getComponentByName("okay");
		okay.setText(CommonsI18n.tr("Okay"));
		okay.setActionCommand("okay");
		okay.addActionListener(this);
		
		apply = (JButton)pane.getComponentByName("appliquer");
		apply.setText(CommonsI18n.tr("Apply"));
		apply.setActionCommand("apply");
		apply.addActionListener(this);
		
		cancel = (JButton)pane.getComponentByName("annuler");
		cancel.setText(CommonsI18n.tr("Cancel"));
		cancel.setActionCommand("annuler");
		cancel.addActionListener(this);

		add(pane);
				
		pack();
		MainGui.desktop.add(this, JLayeredPane.MODAL_LAYER);
		setTitle(Messages.getString("window.planning.title"));
		setLocation(MainGui.centerOnDesktop(getSize()));
		setResizable(true);
		setClosable(true);
		setIconifiable(true);
		setMaximizable(true);
		try {
			setMaximum(true);
		} catch (PropertyVetoException e) {e.printStackTrace();}
		setVisible(true);			
	}
	
	/**
	 * @param e
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("annuler")){
			type.setSelectedIndex(0);
			dispose();
		}else{
			for(int i = 0 ; i< listOfRows.length ; i++){
				Integer eleve = new Integer(pointages.getValueAt(i,0).toString());
				Planning plane = new Planning(eleve);
				ArrayList<Object> values = new ArrayList<Object>(listOfDays.length-2);
				for(int y = 2 ; y < listOfDays.length ; y++){
					Integer valeur = new Integer(pointages.getValueAt(i,y).toString());
					Integer storedValue = new Integer(origsRows[i][y].toString());

					if(
							!valeur.equals(new Integer(Presence.NON))
							&& !valeur.equals(new Integer(Presence.WEEKEND))
					){
						
						Calendar currentDate = Calendar.getInstance();
						currentDate.set(Calendar.DAY_OF_MONTH, 1);
					    Calendar currentCal = new GregorianCalendar(
					    		year, 
								month, 
								1);
					    currentCal.add(Calendar.MONTH, -1);
					    boolean preceding = (currentCal.before(currentDate))?true:false;
					    if(preceding){
							Calendar cal = new GregorianCalendar();
							cal.set(Calendar.DATE, y-1);
							cal.set(Calendar.MONTH, mois.getMonth());
							cal.set(Calendar.YEAR, annee.getYear());
							Object[] o = {cal.getTime(),valeur};
							values.add(o);
					    }
					}else if(
					        valeur.equals(new Integer(Presence.NON))
					        && !storedValue.equals(new Integer(Presence.NON))
					){
					    System.err.println("A supprimer : ligne "+i+" colonne "+y);
						Calendar cal = new GregorianCalendar();
						cal.set(Calendar.DATE, y-1);
						cal.set(Calendar.MONTH, mois.getMonth());
						cal.set(Calendar.YEAR, annee.getYear());
						plane.deleteValue(cal.getTime());
					}
				}
				values.trimToSize();
				plane.storeValues(values);
			}			
		}
		if(e.getActionCommand().equals("okay")){
			type.setSelectedIndex(0);
			dispose();
		}
	}
	
	/**
	 * Crée le conteneur principal.
	 * Cette méthode est utilisée pour que l'appui sur la touche Echap
	 * ferme la fenêtre.
	 * 
	 * @return JRootPane le panneau principal
	 */
	@Override
	protected JRootPane createRootPane() {
		ActionListener actionListener = new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				type.setSelectedIndex(0);
				dispose();
			}
		};
		JRootPane rootPane = new JRootPane();
		KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
		rootPane.registerKeyboardAction(actionListener, stroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		return rootPane;
	}
	
	private void applyPointages(int row, int col){
		if(type.getSelectedIndex()==0) return;
		String value = pointages.getValueAt(row, col).toString();	
		String storedValue = origsRows[row][col].toString();
		System.err.println(storedValue);
		JRadioButton  btn = getSelection(btn_group);
		if(col==0 || col==1) return;
		if(
				btn!=null 
				&& !value.equalsIgnoreCase(String.valueOf(Presence.WEEKEND)) //pas le dimanche !
				&& !value.equalsIgnoreCase(String.valueOf(Presence.NON)) //si non prévu
		){ 
			if(btn.getName().equalsIgnoreCase("deduit"))
			    value = (value.equals(String.valueOf(Presence.DEDUIT)))?
			            		(storedValue.equalsIgnoreCase(String.valueOf(Presence.DEDUIT)))?
			                    String.valueOf(Presence.PRESENT):
			                    storedValue:
			                 String.valueOf(Presence.DEDUIT);
			if(btn.getName().equalsIgnoreCase("a_rembourser"))
				value = (value.equals(String.valueOf(Presence.REMBOURSER)))?
				        		(storedValue.equalsIgnoreCase(String.valueOf(Presence.REMBOURSER)))?
			                    String.valueOf(Presence.PRESENT):
			                    storedValue:
				            String.valueOf(Presence.REMBOURSER);
			if(btn.getName().equalsIgnoreCase("non_justifie"))
				value = (value.equals(String.valueOf(Presence.NON_JUSTIFIE)))?
				        		(storedValue.equalsIgnoreCase(String.valueOf(Presence.NON_JUSTIFIE)))?
			                    String.valueOf(Presence.PRESENT):
			                    storedValue:
				            String.valueOf(Presence.NON_JUSTIFIE);
			if(btn.getName().equalsIgnoreCase("en_attente"))
				value = (value.equals(String.valueOf(Presence.ATTENTE)))?
				        		(storedValue.equalsIgnoreCase(String.valueOf(Presence.ATTENTE)))?
			                    String.valueOf(Presence.PRESENT):
			                    storedValue:
				            String.valueOf(Presence.ATTENTE);
			if(btn.getName().equalsIgnoreCase("imprevu"))
			    value = (storedValue.equalsIgnoreCase(String.valueOf(Presence.IMPREVU)))?String.valueOf(Presence.NON):storedValue;
			if(btn.getName().equalsIgnoreCase("prepaye"))
			    value = (storedValue.equalsIgnoreCase(String.valueOf(Presence.PREPAYE)))?String.valueOf(Presence.NON):storedValue;
		}else if(
			btn!=null
			&& value.equalsIgnoreCase(String.valueOf(Presence.NON))
			&& btn.getName().equalsIgnoreCase("imprevu")
		){
			value = String.valueOf(Presence.IMPREVU);
		}else if(
			btn!=null
			&& value.equalsIgnoreCase(String.valueOf(Presence.NON))
			&& btn.getName().equalsIgnoreCase("prepaye")
		){
		    value = String.valueOf(Presence.PREPAYE);
		}
		pointages.setValueAt(value,row, col);		
	}
	
	private void drawTable(){
		long begin_script = Calendar.getInstance().getTimeInMillis();
		getDays();
		
		/*if(this.reservation.getSelectedIndex()==0) reserv = Presence.CANTINE;
		if(this.reservation.getSelectedIndex()==1) reserv = Presence.CENTRE;*/

		
		listOfRows = new Eleve().presenceELeves(
				this.month, 
				this.year, 
				this.type.getSelectedItem().toString()
		);
		
		origsRows = listOfRows.clone();	
		
		pointages.setModel(new MyTableModel(listOfDays, listOfRows));

		TableColumnModel columnModel = pointages.getColumnModel();
	    
		int colWidth = 30;
		
		for(int i=0;i<pointages.getColumnCount();i++){
			TableColumn column = columnModel.getColumn(i);
			if(i!=0 && i!=1){
				if(listOfRows.length>0 && Launcher.options.isHide_closed()){
					int value = new Integer(pointages.getValueAt(0,i).toString()).intValue();
					colWidth = (value!=-1)?30:0;
				}
				column.setPreferredWidth(colWidth);
				column.setMinWidth(colWidth);
				column.setMaxWidth(colWidth);
				column.setCellRenderer(new PersoCellRenderer());
			}else if(i==1){
				column.setPreferredWidth(100);
				column.setCellRenderer(new TitleCellRenderer());
				//column.addPropertyChangeListener();
			}else if(i==0){
				column.setPreferredWidth(0);
				column.setMinWidth(0);
				column.setMaxWidth(0);
			}
			JLabel label = new JLabel(listOfDays[i], JLabel.CENTER);
			label.setBorder(headerBorder);
			column.setHeaderRenderer(new HeaderRenderer());
			column.setHeaderValue(label);
			
		}		
		MainGui.verifUnicite(NAME, null);
		long end_script = Calendar.getInstance().getTimeInMillis();
		logger.debug("Planning execution time = "+(end_script-begin_script));
	}
	
    /**
     * getSelection(ButtonGroup group)
     * Retourne le RadioButton sélectionné du groupe en paramètre
     * 
     * @param group : ButtonGroup à évaluer
     * @return JRadioButton sélectionné
     */
    public static JRadioButton getSelection(ButtonGroup group) {
        for (Enumeration<?> e=group.getElements(); e.hasMoreElements(); ) {
            JRadioButton b = (JRadioButton)e.nextElement();
            if (b.getModel() == group.getSelection()) {
                return b;
            }
        }
        return null;
    }
	    
	/**
	 * getDays()
	 * implémente le tableau listOfDays pour l'entête de la JTable
	 */
	public void getDays(){
		SimpleDateFormat formatter = new SimpleDateFormat("EEE");
		SimpleDateFormat formatterbis = new SimpleDateFormat("d");
		
	    Calendar cal = new GregorianCalendar(
	    		year, 
				month, 
				1);
	    
		int days = cal.getActualMaximum(Calendar.DAY_OF_MONTH); 
		listOfDays = new String[days+2];	    
	    
	    for(int i=0; i<days+2; i++){
	    	if(i==0 || i==1){listOfDays[i]=" ";}else{
	    		Date curDate = cal.getTime();
	    		cal.add(Calendar.DAY_OF_MONTH, 1);
	    		String result = formatter.format(curDate);
	    		String resultbis = formatterbis.format(curDate);
	    		listOfDays[i] = "<html><center>"+result+"<br>"+resultbis+"</center></html>";
	    	}
	    }
	}
	
    class MyTableModel extends AbstractTableModel {
		private static final long serialVersionUID = 3988208887949011428L;
		private String[] columnNames;
        private Object[][] data;
    	        
        MyTableModel(String[] colNames, Object[][] rowsDatas){
        	this.columnNames = colNames;
        	this.data = rowsDatas;
        }

        /**
         * @return int
         */
        public int getColumnCount() {
            return columnNames.length;
        }

        /**
         * @return int
         */
        public int getRowCount() {
            return data.length;
        }

        /**
         * @param col
         * @return String
         */
        public String getColumnName(int col) {
            return columnNames[col];
        }

        /**
         * @param row
         * @param col
         * @return Object
         */
        public Object getValueAt(int row, int col) {
        	return data[row][col];
        }
        
        /**
         * @param c
         * @return Class
         */
        public Class<?> getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }

        /**
         * @param row
         * @param col
         * @return boolean
         */
        public boolean isCellEditable(int row, int col) {
        	return false;
        }

        /**
         * @param value
         * @param row
         * @param col
         */
        public void setValueAt(Object value, int row, int col) {
            if (DEBUG) {
                System.out.println("Setting value at " + row + "," + col
                                   + " to " + value
                                   + " (an instance of "
                                   + value.getClass() + ")");
            }

            data[row][col] = value;
            fireTableCellUpdated(row, col);

            if (DEBUG) {
                System.out.println("New value of data:");
                printDebugData();
            }
        }

        private void printDebugData() {
            int numRows = getRowCount();
            int numCols = getColumnCount();

            for (int i=0; i < numRows; i++) {
                System.out.print("    row " + i + ":");
                for (int j=0; j < numCols; j++) {
                    System.out.print("  " + data[i][j]);
                }
                System.out.println();
            }
            System.out.println("--------------------------");
        }
    }

    class HeaderRenderer implements TableCellRenderer {
    	/**
    	 * @param table
    	 * @param value
    	 * @param isSelected
    	 * @param hasFocus
    	 * @param row
    	 * @param column
    	 * @return Component
    	 */
    	  public Component getTableCellRendererComponent(JTable table, Object value,
    	      boolean isSelected, boolean hasFocus, int row, int column) {
    	    return (JComponent) value;
    	  }
    }
    
    class PersoCellRenderer extends DefaultTableCellRenderer {
		private static final long serialVersionUID = 6444338034384302880L;
		private Object[] colors = Launcher.options.getCouleurs_pointages();
		/**
		 * 
		 *
		 */
		public PersoCellRenderer() {
		    super();
		  }
    	/**
    	 * @param table
    	 * @param value
    	 * @param isSelected
    	 * @param hasFocus
    	 * @param row
    	 * @param column
    	 * @return Component
    	 */
		  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
		  		boolean hasFocus, int row, int column) {
	        JLabel cell = (JLabel) super.getTableCellRendererComponent(table, value, 
	        		isSelected, hasFocus, row, column);
				Integer toSwitch = Integer.valueOf(value.toString());
	        	switch(toSwitch.intValue()){
					case Presence.WEEKEND:
						cell.setBackground(Color.LIGHT_GRAY);
						cell.setForeground(Color.LIGHT_GRAY);
						break;
					case Presence.NON:
						cell.setBackground((Color) colors[Presence.NON]);
						cell.setForeground((Color)colors[Presence.NON]);
						break;
					case Presence.PRESENT:
						cell.setBackground((Color)colors[Presence.PRESENT]);
						cell.setForeground((Color)colors[Presence.PRESENT]);
						break;
					case Presence.DEDUIT:
						cell.setBackground((Color)colors[Presence.DEDUIT]);
						cell.setForeground((Color)colors[Presence.DEDUIT]);
						break;
					case Presence.REMBOURSER:
						cell.setBackground((Color)colors[Presence.REMBOURSER]);
						cell.setForeground((Color)colors[Presence.REMBOURSER]);
						break;
					case Presence.NON_JUSTIFIE:
						cell.setBackground((Color)colors[Presence.NON_JUSTIFIE]);
						cell.setForeground((Color)colors[Presence.NON_JUSTIFIE]);
						break;
					case Presence.ATTENTE:
						cell.setBackground((Color)colors[Presence.ATTENTE]);
						cell.setForeground((Color)colors[Presence.ATTENTE]);
						break;
					case Presence.IMPREVU:
						cell.setBackground((Color)colors[Presence.IMPREVU]);
						cell.setForeground((Color)colors[Presence.IMPREVU]);
						break;
					case Presence.PREPAYE:
					    cell.setBackground((Color)colors[Presence.PREPAYE]);
						cell.setForeground((Color)colors[Presence.PREPAYE]);
						break;
				}
	            return cell;
		  }
		} 

    class TitleCellRenderer extends DefaultTableCellRenderer {
		private static final long serialVersionUID = 3451430386728843481L;
		/**
		 * 
		 *
		 */
		public TitleCellRenderer() {
		    super();
		  }
    	/**
    	 * @param table
    	 * @param value
    	 * @param isSelected
    	 * @param hasFocus
    	 * @param row
    	 * @param column
    	 * @return Component
    	 */
		  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
		  		boolean hasFocus, int row, int column) {
	        JLabel cell = (JLabel) super.getTableCellRendererComponent(table, value, 
	        		isSelected, hasFocus, row, column);
				cell.setBorder(headerBorder);
				cell.setBackground(couleur);
				cell.setForeground(police_couleur);
	            return cell;
		  }
		} 
 
    //FIXME listener pour sélection multiple
    /*public class SelectionListener implements ListSelectionListener {
        JTable table;
        int firstCol;
        int lastCol;
        int row;
    
        SelectionListener(JTable table) {
            this.table = table;
        }
        
        public void valueChanged(ListSelectionEvent e) {
                if(table.getSelectedRow()!=-1) this.row = table.getSelectedRow();
                this.firstCol = table.getSelectedColumn();
                this.lastCol = table.getColumnModel().getSelectionModel().getMaxSelectionIndex();
                
	            if (!e.getValueIsAdjusting() && this.firstCol!=-1) {
	            	int mini = (this.firstCol<this.lastCol)?this.firstCol:this.lastCol;
	            	int maxi = (this.firstCol<this.lastCol)?this.lastCol:this.firstCol;
	            	for(int i = mini; i <= maxi; i++){
	            		System.err.println("ligne "+row+" colonne "+i);
	            		applyPointages(this.row, i);
	            	}
	            }
        }
    }*/

    /**
     * 
     */
    public class ChangeDateListener implements PropertyChangeListener{
		/**
		 * @param evt
		 */
    	public void propertyChange(PropertyChangeEvent evt) {
			month = mois.getMonth();
			year = annee.getYear();
			drawTable();
		}
    }

}
