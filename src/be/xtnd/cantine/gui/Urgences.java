/*
 * Urgences.java, 14 août 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               Urgences.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://www.x-tnd.be/
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

import be.xtnd.cantine.CantineI18n;
import be.xtnd.cantine.Urgence;
import be.xtnd.commons.GuiCommons;
import be.xtnd.commons.db.Database;
import be.xtnd.commons.db.ResultSetTableModelFactory;
import be.xtnd.commons.gui.EscapeInternalFrame;
import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.gui.TableSorter;
import be.xtnd.commons.i18n.CommonsI18n;
import be.xtnd.validators.MailFieldVerifier;
import be.xtnd.validators.MaskFieldVerifier;

import com.jeta.forms.components.panel.FormPanel;
import com.jeta.forms.components.separator.TitledSeparator;
import com.jgoodies.looks.plastic.PlasticInternalFrameUI;

/**
 * 
 *
 * @author Johan Cwiklinski
 */
public class Urgences {
	public static final String HOSPITALS_LIST_NAME = "liste_hopitaux";
	public static final String DOCTORS_LIST_NAME = "liste_medecins";

	private String type;
	private Urgence urgence;
	private String listQuery;
	private JComboBox parent_cbo;
	boolean modifSelect = false;
	boolean select = false;
	private GuiCommons commons = new GuiCommons();
	private JTable table;
	private ResultSetTableModelFactory factory;
	private Integer idEncours;
	private JPopupMenu popup;


	/**
	 * @param type : medecins ou hopitaux
	 */
	public Urgences(String type){
		this.type = type;
	}
    
	/**
	 * 
	 *
	 */
	public void getList(){
		new Liste();
	}
	
	/**
	 * 
	 * @param box
	 */
	public void getList(JComboBox box){
		this.parent_cbo = box;
		this.select = true;
		new Liste();
	}
	
	/**
	 * 
	 * @param id_urgence
	 */
	public void ModifieUrgence(Integer id_urgence){
		new Fenetre(id_urgence);
	}
	
	/**
	 * 
	 * @param id_urgence
	 * @param parent_cbo
	 */
	public void modifieUrgence(Integer id_urgence, JComboBox parent_cbo){
		this.parent_cbo = parent_cbo;
		this.modifSelect = true;
		ModifieUrgence(id_urgence);
	}
	
	/**
	 * 
	 *
	 */
	public void newUrgence(){
		new Fenetre();
	}
	
	/**
	 * 
	 * @param id_urgence
	 */
	public void deleteUrgence(Integer id_urgence){
		urgence = new Urgence(this.type, id_urgence);
		if(urgence.deleteUrgence(id_urgence)==-2){ //fk fails
			Object[] args = {urgence.getNom(), urgence.fkFamilles(id_urgence)};
			JOptionPane.showMessageDialog(
					MainGui.desktop,
					MessageFormat.format(Messages.getString("window."+this.type+".messages.fk"),args),
					Messages.getString("window."+this.type+".messages.fk.title"),
					JOptionPane.ERROR_MESSAGE
			);
		}
	} 
	
	private class Liste extends EscapeInternalFrame{
		private static final long serialVersionUID = 521830878624355942L;
		private JButton search, nouveau;
		private JTextField nom, ville;
		private TitledSeparator search_title;
		private JLabel nom_label, ville_label;

		/**
		 * 
		 *
		 */
		public Liste(){
			this(new ResultSetTableModelFactory(Launcher.db));

			setIconifiable(false);
			setClosable(true);
			setResizable(true);
			setMaximizable(true);
			setName(Messages.getString("window."+type+".names.list"));
			setSize(500, 350);
			setVisible(true);
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			setLocation(
					MainGui.centerOnDesktop(getSize())
					);
			if(select){MainGui.desktop.add(this, JLayeredPane.POPUP_LAYER);}else{MainGui.desktop.add(this);}	
		}
				
		/**
		 * 
		 * @param f
		 */
		public Liste(ResultSetTableModelFactory f) {
			FormPanel urgences_panel = new FormPanel( "be/xtnd/cantine/gui/descriptions/urgences.jfrm" );
			
			getContentPane().add(urgences_panel);
			
			setTitle(Messages.getString("window."+type+".titles.list"));

			search_title = (TitledSeparator)urgences_panel.getComponentByName("search_title");
			search_title.setText(Messages.getString("window.commons.quick_search"));
			
			search = (JButton)urgences_panel.getButton("search");
			search.setText(Messages.getString("window.commons.perform_search"));
			getRootPane().setDefaultButton(search);
			search.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					displayQueryResults(constructQuery());
					
				}				
			});
			
			nouveau = (JButton)urgences_panel.getButton("new");
			nouveau.setText(CantineI18n.tr("New"));
			nouveau.setToolTipText(Messages.getString("window."+type+".new.tooltip"));
			nouveau.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent e) {
                	if(MainGui.verifUnicite(Messages.getString("window."+type+".names.new"),null)){
            			new Fenetre(nom.getText(), ville.getText());
                	}
                }
            });
			
			nom_label = urgences_panel.getLabel("nom_label");
			nom_label.setText(CantineI18n.tr("Name"));
			
			nom = urgences_panel.getTextField("nom");

			ville_label = urgences_panel.getLabel("ville_label");
			ville_label.setText(CantineI18n.tr("Town"));
			
			ville = urgences_panel.getTextField("ville");
			
			factory = f;
			table = urgences_panel.getTable("liste");
			table.getTableHeader().setReorderingAllowed(false);
			table.addMouseListener(new MouseAdapter(){
				public void mousePressed(MouseEvent e){
			    	if(select && InputEvent.BUTTON1_MASK !=0){
			    		if(e.getClickCount()==2){
			    			urgence = new Urgence(type);
			    			String ty = String.valueOf(table.getValueAt(table.rowAtPoint(e.getPoint()),1));
							parent_cbo.removeAllItems();
							commons.buildCombo(parent_cbo, urgence.listing());
							parent_cbo.setSelectedItem(ty);
							dispose();
			    		}
			    	}
				}
			});
			
			if(type.equalsIgnoreCase(Urgence.MEDECIN_TYPE))
				listQuery = "SELECT id_medecin, nom, cp, ville, tel as telephone FROM "+type+" ORDER BY nom ASC";
			if(type.equalsIgnoreCase(Urgence.HOPITAUX_TYPE))
				listQuery = "SELECT id_hopital, nom, cp, ville, tel as telephone FROM "+type+" ORDER BY nom ASC";
			
			try {
				table.setModel(factory.getResultSetTableModel(listQuery));
			} catch (SQLException e) {e.printStackTrace();}
			
			displayQueryResults(listQuery);
			
			popup = new JPopupMenu();
			popup.setLabel("Label");
			popupEntries(popup, CantineI18n.tr("New"), "new", Launcher.NEW_IMAGE);
			popupEntries(popup, CantineI18n.tr("Edit"), "detailled", Launcher.MODIF_IMAGE);
			popup.addSeparator();
			popupEntries(popup, CantineI18n.tr("Delete"), "del", Launcher.DELETE_IMAGE);

			table.add(popup);
		}

		/**
		 * 
		 * @return String
		 */
		public String constructQuery(){
			StringBuffer query = new StringBuffer();
			query.append(listQuery);
			boolean search = false;
			if(!nom.getText().equals("")){
				query.append(" WHERE LCASE(nom) LIKE '%"+nom.getText().toLowerCase()+"%'");
				search = true;
			}
			if(!ville.getText().equals("")){
				if(search){query.append(" AND ");}else{query.append(" WHERE ");}
				query.append("LCASE(ville) LIKE '%"+ville.getText().toLowerCase()+"%'");
			}
			return query.toString();
		}	
		
		@Override
		protected void popupActions(ActionEvent event){
			String command = event.getActionCommand();
			if(command.equals("new")){
				if(Launcher.verifUnicite(Messages.getString("window."+type+".names.new"),null)){
					newUrgence();
				}
			}else if(command.equals("del")){
				deleteUrgence(idEncours);
				displayQueryResults(listQuery);
			}else if(command.equals("detailled")){
				Object[] args = {idEncours};
				if(Launcher.verifUnicite(
						MessageFormat.format(Messages.getString("window."+type+".names.modif"), args),null
						)){
					ModifieUrgence(idEncours);
				}
			}
		}
	}
		
	private class Fenetre extends EscapeInternalFrame{
		private static final long serialVersionUID = 6020610383515467286L;
		@SuppressWarnings("unused")
		JButton cancelButton;
		private Integer id;
		private JLabel nom_label, adresse_label, cp_label, ville_label, tel_label, fax_label,
			gsm_label, mail_label, commentaires_label;
		private JTextField nom, adresse, cp, ville, mail;
		private JFormattedTextField tel, fax, gsm;
		private MaskFormatter fmPhone;
		private JTextArea commentaires;
		private char mode;
		
		/**
		 * 
		 * @param id
		 */
		public Fenetre(Integer id){
			mode = Database.MODIF;
			this.id = id;
			Object args[] = {id};
			setName(MessageFormat.format(Messages.getString("window."+type+".names.modif"), args));
			urgence = new Urgence(type, id);
			drawWindow();
		}
		
		/**
		 * 
		 *
		 */
		public Fenetre(){
			mode = Database.NEW;
			setName(Messages.getString("window."+type+".names."+mode));
			urgence = new Urgence(type);
			drawWindow();
		}
		
		/**
		 * 
		 * @param nom
		 * @param ville
		 */
		public Fenetre(String nom, String ville){
			mode = Database.NEW;
			urgence = new Urgence(type);
			urgence.setNom(nom);
			urgence.setVille(ville);
			drawWindow();
		}
		
		private void drawWindow(){
	        putClientProperty(
	                PlasticInternalFrameUI.IS_PALETTE,
	                Boolean.TRUE);

			FormPanel pane_medecins = new FormPanel( "be/xtnd/cantine/gui/descriptions/"+type+".jfrm" );
					
			TitledSeparator title = (TitledSeparator)pane_medecins.getComponentByName("title");
			Object args[] = {urgence.getNom()};
			title.setText(MessageFormat.format(Messages.getString("window." + type + ".titles." + ((mode == Database.NEW) ? "new" : "modif")), args));
			
			nom_label = pane_medecins.getLabel("nom_label");
			nom_label.setText(CantineI18n.tr("Name"));
			
			nom = pane_medecins.getTextField("nom");
			nom.setText(urgence.getNom());
			
			adresse_label = pane_medecins.getLabel("adresse_label");
			adresse_label.setText(CantineI18n.tr("Address"));
			
			adresse = pane_medecins.getTextField("adresse");
			adresse.setText(urgence.getAdresse());
			
			cp_label = pane_medecins.getLabel("cp_label");
			cp_label.setText(CantineI18n.tr("Zip code"));
			
			cp = pane_medecins.getTextField("cp");
			cp.setText(urgence.getCp());
			
			ville_label = pane_medecins.getLabel("ville_label");
			ville_label.setText(CantineI18n.tr("Town"));
			
			ville = pane_medecins.getTextField("ville");
			ville.setText(urgence.getVille());

			tel_label = pane_medecins.getLabel("tel_label");
			tel_label.setText(CantineI18n.tr("Phone"));
			
			try {
				fmPhone = new MaskFormatter("## ## ## ## ##");
			} catch (ParseException e) {e.printStackTrace();}

			tel = (JFormattedTextField)pane_medecins.getComponentByName("tel");
			tel.setFormatterFactory(new DefaultFormatterFactory(fmPhone, fmPhone, fmPhone));
			tel.setInputVerifier(new MaskFieldVerifier(urgence.getTel(), fmPhone.getMask()));
			if(!urgence.getTel().trim().equals("")){
				tel.setText(urgence.getTel());
				try {
					tel.commitEdit();
				} catch (ParseException e1) {e1.printStackTrace();}
			}
			
			fax_label = pane_medecins.getLabel("fax_label");
			fax_label.setText(CantineI18n.tr("Fax"));
			
			fax = (JFormattedTextField)pane_medecins.getComponentByName("fax");
			fax.setFormatterFactory(new DefaultFormatterFactory(fmPhone));
			fax.setInputVerifier(new MaskFieldVerifier(urgence.getTel(), fmPhone.getMask()));
			if(!urgence.getFax().trim().equals("")){
				fax.setText(urgence.getFax());
				try {
					fax.commitEdit();
				} catch (ParseException e1) {e1.printStackTrace();}
			}
			
			if(type.equalsIgnoreCase("medecins")){
				gsm_label = pane_medecins.getLabel("gsm_label");
				gsm_label.setText(CantineI18n.tr("Mobile phone"));
				gsm = (JFormattedTextField)pane_medecins.getComponentByName("gsm");
				gsm.setFormatterFactory(new DefaultFormatterFactory(fmPhone));
				gsm.setInputVerifier(new MaskFieldVerifier(urgence.getTel(), fmPhone.getMask()));
				if(!urgence.getTel_portable().trim().equals("")){
					gsm.setText(urgence.getTel_portable());
					try {
						gsm.commitEdit();
					} catch (ParseException e1) {e1.printStackTrace();}
				}
			}
			
			mail_label = pane_medecins.getLabel("mail_label");
			mail_label.setText(CantineI18n.tr("E-Mail"));
			
			mail = pane_medecins.getTextField("mail");
			mail.setText(urgence.getMail());
			mail.setInputVerifier(new MailFieldVerifier(urgence.getMail()));
			
			commentaires_label = pane_medecins.getLabel("commentaires_label");
			commentaires_label.setText(CantineI18n.tr("Comments"));
			
			commentaires = (JTextArea)pane_medecins.getComponentByName("commentaires");
			commentaires.setText(urgence.getCommentaires());
			
			getContentPane().add(pane_medecins);
			
			commons.createButtonBar(this,new OkayEvent());
			
			setMinimumSize(new Dimension(420,200));
			pack();
			Integer windowPos = (select)?JLayeredPane.POPUP_LAYER:JLayeredPane.MODAL_LAYER;
			MainGui.desktop.add(this, windowPos);
			setTitle(Messages.getString((mode == Database.NEW)?"window."+type+".titles.new":"window."+type+".titles.modif"));
			setLocation(MainGui.centerOnDesktop(getMinimumSize()));
			setResizable(true);
			setClosable(true);
			setIconifiable(true);
			setMaximizable(true);
			setVisible(true);			
		}

		private boolean verifEntrees(){
			boolean valid = true;
			StringBuffer required = new StringBuffer();
			required.append(CommonsI18n.tr("The following are required:"));

			if(nom.getText().trim().equals("")){
				required.append("\n- "+Messages.getString("window."+type+".required.name"));
				nom.requestFocus();
				valid = false;
			}
			if(ville.getText().trim().equals("")){
				required.append("\n- "+Messages.getString("window."+type+".required.town"));
				if(valid) ville.requestFocus();
				valid = false;
			}
			if(!valid){
				JOptionPane.showMessageDialog(
						MainGui.desktop, 
						required.toString(),
						CommonsI18n.tr("Error!"),
						JOptionPane.ERROR_MESSAGE);			
			}
			return valid;
		}
		
		class OkayEvent implements ActionListener{
			/**
			 * @param e
			 */
			public void actionPerformed(ActionEvent e) {
				if(verifEntrees()){
					urgence.setNom(nom.getText());
					urgence.setAdresse(adresse.getText());
					urgence.setCp(cp.getText());
					urgence.setVille(ville.getText());
					urgence.setTel(tel.getText());
					urgence.setFax(fax.getText());
					if(type==Urgence.MEDECIN_TYPE) urgence.setTel_portable(gsm.getText());
					urgence.setMail(mail.getText());
					urgence.setCommentaires(commentaires.getText());
					if(mode == Database.NEW) urgence.storeUrgence();
					if(mode == Database.MODIF) urgence.modifyUrgence(id);
					if(!modifSelect){displayQueryResults(listQuery);}else{
						parent_cbo.removeAllItems();
						commons.buildCombo(parent_cbo, urgence.listing());
						parent_cbo.setSelectedItem(urgence.getNom());
					}
					dispose();
				}
			}		
		}
	}

	/**
	 * 
	 * @param q
	 */
	public void displayQueryResults(final String q) {
		MainGui.statusBar.setText("Contacting database...");

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					table.setModel(factory.getResultSetTableModel(q));
					
			        TableSorter sorter = new TableSorter(table.getModel());
			        table.setModel(sorter);
			        sorter.setTableHeader(table.getTableHeader());
			        table.getTableHeader().setToolTipText(CommonsI18n.tr("Click to sort on a column, Ctrl+Click to sort on extra column"));

			        TableColumnModel columnModel = table.getColumnModel();
					columnModel.getColumn(0).setPreferredWidth(0);
					columnModel.getColumn(0).setMinWidth(0);
					columnModel.getColumn(0).setMaxWidth(0);
					columnModel.getColumn(2).setPreferredWidth(50);
					columnModel.getColumn(2).setMinWidth(50);
					columnModel.getColumn(2).setMaxWidth(50);
					columnModel.getColumn(3).setPreferredWidth(80);
					columnModel.getColumn(3).setMinWidth(80);
					columnModel.getColumn(3).setMaxWidth(130);
					columnModel.getColumn(4).setPreferredWidth(05);
					columnModel.getColumn(4).setMinWidth(85);
					columnModel.getColumn(4).setMaxWidth(85);
					columnModel.getColumn(4).setCellRenderer(new PhoneRenderer());
					
					table.addMouseListener(new MouseAdapter(){
						public void mousePressed(MouseEvent e){
					  	  	int nModifier = e.getModifiers();
							idEncours = (Integer)table.getValueAt(table.rowAtPoint(e.getPoint()),0);
					    	if ((nModifier & InputEvent.BUTTON3_MASK) != 0) {
								table.changeSelection(table.rowAtPoint(e.getPoint()),0,false,false);
								popup.show((Component)e.getSource(),e.getX(),e.getY());
					      	}
					    	if(e.getClickCount()==2 && !select){//double click
								Object[] args = {idEncours};
								if(Launcher.verifUnicite(
										MessageFormat.format(Messages.getString("window."+type+".names.modif"), args),null
										)){
									ModifieUrgence(idEncours);
								}
					    	}
						}
					});
					MainGui.verifUnicite(Messages.getString("window."+type+".names.list"), null);
					MainGui.statusBar.setText("");
				} catch (SQLException ex) {
					MainGui.statusBar.setText("Error contacting database !");
					JOptionPane.showMessageDialog(MainGui.desktop,
							new String[] { // Display a 2-line message
							ex.getClass().getName() + ": ", ex.getMessage() });							
					MainGui.statusBar.setText("");		
				}
			}
		});
	}
	
	class PhoneRenderer extends DefaultTableCellRenderer {
		private static final long serialVersionUID = -7825766899386439400L;
		/**
		 * 
		 *
		 */
		public PhoneRenderer() {
		    super();
		  }
		/**
		 * @param table
		 * @param value
		 * @param isSelected
		 * @param hasFocus
		 * @param row
		 * @param column
		 * @return Component
		 */
		  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
		  		boolean hasFocus, int row, int column) {
	        JLabel cell = (JLabel) super.getTableCellRendererComponent(table, value, 
	        		isSelected, hasFocus, row, column);
	        
	        MaskFormatter mf;
			try {
				mf = new javax.swing.text.MaskFormatter("## ## ## ## ##");
				mf.setValueContainsLiteralCharacters(false);		        
				cell.setText(mf.valueToString(value));
			} catch (ParseException e) {e.printStackTrace();}
			return cell;
		  }
	} 
}
