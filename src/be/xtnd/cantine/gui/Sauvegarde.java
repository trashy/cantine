/*
 * Sauvegarde.java, 2006-04-05
 * 
 * This file is part of cantine.
 *
 * Copyright © 2006-2010 Johan Cwiklinski
 *
 * File :               Sauvegarde.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://cantine.ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.apache.log4j.Logger;

import be.xtnd.cantine.ApplicationProperties;
import be.xtnd.cantine.CantineI18n;
import be.xtnd.commons.GuiCommons;
import be.xtnd.commons.db.Db2Sql;
import be.xtnd.commons.db.hsql.Hsql2Sql;
import be.xtnd.commons.db.mysql.Mysql2Sql;
import be.xtnd.commons.gui.EscapeInternalFrame;
import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.utils.SQLFilter;
import be.xtnd.commons.utils.UtilsChooser;
import be.xtnd.commons.utils.XMLFilter;

import com.jeta.forms.components.border.TitledBorderLabel;
import com.jeta.forms.components.label.JETALabel;
import com.jeta.forms.components.panel.FormPanel;
import com.jgoodies.looks.plastic.PlasticInternalFrameUI;

/**
 * Database backup and restoration
 * @author Johan Cwiklinski
 * @since 2006-04-05
 */
public class Sauvegarde extends EscapeInternalFrame {
	private static final long serialVersionUID = 2797459349847025820L;
	GuiCommons commons = new GuiCommons();
	private Sauvegarde window;
	private JRadioButton mode_xml, mode_sql, mode_save, mode_restore;
	private JTextField save_path;
	private JCheckBox structure, datas;
	private static Logger logger = Logger.getLogger(Sauvegarde.class.getName());
	/** Window name */
	public static final String NAME = "backups";

	/**
	 * Default constructor
	 */
	public Sauvegarde(){
		super(CantineI18n.tr("Database backup"), false, true, false, false);
		super.setName(NAME);

        putClientProperty(
                PlasticInternalFrameUI.IS_PALETTE,
                Boolean.TRUE);

        window = this;
		add(initWindow());
		
		pack();
		setMinimumSize(new Dimension(350,getHeight()));
		setSize(new Dimension(350,getHeight()));
		setLocation(
				MainGui.centerOnDesktop(this.getSize())
				);
		setVisible(true);
		MainGui.desktop.add(this);
	}
	
	/**
	 * Build main window
	 * @return JPanel
	 */
	private JPanel initWindow(){
		FormPanel pane = new FormPanel("be/xtnd/cantine/gui/descriptions/sauvegarde.jfrm");
		
		JETALabel title = (JETALabel)pane.getComponentByName("save_title");
		title.setText(CantineI18n.tr("Database backup"));
		title.setIcon(new ImageIcon(ClassLoader.getSystemResource(ApplicationProperties.getString("menus.file.save.icon"))));
		
		JLabel mode_label = pane.getLabel("mode_label");
		mode_label.setText(CantineI18n.tr("Mode:"));
			
		mode_save = pane.getRadioButton("mode_save");
		mode_save.setText(CantineI18n.tr("backup"));
		mode_save.setSelected(true);
		
		mode_restore = pane.getRadioButton("mode_restore");
		mode_restore.setText(CantineI18n.tr("restore"));
		
		ButtonGroup mode_group = new ButtonGroup();
		mode_group.add(mode_save);
		mode_group.add(mode_restore);
		
		TitledBorderLabel path_title = (TitledBorderLabel)pane.getComponentByName("path_title");
		path_title.setText(CantineI18n.tr("Path"));
		
		JLabel path_label = pane.getLabel("path_label");
		path_label.setText(CantineI18n.tr("Give backup file path"));
		
		save_path = pane.getTextField("save_path");
		
		JButton browse = (JButton)pane.getButton("browse");
		browse.setIcon(new ImageIcon(ClassLoader.getSystemResource("be/xtnd/icons/select.png")));
		browse.setToolTipText(CantineI18n.tr("Display file selector"));
		browse.addActionListener(new BrowseListener());
		
		TitledBorderLabel object_title = (TitledBorderLabel)pane.getComponentByName("object_title");
		object_title.setText(CantineI18n.tr("Objects to proceed (backup only)"));
		
		structure = pane.getCheckBox("structure");
		structure.setText(CantineI18n.tr("Database structure (tables)"));
		
		datas = pane.getCheckBox("datas");
		datas.setText(CantineI18n.tr("Database datas"));
		
		TitledBorderLabel mode_title = (TitledBorderLabel)pane.getComponentByName("mode_title");
		mode_title.setText(CantineI18n.tr("File mode"));
		
		mode_sql = pane.getRadioButton("mode_sql");
		mode_sql.setText(CantineI18n.tr("SQL files (*.sql)"));
		mode_sql.setSelected(true);
		
		mode_xml = pane.getRadioButton("mode_xml");
		mode_xml.setText(CantineI18n.tr("XML files (*.xml)"));
		
		ButtonGroup export_group = new ButtonGroup();
		export_group.add(mode_sql);
		export_group.add(mode_xml);
		
		commons.createButtonBar(this, new OkayListener());

		return pane;
	}

	/**
	 * File browser
	 * @author trasher
	 * @since 2006-04-05
	 */
	class BrowseListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			JFileChooser fc = new JFileChooser();
			File f = null;
			try {
				f = new File(new File(System.getProperty("user.dir")).getCanonicalPath());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			fc.setCurrentDirectory(f);
		    fc.addChoosableFileFilter((mode_xml.isSelected())?new XMLFilter():new SQLFilter());
		    int returnVal = (mode_save.isSelected())?fc.showSaveDialog(window):fc.showOpenDialog(window);
		    if(returnVal == JFileChooser.APPROVE_OPTION){
				StringBuffer path = new StringBuffer();
				path.append(fc.getCurrentDirectory());
				path.append(System.getProperty("file.separator"));
				path.append(UtilsChooser.removeExtension(fc.getSelectedFile()));
				path.append(".");
				path.append((mode_xml.isSelected())?Mysql2Sql.XML_MODE:Mysql2Sql.SQL_MODE);
				save_path.setText(path.toString());
		    }
		}		
	}
	
	/**
	 * Action on okay
	 * @author trasher
	 * @since 2006-04-05
	 */
	class OkayListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if(save_path.getText().trim().equals("")){
				JOptionPane.showMessageDialog(MainGui.desktop,
						CantineI18n.tr("No file has been selected.\nYou have to select a file to perform a backup or a restore"),
						CantineI18n.tr("Please select a file"),
						JOptionPane.INFORMATION_MESSAGE);
				logger.fatal("No file selected");
			}else{
				if(mode_save.isSelected()){
					boolean verif = true;
					boolean exists = (new File(save_path.getText())).exists();
					if(exists){
						Object[] args = {save_path.getText()};
						int response = JOptionPane.showConfirmDialog(
								MainGui.desktop, 
								CantineI18n.tr("Specified file, \"{0}\" already exist. Do you want to replace it?", args),
								CantineI18n.tr("File already exist"),
								JOptionPane.YES_NO_OPTION,
								JOptionPane.WARNING_MESSAGE);
			    		if (response != JOptionPane.YES_OPTION) {
			    			verif = false;
			    		}
					}
					if(verif){
						boolean struct = structure.isSelected();
						boolean donnees = datas.isSelected();
						Db2Sql database;
						database = (Launcher.conf.getProvider_name().equalsIgnoreCase("hsqldb"))?
								new Hsql2Sql(Launcher.db, struct, donnees):
									new Mysql2Sql(Launcher.db, struct, donnees);
	
						String dump = database.dump((mode_xml.isSelected())?Db2Sql.XML_MODE:Db2Sql.SQL_MODE);
				        Object[] args = {save_path.getText()};
					    try {
					        BufferedWriter out = new BufferedWriter(new FileWriter(save_path.getText()));
					        out.write(dump);
					        out.close();
					        JOptionPane.showMessageDialog(
					        		MainGui.desktop,
					        		CantineI18n.tr("Backup has successfully been perofmed in file \"{0}\"", args), 
					        		CantineI18n.tr("Backup"),
					        		JOptionPane.INFORMATION_MESSAGE+JOptionPane.OK_OPTION);
					        logger.info("File "+save_path.getText()+" successfully writed");
					    } catch (IOException e2) {
					        JOptionPane.showMessageDialog(
					        		MainGui.desktop,
					        		CantineI18n.tr("Backup to file \"{O}\" has failed", args),
					        		CantineI18n.tr("Backup"),
					        		JOptionPane.ERROR_MESSAGE+JOptionPane.OK_OPTION);
					        logger.debug("IOException while proceeding backup : cause was " + e2.getCause() + " message was " + e2.getMessage());
					    	e2.printStackTrace();
					    }
					}
				}else if(mode_restore.isSelected()){
					//TODO restauration données
				}
			}
		}
	}

}
