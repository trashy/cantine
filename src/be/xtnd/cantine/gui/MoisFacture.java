/*
 * MoisFacture.java, 6 oct. 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               	MoisFacture.java
 * Author's email :     	johan@x-tnd.be
 * Author's Website :   	http://cantine.sourceforge.net
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import be.xtnd.cantine.CantineI18n;
import be.xtnd.cantine.Facture;
import be.xtnd.cantine.Famille;
import be.xtnd.commons.GuiCommons;
import be.xtnd.commons.Print;
import be.xtnd.commons.db.Database;
import be.xtnd.commons.gui.EscapeInternalFrame;
import be.xtnd.commons.gui.MainGui;

import com.jeta.forms.components.border.TitledBorderLabel;
import com.jeta.forms.components.panel.FormPanel;
import com.jeta.forms.components.separator.TitledSeparator;
import com.jgoodies.looks.plastic.PlasticInternalFrameUI;
import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JMonthChooser;
import com.toedter.calendar.JYearChooser;
import com.toedter.components.JSpinField;

/**
 * 
 * 
 * @author Johan Cwiklinski
 *
 */
public class MoisFacture extends EscapeInternalFrame {
	private static final long serialVersionUID = -652970071260715195L;
	private TitledSeparator title;
	private TitledBorderLabel criteres_title;
	private JLabel mode_label, first_date_label, second_date_label, nom_label, num_facture_label, 
		reglement_label;
	private JMonthChooser mois;
    private JYearChooser annee;
    private JDateChooser first_date, second_date;
    private JRadioButton factures, liste, indifferent, regle, non_regle;
    private JTextField nom;
    private JSpinField num_facture;
    private GuiCommons commons = new GuiCommons();
    private Database db;
    /** Nom de la fenêtre */
    public static final String NAME = "mois_facture";
    
    /**
     * 
     *
     */
    public MoisFacture(){
        super();
        super.setName(NAME);
        putClientProperty(
                PlasticInternalFrameUI.IS_PALETTE,
                Boolean.TRUE);

		FormPanel pane = new FormPanel( "be/xtnd/cantine/gui/descriptions/select_mois_facture.jfrm" );
		
		title = (TitledSeparator)pane.getComponentByName("title");
		title.setText(Messages.getString("window.invoices.titles.print"));
		
		mode_label = pane.getLabel("mode_label");
		mode_label.setText(Messages.getString("window.invoices.labels.mode"));
		
		factures = pane.getRadioButton("factures");
		factures.setText(Messages.getString("window.invoices.labels.individual"));
		
		liste = pane.getRadioButton("liste");
		liste.setText(Messages.getString("window.invoices.labels.list"));
		
		ButtonGroup group = new ButtonGroup();
		group.add(factures);
		group.add(liste);

		mois = (JMonthChooser)pane.getComponentByName("mois");
		Calendar cal = Calendar.getInstance();
		mois.setMonth(cal.get(Calendar.MONTH));
		mois.setYearChooser(annee);
		
		annee = (JYearChooser)pane.getComponentByName("annee");
		annee.setMinimum(2000);
		annee.setMaximum(2500);

		criteres_title = (TitledBorderLabel)pane.getComponentByName("criteres_title");
		criteres_title.setText(Messages.getString("window.invoices.labels.criterias"));
		
		nom_label = pane.getLabel("nom_label");
		nom_label.setText(CantineI18n.tr("Name"));
		
		nom = pane.getTextField("nom");
		
		num_facture_label = pane.getLabel("num_facture_label");
		num_facture_label.setText(Messages.getString("window.invoices.labels.num_invoice"));
		
		num_facture = (JSpinField) pane.getComponentByName("num_facture");
		
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		cal.add(Calendar.DAY_OF_MONTH, -1);
		
		second_date_label = pane.getLabel("second_date_label");
		second_date_label.setText(Messages.getString("window.invoices.labels.second_date"));
		
		second_date = (JDateChooser)pane.getComponentByName("second_date");
		second_date.setDate(cal.getTime());
		
		cal.add(Calendar.DAY_OF_MONTH, -1);
		
		first_date_label = pane.getLabel("first_date_label");
		first_date_label.setText(Messages.getString("window.invoices.labels.first_date"));
		
		first_date = (JDateChooser)pane.getComponentByName("first_date");
		first_date.setDate(cal.getTime());
		
		reglement_label = pane.getLabel("reglement_label");
		reglement_label.setText(Messages.getString("window.invoices.labels.settlement"));
		
		indifferent = pane.getRadioButton("indifferent");
		indifferent.setText(Messages.getString("window.invoices.labels.indifferent"));
		
		regle = pane.getRadioButton("regle");
		regle.setText(Messages.getString("window.invoices.labels.settled"));

		non_regle = pane.getRadioButton("non_regle");
		non_regle.setText(Messages.getString("window.invoices.labels.not_settled"));

		ButtonGroup paiement = new ButtonGroup();
		paiement.add(indifferent);
		paiement.add(regle);
		paiement.add(non_regle);
	
		add(pane);
		commons.createButtonBar(this, new OkayEvent());
		
		pack();
		setLocation(
				MainGui.centerOnDesktop(this.getSize())
				);
		setMinimumSize(getSize());
		setVisible(true);
		setClosable(true);
		setIconifiable(true);
		setResizable(true);
		MainGui.desktop.add(this, JLayeredPane.POPUP_LAYER);   
		toFront();
		try {
			setSelected(true);
		} catch (PropertyVetoException e) {}
    }
    
    private class OkayEvent implements ActionListener{
    	/**
    	 * @param e
    	 */
        public void actionPerformed(ActionEvent e) {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            int month = mois.getMonth();
            int year = annee.getYear();
            Calendar cal = new GregorianCalendar(year, month, 1);
            String startDate = df.format(cal.getTime());
            cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
            String endDate = df.format(cal.getTime());
			db =  Launcher.db;
		    StringBuffer qry = new StringBuffer();
		    qry.append("SELECT id_facture, "+Famille.TABLE+"."+Famille.PK+", nom, adresse, cp, ville, date, ht, acompte, deduire, (ht-acompte-deduire) As net, regle FROM "+Facture.TABLE+
			    	" INNER JOIN "+Famille.TABLE+" ON("+Facture.TABLE+"."+Famille.PK+"="+Famille.TABLE+"."+Famille.PK+")"+
			    	" WHERE date BETWEEN '"+startDate+"' AND '"+endDate+"'");
		    if(!nom.getText().trim().equals(""))
		    	qry.append(" AND LCASE(nom) LIKE '"+nom.getText().toLowerCase()+"'");
		    if(num_facture.getValue()!=-1)
		    	qry.append(" AND id_facture="+num_facture.getValue());
		    
		    String report = (factures.isSelected())?"facture.jrxml":"liste_factures.jrxml";
		    
		    if(!indifferent.isSelected()){
		    	boolean paid = (regle.isSelected())?true:(non_regle.isSelected())?false:false;
		    	qry.append(" AND regle="+paid);
		    }
		    
		    HashMap<Object, Object> params = new HashMap<Object, Object>();
		    cal.add(Calendar.MONTH, 1);
		    if(factures.isSelected()){
			    params.put("MOIS_FACTURE", cal.getTime());
			    params.put("FIRST_DATE", first_date.getDate());
			    params.put("SECOND_DATE", second_date.getDate());		    	
		    }
		    if(liste.isSelected()){
		    	StringBuffer criterias = new StringBuffer();
		    	if(!nom.getText().trim().equals(""))
		    		criterias.append(" nom contient '"+nom.getText()+"'");
		    	if(num_facture.getValue()!=-1)
		    		criterias.append(" numéro de facture est "+num_facture.getValue());
		    	if(!indifferent.isSelected()){
		    		if(regle.isSelected())
		    			criterias.append(" reglées");
		    		if(non_regle.isSelected())
		    			criterias.append(" non réglées");
		    	}
		    	if(criterias.length()==0){
		    		criterias.append(" liste complète");
		    	}
		    		
		    	params.put("CRITERES_RECHERCHE", criterias.toString());
		    }else{
		    	String signQry = "SELECT signataire FROM opt_mairie";
		    	try {
					ResultSet rsSign = db.execQuery(signQry);
					while(rsSign.next())
						params.put("SIGNATAIRE", rsSign.getString("signataire"));
				} catch (SQLException e1) {e1.printStackTrace();}
		    }
		    params.put("HEADERS_PATH", ".config/");
		    params.put("REPORT_CONNECTION", db.getConnection());
		    try {
                ResultSet rs = db.execQuery(qry.toString());
                rs.last();
                if(rs.getRow()>0){
                	rs.beforeFirst();
                	new Print(".config", report, rs, params);
                }else{
					JOptionPane.showMessageDialog(
							MainGui.desktop,
							Messages.getString("window.print.nopages"),
							Messages.getString("window.print.nopages.title"),
							JOptionPane.INFORMATION_MESSAGE
					);
                }
            } catch (SQLException e1) {e1.printStackTrace();}            
        }
    }
}
