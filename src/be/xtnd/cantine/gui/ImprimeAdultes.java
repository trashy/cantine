/*
 * ImprimeAdultes.java, 12 oct. 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               	ImprimeAdultes.java
 * Author's email :     	johan@x-tnd.be
 * Author's Website :   	http://cantine.sourceforge.net
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.HashMap;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

import be.xtnd.cantine.CantineI18n;
import be.xtnd.cantine.Eleve;
import be.xtnd.cantine.Famille;
import be.xtnd.commons.GuiCommons;
import be.xtnd.commons.Print;
import be.xtnd.commons.db.Database;
import be.xtnd.commons.gui.EscapeInternalFrame;
import be.xtnd.commons.gui.MainGui;

import com.jeta.forms.components.panel.FormPanel;
import com.jeta.forms.components.separator.TitledSeparator;
import com.jgoodies.looks.plastic.PlasticInternalFrameUI;

/**
 *
 *
 * @author Johan Cwiklinski
 */
public class ImprimeAdultes extends EscapeInternalFrame {
	private static final long serialVersionUID = 4377881825232211648L;
	private JTextField nom, adresse, ville;
	private JLabel nom_label, adresse_label, code_postal_label, ville_label, joker;
	private JFormattedTextField cp;
	private GuiCommons commons = new GuiCommons();
	private MaskFormatter fmCodePostal;
	private Database db;
	/** Nom de la fenêtre */
	public static final String NAME = "edition_adultes";

	/**
	 * 
	 *
	 */
	public ImprimeAdultes(){
		super(CantineI18n.tr("Printing"), true, true, true, true);
		super.setName(NAME);

        putClientProperty(
                PlasticInternalFrameUI.IS_PALETTE,
                Boolean.TRUE);
		
    	add(new JScrollPane(initMenu()));
    	
    	commons.createButtonBar(this, new OkayEvent());

    	pack();
    	setLocation(
    			MainGui.centerOnDesktop(this.getSize())
    			);
    	setMinimumSize(getSize());
    	setVisible(true);
    	setClosable(true);
    	setIconifiable(true);
    	setResizable(true);
    	MainGui.desktop.add(this);
	}
	
	private JPanel initMenu(){
        FormPanel pane = new FormPanel( "be/xtnd/cantine/gui/descriptions/edition_adultes.jfrm" );

        TitledSeparator title = (TitledSeparator)pane.getComponentByName("title");
        title.setText(Messages.getString("window.print.adults.title"));
        
        joker = pane.getLabel("joker");
        joker.setText(Messages.getString("window.print.joker"));
        
        nom_label = pane.getLabel("nom_label");
        nom_label.setText(CantineI18n.tr("Name"));
        
        nom = pane.getTextField("nom");
        
        adresse_label = pane.getLabel("adresse_label");
        adresse_label.setText(CantineI18n.tr("Address"));
        
        adresse = pane.getTextField("adresse");
        
        code_postal_label = pane.getLabel("code_postal_label");
        code_postal_label.setText(CantineI18n.tr("Zip code"));
        
		try {fmCodePostal = new MaskFormatter("#####");} catch (ParseException e) {e.printStackTrace();}
        cp = (JFormattedTextField) pane.getComponentByName("code_postal");
		cp.setFormatterFactory(new DefaultFormatterFactory(fmCodePostal));
        
		ville_label = pane.getLabel("ville_label");
		ville_label.setText(CantineI18n.tr("Town"));
		
        ville = pane.getTextField("ville");
        
        return pane;
	}

	class OkayEvent implements ActionListener {
		/***
		 * @param e
		 */
		public void actionPerformed(ActionEvent e) {
			StringBuffer qry = new StringBuffer();
			qry.append("SELECT * FROM "+Famille.TABLE+" LEFT JOIN "+Eleve.TABLE+
					" ON("+Famille.TABLE+"."+Famille.PK+"="+Eleve.TABLE+"."+Famille.PK+") WHERE adulte=true");
			if(!nom.getText().trim().equals(""))
				qry.append(" AND LCASE(nom) LIKE '"+nom.getText().toLowerCase()+"'");
			if(!adresse.getText().trim().equals(""))
				qry.append(" AND LCASE(adresse) LIKE '"+adresse.getText().toLowerCase()+"'");
			if(!cp.getText().trim().equals(""))
				qry.append(" AND cp LIKE '"+cp.getText()+"'");
			if(!ville.getText().trim().equals(""))
				qry.append(" AND LCASE(ville) LIKE '"+ville.getText().toLowerCase()+"'");
			//ordre de tri
			qry.append(" ORDER by "+Famille.TABLE+".nom ASC");

			db = Launcher.db;
	    	
		    HashMap<Object, Object> params = new HashMap<Object, Object>();
		    params.put("HEADERS_PATH", ".config/");
		    params.put("REPORT_CONNECTION", db.getConnection());
		    try {
                ResultSet rs = db.execQuery(qry.toString());
                rs.last();
                if(rs.getRow()>0){
                	rs.beforeFirst();
                	new Print(".config", "liste_adultes.jrxml", rs, params);
                }else{
					JOptionPane.showMessageDialog(
							MainGui.desktop,
							Messages.getString("window.print.nopages"),
							Messages.getString("window.print.nopages.title"),
							JOptionPane.INFORMATION_MESSAGE
					);
                }
            } catch (SQLException e1) {e1.printStackTrace();}            
		}
	}
}
