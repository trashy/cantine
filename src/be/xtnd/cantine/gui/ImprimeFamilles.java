/*
 * ImprimeFamilles.java, 12 oct. 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               ImprimeFamilles.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://cantine.sourceforge.net
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

import be.xtnd.cantine.CantineI18n;
import be.xtnd.cantine.Classe;
import be.xtnd.cantine.Eleve;
import be.xtnd.cantine.Famille;
import be.xtnd.cantine.Urgence;
import be.xtnd.commons.GuiCommons;
import be.xtnd.commons.Print;
import be.xtnd.commons.db.Database;
import be.xtnd.commons.gui.EscapeInternalFrame;
import be.xtnd.commons.gui.MainGui;

import com.jeta.forms.components.border.TitledBorderLabel;
import com.jeta.forms.components.panel.FormPanel;
import com.jeta.forms.components.separator.TitledSeparator;
import com.jgoodies.looks.plastic.PlasticInternalFrameUI;
import com.toedter.calendar.JDateChooser;

/**
 *
 *
 * @author Johan Cwiklinski
 */
public class ImprimeFamilles extends EscapeInternalFrame{
	private static final long serialVersionUID = -7868598090394642009L;
	private JTextField nom, adresse, ville, nom_enfant;
	private JFormattedTextField cp;
	private JDateChooser date_debut, date_fin;
	private JComboBox classe;
	private JCheckBox cantine/*, centre*/;
	private TitledSeparator title;
	private JLabel joker, nom_label, adresse_label, code_postal_label, ville_label,
		nom_enfant_label, date_debut_label, date_fin_label, classe_label, reservation_label;
	private TitledBorderLabel enfants_title;
	private GuiCommons commons = new GuiCommons();
	private MaskFormatter fmCodePostal;
	private Database db;
	private Date currentDate = Calendar.getInstance().getTime();
	/** Nom de la fenêtre */
	public static final String NAME = "edition_familles";

	/**
	 * 
	 *
	 */
	public ImprimeFamilles(){
		super(CantineI18n.tr("Printing"), true, true, true, true);
		super.setName(NAME);

        putClientProperty(
                PlasticInternalFrameUI.IS_PALETTE,
                Boolean.TRUE);
		
    	add(new JScrollPane(initMenu()));
    	
    	commons.createButtonBar(this, new OkayEvent());

    	pack();
    	setLocation(
    			MainGui.centerOnDesktop(this.getSize())
    			);
    	setMinimumSize(getSize());
    	setVisible(true);
    	setClosable(true);
    	setIconifiable(true);
    	setResizable(true);
    	MainGui.desktop.add(this);
	}
	
	private JPanel initMenu(){
        FormPanel pane = new FormPanel( "be/xtnd/cantine/gui/descriptions/edition_familles.jfrm" );
        
        title = (TitledSeparator)pane.getComponentByName("title");
        title.setText(Messages.getString("window.print.families.title"));
        
        joker = pane.getLabel("joker");
        joker.setText(Messages.getString("window.print.joker"));
        
        nom_label = pane.getLabel("nom_label");
        nom_label.setText(Messages.getString("window.print.families.name"));
        
        nom = pane.getTextField("nom");
        
        adresse_label = pane.getLabel("adresse_label");
        adresse_label.setText(CantineI18n.tr("Address"));
        
        adresse = pane.getTextField("adresse");
        
        code_postal_label = pane.getLabel("code_postal_label");
        code_postal_label.setText(CantineI18n.tr("Zip code"));
        
		try {fmCodePostal = new MaskFormatter("#####");} catch (ParseException e) {e.printStackTrace();}
        cp = (JFormattedTextField) pane.getComponentByName("code_postal");
		cp.setFormatterFactory(new DefaultFormatterFactory(fmCodePostal));
        
		ville_label = pane.getLabel("ville_label");
		ville_label.setText(CantineI18n.tr("Town"));
		
        ville = pane.getTextField("ville");
        
        enfants_title = (TitledBorderLabel)pane.getComponentByName("enfants_title");
        enfants_title.setText(Messages.getString("window.print.families.childstitle"));
        
        nom_enfant_label = pane.getLabel("nom_enfant_label");
        nom_enfant_label.setText(Messages.getString("window.print.families.childname"));
        
        nom_enfant = pane.getTextField("nom_enfant");
        
        date_debut_label = pane.getLabel("date_debut_label");
        date_debut_label.setText(Messages.getString("window.print.families.begindate"));
        
        date_debut = (JDateChooser) pane.getComponentByName("date_debut");
        date_debut.setDate(currentDate);
        
        date_fin_label = pane.getLabel("date_fin_label");
        date_fin_label.setText(Messages.getString("window.print.families.enddate"));
        
        date_fin = (JDateChooser) pane.getComponentByName("date_fin");
        date_fin.setDate(currentDate);
        
        classe_label = pane.getLabel("classe_label");
        classe_label.setText(Messages.getString("window.print.families.classe"));
        
        classe = pane.getComboBox("classe");
        commons.buildCombo(classe, Classe.listing());
        
        reservation_label = pane.getLabel("reservation_label");
        reservation_label.setText(Messages.getString("window.print.families.reservation"));
        
        cantine = pane.getCheckBox("cantine");
        cantine.setText(Messages.getString("window.print.families.cantine"));
        
        /*centre = pane.getCheckBox("centre");
        centre.setText(Messages.getString("window.print.families.centre"));*/
        
        return pane;
	}

	class OkayEvent implements ActionListener {
		/**
		 * @param e
		 */
		public void actionPerformed(ActionEvent e) {
			StringBuffer qry = new StringBuffer();
			qry.append("SELECT DISTINCT("+Famille.TABLE+"."+Famille.PK+"), "+Famille.TABLE+".nom, "+Famille.TABLE+".adresse, "+Famille.TABLE+".cp, "+Famille.TABLE+".ville, "+Famille.TABLE+".telephone, "+Urgence.MEDECINS_TABLE+".nom AS nommedecin, "+Urgence.HOPITAUX_TABLE+".nom AS nomhopital FROM "+Famille.TABLE+" LEFT JOIN "+Eleve.TABLE+
					" ON("+Famille.TABLE+"."+Famille.PK+"="+Eleve.TABLE+"."+Famille.PK+")" +
					" LEFT JOIN "+Urgence.MEDECINS_TABLE+" ON ("+Famille.TABLE+"."+Urgence.MEDECINS_PK+"="+Urgence.MEDECINS_TABLE+"."+Urgence.MEDECINS_PK+")"+
					" LEFT JOIN "+Urgence.HOPITAUX_TABLE+" ON ("+Famille.TABLE+"."+Urgence.HOPITAUX_PK+"="+Urgence.HOPITAUX_TABLE+"."+Urgence.HOPITAUX_PK+")"+
					" WHERE adulte=false");
			if(!nom.getText().trim().equals(""))
				qry.append(" AND LCASE(nom) LIKE '"+nom.getText().toLowerCase()+"'");
			if(!adresse.getText().trim().equals(""))
				qry.append(" AND LCASE(adresse) LIKE '"+adresse.getText().toLowerCase()+"'");
			if(!cp.getText().trim().equals(""))
				qry.append(" AND cp LIKE '"+cp.getText()+"'");
			if(!ville.getText().trim().equals(""))
				qry.append(" AND LCASE(ville) LIKE '"+ville.getText().toLowerCase()+"'");
			if(!nom_enfant.getText().trim().equals(""))
				qry.append(" AND LCASE(eleves.nom) LIKE '"+nom_enfant.getText().toLowerCase()+"'");
			if(date_debut.getDate().before(currentDate) && date_fin.getDate().before(currentDate)
					&& date_debut.getDate().before(date_fin.getDate())){
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				qry.append(" AND (date_naissance BETWEEN '"+df.format(date_debut.getDate())+"' AND '"+df.format(date_fin.getDate())+"')");
			}
			if(classe.getSelectedIndex()!=0)
				qry.append(" AND id_classe="+new Classe(classe.getSelectedItem().toString()).getId_classe());
			if(cantine.isSelected())
				qry.append(" AND cantine="+cantine.isSelected());
			/*if(centre.isSelected())
				qry.append(" AND centre_aere="+centre.isSelected());*/
			//ordre de tri
			qry.append(" ORDER by nom ASC");
			
			
			db = Launcher.db;

	    	HashMap<Object, Object> params = new HashMap<Object, Object>();
		    params.put("HEADERS_PATH", ".config/");
		    params.put("REPORT_CONNECTION", db.getConnection());
		    try {
                ResultSet rs = db.execQuery(qry.toString());
                rs.last();
                if(rs.getRow()>0){
                	rs.beforeFirst();
                	new Print(".config", "liste_familles.jrxml", rs, params);
                }else{
					JOptionPane.showMessageDialog(
							MainGui.desktop,
							Messages.getString("window.print.nopages"),
							Messages.getString("window.print.nopages.title"),
							JOptionPane.INFORMATION_MESSAGE
					);
                }
             
            } catch (SQLException e1) {e1.printStackTrace();}            
		}
	}
}
