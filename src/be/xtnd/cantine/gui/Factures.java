/*
 * Factures.java, 30 sept. 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               	Factures.java
 * Author's email :     	johan@x-tnd.be
 * Author's Website :   	http://cantine.sourceforge.net
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine.gui;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import be.xtnd.cantine.CantineI18n;
import be.xtnd.cantine.Eleve;
import be.xtnd.cantine.Facture;
import be.xtnd.commons.GuiCommons;
import be.xtnd.commons.db.ResultSetTableModelFactory;
import be.xtnd.commons.gui.EscapeInternalFrame;
import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.gui.TableSorter;
import be.xtnd.commons.i18n.CommonsI18n;

import com.jeta.forms.components.panel.FormPanel;
import com.jeta.forms.components.separator.TitledSeparator;
import com.toedter.calendar.JDateChooser;
import com.toedter.components.JSpinField;

/**
 * 
 * 
 * @author Johan Cwiklinski
 *
 */
public class Factures {
	//private Facture facture;
	private GuiCommons commons = new GuiCommons();
	private JTable table;
	private ResultSetTableModelFactory factory; 
	private Point position = null;
	NumberFormat nf = NumberFormat.getCurrencyInstance();
	/** Nom de la fenêtre liste */
	public static final String LIST_NAME = "liste_factures";

	/**
	 * 
	 * @param p
	 */
	public Factures(Point p){
	    this.position = p;
	    new Liste();
	}
	
	/**
	 * 
	 *
	 */
	public Factures(){
		new Liste();
	}
	
	private void detailsFacture(Integer id){
		new Fenetre(id);
	}
	
	private class Liste extends EscapeInternalFrame{
		private static final long serialVersionUID = -4794107012618428889L;
		private Integer idEncours;
		private JPopupMenu popup;
		private JButton generate, chercher, reinit;
		private JSpinField num_facture;
		private JTextField nom;
		private JDateChooser date_debut, date_fin;
		private JLabel num_facture_label, nom_label, date_debut_label, date_fin_label;
		private Date current_date;
		private DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		private TitledSeparator search_title;

		/**
		 * 
		 *
		 */
		public Liste(){
			this(new ResultSetTableModelFactory(Launcher.db));

			setName(LIST_NAME);
			pack();
			setSize(new Dimension(getWidth(), 500));
			setMinimumSize(new Dimension(getWidth(), 500));
			if(position!=null){setLocation(position);}else{setLocation(MainGui.centerOnDesktop(getSize()));}
			setVisible(true);
			MainGui.desktop.add(this);
			toFront();
			try {
				setSelected(true);
			} catch (PropertyVetoException e) {}
		}
		
		/**
		 * 
		 * @param f
		 */
		public Liste(ResultSetTableModelFactory f) {
			super(null, true, true, true, false);
			
			Calendar cal = Calendar.getInstance();
			current_date = cal.getTime();
			setTitle(Messages.getString("window.invoices.titles.list"));
			
			FormPanel pane = new FormPanel( "be/xtnd/cantine/gui/descriptions/liste_factures.jfrm" );

			search_title = (TitledSeparator)pane.getComponentByName("search_title");
			search_title.setText(Messages.getString("window.invoices.titles.search"));
			
			num_facture_label = pane.getLabel("num_facture_label");
			num_facture_label.setText(Messages.getString("window.invoices.labels.num"));
			num_facture = (JSpinField)pane.getComponentByName("num_facture");
			
			nom_label = pane.getLabel("nom_label");
			nom_label.setText(CantineI18n.tr("Name"));
			nom = pane.getTextField("nom");
			
			date_debut_label = pane.getLabel("date_debut_label");
			date_debut_label.setText(Messages.getString("window.invoices.labels.begin_date"));
			date_debut = (JDateChooser)pane.getComponentByName("date_debut");
			date_debut.setDate(current_date);
			
			date_fin_label = pane.getLabel("date_fin_label");
			date_fin_label.setText(Messages.getString("window.invoices.labels.end_date"));
			date_fin = (JDateChooser)pane.getComponentByName("date_fin");
			date_fin.setDate(current_date);
			
			reinit = (JButton)pane.getButton("reinit");
			reinit.setText(Messages.getString("window.invoices.buttons.reinit"));
			reinit.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e){
						num_facture.setValue(0);
						nom.setText("");
						date_debut.setDate(current_date);
						date_fin.setDate(current_date);
						displayQueryResults(Facture.QUERY_LIST);
					}
			});
			
			chercher = (JButton)pane.getButton("chercher");
			chercher.setText(Messages.getString("window.commons.perform_search"));
			chercher.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					displayQueryResults(constructQuery());
					
				}				
			});			
			getRootPane().setDefaultButton(chercher);

			generate = (JButton)pane.getButton("generer");
			generate.setText(Messages.getString("window.invoices.buttons.generate"));
			generate.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent arg0) {
					if(MainGui.verifUnicite(GenereFactures.NAME, null)){
						new GenereFactures();
					}
				}});
			
			//disableElements();
			
			factory = f;
			table = pane.getTable("liste");
			
			table.getTableHeader().setReorderingAllowed(false);
			table.addMouseListener(new MouseAdapter(){
				public void mousePressed(MouseEvent e){
			  	  	int nModifier = e.getModifiers();
			    	if ((nModifier & InputEvent.BUTTON3_MASK) != 0) {
						idEncours = (Integer)table.getValueAt(table.rowAtPoint(e.getPoint()),0);
						table.changeSelection(table.rowAtPoint(e.getPoint()),0,false,false);
						popup.show((Component)e.getSource(),e.getX(),e.getY());
			      	}
		            if (e.getClickCount() == 2) {// double-click
		            	idEncours = (Integer)table.getValueAt(table.rowAtPoint(e.getPoint()),0);
		            	Object[] args = {idEncours};
						if(Launcher.verifUnicite(MessageFormat.format(Messages.getString("window.invoices.names.details"), args),null)){
							detailsFacture(idEncours);
						}
		            }
				}
			});
			
			Container contentPane = getContentPane();
			contentPane.add(pane);
			displayQueryResults(Facture.QUERY_LIST);
			
			popup = new JPopupMenu();
			popupEntries(popup, Messages.getString("window.table_popup.details"),"detailled",Launcher.MODIF_IMAGE);
			popup.addSeparator();
			popupEntries(popup, CantineI18n.tr("Delete"), "del", Launcher.DELETE_IMAGE);

			table.add(popup);
		}
		
		/**
		 * 
		 * @return String
		 */
		public String constructQuery(){
			StringBuffer query = new StringBuffer();
			query.append(Facture.QUERY_NOT_ORDERED);
			boolean search = false;
			if(num_facture.getValue()>0){
				if(search){query.append(" AND ");}else{query.append(" WHERE ");}
				query.append("id_facture="+num_facture.getValue());
				search = true;
			}
			if(!nom.getText().equals("")){
				if(search){query.append(" AND ");}else{query.append(" WHERE ");}
				query.append("LCASE(nom) LIKE '%"+nom.getText().toLowerCase()+"%'");
				search = true;
			}
			if(date_debut.getDate().before(current_date)){
				if(search){query.append(" AND ");}else{query.append(" WHERE ");}
				query.append("date BETWEEN '"+df.format(date_debut.getDate())+"' AND '"+df.format(date_fin.getDate())+"'");
			}
			query.append(Facture.ORDER_CLAUSE);
			return query.toString();
		}		
	
		@Override
		protected void popupActions(ActionEvent event){
			String command = event.getActionCommand();
			if(command.equals("del")){
			    Facture fact = new Facture(idEncours);
			    Object[] args = {fact.getNom(), fact.getId_facture(), fact.getDate(), new Float(fact.getHt())};
				int response = JOptionPane.showConfirmDialog(
						MainGui.desktop, 
						MessageFormat.format(Messages.getString("window.invoices.messages.delete.confirm"), args),
						Messages.getString("window.invoices.messages.delete.title"),
						JOptionPane.YES_NO_OPTION,
						JOptionPane.WARNING_MESSAGE);
				if (response == JOptionPane.YES_OPTION) {				
				    fact.supprimeFacture();
					displayQueryResults(Facture.QUERY_LIST);
				}
			}else if(command.equals("detailled")){
				Object[] args = {idEncours};
				if(Launcher.verifUnicite(MessageFormat.format(Messages.getString("window.invoices.names.details"), args),null)){
					detailsFacture(idEncours);
				}
			}
		}	
	}
	
	/**
	 * 
	 * @param q
	 */
	public void displayQueryResults(final String q) {
		MainGui.statusBar.setText("Contacting database...");

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					table.setModel(factory.getResultSetTableModel(q));
					
			        TableSorter sorter = new TableSorter(table.getModel());
			        table.setModel(sorter);
			        sorter.setTableHeader(table.getTableHeader());
			        table.getTableHeader().setToolTipText(CommonsI18n.tr("Click to sort on a column, Ctrl+Click to sort on extra column"));

			        TableColumnModel columnModel = table.getColumnModel();
					columnModel.getColumn(0).setPreferredWidth(40);
					columnModel.getColumn(0).setMinWidth(40);
					columnModel.getColumn(0).setMaxWidth(40);
					columnModel.getColumn(1).setPreferredWidth(100);
					columnModel.getColumn(1).setMinWidth(100);
					columnModel.getColumn(1).setMaxWidth(150);
					columnModel.getColumn(3).setPreferredWidth(65);
					columnModel.getColumn(3).setMinWidth(65);
					columnModel.getColumn(3).setMaxWidth(65);
					columnModel.getColumn(3).setCellRenderer(new CurrencyRenderer());
					columnModel.getColumn(4).setPreferredWidth(65);
					columnModel.getColumn(4).setMinWidth(65);
					columnModel.getColumn(4).setMaxWidth(65);
					columnModel.getColumn(4).setCellRenderer(new CurrencyRenderer());
					columnModel.getColumn(5).setPreferredWidth(80);
					columnModel.getColumn(5).setMinWidth(80);
					columnModel.getColumn(5).setMaxWidth(130);
					
					MainGui.statusBar.setText("");
				} catch (SQLException ex) {
					MainGui.statusBar.setText("Error contacting database !");
					JOptionPane.showMessageDialog(MainGui.desktop,
							new String[] { // Display a 2-line message
							ex.getClass().getName() + ": ", ex.getMessage() });
					MainGui.statusBar.setText("");					
				}
			}
		});
	}	

	private class Fenetre extends EscapeInternalFrame implements PropertyChangeListener{
		private static final long serialVersionUID = -2887493070675729493L;
		private Integer id;
		private JLabel date, nom, montant, net;
		private JLabel date_label, nom_label, ht_label, acompte_label, a_deduire_label, net_label;
		private JFormattedTextField acompte, deduire;
		private TitledSeparator titre;
		private JTable details;
		private JCheckBox regle;
		private Facture fact;
		
		/**
		 * 
		 * @param id
		 */
		public Fenetre(Integer id){
			this.id = id;
			this.fact = new Facture(id);
			drawWindow();
		}
		
		private void drawWindow(){
			FormPanel pane = new FormPanel( "be/xtnd/cantine/gui/descriptions/details_facture.jfrm" );
			getContentPane().add(pane);
			
			titre = (TitledSeparator)pane.getComponentByName("titre");
			Object[] args = {this.id};
			titre.setText(MessageFormat.format(Messages.getString("window.invoices.titles.details"), args));
			
			date_label = pane.getLabel("date_label");
			date_label.setText(Messages.getString("window.invoices.labels.date"));
			
			date = pane.getLabel("date");
			DateFormat df = new SimpleDateFormat("dd MMMM yyyy");
			date.setText(df.format(fact.getDate()));
			
			nom_label = pane.getLabel("nom_label");
			nom_label.setText(CantineI18n.tr("Name"));
			
			nom = pane.getLabel("nom");
			nom.setText(fact.getNom());
			
			ht_label= pane.getLabel("ht_label");
			ht_label.setText(Messages.getString("window.invoices.labels.ht"));
			
			montant = pane.getLabel("ht");
			montant.setText(nf.format(fact.getHt()));
			
			acompte_label = pane.getLabel("acompte_label");
			acompte_label.setText(Messages.getString("window.invoices.labels.deposit"));
			
			acompte = (JFormattedTextField)pane.getComponentByName("acompte");
			acompte.setFormatterFactory(new DefaultFormatterFactory(new NumberFormatter(nf)));
			acompte.setValue(new Float(fact.getAcompte()));
			acompte.addPropertyChangeListener("value", this);
			
			a_deduire_label = pane.getLabel("a_deduire_label");
			a_deduire_label.setText(Messages.getString("window.invoices.labels.deduct"));
			
			deduire = (JFormattedTextField)pane.getComponentByName("a_deduire");
			deduire.setFormatterFactory(new DefaultFormatterFactory(new NumberFormatter(nf)));
			deduire.setValue(new Float(fact.getDeduire()));
			deduire.addPropertyChangeListener("value", this);
			
			net_label = pane.getLabel("net_label");
			net_label.setText(Messages.getString("window.invoices.labels.net"));
			
			net = pane.getLabel("net");
			net.setText(nf.format(fact.getNet()));
			
			regle =  pane.getCheckBox("settled");
			regle.setText(Messages.getString("window.invoices.labels.settled"));
			regle.setSelected(fact.isRegle());
			
			details = pane.getTable("details");
			String sqlDetails = "SELECT CONCAT(nom, prenom) AS Nom, quantite, prix FROM "+Facture.DETAILS_TABLE+" INNER JOIN "+
				Eleve.TABLE+" ON("+Facture.DETAILS_TABLE+"."+Eleve.PK+"="+Eleve.TABLE+"."+Eleve.PK+")"+
				" WHERE "+Facture.PK+"="+fact.getId_facture();
			displayDetailsResults(sqlDetails);
			details.getTableHeader().setReorderingAllowed(false);
			
			commons.createButtonBar(this,new OkayEvent());
			
			pack();

			setName(
					MessageFormat.format(Messages.getString("window.invoices.names.details"),args)	
			);

			MainGui.desktop.add(this, JLayeredPane.MODAL_LAYER);
			setLocation(MainGui.centerOnDesktop(getSize()));
			
			setResizable(true);
			setClosable(true);
			setIconifiable(true);
			setMaximizable(true);
			setVisible(true);
		}
		 
		/**
		 * 
		 * @param q
		 */
		public void displayDetailsResults(final String q) {
			MainGui.statusBar.setText("Contacting database...");

			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						details.setModel(factory.getResultSetTableModel(q));
						
				        TableSorter sorter = new TableSorter(details.getModel());
				        details.setModel(sorter);
				        sorter.setTableHeader(details.getTableHeader());
				        details.getTableHeader().setToolTipText(CommonsI18n.tr("Click to sort on a column, Ctrl+Click to sort on extra column"));

				        TableColumnModel columnModel = details.getColumnModel();

						columnModel.getColumn(1).setPreferredWidth(50);
						columnModel.getColumn(1).setMinWidth(50);
						columnModel.getColumn(1).setMaxWidth(50);
						columnModel.getColumn(2).setPreferredWidth(50);
						columnModel.getColumn(2).setMinWidth(50);
						columnModel.getColumn(2).setMaxWidth(50);
				        columnModel.getColumn(2).setCellRenderer(new CurrencyRenderer());
						
						MainGui.statusBar.setText("");
					} catch (SQLException ex) {
						MainGui.statusBar.setText("Error contacting database !");
						JOptionPane.showMessageDialog(MainGui.desktop,
								new String[] { // Display a 2-line message
								ex.getClass().getName() + ": ", ex.getMessage() });
						MainGui.statusBar.setText("");					
					}
				}
			});
		}	

		
		class OkayEvent implements ActionListener{
			/**
			 * @param e
			 */
			public void actionPerformed(ActionEvent e) {
				if(fact.isRegle()!=regle.isSelected()) fact.settleInvoice(id, regle.isSelected());
				fact.enregistreMontants(id);
				dispose();
				displayQueryResults(Facture.QUERY_LIST);
			}		
		}

		/**
		 * @param evt
		 */
		public void propertyChange(PropertyChangeEvent evt) {
			Object source = evt.getSource();
	        if (source == acompte || source == deduire) {
	        	fact.setAcompte(new Float(acompte.getValue().toString()).floatValue());
	        	fact.setDeduire(new Float(deduire.getValue().toString()).floatValue());	        	
	        	net.setText(nf.format(
	        			fact.getHt() - fact.getAcompte() - fact.getDeduire()
	        	));
	        }
		}
	}

	class CurrencyRenderer extends DefaultTableCellRenderer {
		private static final long serialVersionUID = -7224248154043000281L;
		/**
		 * 
		 *
		 */
		public CurrencyRenderer() {
		    super();
		  }
		
			/**
			 * @param table
			 * @param value
			 * @param isSelected
			 * @param hasFocus
			 * @param row
			 * @param column
			 * 
			 * @return Component
			 */
		  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
		  		boolean hasFocus, int row, int column) {
	        JLabel cell = (JLabel) super.getTableCellRendererComponent(table, value, 
	        		isSelected, hasFocus, row, column);
	            cell.setHorizontalAlignment(RIGHT);
	            NumberFormat nf = NumberFormat.getCurrencyInstance();
	            String str = new String(); 
				if(!(value == null)){
		            str = nf.format(value);
	            }
	            cell.setText(str);
	            return cell;
		  }
		} 
}
