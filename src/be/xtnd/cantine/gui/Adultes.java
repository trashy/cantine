/*
 * Adultes.java, 26 août 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               	Adultes.java
 * Author's email :     	johan@x-tnd.be
 * Author's Website :   	http://cantine.sourceforge.net
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.TableColumnModel;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

import be.xtnd.cantine.Adulte;
import be.xtnd.cantine.CantineI18n;
import be.xtnd.cantine.Eleve;
import be.xtnd.cantine.Villes;
import be.xtnd.commons.GuiCommons;
import be.xtnd.commons.db.Database;
import be.xtnd.commons.db.ResultSetTableModelFactory;
import be.xtnd.commons.gui.EscapeInternalFrame;
import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.gui.TableSorter;
import be.xtnd.commons.i18n.CommonsI18n;
import be.xtnd.thirdparty.AutoCompletion;
import be.xtnd.validators.FieldLengthVerifier;
import be.xtnd.validators.MailFieldVerifier;
import be.xtnd.validators.MaskFieldVerifier;

import com.jeta.forms.components.border.TitledBorderLabel;
import com.jeta.forms.components.panel.FormPanel;
import com.jeta.forms.components.separator.TitledSeparator;

/**
 * 
 * 
 * @author Johan Cwiklinski
 *
 */
public class Adultes {	
	private Adulte adulte;
	private GuiCommons commons = new GuiCommons();
	private JTable table;
	private ResultSetTableModelFactory factory; 
	/** Nom de la fenêtre liste */
	public static final String LIST_NAME = "liste_adultes";
	/** Nom de la fenêtre d'ajout */
	public static final String NEW_NAME = "nouvel_adulte";

	/**
	 * 
	 *
	 */
	public Adultes(){}
	
	/**
	 * 
	 *
	 */
	public void afficheListe(){
		new Liste();		
	}
	
	/**
	 * 
	 *
	 */
	public void nouvelAdulte(){
		this.adulte = new Adulte();
		new Fenetre(Database.NEW);
	}
	
	/**
	 * 
	 * @param id
	 */
	public void modifieAdulte(Integer id){
		this.adulte = new Adulte(id);
		new Fenetre(Database.MODIF);	
	}
	
	/**
	 * 
	 * @author Johan Cwiklinski
	 *
	 */
	private class Liste extends EscapeInternalFrame{
		private static final long serialVersionUID = -5255108546861895583L;
		private Integer idEncours;
		private JPopupMenu popup;
		private TitledSeparator title;
		private JLabel nom_cherche_label, villes_label;
		private JTextField nom_cherche;
		private JComboBox villes;
		private JButton search, reinit, add;

		/**
		 * 
		 *
		 */
		public Liste(){
			this(new ResultSetTableModelFactory(Launcher.db));

			setIconifiable(false);
			setClosable(true);
			setResizable(true);
			setMaximizable(true);
			setName(LIST_NAME);
			setSize(700, 500);
			setLocation(MainGui.centerOnDesktop(getSize()));
			setVisible(true);
			MainGui.desktop.add(this);	
		}
		/**
		 * 
		 * @param f
		 */
		public Liste(ResultSetTableModelFactory f) {
			setTitle(Messages.getString("window.adulte.titles.list"));

			factory = f;
			table = new JTable();
			table.getTableHeader().setReorderingAllowed(false);
			table.addMouseListener(new MouseAdapter(){
				public void mousePressed(MouseEvent e){
			  	  	int nModifier = e.getModifiers();
					idEncours = (Integer)table.getValueAt(table.rowAtPoint(e.getPoint()),0);
			    	if ((nModifier & InputEvent.BUTTON3_MASK) != 0) {
						table.changeSelection(table.rowAtPoint(e.getPoint()),0,false,false);
						popup.show((Component)e.getSource(),e.getX(),e.getY());
			      	}
			    	if(e.getClickCount()==2){//double click
						Object[] args = {idEncours};
						if(Launcher.verifUnicite(MessageFormat.format(Messages.getString("window.adulte.names.modif"), args),null)){
							modifieAdulte(idEncours);
						}
			    	}
				}
			});
			
			FormPanel pane = new FormPanel( "be/xtnd/cantine/gui/descriptions/header_list_families.jfrm" );
			
			title = (TitledSeparator)pane.getComponentByName("title");
			title.setText(Messages.getString("window.commons.quick_search"));
			
			nom_cherche_label = pane.getLabel("nom_cherche_label");
			nom_cherche_label.setText(CantineI18n.tr("Name"));
			
			nom_cherche = pane.getTextField("nom_cherche");
			
			villes_label = pane.getLabel("villes_label");
			villes_label.setText(CantineI18n.tr("Town"));
			
			villes = pane.getComboBox("villes");
			commons.buildCombo(villes, Villes.liste(true));
			AutoCompletion.enable(villes);
			
			search = (JButton)pane.getButton("search");
			search.setText(Messages.getString("window.commons.perform_search"));
			search.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					buildSearchQuery();
				}
			});
			getRootPane().setDefaultButton(search);
			
			reinit = (JButton)pane.getButton("reinit");
			reinit.setToolTipText(Messages.getString("window.famille.tooltips.reinit"));
			reinit.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					nom_cherche.setText("");
					villes.setSelectedIndex(0);
					buildSearchQuery();
				}
			});
			
			add = (JButton)pane.getButton("add");
			add.setToolTipText(Messages.getString("window.adulte.titles.new"));
			add.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					if(Launcher.verifUnicite(NEW_NAME,null)){
						nouvelAdulte();
					}
				}				
			});

			Container contentPane = getContentPane();
			contentPane.add(pane, BorderLayout.NORTH);
			contentPane.add(new JScrollPane(table), BorderLayout.CENTER);
			displayQueryResults(Adulte.LISTQUERY);
			
			popup = new JPopupMenu();
			popupEntries(popup, CantineI18n.tr("New"), "new", Launcher.NEW_IMAGE);
			popupEntries(popup, CantineI18n.tr("Edit"), "detailled", Launcher.MODIF_IMAGE);
			popup.addSeparator();
			popupEntries(popup, CantineI18n.tr("Delete"), "del", Launcher.DELETE_IMAGE);

			table.add(popup);
		}
		
		@Override
		protected void popupActions(ActionEvent event){
			String command = event.getActionCommand();
			if(command.equals("new")){
				if(Launcher.verifUnicite(NEW_NAME,null)){
					nouvelAdulte();
				}
			}else if(command.equals("del")){
				Adulte adult = new Adulte(idEncours);
				Object[] args = {adult.getNom()};
				int response = JOptionPane.showConfirmDialog(
						MainGui.desktop, 
						MessageFormat.format(Messages.getString("window.adulte.messages.delete.confirm"), args),
						Messages.getString("window.adulte.messages.delete.title"),
						JOptionPane.YES_NO_OPTION,
						JOptionPane.WARNING_MESSAGE);
				if (response == JOptionPane.YES_OPTION) {				
					adult.deleteFamille(idEncours);
					displayQueryResults(Adulte.LISTQUERY);
				}
			}else if(command.equals("detailled")){
				Object[] args = {idEncours};
				if(Launcher.verifUnicite(MessageFormat.format(Messages.getString("window.adulte.names.modif"), args),null)){
					modifieAdulte(idEncours);
				}
			}
		}	

		private void buildSearchQuery(){
			StringBuffer sb = new StringBuffer(Adulte.BASE_QUERY);
			boolean isSearch = false;
			if(!nom_cherche.getText().trim().equals("")){
				sb.append((isSearch)?" OR ":" AND ");
				sb.append("nom LIKE '%" + Database.replace(nom_cherche.getText(), "\'", "\'\'") + "%'");
				isSearch = true;
			}
			if(villes.getSelectedIndex() > 0){
				sb.append((isSearch)?" OR ":" AND ");
				sb.append("ville='" + Database.replace(villes.getSelectedItem().toString(), "\'", "\'\'") + "'");
				isSearch = true;
			}
						
			sb.append(Adulte.ORDER_BY);
			System.err.println(sb.toString());
			displayQueryResults(sb.toString());
		}
	}

	/**
	 * 
	 * @param q
	 */
	public void displayQueryResults(final String q) {
		MainGui.statusBar.setText("Contacting database...");

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					table.setModel(factory.getResultSetTableModel(q));
					
			        TableSorter sorter = new TableSorter(table.getModel());
			        table.setModel(sorter);
			        sorter.setTableHeader(table.getTableHeader());
			        /** FIXME: must be handled in xtnd-commons i18n string, but do not appears in xtnd-commons source code :/ */
			        table.getTableHeader().setToolTipText(CommonsI18n.tr("Click to sort on a column, Ctrl+Click to sort on extra column"));

			        TableColumnModel columnModel = table.getColumnModel();
					columnModel.getColumn(0).setPreferredWidth(0);
					columnModel.getColumn(0).setMinWidth(0);
					columnModel.getColumn(0).setMaxWidth(0);
					columnModel.getColumn(1).setPreferredWidth(150);
					columnModel.getColumn(1).setMinWidth(150);
					columnModel.getColumn(1).setMaxWidth(200);
					columnModel.getColumn(3).setPreferredWidth(50);
					columnModel.getColumn(3).setMinWidth(50);
					columnModel.getColumn(3).setMaxWidth(50);
					columnModel.getColumn(4).setPreferredWidth(120);
					columnModel.getColumn(4).setMinWidth(120);
					columnModel.getColumn(4).setMaxWidth(150);
					
					MainGui.statusBar.setText("");
				} catch (SQLException ex) {
					MainGui.statusBar.setText("Error contacting database !");
					JOptionPane.showMessageDialog(MainGui.desktop,
							new String[] { // Display a 2-line message
							ex.getClass().getName() + ": ", ex.getMessage() });
					MainGui.statusBar.setText("");					
				}
			}
		});
	}	

	private class Fenetre extends EscapeInternalFrame{
		private static final long serialVersionUID = -1342943801317916104L;
		private JLabel civilite_label, nom_label, prenom_label, adresse_label, code_postal_label,
			ville_label, telephone_label, portable_label, mail_label, commentaires_label;
		private TitledBorderLabel reservation_label;
		private char mode;
		private JTextField nom, prenom, cp, ville, mail;
		private JFormattedTextField telephone, portable;
		private MaskFormatter fmPhone;
		private JTextArea adresse, commentaires;
		private JCheckBox cantine/*, centre_aere*/;
		private JComboBox sexe;
		private JButton presence;
		Eleve ad;
		
		/**
		 * 
		 * @param mod
		 */
		public Fenetre(char mod){
			this.mode = mod;
			drawWindow();
		}
		
		private void drawWindow(){
			ad = adulte.getAdulte();
			
			final Integer id_el = ad.getId_eleve();
			FormPanel pane = new FormPanel( "be/xtnd/cantine/gui/descriptions/adultes.jfrm" );
			getContentPane().add(pane);
			
			reservation_label = (TitledBorderLabel)pane.getComponentByName("reservation_label");
			reservation_label.setText(Messages.getString("window.adulte.labels.reservation"));
			
			civilite_label = pane.getLabel("civilite_label");
			civilite_label.setText(Messages.getString("window.adulte.labels.politeness"));
			
			sexe = pane.getComboBox("civilite");
			int sex_selected = 0;
			if(ad.getSexe()==Adulte.MONSIEUR) sex_selected = 1;
			if(ad.getSexe()==Adulte.MADAME) sex_selected = 2;
			if(ad.getSexe()==Adulte.MADEMOISELLE) sex_selected = 3;
			sexe.setSelectedIndex(sex_selected);
			
			nom_label = pane.getLabel("nom_label");
			nom_label.setText(CantineI18n.tr("Name"));
			
			nom = pane.getTextField("nom");
			nom.setText(ad.getNom());
			
			prenom_label = pane.getLabel("prenom_label");
			prenom_label.setText(CantineI18n.tr("First name"));
			
			prenom = pane.getTextField("prenom");
			prenom.setText(ad.getPrenom());
			
			adresse_label = pane.getLabel("adresse_label");
			adresse_label.setText(CantineI18n.tr("Address"));
			
			adresse = (JTextArea)pane.getComponentByName("adresse");
			adresse.setText(adulte.getAdresse());
			
			code_postal_label = pane.getLabel("code_postal_label");
			code_postal_label.setText(CantineI18n.tr("Zip code"));
			
			cp = pane.getTextField("code_postal");
			cp.setDocument(new FieldLengthVerifier(5));
			cp.setText(adulte.getCp());
			
			ville_label = pane.getLabel("ville_label");
			ville_label.setText(CantineI18n.tr("Town"));
			
			ville = pane.getTextField("ville");
			ville.setText(adulte.getVille());

			telephone_label = pane.getLabel("telephone_label");
			telephone_label.setText(CantineI18n.tr("Phone"));
			
			try {
				fmPhone = new MaskFormatter("## ## ## ## ##");
			} catch (ParseException e) {e.printStackTrace();}

			telephone = (JFormattedTextField)pane.getTextComponent("telephone");
			telephone.setFormatterFactory(new DefaultFormatterFactory(fmPhone));
			telephone.setInputVerifier(new MaskFieldVerifier(adulte.getTelephone(), fmPhone.getMask()));
			
			if(!adulte.getTelephone().trim().equals("")){
				telephone.setText(adulte.getTelephone());				
				try {
					telephone.commitEdit();
				} catch (ParseException e1) {e1.printStackTrace();}
			}

			portable_label = pane.getLabel("portable_label");
			portable_label.setText(CantineI18n.tr("Mobile phone"));
			
			portable = (JFormattedTextField)pane.getComponentByName("mobile");
			portable.setFormatterFactory(new DefaultFormatterFactory(fmPhone));
			portable.setInputVerifier(new MaskFieldVerifier(adulte.getTel_portable(), fmPhone.getMask()));
			if(!adulte.getTel_portable().trim().equals("")){
				portable.setText(adulte.getTel_portable());
				try {
					portable.commitEdit();
				} catch (ParseException e1) {e1.printStackTrace();}
			}

			mail_label = pane.getLabel("mail_label");
			mail_label.setText(CantineI18n.tr("E-Mail"));
			
			mail = pane.getTextField("mail");
			mail.setText(adulte.getMail());
			mail.setInputVerifier(new MailFieldVerifier(adulte.getMail()));
												
			cantine = pane.getCheckBox("cantine");
			cantine.setText(Messages.getString("window.adulte.labels.cantine"));
			cantine.setSelected(ad.isCantine());
			cantine.addItemListener(new ItemListener(){
				public void itemStateChanged(ItemEvent e) {
					adulte.getAdulte().setCantine((e.getStateChange() == ItemEvent.SELECTED)?true:false);
					if(e.getStateChange() == ItemEvent.SELECTED){
						presence.setEnabled(true);
					}else{presence.setEnabled(false);}
					/*if(e.getStateChange() == ItemEvent.DESELECTED
							&& !centre_aere.isSelected()){
						presence.setEnabled(false);
					}*/
					
				}
			});

			
			//centre_aere = pane.getCheckBox("centre_aere");
			//centre_aere.setText(Messages.getString("window.adulte.labels.centre"));
			//centre_aere.setSelected(ad.isCentre_aere());
			//centre_aere.setVisible(false);
			//centre_aere.setEnabled(false);
			
			presence = (JButton)pane.getButton("presence");
			presence.setText(Messages.getString("window.adulte.buttons.presence"));
            presence.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent e) {
                	Object[] args = {id_el};
                	if(MainGui.verifUnicite(
                			MessageFormat.format(Messages.getString("window.presence.name"), args),
							null)){
                	new Presences(adulte.getAdulte());     		
                	}
                }
            });			
            
            commentaires_label = pane.getLabel("commentaires_label");
            commentaires_label.setText(CantineI18n.tr("Comments"));
            
			commentaires = (JTextArea)pane.getComponentByName("commentaires");
			commentaires.setText(adulte.getCommentaires());
			
			commons.createButtonBar(this,new OkayEvent());
			
			pack();
			String title = Messages.getString((mode == Database.NEW)?"window.adulte.titles.new":"window.adulte.titles.modif");
			if(mode == Database.NEW){
				setTitle(title);
				setName(NEW_NAME);
				presence.setEnabled(false);
			}else if(mode == Database.MODIF){
				Object args[] = {adulte.getNom()};
				setTitle(MessageFormat.format(title, args));
				Object[] args2 = {adulte.getId_famille()};
				setName(MessageFormat.format(Messages.getString("window.adulte.names.modif"), args2));
			}

			MainGui.desktop.add(this, JLayeredPane.MODAL_LAYER);
			setLocation(MainGui.centerOnDesktop(getSize()));
			
			setResizable(true);
			setClosable(true);
			setIconifiable(true);
			setMaximizable(true);
			setVisible(true);
		}
		
		/**
		 * Valide les entrées obligtoires du formulaire,
		 * affiche un message sinon.
		 * 
		 * @return boolean résultat
		 */
		private boolean verifEntrees(){
			boolean valid = true;
			StringBuffer required = new StringBuffer();
			required.append(CommonsI18n.tr("The following are required:"));

			if(sexe.getSelectedIndex()==0){
				required.append("\n- " + Messages.getString("window.adulte.required.sex"));
				sexe.requestFocus();
				valid = false;
			}
			if(nom.getText().trim().equalsIgnoreCase("")){
				required.append("\n- " + Messages.getString("window.adulte.required.name"));
				if(valid) nom.requestFocus();
				valid = false;
			}
			if(adresse.getText().trim().equalsIgnoreCase("")){
				required.append("\n- " + Messages.getString("window.adulte.required.adress"));
				if(valid) adresse.requestFocus();
				valid = false;
			}
			if(ville.getText().trim().equalsIgnoreCase("")){
				required.append("\n- " + Messages.getString("window.adulte.required.town"));
				if(valid) ville.requestFocus();
				valid = false;
			}

			if(!valid){
				JOptionPane.showMessageDialog(
						MainGui.desktop, 
						required.toString(),
						CommonsI18n.tr("Error!"),
						JOptionPane.ERROR_MESSAGE);	
			}
			return valid;
		}
		 
		class OkayEvent implements ActionListener{
			/**
			 * @param e
			 */
			public void actionPerformed(ActionEvent e) {
				if(verifEntrees()){
					ad.setNom(nom.getText());
					ad.setPrenom(prenom.getText());
					ad.setCantine(cantine.isSelected());
					//ad.setCentre_aere(centre_aere.isSelected());
					int sex_selected = sexe.getSelectedIndex();
					switch(sex_selected){
						case 1:
							ad.setSexe(Adulte.MONSIEUR);
							break;
						case 2:
							ad.setSexe(Adulte.MADAME);
							break;
						case 3:
							ad.setSexe(Adulte.MADEMOISELLE);
							break;
					}

					adulte.setNom(nom.getText() + " " + prenom.getText());
					adulte.setAdresse(adresse.getText());
					adulte.setCp(cp.getText());
					adulte.setVille(ville.getText());
					adulte.setTelephone(telephone.getText());
					adulte.setTel_portable(portable.getText());
					adulte.setMail(mail.getText());
					adulte.setCommentaires(commentaires.getText());
					adulte.setEnfants(ad);
					
					if(mode == Database.NEW){
						adulte.storeFamille();
						ad.setId_famille(adulte.getId_famille());
						ad.storeEleve();
					}
					if(mode == Database.MODIF){
						adulte.modifyFamille(adulte.getId_famille());
						ad.setId_famille(adulte.getId_famille());
						ad.modifyEleve(ad.getId_eleve());
					}
						
					dispose();
					displayQueryResults(Adulte.LISTQUERY);
				}
			}		
		}
	}
}
