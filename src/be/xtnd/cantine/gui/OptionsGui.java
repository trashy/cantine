/*
 * OptionsGui.java, 2005-09-22
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               OptionsGui.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyVetoException;
import java.io.File;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

import be.xtnd.cantine.ApplicationProperties;
import be.xtnd.cantine.CantineI18n;
import be.xtnd.cantine.Options;
import be.xtnd.commons.GuiCommons;
import be.xtnd.commons.gui.EscapeInternalFrame;
import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.i18n.CommonsI18n;
import be.xtnd.commons.utils.ImageFilter;
import be.xtnd.commons.utils.ImagePreview;
import be.xtnd.validators.MailFieldVerifier;
import be.xtnd.validators.MaskFieldVerifier;

import com.jeta.forms.components.panel.FormPanel;
import com.jeta.forms.components.separator.TitledSeparator;
import com.jgoodies.looks.plastic.PlasticInternalFrameUI;
import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JYearChooser;

/**
 * Gui to set cantine's preferences
 * 
 * @author Johan Cwiklinski
 * @since 2005-09-22
 */
public class OptionsGui extends EscapeInternalFrame {
	private static final long serialVersionUID = 2583554231610238775L;

	private GuiCommons commons = new GuiCommons();
	private Options options = Launcher.options;
	private FormPanel pane;
	/* graphical components */
	private JTabbedPane tabs;
	private JCheckBox lundi, mardi, mercredi, jeudi, vendredi, samedi,dimanche, hide;
	private JTable tarifs;
	// for town hall
	private JTextField nom_mairie, ville_mairie, email_mairie, logo, site_web, signataire;
	private JButton open_logo;
	private JFormattedTextField code_postal_mairie, telephone_mairie,fax_mairie;
	private JTextArea adresse_mairie;
	// for caterer
	private JTextField raison_sociale, ville, email;
	private JTextArea adresse;
	private JFormattedTextField telephone, fax, code_postal;
	private MaskFormatter fmPhone, fmCodePostal;
	private static final boolean DEBUG = true;
	private boolean[] select = new boolean[7];
	private String[] coordonnees;
	private String[] mairie;
	private Dimension size = new Dimension(470, 330);
	// for holidays
	private JComboBox combo_annee;
	private JTable table_conges;
	private String[] colonnesConges;
	private String[] colonnesTarifs;
	private Object[][] lignesConges;
	private Object[][] lignesTarifs;
	private JPopupMenu popup, popupT;
	private JButton okay, annuler, appliquer, add_school_year;
	/** Window name */
	public static final String NAME = "options";
	/** Add year window name */
	public static final String NEW_YEAR_NAME = "ajout_annee_scolaire";

	/**
	 * 
	 *
	 */
	public OptionsGui() {
		super(null, false, true, true, true);
		super.setName(NAME);

		putClientProperty(PlasticInternalFrameUI.IS_PALETTE, Boolean.TRUE);

		pane = new FormPanel("be/xtnd/cantine/gui/descriptions/options.jfrm");
		pane.setPreferredSize(new Dimension(380, 200));

		TitledSeparator title = (TitledSeparator)pane.getComponentByName("title");
		title.setText(CantineI18n.tr("{0} preferences", ApplicationProperties.getString("appli.name")));
		
		tabs = pane.getTabbedPane("tabbed_pane");

		tabs.setTitleAt(0, CantineI18n.tr("Planning"));
		tabs.setTitleAt(1, CantineI18n.tr("Prices list"));
		tabs.setTitleAt(2, CantineI18n.tr("Caterer"));
		tabs.setTitleAt(3, CantineI18n.tr("Coordinates"));
		tabs.setTitleAt(4, CantineI18n.tr("Holydays"));
		
		buildDays();
		buildTarifs();
		buildTraiteur();
		buildMairie();
		buildVacances();

		okay = (JButton)pane.getButton("okay");
		okay.setText(CommonsI18n.tr("Okay"));
		okay.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				saveValues(true);
			}
			
		});
		okay.addFocusListener(new DateFocus());
		
		appliquer = (JButton)pane.getButton("appliquer");
		appliquer.setText(CommonsI18n.tr("Apply"));
		appliquer.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				saveValues(false);
			}
		});

		appliquer.addFocusListener(new DateFocus());

		annuler = (JButton)pane.getButton("annuler");
		annuler.setText(CommonsI18n.tr("Cancel"));
		annuler.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		add(new JScrollPane(pane));

		this.setMinimumSize(size);
		this.setPreferredSize(size);
		this.setTitle(CantineI18n.tr("{0} preferences", ApplicationProperties.getString("appli.name")));
		this.getRootPane().setDefaultButton(okay);
		pack();

		setLocation(MainGui.centerOnDesktop(this.getSize()));
		setMinimumSize(getSize());
		setVisible(true);
		setClosable(true);
		setIconifiable(true);
		setResizable(true);
		MainGui.desktop.add(this, JLayeredPane.POPUP_LAYER);
		try {
			setSelected(true);
		} catch (PropertyVetoException e1) {}
	}

	private void buildDays() {
		JLabel exclude = pane.getLabel("exclude");
		exclude.setText(CantineI18n.tr("Select days to exclude for planning"));
		
		lundi = pane.getCheckBox("lundi");
		lundi.setText(CantineI18n.tr("Monday"));
		lundi.setSelected(options.getActiveDays()[0]);

		mardi = pane.getCheckBox("mardi");
		mardi.setText(CantineI18n.tr("Tuesday"));
		mardi.setSelected(options.getActiveDays()[1]);

		mercredi = pane.getCheckBox("mercredi");
		mercredi.setText(CantineI18n.tr("Wednesday"));
		mercredi.setSelected(options.getActiveDays()[2]);

		jeudi = pane.getCheckBox("jeudi");
		jeudi.setText(CantineI18n.tr("Thursday"));
		jeudi.setSelected(options.getActiveDays()[3]);

		vendredi = pane.getCheckBox("vendredi");
		vendredi.setText(CantineI18n.tr("Friday"));
		vendredi.setSelected(options.getActiveDays()[4]);

		samedi = pane.getCheckBox("samedi");
		samedi.setText(CantineI18n.tr("Saturday"));
		samedi.setSelected(options.getActiveDays()[5]);

		dimanche = pane.getCheckBox("dimanche");
		dimanche.setText(CantineI18n.tr("Sunday"));
		dimanche.setSelected(options.getActiveDays()[6]);
		
		hide = pane.getCheckBox("hide_closed");
		hide.setText(CantineI18n.tr("Hide holydays and closing days in planning?"));
		hide.setSelected(options.isHide_closed());

	}

	/**
	 * 
	 * @return boolean
	 */
	public boolean getSelectHideClosed(){	
		boolean select = false;
		select = hide.isSelected();
		return select; 
	}	

	/**
	 * renvoie les jour sélectionnés pour fermeture,
	 * panneau options - pointages
	 * 
	 * @return boolean[] jours sélectionnés
	 */
	public boolean[] getSelectionDays() {
		select = new boolean[7];
		select[0] = lundi.isSelected();
		select[1] = mardi.isSelected();
		select[2] = mercredi.isSelected();
		select[3] = jeudi.isSelected();
		select[4] = vendredi.isSelected();
		select[5] = samedi.isSelected();
		select[6] = dimanche.isSelected();

		return select;
	}

	private void buildTarifs() {
		JLabel tarifs_label = pane.getLabel("tarifs_label");
		tarifs_label.setText(CantineI18n.tr("Enter {0} prices lists", ApplicationProperties.getString("appli.name")));
		
		tarifs = pane.getTable("tarifs");

		colonnesTarifs = new String[]{"id", CantineI18n.tr("Quantity"), CantineI18n.tr("Price")};
		lignesTarifs = options.getTarifs();
		
		popupT = new JPopupMenu();
		popupEntries(popupT, CantineI18n.tr("New"), "new_tarif", Launcher.NEW_IMAGE);
		popupT.addSeparator();
		popupEntries(popupT, CantineI18n.tr("Delete"), "del_tarif", Launcher.DELETE_IMAGE);

		tarifs.add(popupT);

		tarifs.addMouseListener(new MouseAdapter(){
			public void mousePressed(MouseEvent e){
		  	  	int nModifier = e.getModifiers();
		    	if ((nModifier & InputEvent.BUTTON3_MASK) != 0) {
					tarifs.changeSelection(tarifs.rowAtPoint(e.getPoint()),0,false,false);
					popupT.show((Component)e.getSource(),e.getX(),e.getY());
		      	}
			}
		});

		drawTarifsTable();
	}

	private void buildTraiteur() {
		try {
			fmPhone = new MaskFormatter("## ## ## ## ##");
			fmCodePostal = new MaskFormatter("#####");
		} catch (ParseException e) {
			e.printStackTrace();
		}

		JLabel rs_label = pane.getLabel("raison_sociale_label");
		rs_label.setText(CantineI18n.tr("Corporate name"));
		
		raison_sociale = pane.getTextField("raison_sociale");
		raison_sociale.setText(options.getTraiteur()[0].toString());

		JLabel adresse_label = pane.getLabel("adresse_label");
		adresse_label.setText(CantineI18n.tr("Address"));
		
		adresse = (JTextArea) pane.getComponentByName("adresse");
		adresse.setText(options.getTraiteur()[1].toString());

		JLabel cp_label = pane.getLabel("code_postal_label");
		cp_label.setText(CantineI18n.tr("Zip code"));
		
		code_postal = (JFormattedTextField) pane.getComponentByName("code_postal");
		code_postal.setFormatterFactory(new DefaultFormatterFactory(fmCodePostal));
		
		if (!options.getTraiteur()[2].toString().trim().equals("")) {
		code_postal.setText(options.getTraiteur()[2].toString());
		try {code_postal.commitEdit();} catch (ParseException e) {e.printStackTrace();}}

		JLabel ville_label = pane.getLabel("ville_label");
		ville_label.setText(CantineI18n.tr("Town"));
		
		ville = pane.getTextField("ville");
		ville.setText(options.getTraiteur()[3].toString());

		JLabel tel_label = pane.getLabel("telephone_label");
		tel_label.setText(CantineI18n.tr("Phone"));
		
		telephone = (JFormattedTextField) pane.getComponentByName("telephone");
		telephone.setFormatterFactory(new DefaultFormatterFactory(fmPhone));
		telephone.setInputVerifier(new MaskFieldVerifier(
				options.getTraiteur()[4].toString(), fmPhone.getMask()));

		if (!options.getTraiteur()[4].toString().trim().equals("")) {
			telephone.setText(options.getTraiteur()[4].toString());
			try {telephone.commitEdit();} catch (ParseException e) {e.printStackTrace();}
		}

		JLabel fax_label = pane.getLabel("fax_label");
		fax_label.setText(CantineI18n.tr("Fax"));
		
		fax = (JFormattedTextField) pane.getComponentByName("fax");
		fax.setFormatterFactory(new DefaultFormatterFactory(fmPhone));
		fax.setInputVerifier(new MaskFieldVerifier(options.getTraiteur()[5].toString(), fmPhone.getMask()));

		if (!options.getTraiteur()[5].toString().trim().equals("")) {
			fax.setText(options.getTraiteur()[5].toString());
			try {fax.commitEdit();} catch (ParseException e) {e.printStackTrace();}
		}

		JLabel mail_label = pane.getLabel("mail_label");
		mail_label.setText(CantineI18n.tr("E-Mail"));
		
		email = pane.getTextField("mail");
		email.setText(options.getTraiteur()[6].toString());
		email.setInputVerifier(new MailFieldVerifier(options.getTraiteur()[6].toString()));
	}

	private String[] getSaisieTraiteur() {
		coordonnees = new String[7];

		coordonnees[0] = raison_sociale.getText().toString();

		coordonnees[1] = adresse.getText().toString();
		coordonnees[2] = code_postal.getText().toString();
		coordonnees[3] = ville.getText().toString();
		coordonnees[4] = telephone.getText().toString();
		coordonnees[5] = fax.getText().toString();
		coordonnees[6] = email.getText().toString();
		return coordonnees;
	}

	private void buildMairie() {
		try {
			fmPhone = new MaskFormatter("## ## ## ## ##");
			fmCodePostal = new MaskFormatter("#####");
		} catch (ParseException e) {
			e.printStackTrace();
		}

		JLabel nom_label = pane.getLabel("nom_label");
		nom_label.setText(CantineI18n.tr("Name"));
		
		nom_mairie = pane.getTextField("nom_mairie");
		nom_mairie.setText(options.getMairie()[0].toString());

		JLabel adresse_label = pane.getLabel("adresse_mairie_label");
		adresse_label.setText(CantineI18n.tr("Address"));
		
		adresse_mairie = (JTextArea) pane.getComponentByName("adresse_mairie");
		adresse_mairie.setText(options.getMairie()[1].toString());

		JLabel cp_label = pane.getLabel("code_postal_mairie_label");
		cp_label.setText(CantineI18n.tr("Zip code"));
		
		code_postal_mairie = (JFormattedTextField) pane.getComponentByName("code_postal_mairie");
		code_postal_mairie.setFormatterFactory(new DefaultFormatterFactory(fmCodePostal));
		if (!options.getMairie()[2].toString().trim().equals("")) {
		code_postal_mairie.setText(options.getMairie()[2].toString());
		try { code_postal_mairie.commitEdit();} catch (ParseException e1) {e1.printStackTrace();}}

		JLabel ville_label = pane.getLabel("ville_mairie_label");
		ville_label.setText(CantineI18n.tr("Town"));
		
		ville_mairie = pane.getTextField("ville_mairie");
		ville_mairie.setText(options.getMairie()[3].toString());

		JLabel phone_label = pane.getLabel("telephone_mairie_label");
		phone_label.setText(CantineI18n.tr("Phone"));
		
		telephone_mairie = (JFormattedTextField) pane.getComponentByName("telephone_mairie");
		telephone_mairie.setFormatterFactory(new DefaultFormatterFactory(fmPhone));
		telephone_mairie.setInputVerifier(new MaskFieldVerifier(
				options.getMairie()[4].toString(), 
				fmPhone.getMask()));

		if (!options.getMairie()[4].toString().trim().equals("")) {
			telephone_mairie.setText(options.getMairie()[4].toString());
		try { telephone_mairie.commitEdit();} catch (ParseException e1) {e1.printStackTrace();}}			

		JLabel fax_label = pane.getLabel("fax_mairie_label");
		fax_label.setText(CantineI18n.tr("Fax"));
		
		fax_mairie = (JFormattedTextField) pane.getComponentByName("fax_mairie");
		fax_mairie.setFormatterFactory(new DefaultFormatterFactory(fmPhone));
		fax_mairie.setInputVerifier(new MaskFieldVerifier(
				options.getMairie()[5].toString(), 
				fmPhone.getMask()));

		if (!options.getMairie()[5].toString().trim().equals("")) {
			fax_mairie.setText(options.getMairie()[5].toString());
		try {fax_mairie.commitEdit();} catch (ParseException e1) {e1.printStackTrace();}}

		JLabel mail_label = pane.getLabel("mail_mairie_label");
		mail_label.setText(CantineI18n.tr("E-Mail"));
		
		email_mairie = pane.getTextField("mail_mairie");
		email_mairie.setText(options.getMairie()[6].toString());
		email_mairie.setInputVerifier(new MailFieldVerifier(options.getMairie()[6].toString()));

		JLabel site_label = pane.getLabel("site_web_label");
		site_label.setText(CantineI18n.tr("Website"));
		
		site_web = pane.getTextField("site_web");
		site_web.setText(options.getMairie()[7].toString());
		
		JLabel logo_label = pane.getLabel("logo_label");
		logo_label.setText(CantineI18n.tr("Logo"));
		
		logo = pane.getTextField("logo");
		logo.setText(options.getMairie()[8].toString());

		// sélection d'un nouveau logo
		open_logo = (JButton) pane.getButton("open_logo");
		open_logo.setToolTipText(CantineI18n.tr("Select your logo"));
		open_logo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File select = null;

				JFileChooser fc = new JFileChooser();
				fc.addChoosableFileFilter(new ImageFilter());
				fc.setAccessory(new ImagePreview(fc));
				fc.showOpenDialog(null);
				select = fc.getSelectedFile();
				if (select != null)
					logo.setText(fc.getSelectedFile().toString());
			}

		});

		JLabel sign_label = pane.getLabel("signataire_label");
		sign_label.setText(CantineI18n.tr("Signatory"));
		
		signataire = pane.getTextField("signataire");
		signataire.setText(options.getMairie()[9].toString());
	}

	private String[] getSaisieMairie() {
		mairie = new String[10];

		mairie[0] = nom_mairie.getText().toString();
		mairie[1] = adresse_mairie.getText().toString();
		mairie[2] = code_postal_mairie.getText().toString();
		mairie[3] = ville_mairie.getText().toString();
		mairie[4] = telephone_mairie.getText().toString();
		mairie[5] = fax_mairie.getText().toString();
		mairie[6] = email_mairie.getText().toString();
		mairie[7] = site_web.getText().toString();
		mairie[8] = logo.getText().toString();
		mairie[9] = signataire.getText();

		return mairie;
	}

	private void buildVacances() {
		JLabel year_label = pane.getLabel("school_year_label");
		year_label.setText(CantineI18n.tr("School year"));
		
		combo_annee = pane.getComboBox("school_year");
		commons.buildCombo(combo_annee, options.getAnnees());

		colonnesConges = new String[5];
		colonnesConges[0] = "id"; 
		colonnesConges[1] = CantineI18n.tr("Year"); 
		colonnesConges[2] = CantineI18n.tr("Name"); 
		colonnesConges[3] = CantineI18n.tr("Begin year"); 
		colonnesConges[4] = CantineI18n.tr("End year"); 
		
		lignesConges = options.getConges();

		table_conges = pane.getTable("vacances");
		table_conges.addMouseListener(new MouseAdapter(){
			public void mousePressed(MouseEvent e){
		  	  	int nModifier = e.getModifiers();
		    	if ((nModifier & InputEvent.BUTTON3_MASK) != 0) {
					table_conges.changeSelection(table_conges.rowAtPoint(e.getPoint()),0,false,false);
					popup.show((Component)e.getSource(),e.getX(),e.getY());
		      	}
			}
		});
		
		popup = new JPopupMenu();
		popupEntries(popup, CantineI18n.tr("New"), "new_conge", Launcher.NEW_IMAGE);
		popup.addSeparator();
		popupEntries(popup, CantineI18n.tr("Delete"), "del_conge", Launcher.DELETE_IMAGE);

		table_conges.add(popup);
		drawCongesTable();
		
		combo_annee.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				JComboBox cb = (JComboBox)e.getSource();
				if(cb.getItemCount()>0){
					options.setAnneeCourante(cb.getSelectedItem().toString());
					lignesConges = options.getConges();
					drawCongesTable();
				}					
			}			
		});
		combo_annee.setSelectedItem(options.getAnneeCourante());
		
		add_school_year = (JButton)pane.getButton("add_school_year");
		add_school_year.setToolTipText(CantineI18n.tr("Add new school year"));
		add_school_year.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				if(MainGui.verifUnicite(NEW_YEAR_NAME,null))
					new AddSchoolYear();
			}
		});
	}

	@Override
	protected void popupActions(ActionEvent event){
		String command = event.getActionCommand();
		//tarifs
		if(command.equals("new_tarif")){
			int t = lignesTarifs.length+1;
			Object[][] memo_lignes = lignesTarifs;
			lignesTarifs = new Object[t][];
			for(int i = 0 ; i < memo_lignes.length ; i++)
				lignesTarifs[i] = memo_lignes[i];
			
			lignesTarifs[t-1] = new Object[]{"-1", new Integer(0), new Float(0)};
			drawTarifsTable();
		}else if(command.equals("del_tarif")){
			Object[] args = {
					new Float(tarifs.getValueAt(tarifs.getSelectedRow(),2).toString()),
					new Integer(tarifs.getValueAt(tarifs.getSelectedRow(),1).toString())
				};
  			int response = JOptionPane.showConfirmDialog(
  					MainGui.desktop, 
  					MessageFormat.format(Messages.getString("window.options.prices.delete.confirm"), args),
  					Messages.getString("window.options.prices.delete.title"),
  					JOptionPane.YES_NO_OPTION,
  					JOptionPane.WARNING_MESSAGE);
  			if (response == JOptionPane.YES_OPTION) {				
				Integer id = new Integer(tarifs.getValueAt(tarifs.getSelectedRow(),0).toString());
				options.supprimeTarif(id);
				lignesTarifs = options.getTarifs();
				drawTarifsTable();
			}			
		}
		//congés
		if(command.equals("new_conge")){
	         int t = lignesConges.length+1;
	         Object[][] memo_lignes = lignesConges;
	         lignesConges = new Object[t][];
	         for(int i = 0; i<memo_lignes.length; i++)
	        	 lignesConges[i] = memo_lignes[i];
	         
	         Date current = Calendar.getInstance().getTime();
	         
	         lignesConges[t-1] = new Object[5];
	         lignesConges[t-1][0] =  "-1"; // id fictif
	         lignesConges[t-1][1] =  combo_annee.getSelectedItem().toString(); // annee
	         lignesConges[t-1][2] =  new String(); // nom
	         lignesConges[t-1][3] =  current; // debut
	         lignesConges[t-1][4] =  current; // fin
	         drawCongesTable();
		}else if(command.equals("del_conge")){
			Object[] args = {
					table_conges.getValueAt(table_conges.getSelectedRow(),2).toString(),
					table_conges.getValueAt(table_conges.getSelectedRow(),1).toString()
			};
  			int response = JOptionPane.showConfirmDialog(
  					MainGui.desktop, 
  					MessageFormat.format(Messages.getString("window.options.holidays.delete.confirm"), args),
  					Messages.getString("window.options.holidays.delete.title"),
  					JOptionPane.YES_NO_OPTION,
  					JOptionPane.WARNING_MESSAGE);
  			if (response == JOptionPane.YES_OPTION) {				
				Integer id = new Integer(table_conges.getValueAt(table_conges.getSelectedRow(),0).toString());
				String cboSelected = combo_annee.getSelectedItem().toString();
				options.supprimeConges(id);
				lignesConges = options.getConges();
				drawCongesTable();
				combo_annee.removeAllItems();
				commons.buildCombo(combo_annee, options.getAnnees());
				combo_annee.setSelectedItem(cboSelected);
			}
  		}
	}	
	
	private void drawCongesTable(){
		table_conges.setModel(new MyTableModel(colonnesConges, lignesConges));
		table_conges.getTableHeader().setReorderingAllowed(false);
		table_conges.getColumnModel().getColumn(0).setMinWidth(0);
		table_conges.getColumnModel().getColumn(0).setMaxWidth(0);
		table_conges.getColumnModel().getColumn(1).setMinWidth(0);
		table_conges.getColumnModel().getColumn(1).setMaxWidth(0);
		table_conges.getColumnModel().getColumn(3).setCellEditor(new JDateEditor());
		table_conges.getColumnModel().getColumn(4).setCellEditor(new JDateEditor());
		table_conges.updateUI();		
	}

	private void drawTarifsTable(){
		tarifs.setModel(new MyTableModel(colonnesTarifs, lignesTarifs));
		tarifs.updateUI();		
		tarifs.getColumnModel().getColumn(0).setPreferredWidth(0);
		tarifs.getColumnModel().getColumn(0).setMinWidth(0);
		tarifs.getColumnModel().getColumn(0).setMaxWidth(0);
		tarifs.getColumnModel().getColumn(1).setPreferredWidth(50);
		tarifs.getColumnModel().getColumn(1).setMinWidth(50);
		tarifs.getColumnModel().getColumn(1).setMaxWidth(50);
		tarifs.getTableHeader().setReorderingAllowed(false);

		final TableCellRenderer defaultNumberRenderer = tarifs.getDefaultRenderer(Number.class);
		final DecimalFormat twoDecimalsFormat = new DecimalFormat("0.00");
		TableCellRenderer twoDecimalsRenderer = new TableCellRenderer() {
			public Component getTableCellRendererComponent(JTable table,
					Object value, boolean isSelected, boolean hasFocus,
					int row, int column) {
				// Format number before sending it two JTable default number
				// renderer
				value = twoDecimalsFormat.format(value);
				return defaultNumberRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);}
		};
		tarifs.getColumnModel().getColumn(2).setCellRenderer(twoDecimalsRenderer);
		tarifs.getColumnModel().getColumn(1).setCellEditor(new IntegerEditor(0, 100));
		tarifs.getColumnModel().getColumn(2).setCellEditor(new FloatEditor());
	}
	
	private void saveValues(boolean close){
		if (champObligatoire()) {
			options.setActivedays(getSelectionDays());
			options.setTarifs(lignesTarifs);
			options.setTraiteur(getSaisieTraiteur());
			options.setMairie(getSaisieMairie());
			options.setHide_closed(getSelectHideClosed());
			options.setConges(lignesConges);
			//Launcher.options = options;
			if(close) dispose();
			if(!close){ //rafraichissement des JTable pour prise en compte des nouveaux enregistrements
				lignesConges = options.getConges();
				drawCongesTable();
				lignesTarifs = options.getTarifs();
				drawTarifsTable();
			}
		}

	}
	
	private boolean champObligatoire() {
		boolean valid = true;
		StringBuffer required = new StringBuffer();
		required.append(CommonsI18n.tr("The following are required:"));

		/* TRAITEUR */
		if (raison_sociale.getText().trim().equalsIgnoreCase("")) {
			required.append("\n- " + CantineI18n.tr("Corporate name"));
			if (valid){raison_sociale.requestFocus();tabs.setSelectedIndex(2);}
			valid = false;
		}
		if (telephone.getText().trim().equalsIgnoreCase("")) {
			required.append("\n- " + CantineI18n.tr("Phone"));
			if (valid){telephone.requestFocus();tabs.setSelectedIndex(2);}
			valid = false;
		}
		if (fax.getText().trim().equalsIgnoreCase("")) {
			required.append("\n- " + CantineI18n.tr("Fax"));
			if (valid){fax.requestFocus();tabs.setSelectedIndex(2);}
			valid = false;
		}
		
		/* MAIRIE */
		if (nom_mairie.getText().trim().equalsIgnoreCase("")) {
			required.append("\n- " + CantineI18n.tr("Name"));
			if (valid){nom_mairie.requestFocus();tabs.setSelectedIndex(3);}
			valid = false;
		}
		if (adresse_mairie.getText().trim().equalsIgnoreCase("")) {
			required.append("\n- " + CantineI18n.tr("Address"));
			if (valid){adresse_mairie.requestFocus();tabs.setSelectedIndex(3);}
			valid = false;
		}
		if (code_postal_mairie.getText().trim().equalsIgnoreCase("")) {
			required.append("\n- " + CantineI18n.tr("Zip code"));
			if (valid) code_postal_mairie.requestFocus();tabs.setSelectedIndex(3);
			valid = false;
		}
		if (ville_mairie.getText().trim().equalsIgnoreCase("")) {
			required.append("\n- " + CantineI18n.tr("Town"));
			if (valid){ville_mairie.requestFocus();tabs.setSelectedIndex(3);}
			valid = false;
		}
		if (telephone_mairie.getText().trim().equalsIgnoreCase("")) {
			required.append("\n- " + CantineI18n.tr("Phone"));
			if (valid){telephone_mairie.requestFocus();tabs.setSelectedIndex(3);}
			valid = false;
		}
		if (fax_mairie.getText().trim().equalsIgnoreCase("")) {
			required.append("\n- " + CantineI18n.tr("Fax"));
			if (valid){fax_mairie.requestFocus();tabs.setSelectedIndex(3);}
			valid = false;
		}
		if (email_mairie.getText().trim().equalsIgnoreCase("")) {
			required.append("\n- " + CantineI18n.tr("E-Mail"));
			if (valid) email_mairie.requestFocus();
			valid = false;
		}
		if (logo.getText().trim().equalsIgnoreCase("")) {
			required.append("\n- " + CantineI18n.tr("Logo"));
			if (valid){logo.requestFocus();tabs.setSelectedIndex(3);}
			valid = false;
		}
		if (signataire.getText().trim().equalsIgnoreCase("")) {
			required.append("\n- " + CantineI18n.tr("Signatory"));
			if (valid){signataire.requestFocus();tabs.setSelectedIndex(3);}
			valid = false;
		}

		if (!valid) {
			JOptionPane.showMessageDialog(
					MainGui.desktop, 
					required.toString(),
					CommonsI18n.tr("Error!"),
					JOptionPane.ERROR_MESSAGE);
		}

		return valid;
	}

	class DateFocus implements FocusListener{
		/**
		 * @param e
		 */
		public void focusGained(FocusEvent e) {
			try{
				table_conges.getCellEditor().stopCellEditing();
				throw new Exception();
			}catch(Exception ex){/*si la table n'est pas sélectionnée...*/}
		}
		/**
		 * @param e
		 */
		public void focusLost(FocusEvent e) {}		
	}
	
	class MyTableModel extends AbstractTableModel {
		private static final long serialVersionUID = -6563418675769600827L;

		private String[] columnNames;

		private Object[][] data;

		MyTableModel(String[] colNames, Object[][] rowsDatas) {
			this.columnNames = colNames;
			this.data = rowsDatas;
		}
		
		/**
		 * @return int
		 */
		public int getColumnCount() {
			return columnNames.length;
		}

		/**
		 * @return int
		 */
		public int getRowCount() {
			return data.length;
		}

		/**
		 * @param col
		 * @return String
		 */
		public String getColumnName(int col) {
			return columnNames[col];
		}

		/**
		 * @param row
		 * @param col
		 * @return Object
		 */
		public Object getValueAt(int row, int col) {
			return data[row][col];
		}

		/**
		 * @param c
		 * @return Class
		 */
		public Class<?> getColumnClass(int c) {
			return getValueAt(0, c).getClass();
		}

		/**
		 * @param row
		 * @param col
		 * @return boolean
		 */
		public boolean isCellEditable(int row, int col) {
			return true;
		}

		/**
		 * @param value
		 * @param row
		 * @param col
		 */
		public void setValueAt(Object value, int row, int col) {
			if (DEBUG) {
				System.out.println("Setting value at " + row + "," + col
						+ " to " + value + " (an instance of "
						+ value.getClass() + ")");
			}

			data[row][col] = value;
			fireTableCellUpdated(row, col);

			if (DEBUG) {
				System.out.println("New value of data:");
				printDebugData();
			}
		}

		private void printDebugData() {
			int numRows = getRowCount();
			int numCols = getColumnCount();

			for (int i = 0; i < numRows; i++) {
				System.out.print("    row " + i + ":");
				for (int j = 0; j < numCols; j++) {
					System.out.print("  " + data[i][j]);
				}
				System.out.println();
			}
			System.out.println("--------------------------");
		}

	}

	/**
	 * Installe un JDateChooser pour l'édition des valeurs des cellules de date.
	 * 
	 * @author Johan Cwiklinski
	 *
	 */
    public class JDateEditor extends AbstractCellEditor implements TableCellEditor {
		private static final long serialVersionUID = 5679468972753787775L;
        JComponent component = new JDateChooser();
    
        /**
         * Appelé quand la valeur d'une cellule est éditée par l'utilisateur
         * 
         * @param table
         * @param value
         * @param isSelected 
         * @param rowIndex
         * @param vColIndex
         * @return Component
         */
        public Component getTableCellEditorComponent(JTable table, Object value,
                boolean isSelected, int rowIndex, int vColIndex) {
        
            ((JDateChooser)component).setDate((Date)value);
            ((JDateChooser)component).setDateFormatString("dd MMM yyyy");

            return component;
        }
    
        /**
         * Appelée quand l'édition est terminée.
         * 
         * @return nouvelle valeur stockée dans la cellule
         */
        public Object getCellEditorValue() {
            return ((JDateChooser)component).getDate();
        }
    }
    
    class AddSchoolYear extends EscapeInternalFrame{
		private static final long serialVersionUID = -4904011190060329010L;
		private JYearChooser begin, end;

		/**
		 * 
		 *
		 */
		public AddSchoolYear(){
			super(null, false, true, false, false);
			super.setName(NEW_YEAR_NAME);

			putClientProperty(PlasticInternalFrameUI.IS_PALETTE, Boolean.TRUE);
			
			pane = new FormPanel("be/xtnd/cantine/gui/descriptions/ajout_annee_conges.jfrm");
			
			TitledSeparator title = (TitledSeparator)pane.getComponentByName("title");
			title.setText(CantineI18n.tr("School year"));
			
			String lastValue = combo_annee.getItemAt(combo_annee.getItemCount()-1).toString(); 
			
			int initial = new Integer(lastValue.substring(5,9));
			
			JLabel debut_label = pane.getLabel("debut_label");
			debut_label.setText(CantineI18n.tr("Begin year"));
			
			begin = (JYearChooser)pane.getComponentByName("debut");
			begin.setYear(initial);
			
			JLabel fin_label = pane.getLabel("fin_label");
			fin_label.setText(CantineI18n.tr("End year"));

			end = (JYearChooser)pane.getComponentByName("fin");
			end.setYear(initial+1);

			commons.createButtonBar(this, new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					String new_year = begin.getYear()+"-"+end.getYear();
					if(options.isNewYear(new_year)){
						combo_annee.addItem(new_year);
						combo_annee.setSelectedItem(new_year);
						options.setAnneeCourante(combo_annee.getSelectedItem().toString());
						lignesConges = options.getConges();
						drawCongesTable();
				         int t = lignesConges.length+1;
				         Object[][] memo_lignes = lignesConges;
				         lignesConges = new Object[t][];
				         for(int i = 0; i<memo_lignes.length; i++)
				        	 lignesConges[i] = memo_lignes[i];
				         
				         Date current = Calendar.getInstance().getTime();
				         
				         lignesConges[t-1] = new Object[5];
				         lignesConges[t-1][0] =  "-1"; // id fictif
				         lignesConges[t-1][1] =  combo_annee.getSelectedItem().toString(); // annee
				         lignesConges[t-1][2] =  new String(); // nom
				         lignesConges[t-1][3] =  current; // debut
				         lignesConges[t-1][4] =  current; // fin
				         drawCongesTable();
					}else{
						combo_annee.setSelectedItem(new_year);
						options.setAnneeCourante(combo_annee.getSelectedItem().toString());
						lignesConges = options.getConges();
						drawCongesTable();
					}
					dispose();
				}}
			);
			
			add(pane);
			pack();

			setLocation(MainGui.centerOnDesktop(this.getSize()));
			MainGui.desktop.add(this, JLayeredPane.DRAG_LAYER);
			setVisible(true);
			try {
				setSelected(true);
			} catch (PropertyVetoException e1) {e1.printStackTrace();}
		}
    }
}

