/*
 * Presences.java, 15 sept. 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               	Presences.java
 * Author's email :     	johan@x-tnd.be
 * Author's Website :   	http://cantine.sourceforge.net
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.MessageFormat;

import javax.swing.JCheckBox;
import javax.swing.JLayeredPane;

import be.xtnd.cantine.CantineI18n;
import be.xtnd.cantine.Eleve;
import be.xtnd.cantine.Presence;
import be.xtnd.commons.GuiCommons;
import be.xtnd.commons.gui.EscapeInternalFrame;
import be.xtnd.commons.gui.MainGui;

import com.jeta.forms.components.panel.FormPanel;
import com.jeta.forms.components.separator.TitledSeparator;
import com.jgoodies.looks.plastic.PlasticInternalFrameUI;

/**
 * 
 * 
 * @author Johan Cwiklinski
 *
 */
public class Presences extends EscapeInternalFrame{
	private static final long serialVersionUID = 3697288799039066499L;
		private TitledSeparator title;
		private JCheckBox cantine_lundi, cantine_mardi, cantine_mercredi, cantine_jeudi, cantine_vendredi,
			cantine_samedi, cantine_dimanche;
		private Presence presence;
		private Eleve eleve;
		private GuiCommons commons = new GuiCommons();
		
		/**
		 * 
		 * @param el
		 */
		public Presences(Eleve el){
			super();
			this.eleve = el;
	        putClientProperty(
	                PlasticInternalFrameUI.IS_PALETTE,
	                Boolean.TRUE);
			FormPanel pane = new FormPanel( "be/xtnd/cantine/gui/descriptions/presences.jfrm" );
			getContentPane().add(pane);
			
			Object[] titleArgs = {eleve.getNom()+" "+eleve.getPrenom()};
			title = (TitledSeparator)pane.getComponentByName("titre");
			title.setText(MessageFormat.format(Messages.getString("window.presence.cantine"), titleArgs));

			presence = el.getPresence_cantine();

			cantine_lundi = pane.getCheckBox("cantine_lundi");
			cantine_lundi.setText(CantineI18n.tr("Monday"));
			if(!Launcher.options.getActiveDays()[0]){
				cantine_lundi.setEnabled(eleve.isCantine());
			}else{cantine_lundi.setEnabled(false);}
			cantine_lundi.setSelected(presence.isLundi());
			
			cantine_mardi = pane.getCheckBox("cantine_mardi");
			cantine_mardi.setText(CantineI18n.tr("Tuesday"));
			if(!Launcher.options.getActiveDays()[1]){
				cantine_mardi.setEnabled(eleve.isCantine());
			}else{cantine_mardi.setEnabled(false);}
			cantine_mardi.setSelected(presence.isMardi());
			
			cantine_mercredi = pane.getCheckBox("cantine_mercredi");
			cantine_mercredi.setText(CantineI18n.tr("Wednesday"));
			if(!Launcher.options.getActiveDays()[2]){
				cantine_mercredi.setEnabled(eleve.isCantine());
			}else{cantine_mercredi.setEnabled(false);}
			cantine_mercredi.setSelected(presence.isMercredi());

			cantine_jeudi = pane.getCheckBox("cantine_jeudi");
			cantine_jeudi.setText(CantineI18n.tr("Thursday"));
			if(!Launcher.options.getActiveDays()[3]){
				cantine_jeudi.setEnabled(eleve.isCantine());
			}else{cantine_jeudi.setEnabled(false);}
			cantine_jeudi.setSelected(presence.isJeudi());

			cantine_vendredi = pane.getCheckBox("cantine_vendredi");
			cantine_vendredi.setText(CantineI18n.tr("Friday"));
			if(!Launcher.options.getActiveDays()[4]){
				cantine_vendredi.setEnabled(eleve.isCantine());
			}else{cantine_vendredi.setEnabled(false);}
			cantine_vendredi.setSelected(presence.isVendredi());

			cantine_samedi = pane.getCheckBox("cantine_samedi");
			cantine_samedi.setText(CantineI18n.tr("Saturday"));
			if(!Launcher.options.getActiveDays()[5]){
				cantine_samedi.setEnabled(eleve.isCantine());
			}else{cantine_samedi.setEnabled(false);}
			cantine_samedi.setSelected(presence.isSamedi());
			
			cantine_dimanche = pane.getCheckBox("cantine_dimanche");
			cantine_dimanche.setText(CantineI18n.tr("Sunday"));
			if(!Launcher.options.getActiveDays()[6]){
				cantine_dimanche.setEnabled(eleve.isCantine());
			}else{cantine_dimanche.setEnabled(false);}
			cantine_dimanche.setSelected(presence.isDimanche());
						
			commons.createButtonBar(this, new OkayEvent());
			
			pack();
			
			Object[] nameArgs = {eleve.getId_eleve()};
			setName(MessageFormat.format(Messages.getString("window.presence.name"), nameArgs));
			
			MainGui.desktop.add(this, JLayeredPane.POPUP_LAYER);
			setLocation(MainGui.centerOnDesktop(getSize()));
			
			setClosable(true);
			setVisible(true);			
		}
		
		class OkayEvent implements ActionListener{
			/**
			 * @param e
			 */
			public void actionPerformed(ActionEvent e) {
				if(eleve.isCantine()){
					presence.setLundi(cantine_lundi.isSelected());
					presence.setMardi(cantine_mardi.isSelected());
					presence.setMercredi(cantine_mercredi.isSelected());
					presence.setJeudi(cantine_jeudi.isSelected());
					presence.setVendredi(cantine_vendredi.isSelected());
					presence.setSamedi(cantine_samedi.isSelected());
				}				
				dispose();
			}		
		}
}
