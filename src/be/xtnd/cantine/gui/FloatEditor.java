/*
 * FloatEditor.java, 2005-09-28
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               	FloatEditor.java
 * Author's email :     	johan@x-tnd.be
 * Author's Website :   	http://cantine.ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine.gui;

import java.awt.Component;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.NumberFormat;
import java.text.ParseException;

import javax.swing.AbstractAction;
import javax.swing.DefaultCellEditor;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import be.xtnd.cantine.CantineI18n;
import be.xtnd.commons.i18n.CommonsI18n;


/**
 * Implements a cell editor that uses a formatted text field
 * to edit Float values.
 * 
 * @author Johan Cwiklinski
 * @since 2005-09-28
 */
public class FloatEditor extends DefaultCellEditor {
	private static final long serialVersionUID = 4688110429484736011L;
	JFormattedTextField ftf;
    NumberFormat integerFormat;
    private Float initValue;
    private boolean DEBUG = false;

    /**
     * Default constructor
     */
    public FloatEditor() {
        super(new JFormattedTextField());
        ftf = (JFormattedTextField)getComponent();

        //Set up the editor for the integer cells.
        integerFormat = NumberFormat.getNumberInstance();
        NumberFormatter intFormatter = new NumberFormatter(integerFormat);
        intFormatter.setFormat(integerFormat);
        
        ftf.setFormatterFactory(
                new DefaultFormatterFactory(intFormatter));
        ftf.setHorizontalAlignment(JTextField.TRAILING);
        ftf.setFocusLostBehavior(JFormattedTextField.PERSIST);

        //React when the user presses Enter while the editor is
        //active.  (Tab is handled as specified by
        //JFormattedTextField's focusLostBehavior property.)
        ftf.getInputMap().put(KeyStroke.getKeyStroke(
                                        KeyEvent.VK_ENTER, 0),
                                        "check");
        ftf.getActionMap().put("check", new AbstractAction() {
			private static final long serialVersionUID = 2539154902504377171L;

			public void actionPerformed(ActionEvent e) {
            	if (!ftf.isEditValid()) { //The text is invalid.
                    if (userSaysRevert()) { //reverted
                    	ftf.postActionEvent(); //inform the editor
                    }
                } else try {              //The text is valid,
                    ftf.commitEdit();     //so use it.
                    ftf.postActionEvent(); //stop editing
                } catch (java.text.ParseException exc) { }
            }
        });
    }

    /**
     * Override to invoke setValue on the formatted text field.
     * @param table
     * @param value
     * @param isSelected
     * @param row
     * @param column
     * @return Component
     */
    @Override
    public Component getTableCellEditorComponent(JTable table,
            Object value, boolean isSelected,
            int row, int column) {
        JFormattedTextField ftf =
            (JFormattedTextField)super.getTableCellEditorComponent(
                table, value, isSelected, row, column);
        System.out.println("value : "+value+", "+value.getClass());
        initValue = new Float(value.toString());
        ftf.setValue(value);
        return ftf;
    }

    /**
     * Override to ensure that the value remains an Integer.
     * 
     * @return Object
     */
    @Override
    public Object getCellEditorValue() {
        JFormattedTextField ftf = (JFormattedTextField)getComponent();
        Object o = ftf.getValue();
        System.err.println(ftf.getValue()+", "+ftf.getValue().getClass());
        if (o instanceof Number) {
       		return new Float(Float.parseFloat(o.toString()));
        } else {
            if (DEBUG) {
                System.out.println("getCellEditorValue: o isn't a Number");
            }
            try {
                return integerFormat.parseObject(o.toString());
            } catch (ParseException exc) {
                System.err.println("getCellEditorValue: can't parse o: " + o);
                return null;
            }
        }
    }

    /**
     * Override to check whether the edit is valid,
     * setting the value if it is and complaining if
     * it isn't.  If it's OK for the editor to go
     * away, we need to invoke the superclass's version 
     * of this method so that everything gets cleaned up.
     * 
     * @return boolean
     */
    @Override
    public boolean stopCellEditing() {
        JFormattedTextField ftf = (JFormattedTextField)getComponent();
        if (ftf.isEditValid()) {
        	if(ftf.getValue().getClass()==Long.class){
                if (!userSaysRevert()) { //user wants to edit
        	        return false; //don't let the editor go away
                }
                ftf.setValue(initValue);
        	}else{
	            try {
	                ftf.commitEdit();
	            } catch (java.text.ParseException exc) { }
        	}
        } else { //text is invalid
            if (!userSaysRevert()) { //user wants to edit
	        return false; //don't let the editor go away
	    } 
        }
        return super.stopCellEditing();
    }

    /** 
     * Lets the user know that the text they entered is 
     * bad. Returns true if the user elects to revert to
     * the last good value.  Otherwise, returns false, 
     * indicating that the user wants to continue editing.
     * 
     * @return boolean
     */
    protected boolean userSaysRevert() {
        Toolkit.getDefaultToolkit().beep();
        ftf.selectAll();
        Object[] options = {
        		CantineI18n.tr("Edit"),
				CantineI18n.tr("Cancel keyboarding")
		};
        
        int answer = JOptionPane.showOptionDialog(
            SwingUtilities.getWindowAncestor(ftf),
            Messages.getString("window.messages.error.float"),
            CommonsI18n.tr("Error!"),
            JOptionPane.YES_NO_OPTION,
            JOptionPane.ERROR_MESSAGE,
            null,
            options,
            options[1]);
	    
        if (answer == 1) { //Revert!
            ftf.setValue(ftf.getValue());
	    return true;
        }
	return false;
    }
}
