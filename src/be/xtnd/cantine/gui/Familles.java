/*
 * Familles.java, 12 août 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               	Familles.java
 * Author's email :     	johan@x-tnd.be
 * Author's Website :   	http://www.x-tnd.be/
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.TableColumnModel;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

import be.xtnd.cantine.CantineI18n;
import be.xtnd.cantine.Classe;
import be.xtnd.cantine.Eleve;
import be.xtnd.cantine.Famille;
import be.xtnd.cantine.Mere;
import be.xtnd.cantine.Pere;
import be.xtnd.cantine.Role;
import be.xtnd.cantine.Tiers;
import be.xtnd.cantine.Urgence;
import be.xtnd.cantine.Villes;
import be.xtnd.commons.GuiCommons;
import be.xtnd.commons.db.Database;
import be.xtnd.commons.db.ResultSetTableModelFactory;
import be.xtnd.commons.gui.EscapeInternalFrame;
import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.gui.TableSorter;
import be.xtnd.commons.i18n.CommonsI18n;
import be.xtnd.thirdparty.AutoCompletion;
import be.xtnd.validators.MailFieldVerifier;
import be.xtnd.validators.MaskFieldVerifier;

import com.jeta.forms.components.border.TitledBorderLabel;
import com.jeta.forms.components.panel.FormPanel;
import com.jeta.forms.components.separator.TitledSeparator;
import com.toedter.calendar.JDateChooser;

/**
 * 
 *
 * @author Johan Cwiklinski
 */
public class Familles{
	/** */
	public Famille famille; 
	private Familles familles;
	private Urgence medecin;
	private Urgence hopital;
	private GuiCommons commons = new GuiCommons();
	private ResultSetTableModelFactory factory; 
	private JTable table;
	/** Nom de la fenêtre de liste */
	public static final String LIST_NAME = "liste_familles";
	/** Nom de la fenêtre nouveau */
	public static final String NEW_NAME = "nouvelle_famille";
	private char mode;
	/* DONNEES ELEVES */
	private JTextField[] nom_eleve, prenom_eleve, lieu_naissance_eleve;
	private JComboBox[] classe;
	private JTextArea[] observations_eleve, allergies_eleve;
	private JRadioButton[] eleve_g, eleve_f;
	private JCheckBox[] cantine;
	private JDateChooser[] date_naissance;
	private JButton[] presence_eleve;
	/* TABBED PANES */
	JTabbedPane tiers_tabs, children_tabs;
	private JButton ajouter_parent, supprimer_parent, ajouter_enfant, supprimer_enfant;
	/* DONNEES TIERS */
	private JTextField[] tiers_nom, tiers_prenom, tiers_mail, tiers_profession, tiers_employeur;
	private JComboBox[] roles;
	private JFormattedTextField[] tiers_gsm, tiers_tel_pro, tiers_poste_tel_pro;
	private JTextArea[] tiers_commentaires;
	private MaskFormatter fmPhone, fmPoste;
	
	/**
	 * 
	 *
	 */
	public Familles(){}
	
	/**
	 * 
	 *
	 */
	public void afficheListe(){
		new Liste();
	}
	
	/**
	 * 
	 *
	 */
	public void nouvelleFamille(){
		this.famille = new Famille();
		this.medecin = new Urgence(Urgence.MEDECIN_TYPE);
		this.hopital = new Urgence(Urgence.HOPITAUX_TYPE);
		familles = this;
		new Fenetre(Database.NEW);
	}
	
	/**
	 * 
	 * @param i
	 */
	public void modifieFamille(Integer i){
		this.famille = new Famille(i);	
		this.medecin = new Urgence(Urgence.MEDECIN_TYPE, famille.getId_medecin());
		this.hopital = new Urgence(Urgence.HOPITAUX_TYPE, famille.getId_hopital());
		familles = this;
		new Fenetre(Database.MODIF);
	}

	/**
	 * 
	 *
	 */
	public void enfantsTab(){
		int size=1;
		size = famille.getEnfants().size();
		children_tabs.removeAll();
		
		if(size == 0){
			supprimer_enfant.setEnabled(false);
		}else if(!supprimer_enfant.isEnabled()) supprimer_enfant.setEnabled(true);

		nom_eleve = new JTextField[size];
		prenom_eleve = new JTextField[size];
		date_naissance = new JDateChooser[size];
		lieu_naissance_eleve = new JTextField[size];
		classe = new JComboBox[size];
		eleve_g = new JRadioButton[size];
		eleve_f = new JRadioButton[size];
		cantine = new JCheckBox[size];
		observations_eleve = new JTextArea[size];
		allergies_eleve = new JTextArea[size];
		presence_eleve = new JButton[size];

		for(int i=0; i<size; i++){
			JLabel classe_label, nom_enfant_label, prenom_enfant_label, sexe_enfant_label,
			date_naissance_label, lieu_naissance_label, allergies_label, observations_label;
			TitledBorderLabel reservation_label;
			TitledSeparator naissance_label, divers_label;

			final Eleve el = famille.getEnfants().get(i);
			final Integer id_el = el.getId_eleve();

			FormPanel elevesPane = new FormPanel( "be/xtnd/cantine/gui/descriptions/enfants.jfrm" );

			reservation_label = (TitledBorderLabel)elevesPane.getComponentByName("reservation_label");
			reservation_label.setText(Messages.getString("window.eleve.labels.reservation"));
			
			naissance_label = (TitledSeparator)elevesPane.getComponentByName("naissance_label");
			naissance_label.setText(Messages.getString("window.eleve.labels.birth"));

			divers_label = (TitledSeparator)elevesPane.getComponentByName("divers_label");
			divers_label.setText(Messages.getString("window.eleve.labels.divers"));

			nom_enfant_label = elevesPane.getLabel("nom_enfant_label");
			nom_enfant_label.setText(CantineI18n.tr("Name"));

			nom_eleve[i] = elevesPane.getTextField("nom_enfant");
			nom_eleve[i].setText(el.getNom());
			
			prenom_enfant_label = elevesPane.getLabel("prenom_enfant_label");
			prenom_enfant_label.setText(CantineI18n.tr("First name"));

			prenom_eleve[i] = elevesPane.getTextField("prenom_enfant");
			prenom_eleve[i].setText(el.getPrenom());
			
			date_naissance_label = elevesPane.getLabel("date_naissance_label");
			date_naissance_label.setText(Messages.getString("window.eleve.labels.birthdate"));

			date_naissance[i] = (JDateChooser)elevesPane.getComponentByName("date_naissance");
			date_naissance[i].setDate(el.getDate_naissance());
			
			lieu_naissance_label = elevesPane.getLabel("lieu_naissance_label");
			lieu_naissance_label.setText(Messages.getString("window.eleve.labels.birthplace"));

			lieu_naissance_eleve[i] = elevesPane.getTextField("lieu_naissance");
			lieu_naissance_eleve[i].setText(el.getLieu_naissance());
			
			classe_label = elevesPane.getLabel("classe_label");
			classe_label.setText(Messages.getString("window.eleve.labels.classe"));
			
			classe[i] = elevesPane.getComboBox("classe");
			commons.buildCombo(classe[i], Classe.listing());
			classe[i].setSelectedItem(el.getClasse().getClasse());
			if(classe[i].getSelectedIndex()<1){classe[i].setSelectedIndex(0);}
			
			sexe_enfant_label = elevesPane.getLabel("sexe_enfant_label");
			sexe_enfant_label.setText(Messages.getString("window.eleve.labels.sex"));				

			char sexe = el.getSexe();
			
			eleve_g[i] = elevesPane.getRadioButton("garcon_radio");
			eleve_g[i].setText(Messages.getString("window.eleve.labels.boy"));
			if(sexe=='G') eleve_g[i].setSelected(true);
			
			eleve_f[i] = elevesPane.getRadioButton("fille_radio");
			eleve_f[i].setText(Messages.getString("window.eleve.labels.girl"));
			if(sexe=='F') eleve_f[i].setSelected(true);
			
			cantine[i] = elevesPane.getCheckBox("cantine");
			cantine[i].setText(Messages.getString("window.eleve.labels.cantine"));
			cantine[i].setSelected(el.isCantine());
			cantine[i].addItemListener(new ItemListener(){
				public void itemStateChanged(ItemEvent e) {
					el.setCantine((e.getStateChange() == ItemEvent.SELECTED)?true:false);
				}
			});
			
			presence_eleve[i] = (JButton)elevesPane.getButton("presence");
			presence_eleve[i].setText(Messages.getString("window.eleve.labels.presence"));
            presence_eleve[i].addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent e) {
                	Object[] args = {id_el};
                	if(MainGui.verifUnicite(
                			MessageFormat.format(Messages.getString("window.presence.name"), args),
							null)){
	                	new Presences(famille.getEnfants().get(children_tabs.getSelectedIndex()));	                		
                	}
                }
            });			
            
    		observations_label = elevesPane.getLabel("observations_label");
    		observations_label.setText(Messages.getString("window.eleve.labels.observations"));	    		

			observations_eleve[i] = (JTextArea)elevesPane.getComponentByName("observations");
			observations_eleve[i].setText(el.getObservations());
			
			allergies_label = elevesPane.getLabel("allergies_label");
			allergies_label.setText(Messages.getString("window.eleve.labels.allergies"));
			
			allergies_eleve[i] = (JTextArea)elevesPane.getComponentByName("allergies");
			allergies_eleve[i].setText(el.getAllergies());
			
        	children_tabs.addTab(el.getPrenom(),elevesPane);
		}
	}
	
	/**
	 * 
	 *
	 */
	public void tiersTab(){	
		int size=1;
		size = famille.getTiers().size();	
		tiers_tabs.removeAll();
		
		if(size == 0){
			supprimer_parent.setEnabled(false);
		}else if(!supprimer_parent.isEnabled()) supprimer_parent.setEnabled(true);
		
		tiers_nom = new JTextField[size];
		tiers_prenom = new JTextField[size];
		tiers_gsm = new JFormattedTextField[size];
		tiers_mail = new JTextField[size];
		tiers_profession = new JTextField[size];
		tiers_employeur = new JTextField[size];
		tiers_tel_pro = new JFormattedTextField[size];
		tiers_poste_tel_pro = new JFormattedTextField[size];
		tiers_commentaires = new JTextArea[size];
		roles = new JComboBox[size];

		for(int i=0 ; i<size ; i++){
			JLabel nom_label, prenom_label, tel_portable_label, mail_label, profession_label,
				employeur_label, tel_pro_label, poste_tel_pro_label, commentaires_label, role_label;
			final int current = i;

			Tiers t;
			t = famille.getTiers().get(i);
			FormPanel tiersPane = new FormPanel( "be/xtnd/cantine/gui/descriptions/parents.jfrm" );

			JButton ajout_role = (JButton)tiersPane.getButton("ajout_role");
			ajout_role.setEnabled(false);
			ajout_role.setVisible(false);
			
			role_label = tiersPane.getLabel("role_label");
			role_label.setText(Messages.getString("window.roles.labels.role"));
			
			roles[i] = tiersPane.getComboBox("roles");
			commons.buildCombo(roles[i], Role.listing());
			roles[i].removeItemAt(0);
			roles[i].setSelectedItem(t.getRoleName());
			roles[i].addItemListener(new ItemListener(){
				public void itemStateChanged(ItemEvent e) {
					tiers_tabs.setTitleAt(tiers_tabs.getSelectedIndex(), roles[current].getSelectedItem().toString());
					famille.getTiers().get(current).setRole(roles[current].getSelectedItem().toString());					
				}
			});
			
			nom_label = tiersPane.getLabel("nom_label");
			nom_label.setText(CantineI18n.tr("Name"));

			tiers_nom[i] = tiersPane.getTextField("nom");
			tiers_nom[i].setText(t.getNom());
			
			prenom_label = tiersPane.getLabel("prenom_label");
			prenom_label.setText(CantineI18n.tr("First name"));

			tiers_prenom[i] = tiersPane.getTextField("prenom");
			tiers_prenom[i].setText(t.getPrenom());
			
			tel_portable_label = tiersPane.getLabel("tel_portable_label");
			tel_portable_label.setText(CantineI18n.tr("Mobile phone"));

			tiers_gsm[i] = (JFormattedTextField)tiersPane.getComponentByName("tel_portable");
			tiers_gsm[i].setFormatterFactory(new DefaultFormatterFactory(fmPhone));
			tiers_gsm[i].setInputVerifier(new MaskFieldVerifier(t.getTel_portable(), fmPhone.getMask()));
			if(!t.getTel_portable().trim().equals("")){
				tiers_gsm[i].setText(t.getTel_portable());
				try {
					tiers_gsm[i].commitEdit();
				} catch (ParseException e) {e.printStackTrace();}
			}
			
			mail_label = tiersPane.getLabel("mail_label");
			mail_label.setText(CantineI18n.tr("E-Mail"));

			tiers_mail[i] = tiersPane.getTextField("mail");
			tiers_mail[i].setText(t.getMail());
			tiers_mail[i].setInputVerifier(new MailFieldVerifier(t.getMail()));
			
			profession_label = tiersPane.getLabel("profession_label");
			profession_label.setText(Messages.getString("window.tiers.labels.profession"));

			tiers_profession[i] = tiersPane.getTextField("profession");
			tiers_profession[i].setText(t.getProfession());
			
			employeur_label = tiersPane.getLabel("employeur_label");
			employeur_label.setText(Messages.getString("window.tiers.labels.employer"));

			tiers_employeur[i] = tiersPane.getTextField("employeur");
			tiers_employeur[i].setText(t.getEmployeur());
			
			tel_pro_label = tiersPane.getLabel("tel_pro_label");
			tel_pro_label.setText(CantineI18n.tr("Phone"));

			tiers_tel_pro[i] = (JFormattedTextField)tiersPane.getComponentByName("tel_pro");
			tiers_tel_pro[i].setFormatterFactory(new DefaultFormatterFactory(fmPhone));
			tiers_tel_pro[i].setInputVerifier(new MaskFieldVerifier(t.getTel_travail(), fmPhone.getMask()));
			if(!t.getTel_travail().trim().equals("")){
				tiers_tel_pro[i].setText(t.getTel_travail());
				try {
					tiers_tel_pro[i].commitEdit();
				} catch (ParseException e2) {e2.printStackTrace();}
			}
			
			poste_tel_pro_label = tiersPane.getLabel("poste_tel_pro_label");
			poste_tel_pro_label.setText(Messages.getString("window.tiers.labels.pro_poste"));

			try {
				fmPoste = new MaskFormatter("*******");
				fmPoste.setValidCharacters("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
			} catch (ParseException e1) {e1.printStackTrace();}
			
			tiers_poste_tel_pro[i] = (JFormattedTextField)tiersPane.getComponentByName("poste_tel_pro");
			fmPoste.install(tiers_poste_tel_pro[i]);
			tiers_poste_tel_pro[i].setText(t.getPoste_tel_travail());
			if(!tiers_poste_tel_pro[i].getText().trim().equals("")){
				try {
					tiers_poste_tel_pro[i].commitEdit();
				} catch (ParseException e3) {e3.printStackTrace();}
			}
			
			commentaires_label = tiersPane.getLabel("commentaires_label");
			commentaires_label.setText(CantineI18n.tr("Comments"));

			tiers_commentaires[i] = (JTextArea)tiersPane.getComponentByName("commentaires");
			tiers_commentaires[i].setText(t.getCommentaires());
			
        	tiers_tabs.addTab(famille.getTiers().get(i).getRoleName() ,tiersPane);
		}
	}

	private class Liste extends EscapeInternalFrame{
		private static final long serialVersionUID = 1138306166236248944L;
		private Integer idEncours;
		private JPopupMenu popup;
		private TitledSeparator title;
		private JLabel nom_cherche_label, villes_label;
		private JTextField nom_cherche;
		private JComboBox villes;
		private JButton search, reinit, add;

		/**
		 * 
		 *
		 */
		public Liste(){
			this(new ResultSetTableModelFactory(Launcher.db));

			setIconifiable(false);
			setClosable(true);
			setResizable(true);
			setMaximizable(true);
			setName(LIST_NAME);
			setSize(700, 500);
			setLocation(MainGui.centerOnDesktop(getSize()));
			setVisible(true);
			MainGui.desktop.add(this);	
		}
		
		/**
		 * 
		 * @param f
		 */
		public Liste(ResultSetTableModelFactory f) {
			setTitle(Messages.getString("window.famille.titles.list"));

			factory = f;
			table = new JTable();
			table.getTableHeader().setReorderingAllowed(false);
			table.addMouseListener(new MouseAdapter(){
				public void mousePressed(MouseEvent e){
			  	  	int nModifier = e.getModifiers();
					idEncours = (Integer)table.getValueAt(table.rowAtPoint(e.getPoint()),0);
			    	if(e.getClickCount()==2){//double click
						Object[] args = {idEncours};
						if(Launcher.verifUnicite(MessageFormat.format(Messages.getString("window.famille.names.modif"), args),null)){
							modifieFamille(idEncours);
						}
			    	}
			    	if ((nModifier & InputEvent.BUTTON3_MASK) != 0) {
						table.changeSelection(table.rowAtPoint(e.getPoint()),0,false,false);
						popup.show((Component)e.getSource(),e.getX(),e.getY());
			      	}
				}
			});
			
			Container contentPane = getContentPane();
			
			FormPanel pane = new FormPanel( "be/xtnd/cantine/gui/descriptions/header_list_families.jfrm" );
			
			title = (TitledSeparator)pane.getComponentByName("title");
			title.setText(Messages.getString("window.commons.quick_search"));
			
			nom_cherche_label = pane.getLabel("nom_cherche_label");
			nom_cherche_label.setText(CantineI18n.tr("Name"));
			
			nom_cherche = pane.getTextField("nom_cherche");
			
			villes_label = pane.getLabel("villes_label");
			villes_label.setText(CantineI18n.tr("Town"));
			
			villes = pane.getComboBox("villes");
			commons.buildCombo(villes, Villes.liste(false));
			AutoCompletion.enable(villes);
			
			search = (JButton)pane.getButton("search");
			search.setText(Messages.getString("window.commons.perform_search"));
			search.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					buildSearchQuery();
				}
			});
			getRootPane().setDefaultButton(search);
			
			reinit = (JButton)pane.getButton("reinit");
			reinit.setToolTipText(Messages.getString("window.famille.tooltips.reinit"));
			reinit.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					nom_cherche.setText("");
					villes.setSelectedIndex(0);
					buildSearchQuery();
				}
			});
			
			add = (JButton)pane.getButton("add");
			add.setToolTipText(Messages.getString("window.famille.titles.new"));
			add.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					if(Launcher.verifUnicite(NEW_NAME,null)){
						nouvelleFamille();
					}	
				}				
			});

			contentPane.add(pane, BorderLayout.NORTH);
			contentPane.add(new JScrollPane(table), BorderLayout.CENTER);
			displayQueryResults(Famille.QUERY_LIST);
			
			popup = new JPopupMenu();
			popupEntries(popup, CantineI18n.tr("New"), "new", Launcher.NEW_IMAGE);
			popupEntries(popup, CantineI18n.tr("Edit"), "detailled", Launcher.MODIF_IMAGE);
			popup.addSeparator();
			popupEntries(popup, CantineI18n.tr("Delete"), "del", Launcher.DELETE_IMAGE);

			table.add(popup);
		}	
	
		@Override
		protected void popupActions(ActionEvent event){
			String command = event.getActionCommand();
			if(command.equals("new")){
				if(Launcher.verifUnicite(NEW_NAME,null)){
					nouvelleFamille();
				}
			}else if(command.equals("del")){
				Famille fam = new Famille(idEncours);
				Object[] args = {
						fam.getNom(), 
						new Integer(fam.getEnfants().size()),
						new Integer(fam.getTiers().size())
					};
				int response = JOptionPane.showConfirmDialog(
						MainGui.desktop, 
						MessageFormat.format(Messages.getString("window.famille.messages.delete.confirm"), args),
						Messages.getString("window.famille.messages.delete.title"),
						JOptionPane.YES_NO_OPTION,
						JOptionPane.WARNING_MESSAGE);
				if(response==JOptionPane.YES_OPTION){
					fam.deleteFamille(fam.getId_famille());
					displayQueryResults(Famille.QUERY_LIST);
				}
			}else if(command.equals("detailled")){
				Object[] args = {idEncours};
				if(Launcher.verifUnicite(MessageFormat.format(Messages.getString("window.famille.names.modif"), args),null)){
					modifieFamille(idEncours);
				}
			}
		}	

		private void buildSearchQuery(){
			StringBuffer sb = new StringBuffer(Famille.BASE_QUERY);
			boolean isSearch = false;
			if(!nom_cherche.getText().trim().equals("")){
				sb.append((isSearch)?" OR ":" AND ");
				sb.append("nom LIKE '%" + Database.replace(nom_cherche.getText(), "\'", "\'\'") + "%'");
				isSearch = true;
			}
			if(villes.getSelectedIndex() > 0){
				sb.append((isSearch)?" OR ":" AND ");
				sb.append("ville='" + Database.replace(villes.getSelectedItem().toString(), "\'", "\'\'") + "'");
				isSearch = true;
			}
						
			sb.append(Famille.ORDER_BY);
			System.err.println(sb.toString());
			displayQueryResults(sb.toString());
		}
	}
		
	/**
	 * 
	 * @param q
	 */
	public void displayQueryResults(final String q) {
		MainGui.statusBar.setText("Contacting database...");

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					table.setModel(factory.getResultSetTableModel(q));
					
			        TableSorter sorter = new TableSorter(table.getModel());
			        table.setModel(sorter);
			        sorter.setTableHeader(table.getTableHeader());
			        table.getTableHeader().setToolTipText(CommonsI18n.tr("Click to sort on a column, Ctrl+Click to sort on extra column"));

			        TableColumnModel columnModel = table.getColumnModel();
					columnModel.getColumn(0).setPreferredWidth(0);
					columnModel.getColumn(0).setMinWidth(0);
					columnModel.getColumn(0).setMaxWidth(0);
					columnModel.getColumn(1).setPreferredWidth(150);
					columnModel.getColumn(1).setMinWidth(150);
					columnModel.getColumn(1).setMaxWidth(200);
					columnModel.getColumn(3).setPreferredWidth(50);
					columnModel.getColumn(3).setMinWidth(50);
					columnModel.getColumn(3).setMaxWidth(50);
					columnModel.getColumn(4).setPreferredWidth(120);
					columnModel.getColumn(4).setMinWidth(120);
					columnModel.getColumn(4).setMaxWidth(150);
										
					MainGui.statusBar.setText("");
				} catch (SQLException ex) {
					MainGui.statusBar.setText("Error contacting database !");
					JOptionPane.showMessageDialog(MainGui.desktop,
							new String[] { // Display a 2-line message
							ex.getClass().getName() + ": ", ex.getMessage() });
					MainGui.statusBar.setText("");					
				}
			}
		});
	}
	
	private class Fenetre extends EscapeInternalFrame{
		private static final long serialVersionUID = 4758642152949965244L;
		/* DONNEES FAMILLE */
		private JTextArea adresse;
		private JTextField code_postal, ville, mail, assurance, num_police;
		private JFormattedTextField telephone;
		private JComboBox cbo_medecins, cbo_hopitaux;
		private JButton btn_medecin, btn_hopital;
		boolean pere = true; //pour création
		boolean mere = true; //pour création
		/* LABELS */
		private JLabel parents_label, tiers_label, enfants_label, urgences_label, adresse_label, 
			code_postal_label, ville_label, telephone_label, mail_label, assurance_label, 
			numero_police_label;
		
		/**
		 * 
		 * @param mod
		 */
		public Fenetre(char mod){
			mode = mod;
			drawWindow();
		}
		
		private void drawWindow(){
			FormPanel pane = new FormPanel( "be/xtnd/cantine/gui/descriptions/famille.jfrm" );

			getContentPane().add(pane);

			parents_label = pane.getLabel("parents_label");
			parents_label.setText(Messages.getString("window.famille.labels.parents"));
			
			tiers_label = pane.getLabel("tiers_label");
			tiers_label.setText(Messages.getString("window.famille.labels.tiers"));
			
			enfants_label = pane.getLabel("enfants_label");
			enfants_label.setText(Messages.getString("window.famille.labels.children"));
			
			urgences_label = pane.getLabel("urgences_label");
			urgences_label.setText(Messages.getString("window.famille.labels.emergencies"));

			adresse_label = pane.getLabel("adresse_label");
			adresse_label.setText(CantineI18n.tr("Address"));
			
			adresse = (JTextArea)pane.getComponentByName("adresse");
			adresse.setText(famille.getAdresse());
			
			code_postal_label = pane.getLabel("code_postal_label");
			code_postal_label.setText(CantineI18n.tr("Zip code"));
			
			code_postal = pane.getTextField("code_postal");
			code_postal.setText(famille.getCp());
			
			ville_label = pane.getLabel("ville_label");
			ville_label.setText(CantineI18n.tr("Town"));
			
			ville = pane.getTextField("ville");
			ville.setText(famille.getVille());

			telephone_label = pane.getLabel("telephone_label");
			telephone_label.setText(CantineI18n.tr("Phone"));
			
			try {
				fmPhone = new MaskFormatter("## ## ## ## ##");
			} catch (ParseException e) {e.printStackTrace();}

			telephone = (JFormattedTextField)pane.getComponentByName("telephone");
			telephone.setFormatterFactory(new DefaultFormatterFactory(fmPhone));
			telephone.setInputVerifier(new MaskFieldVerifier(famille.getTelephone(), fmPhone.getMask()));
			if(!famille.getTelephone().trim().equals("")){
				telephone.setText(famille.getTelephone());
				try {
					telephone.commitEdit();
				} catch (ParseException e1) {e1.printStackTrace();}
			}
			
			mail_label = pane.getLabel("mail_label");
			mail_label.setText(CantineI18n.tr("E-Mail"));
			
			mail = pane.getTextField("mail");
			mail.setText(famille.getMail());
			mail.setInputVerifier(new MailFieldVerifier(famille.getMail()));
			
			assurance_label = pane.getLabel("assurance_label");
			assurance_label.setText(Messages.getString("window.famille.labels.assurance"));
			
			assurance = pane.getTextField("assurance");
			assurance.setText(famille.getAssurance());
			
			numero_police_label = pane.getLabel("numero_police_label");
			numero_police_label.setText(Messages.getString("window.famille.labels.num_police"));
			
			num_police = pane.getTextField("numero_police");
			num_police.setText(famille.getNum_police());
			
			cbo_medecins = pane.getComboBox("combo_medecins");
			commons.buildCombo(cbo_medecins, medecin.listing());
			cbo_medecins.setSelectedItem(medecin.getNom());
			if(cbo_medecins.getSelectedIndex()<1){cbo_medecins.setSelectedIndex(0);}
			
			btn_medecin = (JButton)pane.getButton("medecin_button");
			btn_medecin.setText(Messages.getString("window.famille.labels.doctor"));
			btn_medecin.setToolTipText(Messages.getString("window.famille.tooltips.doctor"));
			btn_medecin.addMouseListener(new DoubleClickListener());
			
			cbo_hopitaux = pane.getComboBox("combo_hopitaux");
			commons.buildCombo(cbo_hopitaux, hopital.listing());
			cbo_hopitaux.setSelectedItem(hopital.getNom());
			if(cbo_hopitaux.getSelectedIndex()<1){cbo_hopitaux.setSelectedIndex(0);}

			btn_hopital = (JButton)pane.getButton("hopital_button");
			btn_hopital.setText(Messages.getString("window.famille.labels.hospital"));
			btn_hopital.setToolTipText(Messages.getString("window.famille.tooltips.hospital"));
			btn_hopital.addMouseListener(new DoubleClickListener());
			
			ajouter_parent = (JButton)pane.getButton("ajouter_parent");
			ajouter_parent.setToolTipText(Messages.getString("window.famille.tooltips.add_tiers"));
			ajouter_parent.addActionListener(new java.awt.event.ActionListener(){
				public void actionPerformed(java.awt.event.ActionEvent e){
					if(MainGui.verifUnicite(be.xtnd.cantine.gui.Tiers.NEW_NAME,null)){
						new be.xtnd.cantine.gui.Tiers().nouveauTiers(familles);
					}
				}
			});
			supprimer_parent = (JButton)pane.getButton("supprimer_parent");
			supprimer_parent.setToolTipText(Messages.getString("window.famille.tooltips.del_tiers"));
			supprimer_parent.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					Tiers t = famille.getTiers().get(tiers_tabs.getSelectedIndex());
					Object[] args = {
						t.getNom()+" "+t.getPrenom()+", "+t.getRoleName(),
						famille.getNom()
					};
					if(famille.hasOthersParent(t)){
						int response = JOptionPane.showConfirmDialog(
								MainGui.desktop, 
								MessageFormat.format(Messages.getString("window.famille.messages.del_tiers.confirm"), args),
								Messages.getString("window.famille.messages.del_tiers.title"),
								JOptionPane.YES_NO_OPTION,
								JOptionPane.WARNING_MESSAGE);
						if(response==JOptionPane.YES_OPTION){
								t.deleteTiers(t.getId_tiers());
								famille.getTiers().remove(tiers_tabs.getSelectedIndex());
								tiersTab();
								//dispose();
								//modifieFamille(famille.getId_famille());
						}
					}else{
						JOptionPane.showMessageDialog(
								MainGui.desktop,
								Messages.getString("window.tiers.required.parents"),
								Messages.getString("window.famille.messages.del_tiers.title"),
								JOptionPane.ERROR_MESSAGE
							);
					}
				}
			});

			tiers_tabs = pane.getTabbedPane("parents");
			
			ajouter_enfant = (JButton)pane.getButton("ajouter_enfant");
			ajouter_enfant.setToolTipText(Messages.getString("window.famille.tooltips.add_child"));
			ajouter_enfant.addActionListener(new java.awt.event.ActionListener() {
	                public void actionPerformed(java.awt.event.ActionEvent e) {
	                	if(MainGui.verifUnicite(Eleves.NAME, null)){
	                		new Eleves().nouvelEleve(familles);
	                	}
	                }
	            });
			supprimer_enfant = (JButton)pane.getButton("supprimer_enfant");
			supprimer_enfant.setToolTipText(Messages.getString("window.famille.tooltips.del_child"));
			supprimer_enfant.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					Eleve el = famille.getEnfants().get(children_tabs.getSelectedIndex());
					Object[] args = {
						el.getPrenom(),
						famille.getNom()
					};
					int response = JOptionPane.showConfirmDialog(
							MainGui.desktop, 
							MessageFormat.format(Messages.getString("window.famille.messages.del_eleve.confirm"), args),
							Messages.getString("window.famille.messages.del_eleve.title"),
							JOptionPane.YES_NO_OPTION,
							JOptionPane.WARNING_MESSAGE);
					if(response==JOptionPane.YES_OPTION)
						el.deleteEleve(el.getId_eleve());
						famille.getEnfants().remove(children_tabs.getSelectedIndex());
						enfantsTab();
				}
			});
			
			children_tabs = pane.getTabbedPane("enfants");
			
			tiersTab();
			enfantsTab();
			
			String title = Messages.getString((mode == Database.NEW)?"window.famille.titles.new":"window.famille.titles.modif");
			
			if(mode == Database.MODIF){
				Object args[] = {famille.getNom()};
				setTitle(MessageFormat.format(title, args));
				Object[] args2 = {famille.getId_famille()};
				setName(MessageFormat.format(
						Messages.getString("window.famille.names.modif")
						, args2));
			}else{
				setTitle(title);
				setName(NEW_NAME);
			}
			
			commons.createButtonBar(this,new OkayEvent());
			pack();

			MainGui.desktop.add(this, JLayeredPane.MODAL_LAYER);
			setLocation(MainGui.centerOnDesktop(getSize()));
			
			setResizable(true);
			setClosable(true);
			setIconifiable(true);
			setMaximizable(true);
			setVisible(true);
		}
		
		/**
		 * Valide les entrées obligtoires du formulaire,
		 * affiche un message sinon.
		 * 
		 * @return boolean résultat
		 */
		private boolean verifEntrees(){
			boolean valid = true;
			StringBuffer required = new StringBuffer();
			required.append(CommonsI18n.tr("The following are required:"));

			/* VERIFICATION CHAMPS FAMILLE */
			if(adresse.getText().trim().equalsIgnoreCase("")){
				required.append("\n- " + Messages.getString("window.famille.required.address"));
				if(valid) adresse.requestFocus();
				valid = false;
			}
			if(ville.getText().trim().equalsIgnoreCase("")){
				required.append("\n- " + Messages.getString("window.famille.required.town"));
				if(valid) ville.requestFocus();
				valid = false;
			}		
			if(cbo_medecins.getSelectedIndex()==0){
				required.append("\n- " + Messages.getString("window.famille.required.doctor"));
				if(valid) cbo_medecins.requestFocus();
				valid = false;				
			}
			if(cbo_hopitaux.getSelectedIndex()==0){
				required.append("\n- " + Messages.getString("window.famille.required.hospital"));
				if(valid) cbo_hopitaux.requestFocus();
				valid = false;				
			}
			/* VERIFICATION CHAMPS TIERS */
			//FIXME la vérification devrait s'effectuer sur les tiers en recherche d'au moins un parent
			if(mode == Database.MODIF){
				int tsize = famille.getTiers().size();
				for(int i = 0; i < tsize; i++){
					if(tiers_nom[i].getText().trim().equalsIgnoreCase("")){
						required.append("\n- " + Messages.getString("window.tiers.required.name"));
						if(valid) tiers_tabs.setSelectedIndex(i);
						if(valid) tiers_nom[i].requestFocus();
						valid = false;
					}
					if(tiers_prenom[i].getText().trim().equalsIgnoreCase("")){
						required.append("\n- " + Messages.getString("window.tiers.required.surname"));
						if(valid) tiers_tabs.setSelectedIndex(i);
						if(valid) tiers_prenom[i].requestFocus();
						valid = false;
					}				
				}
			}else{ //cas création, gestion de famille monoparentale
				boolean erreur = false;
				if(tiers_nom[Role.PERE-1].getText().trim().equals("") 
						&& tiers_prenom[Role.PERE-1].getText().trim().equals("") ){
					pere = false;
				}else{pere = true;}
				if(tiers_nom[Role.MERE-1].getText().trim().equals("") 
						&& tiers_prenom[Role.MERE-1].getText().trim().equals("") ){
					mere = false;
				}else{mere=true;}
				
				if(!pere && !mere){
					required.append("\n- " + Messages.getString("window.tiers.required.parents"));
					valid = false;
				}
				
				if(		(!tiers_nom[Role.PERE-1].getText().trim().equals("") 
						&& tiers_prenom[Role.PERE-1].getText().trim().equals("")) 
						||
						(tiers_nom[Role.PERE-1].getText().trim().equals("") 
						&& !tiers_prenom[Role.PERE-1].getText().trim().equals("")) 
				){erreur = true;}
				if(		(!tiers_nom[Role.MERE-1].getText().trim().equals("") 
						&& tiers_prenom[Role.MERE-1].getText().trim().equals("")) 
						||
						(tiers_nom[Role.MERE-1].getText().trim().equals("") 
						&& !tiers_prenom[Role.MERE-1].getText().trim().equals("")) 
				){erreur = true;}
				if(erreur){
					required.append("\n- " + Messages.getString("window.tiers.required.parents_name"));
					valid=false;
				}			
			}
			/* VERIFICATION CHAMPS ENFANTS */
			if(mode == Database.MODIF){
				int esize = famille.getEnfants().size();
				for(int i =0; i<esize;i++){
					if(!eleve_g[i].isSelected() && !eleve_f[i].isSelected()){
						required.append("\n- " + Messages.getString("window.eleve.required.sex"));
						valid=false; 
					}
					if(prenom_eleve[i].getText().trim().equalsIgnoreCase("")){
						required.append("\n- " + Messages.getString("window.eleve.required.surname"));
						if(valid) children_tabs.setSelectedIndex(i);
						if(valid) prenom_eleve[i].requestFocus();
						valid = false;
					}
					if(classe[i].getSelectedIndex()==0){
						required.append("\n- " + Messages.getString("window.eleve.required.class"));
						if(valid) children_tabs.setSelectedIndex(i);
						if(valid) classe[i].requestFocus();
						valid = false;
					}
				}
			}
			if(!valid){
				JOptionPane.showMessageDialog(
						MainGui.desktop, 
						required.toString(),
						CommonsI18n.tr("Error!"),
						JOptionPane.ERROR_MESSAGE);			
			}
			return valid;
		}
		
		private void queries(){
			Tiers p = new Pere();
			Tiers m = new Mere();

			famille.setAdresse(adresse.getText());
			famille.setCp(code_postal.getText());
			famille.setVille(ville.getText());
			famille.setTelephone(telephone.getText());
			famille.setTel_portable(new String());
			famille.setMail(mail.getText());
			famille.setAssurance(assurance.getText());
			famille.setNum_police(num_police.getText());
			famille.setId_hopital(new Urgence(Urgence.HOPITAUX_TYPE, cbo_hopitaux.getSelectedItem().toString()).getId());
			famille.setId_medecin(new Urgence(Urgence.MEDECIN_TYPE, cbo_medecins.getSelectedItem().toString()).getId());
			
			if(mode == Database.NEW){
				famille.storeFamille();
			}else{
				famille.modifyFamille(famille.getId_famille());
			}
			
			int tabs;
			/* GESTION DES TIERS*/
			tabs = famille.getTiers().size();
			for(int i = 0; i < tabs; i++){
				Tiers t = traiteTiers(famille.getTiers().get(i), i);
				if(t.getRole().getId_role() == Role.PERE){ //gestion père
					p = t;
					if(pere){
						if(mode == Database.NEW || p.getId_tiers() == null) p.storeTiers();
						if(mode == Database.MODIF) p.modifyTiers(p.getId_tiers());
					}
				}else if(t.getRole().getId_role() == Role.MERE){//gestion mère
					m = t;
					if(mere){
						if(mode == Database.NEW || m.getId_tiers() == null) m.storeTiers();
						if(mode == Database.MODIF) m.modifyTiers(m.getId_tiers());
					}
				}else{//gestion autres tiers
					if(mode == Database.NEW || t.getId_tiers() == null){
						t.storeTiers();
					}else{
						t.modifyTiers(t.getId_tiers());
					}
				}
			}

			String nomPere = (p.getNom()!=null)?p.getNom():"";
			String nomMere = (m.getNom()!=null)?m.getNom():"";

			String nomFamille = "";
			
			if(!nomPere.trim().equals("") && !nomMere.trim().equals("")){
				nomFamille = nomPere + "/" + nomMere;
			}else{
				if(!nomPere.trim().equals("")) nomFamille = nomPere;
				if(!nomMere.trim().equals("")) nomFamille = nomMere;				
			}
			
			famille.setNom(nomFamille);
			//famille.setNom(nomPere + "/" + nomMere);
			famille.ajouteNom(famille.getId_famille());
			/* GESTION ELEVES */
			tabs = famille.getEnfants().size();
			for(int i = 0 ; i < tabs ; i++){
				Eleve el = traiteEleves(famille.getEnfants().get(i), i);
				if(mode == Database.NEW){
					el.storeEleve();
				}else{
					el.modifyEleve(el.getId_eleve());
				}
			}
		}
		
		private Tiers traiteTiers(Tiers t, int tab){
			t.setNom(tiers_nom[tab].getText());
			t.setPrenom(tiers_prenom[tab].getText());
			t.setTel_portable(tiers_gsm[tab].getText());
			t.setMail(tiers_mail[tab].getText());
			t.setEmployeur(tiers_employeur[tab].getText());
			t.setProfession(tiers_profession[tab].getText());
			t.setTel_travail(tiers_tel_pro[tab].getText());
			t.setPoste_tel_travail(tiers_poste_tel_pro[tab].getText());
			t.setCommentaires(tiers_commentaires[tab].getText());
			t.setId_famille(famille.getId_famille());
			t.setRole(roles[tab].getSelectedItem().toString());
			return t;
		}
		
		private Eleve traiteEleves(Eleve el, int tab){
			el.setNom(nom_eleve[tab].getText());
			el.setPrenom(prenom_eleve[tab].getText());
			if(eleve_g[tab].isSelected()) el.setSexe('G');
			if(eleve_f[tab].isSelected()) el.setSexe('F');
			el.setDate_naissance(date_naissance[tab].getDate());
			el.setLieu_naissance(lieu_naissance_eleve[tab].getText());
			el.setClasse(new Classe(classe[tab].getSelectedItem().toString()));
			el.setCantine(cantine[tab].isSelected());
			//el.setCentre_aere(centre_aere[tab].isSelected());
			el.setObservations(observations_eleve[tab].getText());
			el.setAllergies(allergies_eleve[tab].getText());
			el.setId_famille(famille.getId_famille());
			return el;
		}
		
		class OkayEvent implements ActionListener{
			/**
			 * @param e
			 */
			public void actionPerformed(ActionEvent e) {
				if(verifEntrees()){
					queries();
					dispose();
					if(!Launcher.verifUnicite(Familles.LIST_NAME, null)){
						//System.err.println("reload list");
						displayQueryResults(Famille.QUERY_LIST);
					}
				}
			}		
		}

		/**
		 * 
		 * @author Johan Cwiklinski
		 *
		 */
		public class DoubleClickListener extends MouseAdapter {
			/**
			 * @param evt
			 */
	        public void mouseClicked(MouseEvent evt) {
	            if (evt.getClickCount() == 2) {// double-click
	            	if(evt.getSource().equals(btn_medecin)){
	            		new Urgences("medecins").getList(cbo_medecins);
	            	}else if(evt.getSource().equals(btn_hopital)){
	            		new Urgences("hopitaux").getList(cbo_hopitaux);
	            	}
	            } else if (evt.getClickCount() == 1) {// simple-click
	            	if(evt.getSource().equals(btn_medecin)){ //cas médecin
	                	if(cbo_medecins.getSelectedIndex()!=0){
		                	Urgence medecin_en_cours = new Urgence(Urgence.MEDECIN_TYPE);
		                	String name = cbo_medecins.getSelectedItem().toString();
		                	Integer id = medecin_en_cours.getIdFromName(name);
		                	Object args[] = {id};
		                	if(MainGui.verifUnicite(
		                			MessageFormat.format(Messages.getString("window.medecins.names.modif"), args),
									null)){
		                		new Urgences("medecins").modifieUrgence(id, cbo_medecins);
		                	}
	                	}	            		
	            	}else if(evt.getSource().equals(btn_hopital)){//cas hôpital
	                	if(cbo_hopitaux.getSelectedIndex()!=0){
		                	Urgence hopital_en_cours = new Urgence(Urgence.HOPITAUX_TYPE);
		                	String name = cbo_hopitaux.getSelectedItem().toString();
		                	Integer id = hopital_en_cours.getIdFromName(name);
		                	Object args[] = {id};
		                	if(MainGui.verifUnicite(
		                			MessageFormat.format(Messages.getString("window.hopitaux.names.modif"), args),
									null)){
		                		new Urgences("hopitaux").modifieUrgence(id, cbo_hopitaux);
		                	}
	                	}
	            	}
	            }
	        }
	    }	
	}
}
