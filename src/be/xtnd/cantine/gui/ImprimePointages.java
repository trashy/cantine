/*
 * ImprimePointages.java, 20 oct. 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               ImprimePointages.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://www.x-tnd.be/
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

/**
 * 
 */
package be.xtnd.cantine.gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.table.AbstractTableModel;

import net.sf.jasperreports.engine.JRAlignment;
import net.sf.jasperreports.engine.JRElement;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.engine.design.JRDesignBand;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignField;
import net.sf.jasperreports.engine.design.JRDesignLine;
import net.sf.jasperreports.engine.design.JRDesignRectangle;
import net.sf.jasperreports.engine.design.JRDesignSection;
import net.sf.jasperreports.engine.design.JRDesignStaticText;
import net.sf.jasperreports.engine.design.JRDesignStyle;
import net.sf.jasperreports.engine.design.JRDesignTextField;
import net.sf.jasperreports.engine.design.JasperDesign;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import be.xtnd.cantine.CantineI18n;
import be.xtnd.cantine.Classe;
import be.xtnd.cantine.Eleve;
import be.xtnd.cantine.Presence;
import be.xtnd.commons.GuiCommons;
import be.xtnd.commons.Print;
import be.xtnd.commons.db.Database;
import be.xtnd.commons.gui.EscapeInternalFrame;
import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.i18n.CommonsI18n;

import com.jeta.forms.components.panel.FormPanel;
import com.jeta.forms.components.separator.TitledSeparator;
import com.jgoodies.looks.plastic.PlasticInternalFrameUI;
import com.toedter.calendar.JMonthChooser;
import com.toedter.calendar.JYearChooser;

/**
 * Permet l'impression des pointages.
 * 
 * @author Johan Cwiklinski
 * @version 1.0
 */
public class ImprimePointages extends EscapeInternalFrame {
	private static final long serialVersionUID = -1460032567372500531L;
	private GuiCommons commons = new GuiCommons();
	private TitledSeparator title;
	private JLabel type_label/*, reservation_label*/, mode_impression;
	private JMonthChooser months;
	private JYearChooser years;
	private JComboBox types;
	private JRadioButton color, bw;
	static Logger logger = Logger.getLogger(ImprimePointages.class.getName());
	private int month, year;
	
	private static CustomTableModel tbm;
	
	private static String[] colonnes;
	private static Object[][] lignes;
	private static Integer[] totaux;
	
	private static Object[] colors = Launcher.options.getCouleurs_pointages();
	
	private static boolean colored = false;

	static JRDesignStyle normalStyle, boldStyle, titleStyle, smallStyle;	
	static JasperDesign jasperDesign;
	
	/** Nom de la fenêtre */
	public static final String NAME = "imprime_pointages";

	/**
	 * Dessine la fenêtre d'impression des pointages.
	 *
	 */
	public ImprimePointages(){
		super(CantineI18n.tr("Printing"), true, true, true, true);
		super.setName(NAME);

		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/cantine/log4j.properties"));

        putClientProperty(
                PlasticInternalFrameUI.IS_PALETTE,
                Boolean.TRUE);
        FormPanel pane = new FormPanel( "be/xtnd/cantine/gui/descriptions/select_mois_pointages.jfrm" );

        title = (TitledSeparator)pane.getComponentByName("title");
        title.setText(Messages.getString("window.planning.print_title"));

        type_label = pane.getLabel("type_label");
        type_label.setText(Messages.getString("window.planning.labels.type"));
        
        types = pane.getComboBox("type");
        commons.buildCombo(types, new Classe().getCategories());
        
        /*reservation_label = pane.getLabel("reservation_label");
        reservation_label.setText(Messages.getString("window.planning.labels.reservation"));*/
               
        mode_impression = pane.getLabel("mode_impression");
        mode_impression.setText(Messages.getString("window.planning.labels.print_mode"));
        
        color = pane.getRadioButton("color");
        color.setText(Messages.getString("window.planning.labels.color"));
        
        bw = pane.getRadioButton("bw");
        bw.setText(Messages.getString("window.planning.labels.bw"));
        
        years = (JYearChooser)pane.getComponentByName("year");
        years.setYear(Calendar.getInstance().get(Calendar.YEAR));
        
        months = (JMonthChooser)pane.getComponentByName("month");
        months.setMonth(Calendar.getInstance().get(Calendar.MONTH));
        months.setYearChooser(years);
        
    	add(new JScrollPane(pane));

        commons.createButtonBar(this, new OkayEvent());

    	pack();
    	setLocation(
    			MainGui.centerOnDesktop(this.getSize())
    			);
    	setMinimumSize(getSize());
    	setVisible(true);
    	setClosable(true);
    	setIconifiable(true);
    	setResizable(true);
    	MainGui.desktop.add(this);
    	toFront();
    	try {
			setSelected(true);
		} catch (PropertyVetoException e) {}
	
	}
	
	class OkayEvent implements ActionListener {
		/**
		 * @param e
		 */
		public void actionPerformed(ActionEvent e) {
			if(types.getSelectedIndex()!=0){
				month = months.getMonth();
				year = years.getYear();
				//char reserv = (reservations.getSelectedIndex()==0)?Presence.CANTINE:Presence.CENTRE;
				
				colored = (color.isSelected())?true:false;
				
				SimpleDateFormat fmt = new SimpleDateFormat("MMMM yyyy");
				Calendar cal = new GregorianCalendar(year, month, 1);
				getDays();	
				Object[][] origLines = new Eleve().presenceELeves(
						month, 
						year, 
						types.getSelectedItem().toString()
					);
				lignes = delEmptyLines(origLines); 
			    //delEmptyLines(lignes);
			    tbm = new CustomTableModel(colonnes, lignes);
				getTotals();
				
				Database db = Launcher.db; //new Database(true);
				long start = System.currentTimeMillis();
				String jasperFile = ".config/details_pointage.jasper";
				try {
					JasperDesign jasperDesign = getJasperDesign();
					JasperCompileManager.compileReportToFile(jasperDesign, jasperFile);
					logger.debug("Compile time : " + (System.currentTimeMillis() - start));
				} catch (JRException e1) {e1.printStackTrace();}
								
				HashMap<Object, Object> parameters = new HashMap<Object, Object>();
				parameters.put("CURRENT_CATEGORY", types.getSelectedItem().toString().toUpperCase());
				parameters.put("CURRENT_CHECKINGS", fmt.format(cal.getTime()).toUpperCase());
			    parameters.put("HEADERS_PATH", ".config/");
			    parameters.put("REPORT_CONNECTION", db.getConnection());
			    
			    parameters.put("TABLEMODEL", new JRTableModelDataSource(tbm));
	
	           	new Print(".config", "pointages.jrxml", parameters);
	           	
	            boolean success = (new File(jasperFile)).delete();
	            if (!success) {
	            	logger.warn("Temporary file "+jasperFile+" was not deleted !");
	            }else{
	            	logger.info("Temporary file "+jasperFile+" successfully deleted.");
	            }
			}else{
				JOptionPane.showMessageDialog(
						MainGui.desktop,
						Messages.getString("window.print.type_select"),
						CommonsI18n.tr("Error!"),
						JOptionPane.WARNING_MESSAGE
				);
			}
		}
	}
	
	private Object[][] delEmptyLines(Object[][] lines){
		ArrayList<Object> newLines = new ArrayList<Object>();
		int empty = 0;
		int full = 0;
		for(int i = 0 ; i < lines.length ; i++){
			boolean isEmpty = true;
			for(int y = 2 ; y < lines[i].length ; y++){
				if(!lines[i][y].toString().equals(String.valueOf(Presence.NON))
						&& !lines[i][y].toString().equals(String.valueOf(Presence.WEEKEND))) isEmpty = false;
			}
			if(!isEmpty) newLines.add(lines[i]);
			if(isEmpty){empty++;}else{full++;}
		}
		//DEBUG System.err.println("lignes pleines : "+full+" ; lignes vides : "+empty+" nouvelle liste :"+newLines.size());
		Object[][] newObject = new Object[newLines.size()][];
		for(int i = 0 ; i < newLines.size() ; i++){
			newObject[i] = (Object[]) newLines.get(i);
		}
		return newObject;
	}
	
	/**
	 * getDays()
	 * implémente le tableau listOfDays pour l'entête de la JTable
	 */
	public void getDays(){
		SimpleDateFormat formatter = new SimpleDateFormat("E");
		SimpleDateFormat formatterbis = new SimpleDateFormat("d");
		
	    Calendar cal = new GregorianCalendar(year,month,1);
	    
		int days = cal.getActualMaximum(Calendar.DAY_OF_MONTH); 
		colonnes = new String[days+2];	    
	    
	    for(int i=0; i<days+2; i++){
	    	if(i==0 || i==1){colonnes[i]=" ";}else{
	    		Date curDate = cal.getTime();
	    		cal.add(Calendar.DAY_OF_MONTH, 1);
	    		String result = formatter.format(curDate);
	    		String resultbis = formatterbis.format(curDate);
	    		colonnes[i] = result.substring(0,1).toUpperCase()+"/"+resultbis;
	    	}
	    }
	}
	
	private void getTotals(){
		totaux = new Integer[colonnes.length-2];
		logger.debug("Compte colonnes : "+colonnes.length);
		for(int i = 2 ; i < colonnes.length ; i++){
			int present=0, imprevu=0, prepaye=0;
			for(int y = 0 ; y < lignes.length ; y++){
				int toSwitch = new Integer(tbm.getValueAt(y,i).toString());
				switch(toSwitch){
					case Presence.PRESENT:
						present++;
						break;
					case Presence.IMPREVU:
						imprevu++;
						break;
					case Presence.PREPAYE:
						prepaye++;
						break;
				}
			}
			totaux[i-2] = present+imprevu+prepaye;
			logger.debug("valeur total "+new Integer(i-2)+" : "+totaux[i-2]);
		}
	}
	
	private static JasperDesign getJasperDesign() throws JRException{
		//compte jours fermture
		int closed = 0;
		for (int y = 2 ; y < colonnes.length ; y++){
			//DEBUG System.err.println("getting value at : 1,"+y);
			if(new Integer(tbm.getValueAt(0,y).toString())==-1) closed++;
		}
		jasperDesign = initDesign();
		initStyles();
		//initParameters();
		
		JRDesignField field;
		
		for(int i = 0 ; i < colonnes.length ; i++){
			field = new JRDesignField();
			field.setName("COLUMN_"+i);
			field.setValueClass(java.lang.String.class);
			jasperDesign.addField(field);			
		}
		
		//Page header
		JRDesignBand band = new JRDesignBand();
		band.setHeight(20);
		band.addElement(getHeaderBand()); 
		
		JRDesignStaticText staticText;
		
		int pos = 100;
		int width = (539-100)/(colonnes.length-closed-2);
		
		for(int i = 2 ; i < colonnes.length ; i++){
			if(new Integer(tbm.getValueAt(0,i).toString())!=-1){
				staticText = new JRDesignStaticText();
				staticText.setX(pos);
				staticText.setY(3);
				staticText.setWidth(width);
				staticText.setHeight(15);
				staticText.setForecolor(Color.white);
				staticText.setBackcolor(new Color(0x33, 0x33, 0x33));
				staticText.setMode(JRElement.MODE_OPAQUE);
				staticText.setHorizontalAlignment(JRAlignment.HORIZONTAL_ALIGN_CENTER);
				staticText.setStyle(boldStyle);
				staticText.setText(colonnes[i]);
				band.addElement(staticText);	
				pos = pos + width;
			}
		}
		JRDesignLine line = new JRDesignLine();
		line.setX(0);
		line.setY(19);
		line.setWidth(539);
		line.setHeight(0);
		line.setForecolor(new Color(0x80, 0x80, 0x80));
		line.setPositionType(JRElement.POSITION_TYPE_FLOAT);
		band.addElement(line);
		((JRDesignSection)jasperDesign.getDetailSection()).addBand(band);
		
		jasperDesign.setPageHeader(band);

		//Column header
		band = new JRDesignBand();
		jasperDesign.setColumnHeader(band);

		//Detail
		band = new JRDesignBand();
		band.setHeight(15);
		
		JRDesignTextField textField = new JRDesignTextField();
		textField.setX(0);
		textField.setY(2);
		textField.setWidth(100);
		textField.setHeight(12);
		textField.setHorizontalAlignment(JRAlignment.HORIZONTAL_ALIGN_LEFT);
		textField.setStyle(normalStyle);
		JRDesignExpression expression = new JRDesignExpression();
		expression.setValueClass(java.lang.String.class);
		expression.setText("$F{COLUMN_1}");
		textField.setExpression(expression);
		band.addElement(textField);
				
		int pos2 = 100;

		for(int i = 2 ; i < colonnes.length ; i++){
			line = new JRDesignLine();
			line.setX(pos2);
			line.setY(0);
			line.setWidth(1);
			line.setHeight(15);
			line.setForecolor(Color.GRAY);
			line.setPositionType(JRElement.POSITION_TYPE_FLOAT);
			band.addElement(line);
			if(new Integer(tbm.getValueAt(0,i).toString())!=-1){
				/* textField NON */
				textField = getCheckField(Presence.NON, pos2, width, i);
				band.addElement(textField);
				
				/* textField PRESENT */
				textField = getCheckField(Presence.PRESENT, pos2, width, i);
				band.addElement(textField);

				/* textField DEDUIT */
				textField = getCheckField(Presence.DEDUIT, pos2, width, i);
				band.addElement(textField);

				/* textField REMBOURSER */
				textField = getCheckField(Presence.REMBOURSER, pos2, width, i);
				band.addElement(textField);

				/* textField NON_JUSTIFIE */
				textField = getCheckField(Presence.NON_JUSTIFIE, pos2, width, i);
				band.addElement(textField);

				/* textField ATTENTE */
				textField = getCheckField(Presence.ATTENTE, pos2, width, i);
				band.addElement(textField);

				/* textField IMPREVU */
				textField = getCheckField(Presence.IMPREVU, pos2, width, i);
				band.addElement(textField);

				/* textField PREPAYE */
				textField = getCheckField(Presence.PREPAYE, pos2, width, i);
				band.addElement(textField);

				pos2 = pos2 + width;
			}
		}
		
		line = new JRDesignLine();
		line.setX(0);
		line.setY(14);
		line.setWidth(539);
		line.setHeight(0);
		line.setForecolor(new Color(0x80, 0x80, 0x80));
		line.setPositionType(JRElement.POSITION_TYPE_FLOAT);
		band.addElement(line);
		((JRDesignSection)jasperDesign.getDetailSection()).addBand(band);
		
		//Page footer
		band = new JRDesignBand();
		band.setHeight(15);
		
		JRDesignRectangle rectangle = new JRDesignRectangle();
		rectangle.setX(0);
		rectangle.setY(0);
		rectangle.setWidth(539);
		rectangle.setHeight(15);
		rectangle.setForecolor(Color.LIGHT_GRAY);
		rectangle.setBackcolor(Color.LIGHT_GRAY);
		band.addElement(rectangle);
		
		textField = new JRDesignTextField();
		textField.setX(0);
		textField.setY(2);
		textField.setWidth(100);
		textField.setHeight(12);
		textField.setHorizontalAlignment(JRAlignment.HORIZONTAL_ALIGN_LEFT);
		textField.setStyle(boldStyle);
		expression = new JRDesignExpression();
		expression.setValueClass(java.lang.String.class);
		expression.setText("new String(\"TOTAUX\")");
		textField.setExpression(expression);
		band.addElement(textField);
		
		pos2 = 100;
		
		for(int z = 2; z < colonnes.length ; z++){
			if(new Integer(tbm.getValueAt(0,z).toString())!=-1){
				textField = new JRDesignTextField();
				textField.setX(pos2);
				textField.setY(2);
				textField.setWidth(width);
				textField.setHeight(12);
				textField.setHorizontalAlignment(JRAlignment.HORIZONTAL_ALIGN_CENTER);
				textField.setStyle(boldStyle);
				expression = new JRDesignExpression();
				expression.setValueClass(java.lang.Integer.class);
				expression.setText("new Integer("+totaux[z-2]+")");
				textField.setExpression(expression);
				band.addElement(textField);
				pos2 = pos2 + width;
			}
		}
		
		jasperDesign.setPageFooter(band);

		return jasperDesign;
	}
	
	/**
	 * Iniitalise l'objet JasperDesign
	 * @return Objet initialisé
	 */
	private static JasperDesign initDesign(){
		JasperDesign jasperDesign = new JasperDesign();
		jasperDesign.setName("details_pointage");
		jasperDesign.setPageWidth(595);
		jasperDesign.setPageHeight(842);
		jasperDesign.setColumnWidth(539);
		jasperDesign.setColumnSpacing(0);
		jasperDesign.setLeftMargin(0);
		jasperDesign.setRightMargin(0);
		jasperDesign.setTopMargin(00);
		jasperDesign.setBottomMargin(0);
		return jasperDesign;
	}
	
	/**
	 * Initialise les styles
	 * @throws JRException 
	 */
	private static void initStyles() throws JRException{
		normalStyle = new JRDesignStyle();
		normalStyle.setName("Arial_Normal");
		normalStyle.setDefault(true);
		normalStyle.setFontName("Arial");
		normalStyle.setFontSize(9);
		normalStyle.setPdfFontName("Helvetica");
		normalStyle.setPdfEncoding("Cp1252");
		normalStyle.setPdfEmbedded(false);
		jasperDesign.addStyle(normalStyle);

		boldStyle = new JRDesignStyle();
		boldStyle.setName("Arial_Bold");
		boldStyle.setFontName("Arial");
		boldStyle.setFontSize(9);
		boldStyle.setBold(true);
		boldStyle.setPdfFontName("Helvetica-Bold");
		boldStyle.setPdfEncoding("Cp1252");
		boldStyle.setPdfEmbedded(false);
		jasperDesign.addStyle(boldStyle);
	}
	
	private static JRDesignRectangle getHeaderBand(){
		JRDesignRectangle rectangle = new JRDesignRectangle();
		rectangle.setX(0);
		rectangle.setY(0);
		rectangle.setWidth(539);
		rectangle.setHeight(20);
		rectangle.setForecolor(new Color(0x33, 0x33, 0x33));
		rectangle.setBackcolor(new Color(0x33, 0x33, 0x33));
		return rectangle;
	}
	
	private static JRDesignTextField getCheckField(int presence, int pos, int width, int i){
		JRDesignTextField textField = new JRDesignTextField();
		textField.setX(pos+1);
		textField.setY(2);
		textField.setWidth(width-2);
		textField.setHeight(10);
		textField.setHorizontalAlignment(JRAlignment.HORIZONTAL_ALIGN_CENTER);
		JRDesignExpression expression = new JRDesignExpression();
		expression.setValueClass(java.lang.String.class);
		expression.setText("(Integer.parseInt($F{COLUMN_"+i+"})=="+presence+")?$F{COLUMN_"+i+"}:null");
		textField.setExpression(expression);
		textField.setMode(JRElement.MODE_OPAQUE);
		Color couleur = null;
		if(colored){
			 couleur = (Color)colors[presence];
		}else{
			if(
					presence == Presence.PRESENT ||
					presence == Presence.PREPAYE ||
					presence == Presence.IMPREVU
				){
				couleur = (Color)colors[Presence.PRESENT];
			}else{
				couleur = (Color)colors[Presence.NON];
			}
		}

		textField.setBackcolor(couleur);
		textField.setForecolor(couleur);
		
		expression = new JRDesignExpression();
		expression.setValueClass(java.lang.Boolean.class);
		expression.setText("new Boolean(Integer.parseInt($F{COLUMN_"+i+"})=="+presence+")");				
		textField.setPrintWhenExpression(expression);	
		return textField;
	}
	
	private class CustomTableModel extends AbstractTableModel{
		private static final long serialVersionUID = 8770149376797798907L;

		private String[] columnNames;

		private Object[][] data;


		/**
		 * 
		 * @param cols
		 * @param rows
		 */
		public CustomTableModel(String[] cols, Object[][] rows){
			this.columnNames = cols;
			this.data = rows;
		}
		
		/**
		 *
		 */
		@SuppressWarnings("unused")
		public CustomTableModel(){}


		/**
		 * @return int
		 */
		public int getColumnCount(){
			return this.columnNames.length;
		}

		/**
		 * @param columnIndex
		 * @return String
		 */
		public String getColumnName(int columnIndex)
		{
			return this.columnNames[columnIndex];
		}

		/**
		 * @return int
		 */
		public int getRowCount(){
			return this.data.length;
		}

		/**
		 * @param rowIndex
		 * @param columnIndex
		 * @return Object
		 */
		public Object getValueAt(int rowIndex, int columnIndex){
			return this.data[rowIndex][columnIndex];
		}
	}
}
