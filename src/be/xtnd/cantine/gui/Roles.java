/*
 * Roles.java, 20 sept. 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               	Roles.java
 * Author's email :     	johan@x-tnd.be
 * Author's Website :   	http://cantine.sourceforge.net
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyVetoException;
import java.sql.SQLException;
import java.text.MessageFormat;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableColumnModel;

import be.xtnd.cantine.CantineI18n;
import be.xtnd.cantine.Role;
import be.xtnd.commons.GuiCommons;
import be.xtnd.commons.db.Database;
import be.xtnd.commons.db.ResultSetTableModelFactory;
import be.xtnd.commons.gui.EscapeInternalFrame;
import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.gui.TableSorter;
import be.xtnd.commons.i18n.CommonsI18n;

import com.jeta.forms.components.panel.FormPanel;

/**
 * 
 * 
 * @author Johan Cwiklinski
 *
 */
public class Roles {
	private Role role;
	private GuiCommons commons = new GuiCommons();
	private JTable table;
	private ResultSetTableModelFactory factory; 
	private JComboBox parent_box;
	private boolean select = false;
	/** Nom de la fenêtre de liste */
	public static final String LIST_NAME = "liste_roles";
	/** Nom de la fenêtre d'ajout */
	public static final String NEW_NAME = "nouveau_role";

	/**
	 * 
	 *
	 */
	public Roles(){}
	
	/**
	 * 
	 *
	 */
	public void afficheListe(){
		new Liste();		
	}
	
	/**
	 * 
	 * @param box
	 */
	public void afficheListe(JComboBox box){
		this.parent_box = box;
		this.select = true;
		this.role = new Role();
		new Liste();
	}
	
	/**
	 * 
	 *
	 */
	public void nouveauRole(){
		this.role = new Role();
		new Fenetre(Database.NEW);
	}
	
	/**
	 * 
	 * @param id
	 */
	public void modifieRole(Integer id){
		this.role = new Role(id);
		new Fenetre(Database.MODIF);	
	}
	
	private class Liste extends EscapeInternalFrame{
		private static final long serialVersionUID = 628834258363225410L;
		private Integer idEncours;
		//private Liste qf;
		private JPopupMenu popup;

		/**
		 * 
		 *
		 */
		public Liste(){
			this(new ResultSetTableModelFactory(Launcher.db));

			setIconifiable(false);
			setClosable(true);
			setResizable(true);
			setMaximizable(true);
			setName(LIST_NAME);
			setSize(200, 150);
			setLocation(MainGui.centerOnDesktop(getSize()));
			setVisible(true);
			if(select){MainGui.desktop.add(this,JLayeredPane.POPUP_LAYER);}else{MainGui.desktop.add(this);}
			try {setSelected(true);} catch (PropertyVetoException e) {}
		}
		
		/**
		 * 
		 * @param f
		 */
		public Liste(ResultSetTableModelFactory f) {
			setTitle(Messages.getString("window.roles.titles.list"));

			factory = f;
			table = new JTable();
			table.getTableHeader().setReorderingAllowed(false);
			table.addMouseListener(new MouseAdapter(){
				public void mousePressed(MouseEvent e){
			  	  	int nModifier = e.getModifiers();
					idEncours = (Integer)table.getValueAt(table.rowAtPoint(e.getPoint()),0);
			    	if ((nModifier & InputEvent.BUTTON3_MASK) != 0) {
						table.changeSelection(table.rowAtPoint(e.getPoint()),0,false,false);
						popup.show((Component)e.getSource(),e.getX(),e.getY());
			      	}
			    	if(select && InputEvent.BUTTON1_MASK !=0){
			    		if(e.getClickCount()==2){
			    			String ty = String.valueOf(table.getValueAt(table.rowAtPoint(e.getPoint()),1));
							parent_box.removeAllItems();
							commons.buildCombo(parent_box, Role.listing());
							parent_box.setSelectedItem(ty);
							dispose();
			    		}
			    	}
			    	if(e.getClickCount()==2 && !select){//double click
						if(idEncours.intValue() == Role.PERE || idEncours.intValue() == Role.MERE){
							noDelete();
						}else{
							Object[] args = {idEncours};
							if(Launcher.verifUnicite(MessageFormat.format(Messages.getString("window.roles.names.modif"), args),null)){
								modifieRole(idEncours);
							}
						}
			    	}
				}
			});
			
			Container contentPane = getContentPane();
			contentPane.add(new JScrollPane(table), BorderLayout.CENTER);
			displayQueryResults(Role.LISTQUERY);
			
			popup = new JPopupMenu();
			popupEntries(popup, CantineI18n.tr("New"), "new", Launcher.NEW_IMAGE);
			popupEntries(popup, CantineI18n.tr("Edit"), "detailled", Launcher.MODIF_IMAGE);
			popup.addSeparator();
			popupEntries(popup, CantineI18n.tr("Delete"), "del", Launcher.DELETE_IMAGE);

			table.add(popup);
		}
	
		@Override
		protected void popupActions(ActionEvent event){
			String command = event.getActionCommand();
			if(command.equals("new")){
				if(Launcher.verifUnicite(NEW_NAME,null)){
					nouveauRole();
				}
			}else if(command.equals("del")){
				if(idEncours.intValue() == Role.PERE || idEncours.intValue() == Role.MERE){
					noDelete();
				}else{
					Role role = new Role(idEncours);
					Object[] args = {role.getRole()};
					int response = JOptionPane.showConfirmDialog(
							MainGui.desktop, 
							MessageFormat.format(Messages.getString("window.roles.messages.delete.confirm"), args),
							Messages.getString("window.roles.messages.delete.title"),
							JOptionPane.YES_NO_OPTION,
							JOptionPane.WARNING_MESSAGE);
					if (response == JOptionPane.YES_OPTION) {				
						if(role.deleteRole(idEncours)==-2){ //fk fails
							JOptionPane.showMessageDialog(
									MainGui.desktop,
									MessageFormat.format(Messages.getString("window.roles.messages.fk"),args),
									Messages.getString("window.roles.messages.fk.title"),
									JOptionPane.ERROR_MESSAGE
							);
						}
						displayQueryResults(Role.LISTQUERY);
					}
				}
			}else if(command.equals("detailled")){
				if(idEncours.intValue() == Role.PERE || idEncours.intValue() == Role.MERE){
					noDelete();
				}else{
					Object[] args = {idEncours};
					if(Launcher.verifUnicite(MessageFormat.format(Messages.getString("window.roles.names.modif"), args),null)){
						modifieRole(idEncours);
					}
				}
			}
		}	
	}

	private void noDelete(){
		JOptionPane.showMessageDialog(
				MainGui.desktop,
				Messages.getString("window.roles.messages.nodelete"),
				Messages.getString("window.roles.messages.nodelete.title"),
				JOptionPane.ERROR_MESSAGE
		);		
	}
	
	/**
	 * 
	 * @param q
	 */
	public void displayQueryResults(final String q) {
		MainGui.statusBar.setText("Contacting database...");

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					table.setModel(factory.getResultSetTableModel(q));
					
			        TableSorter sorter = new TableSorter(table.getModel());
			        table.setModel(sorter);
			        sorter.setTableHeader(table.getTableHeader());
			        table.getTableHeader().setToolTipText(CommonsI18n.tr("Click to sort on a column, Ctrl+Click to sort on extra column"));

			        TableColumnModel columnModel = table.getColumnModel();
					columnModel.getColumn(0).setPreferredWidth(0);
					columnModel.getColumn(0).setMinWidth(0);
					columnModel.getColumn(0).setMaxWidth(0);
					
					MainGui.statusBar.setText("");
				} catch (SQLException ex) {
					MainGui.statusBar.setText("Error contacting database !");
					JOptionPane.showMessageDialog(MainGui.desktop,
							new String[] { // Display a 2-line message
							ex.getClass().getName() + ": ", ex.getMessage() });
					MainGui.statusBar.setText("");					
				}
			}
		});
	}	

	private class Fenetre extends EscapeInternalFrame{
		private static final long serialVersionUID = -4687661419905061320L;
		private char mode;
		private JLabel role_label;
		private JTextField role_text;
		
		/**
		 * 
		 * @param mod
		 */
		public Fenetre(char mod){
			this.mode = mod;
			drawWindow();
		}
		
		private void drawWindow(){
			FormPanel pane = new FormPanel( "be/xtnd/cantine/gui/descriptions/roles.jfrm" );
			getContentPane().add(pane);
			
			role_label = pane.getLabel("role_label");
			role_label.setText(Messages.getString("window.roles.labels.role"));
			
			role_text = pane.getTextField("role");
			role_text.setText(role.getRole());
			commons.createButtonBar(this,new OkayEvent());
			
			pack();
			String title = Messages.getString((mode == Database.NEW)?"window.roles.titles.new":"window.roles.titles.modif");
			if(mode == Database.NEW){
				setTitle(title);
				setName(NEW_NAME);
			}else if(mode == Database.MODIF){
				Object args[] = {role.getRole()};
				setTitle(MessageFormat.format(title, args));
				Object[] args2 = {role.getId_role()};
				setName(MessageFormat.format(Messages.getString("window.roles.names.modif"), args2));
			}

			Integer layer_type = (select)?JLayeredPane.POPUP_LAYER:JLayeredPane.MODAL_LAYER;
			MainGui.desktop.add(this, layer_type);
			setLocation(MainGui.centerOnDesktop(getSize()));
			
			setResizable(true);
			setClosable(true);
			setIconifiable(true);
			setMaximizable(true);
			setVisible(true);
		}
		
		/**
		 * Valide les entrées obligtoires du formulaire,
		 * affiche un message sinon.
		 * 
		 * @return boolean résultat
		 */
		private boolean verifEntrees(){
			boolean valid = true;
			StringBuffer required = new StringBuffer();
			required.append(CommonsI18n.tr("The following are required:"));

			if(role_text.getText().trim().equalsIgnoreCase("")){
				required.append("\n- " + Messages.getString("window.roles.required.role"));
				if(valid) role_text.requestFocus();
				valid = false;
			}

			if(!valid){
				JOptionPane.showMessageDialog(
						MainGui.desktop, 
						required.toString(),
						CommonsI18n.tr("Error!"),
						JOptionPane.ERROR_MESSAGE);	
			}
			return valid;
		}
		 
		class OkayEvent implements ActionListener{
			/**
			 * @param e
			 */
			public void actionPerformed(ActionEvent e) {
				if(verifEntrees()){
					role.setRole(role_text.getText());
					
					if(mode == Database.NEW){
						if(role.storeRole()==-2){
							Object[] args = {role.getRole()};
							JOptionPane.showMessageDialog(
									MainGui.desktop,
									MessageFormat.format(Messages.getString("window.roles.messages.unique"),args),
									Messages.getString("window.roles.messages.unique.title"),
									JOptionPane.ERROR_MESSAGE
							);		
						}
					}
					if(mode == Database.MODIF){
						role.modifyRole(role.getId_role());
					}
					dispose();
					displayQueryResults(Role.LISTQUERY);
				}
			}		
		}
	}
}
