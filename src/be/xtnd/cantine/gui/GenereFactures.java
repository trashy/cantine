/*
 * GenereFactures.java, 01 oct. 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               	GenereFactures.java
 * Author's email :     	johan@x-tnd.be
 * Author's Website :   	http://cantine.sourceforge.net
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */
package be.xtnd.cantine.gui;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import be.xtnd.cantine.Facture;
import be.xtnd.commons.gui.EscapeInternalFrame;
import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.i18n.CommonsI18n;

import com.jeta.forms.components.panel.FormPanel;
import com.jeta.forms.components.separator.TitledSeparator;
import com.jgoodies.looks.plastic.PlasticInternalFrameUI;
import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JMonthChooser;
import com.toedter.calendar.JYearChooser;

/**
 * 
 * @author Johan Cwiklinski
 */
public class GenereFactures extends EscapeInternalFrame {
	private static final long serialVersionUID = -1010790622431427791L;
	private JLabel mois_annee_label, date_facture_label;
	private TitledSeparator title;
	private JMonthChooser mois;
	private JYearChooser annee;
	private JDateChooser date_facture;
	private JButton generer, annuler;
	private Calendar cal = Calendar.getInstance();
	private int month, year;
	/** Nom de la fenêtre */
	public static final String NAME = "genere_factures";

	/**
	 * 
	 *
	 */
	public GenereFactures(){
		super();
		
        putClientProperty(
                PlasticInternalFrameUI.IS_PALETTE,
                Boolean.TRUE);

		setClosable(true);
		setName(NAME);
		
		FormPanel pane = new FormPanel( "be/xtnd/cantine/gui/descriptions/generation_factures.jfrm" );

		title = (TitledSeparator)pane.getComponentByName("title");
		title.setText(Messages.getString("window.invoices.titles.generate"));
		
		mois_annee_label = pane.getLabel("mois_annee_label");
		mois_annee_label.setText(Messages.getString("window.invoices.labels.month_year"));
		
		date_facture_label = pane.getLabel("date_facture_label");
		date_facture_label.setText(Messages.getString("window.invoices.labels.invoice_date"));
		
		date_facture = (JDateChooser)pane.getComponentByName("date_facture");
		date_facture.setDate(cal.getTime());

		cal.add(Calendar.MONTH, 1);
		
		annee = (JYearChooser)pane.getComponentByName("annee");
		annee.setYear(year = cal.get(Calendar.YEAR));
		annee.addPropertyChangeListener(new ChangeDateListener());
		
		mois = (JMonthChooser)pane.getComponentByName("mois");
		mois.setMonth(month = cal.get(Calendar.MONTH));
		mois.addPropertyChangeListener(new ChangeDateListener());
		mois.setYearChooser(annee);
		
		generer = (JButton)pane.getButton("generer");
		generer.setText(Messages.getString("window.invoices.buttons.generate2"));
		generer.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Calendar date = new GregorianCalendar(
						annee.getYear(), 
						mois.getMonth(), 
						cal.get(Calendar.DAY_OF_MONTH));
				date.add(Calendar.MONTH,-1); //factures de mois prennent en compte les pointages mois-1
				SimpleDateFormat formatter = new SimpleDateFormat("MMMM yyyy");

				Calendar calFact = (Calendar)date.clone();
				calFact.set(Calendar.DAY_OF_MONTH, date.getActualMaximum(Calendar.DAY_OF_MONTH));
				calFact.add(Calendar.DAY_OF_MONTH,1);
				
				Object[] titleArgs = {formatter.format(calFact.getTime())};
				if(date.after(cal)){
					JOptionPane.showMessageDialog(
							MainGui.desktop,
							Messages.getString("window.invoices.messages.generate.impossible"),
							MessageFormat.format(Messages.getString("window.invoices.messages.generate.title"), titleArgs),
							JOptionPane.ERROR_MESSAGE
					);
				}else if(!date_facture.getDate().before(calFact.getTime())){
					JOptionPane.showMessageDialog(
							MainGui.desktop,
							Messages.getString("window.invoices.messages.generate.incorrect_date"),
							MessageFormat.format(Messages.getString("window.invoices.messages.generate.title"), titleArgs),
							JOptionPane.ERROR_MESSAGE
					);					
					date_facture.getSpinner().getEditor().getComponent(0).requestFocus();
				}else{
					int response = JOptionPane.showConfirmDialog(
							MainGui.desktop, 
							MessageFormat.format(Messages.getString("window.invoices.messages.generate.confirm"), titleArgs),
							MessageFormat.format(Messages.getString("window.invoices.messages.generate.title"), titleArgs),
							JOptionPane.YES_NO_OPTION,
							JOptionPane.WARNING_MESSAGE);
					if (response == JOptionPane.YES_OPTION) {				
						System.err.println("Génération factures");
						Facture fact = new Facture();
						fact.calculFactures(
								date.get(Calendar.MONTH), 
								date.get(Calendar.YEAR),
								date_facture.getDate());

						JInternalFrame[] fenetres = MainGui.desktop.getAllFrames();
						int i=0;
						Point sPos = null;
						while(i < fenetres.length){
							if(fenetres[i].getName() == Factures.LIST_NAME){
								sPos = fenetres[i].getLocation();
								fenetres[i].dispose();
							}
							i++;
						}
						new Factures(sPos);
						dispose();
					}

				}
			}
		});
		
		annuler = (JButton)pane.getComponentByName("annuler");
		annuler.setText(CommonsI18n.tr("Cancel"));
		annuler.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				dispose();
			}	
		});
		
		add(pane);
		
		pack();
		setLocation(MainGui.centerOnDesktop(getSize()));
		setVisible(true);
		MainGui.desktop.add(this);			
	}
	
    /**
     * 
     */
    public class ChangeDateListener implements PropertyChangeListener{
		/**
		 * @param evt
		 */
    	public void propertyChange(PropertyChangeEvent evt) {
			month = mois.getMonth();
			year = annee.getYear();

			SimpleDateFormat formatter = new SimpleDateFormat("d");
			int jour = new Integer(formatter.format(date_facture.getDate()));
			
			Calendar calend = new GregorianCalendar(
					year,month,jour
				);
			calend.add(Calendar.MONTH,-1);
			date_facture.setDate(
					calend.getTime()
				);
		}
    }
}
