/*
 * DateCommande.java, 6 oct. 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               	DateCommande.java
 * Author's email :     	johan@x-tnd.be
 * Author's Website :   	http://cantine.sourceforge.net
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;

import be.xtnd.cantine.Classe;
import be.xtnd.cantine.Eleve;
import be.xtnd.cantine.Planning;
import be.xtnd.cantine.Presence;
import be.xtnd.commons.GuiCommons;
import be.xtnd.commons.Print;
import be.xtnd.commons.db.Database;
import be.xtnd.commons.gui.EscapeInternalFrame;
import be.xtnd.commons.gui.MainGui;

import com.jeta.forms.components.panel.FormPanel;
import com.jeta.forms.components.separator.TitledSeparator;
import com.jgoodies.looks.plastic.PlasticInternalFrameUI;
import com.toedter.calendar.JDateChooser;

/**
 * 
 * 
 * @author Johan Cwiklinski
 *
 */
public class DateCommande extends EscapeInternalFrame{
	private static final long serialVersionUID = -7794526910130536955L;
	private JDateChooser date;
    private GuiCommons commons = new GuiCommons();
    private Database db;
    /** Nom de la fenêtre */
    public static final String NAME = "date_commande";
    
    /**
     * 
     *
     */
    public DateCommande(){
        super();
        super.setName(NAME);
        putClientProperty(
                PlasticInternalFrameUI.IS_PALETTE,
                Boolean.TRUE);

		FormPanel pane = new FormPanel( "be/xtnd/cantine/gui/descriptions/select_date_commande.jfrm" );

		TitledSeparator title = (TitledSeparator)pane.getComponentByName("title");
		title.setText(Messages.getString("window.print.select_date"));
		
		date = (JDateChooser)pane.getComponentByName("date");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, 1);
		date.setDate(cal.getTime());
		
		add(pane);
		commons.createButtonBar(this, new OkayEvent());
		
		pack();
		setLocation(
				MainGui.centerOnDesktop(this.getSize())
				);
		setMinimumSize(getSize());
		setVisible(true);
		setClosable(true);
		setIconifiable(true);
		setResizable(true);
		MainGui.desktop.add(this, JLayeredPane.POPUP_LAYER);    
		toFront();
		try {
			setSelected(true);
		} catch (PropertyVetoException e) {}

    }
    
    private class OkayEvent implements ActionListener{
    	/**
    	 * @param e
    	 */
        public void actionPerformed(ActionEvent e) {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String forDate = df.format(date.getDate());
			db = Launcher.db;	
		    String strQry = "SELECT COUNT("+Eleve.TABLE+"."+Eleve.PK+") AS compte, "+Classe.TABLE+".categorie FROM "+Eleve.TABLE+
		    		" INNER JOIN "+Planning.TABLE+" ON("+Eleve.TABLE+"."+Eleve.PK+"="+Planning.TABLE+"."+Eleve.PK+")"+
		    		" INNER JOIN "+Classe.TABLE+" ON("+Eleve.TABLE+"."+Classe.PK+"="+Classe.TABLE+"."+Classe.PK+")"+
		    		" WHERE date='"+forDate+"' AND " +
		    		"(id_type="+Presence.PRESENT+" OR id_type="+Presence.IMPREVU+" OR id_type="+Presence.PREPAYE+")"+
		    		" GROUP BY classes.categorie";
		    HashMap<Object, Object> params = new HashMap<Object, Object>();
		    params.put("HEADERS_PATH", ".config/");
		    params.put("DATE_COMMANDE", date.getDate());
		    params.put("REPORT_CONNECTION", db.getConnection());
		    try {
                ResultSet rs = db.execQuery(strQry);
                rs.last();
                if(rs.getRow()>0){
                	rs.beforeFirst();
                	new Print(".config", "commande_jour.jrxml", rs, params);
                }else{
					JOptionPane.showMessageDialog(
							MainGui.desktop,
							Messages.getString("window.print.nopages"),
							Messages.getString("window.print.nopages.title"),
							JOptionPane.INFORMATION_MESSAGE
					);
                }
            } catch (SQLException e1) {e1.printStackTrace();}            
        }
    }
}
