/*
 * Tiers.java, 20 sept. 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               	Tiers.java
 * Author's email :     	johan@x-tnd.be
 * Author's Website :   	http://cantine.sourceforge.net
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

import be.xtnd.cantine.CantineI18n;
import be.xtnd.cantine.Famille;
import be.xtnd.cantine.Role;
import be.xtnd.commons.GuiCommons;
import be.xtnd.commons.gui.EscapeInternalFrame;
import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.i18n.CommonsI18n;
import be.xtnd.validators.MailFieldVerifier;
import be.xtnd.validators.MaskFieldVerifier;

import com.jeta.forms.components.panel.FormPanel;
import com.jgoodies.looks.plastic.PlasticInternalFrameUI;

/**
 * 
 * 
 * @author Johan Cwiklinski
 *
 */
public class Tiers {
	private be.xtnd.cantine.Tiers tiers;
	private GuiCommons commons = new GuiCommons();
	private JTextField nom_tiers, prenom_tiers, mail_tiers, profession_tiers, employeur_tiers;
	private JFormattedTextField gsm_tiers, tel_pro_tiers, poste_tel_pro_tiers;
	private MaskFormatter fmPhone, fmPoste;
	private JTextArea commentaires_tiers;
	private JButton ajout_role;
	private JComboBox role;
	//private Integer id_famille;
	private JLabel role_label, nom_label, prenom_label, tel_portable_label, mail_label, profession_label,
		employeur_label, tel_pro_label, poste_tel_pro_label, commentaires_label;
	/** Nom de la fenêtre nouveau */
	public static final String NEW_NAME = "nouveau_tiers";
	private Familles familles;
	private Famille famille;
	
	/**
	 * 
	 * @param familles
	 */
	public void nouveauTiers(Familles familles){
	//public void nouveauTiers(Integer famille, JInternalFrame parent){
		this.familles = familles;
		this.famille = familles.famille;
		new Fenetre();
	}
	
	private void setValues(){
		tiers.setNom(nom_tiers.getText());
		tiers.setPrenom(prenom_tiers.getText());
		tiers.setTel_portable(gsm_tiers.getText());
		tiers.setMail(mail_tiers.getText());
		tiers.setEmployeur(employeur_tiers.getText());
		tiers.setProfession(profession_tiers.getText());
		tiers.setTel_travail(tel_pro_tiers.getText());
		tiers.setPoste_tel_travail(poste_tel_pro_tiers.getText());
		tiers.setCommentaires(commentaires_tiers.getText());
		tiers.setRole(new Role(role.getSelectedItem().toString()).getId_role().intValue());
		tiers.setId_famille(famille.getId_famille());
		
		//tiers.storeTiers();
		famille.getTiers().add(tiers);
	}
	
	private boolean verifEntrees(){
		boolean valid = true;
		StringBuffer required = new StringBuffer();
		required.append(CommonsI18n.tr("The following are required:"));		
		
		if(role.getSelectedIndex()==0){
			required.append("\n- " + Messages.getString("window.tiers.required.role"));
			valid = false;
		}
		if(nom_tiers.getText().trim().equalsIgnoreCase("")){
			required.append("\n- " + Messages.getString("window.tiers.required.name"));
			if(valid) nom_tiers.requestFocus();
			valid = false;
		}
		if(prenom_tiers.getText().trim().equalsIgnoreCase("")){
			required.append("\n- " + Messages.getString("window.tiers.required.surname"));
			if(valid) prenom_tiers.requestFocus();
			valid = false;
		}

		if(!valid){
			JOptionPane.showMessageDialog(
					MainGui.desktop, 
					required.toString(),
					CommonsI18n.tr("Error!"),
					JOptionPane.ERROR_MESSAGE);			
		}
		return valid;
	}

	/**
	 * 
	 * @return FormPanel
	 */
	public FormPanel tiersPanel(){
		FormPanel pane = new FormPanel( "be/xtnd/cantine/gui/descriptions/parents.jfrm" );
		
		role_label = pane.getLabel("role_label");
		role_label.setText(Messages.getString("window.tiers.labels.role"));
		
		role = pane.getComboBox("roles");		
		commons.buildCombo(role, Role.listing());

		ajout_role = (JButton)pane.getButton("ajout_role");
		ajout_role.setToolTipText(Messages.getString("window.tiers.tooltips.add_role"));
		ajout_role.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				new Roles().afficheListe(role);
			}			
		});
		
		nom_label = pane.getLabel("nom_label");
		nom_label.setText(CantineI18n.tr("Name"));
		
		nom_tiers = pane.getTextField("nom");
						
		prenom_label = pane.getLabel("prenom_label");
		prenom_label.setText(CantineI18n.tr("First name"));
		
		prenom_tiers = pane.getTextField("prenom");
		
		tel_portable_label = pane.getLabel("tel_portable_label");
		tel_portable_label.setText(CantineI18n.tr("Mobile phone"));
		
		try {
			fmPhone = new MaskFormatter("## ## ## ## ##");
		} catch (ParseException e) {e.printStackTrace();}
		
		gsm_tiers = (JFormattedTextField)pane.getComponentByName("tel_portable");
		gsm_tiers.setFormatterFactory(new DefaultFormatterFactory(fmPhone));
		gsm_tiers.setInputVerifier(new MaskFieldVerifier("", fmPhone.getMask()));
		
		mail_label = pane.getLabel("mail_label");
		mail_label.setText(CantineI18n.tr("E-Mail"));
		
		mail_tiers = pane.getTextField("mail");
		mail_tiers.setInputVerifier(new MailFieldVerifier(""));
		
		profession_label = pane.getLabel("profession_label");
		profession_label.setText(Messages.getString("window.tiers.labels.profession"));
		
		profession_tiers = pane.getTextField("profession");
		
		employeur_label = pane.getLabel("employeur_label");
		employeur_label.setText(Messages.getString("window.tiers.labels.employer"));
		
		employeur_tiers = pane.getTextField("employeur");
		
		tel_pro_label = pane.getLabel("tel_pro_label");
		tel_pro_label.setText(CantineI18n.tr("Phone"));
		
		tel_pro_tiers = (JFormattedTextField)pane.getComponentByName("tel_pro");
		tel_pro_tiers.setFormatterFactory(new DefaultFormatterFactory(fmPhone));
		tel_pro_tiers.setInputVerifier(new MaskFieldVerifier("", fmPhone.getMask()));
		
		poste_tel_pro_label = pane.getLabel("poste_tel_pro_label");
		poste_tel_pro_label.setText(Messages.getString("window.tiers.labels.pro_poste"));
		
		try {
			fmPoste = new MaskFormatter("*******");
			fmPoste.setValidCharacters("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
		} catch (ParseException e1) {e1.printStackTrace();}
		
		poste_tel_pro_tiers = (JFormattedTextField)pane.getComponentByName("poste_tel_pro");
		fmPoste.install(poste_tel_pro_tiers);
		
		commentaires_label = pane.getLabel("commentaires_label");
		commentaires_label.setText(CantineI18n.tr("Comments"));
		
		commentaires_tiers = (JTextArea)pane.getComponentByName("commentaires");

		return pane;
	} 

	private class Fenetre extends EscapeInternalFrame{
		private static final long serialVersionUID = 7181011312270145846L;
		//private JInternalFrame familly_frame;

		/**
		 * 
		 */
		public Fenetre(){
			super();
			//this.familly_frame = familly_frame;
			tiers = new be.xtnd.cantine.Tiers();
			tiers.setId_famille(famille.getId_famille());
			
	        putClientProperty(
	                PlasticInternalFrameUI.IS_PALETTE,
	                Boolean.TRUE);
	        
	        FormPanel elevesPane = tiersPanel();
			getContentPane().add(elevesPane);
			elevesPane.setBorder(new EmptyBorder(3,3,3,3));
			
			commons.createButtonBar(this, new OkayEvent());
			
			pack();
			setTitle(Messages.getString("window.tiers.titles.new"));
			setName(NEW_NAME);

			MainGui.desktop.add(this, JLayeredPane.POPUP_LAYER);
			setLocation(MainGui.centerOnDesktop(getSize()));
			
			setResizable(true);
			setClosable(true);
			setIconifiable(true);
			setMaximizable(true);
			setVisible(true);
		}

		class OkayEvent implements ActionListener{
			/**
			 * @param e
			 */			
			public void actionPerformed(ActionEvent e) {
				if(verifEntrees()){
					setValues();
					//rafraichit la fenetre famille
					familles.tiersTab();
					dispose();
				}
			}		
		}

	}
}
