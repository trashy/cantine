/*
 * Launcher.java, 2005-08-08
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               Launcher.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://cantine.ulysses.fr/
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine.gui;

import java.awt.Dimension;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.ImageIcon;

import be.xtnd.cantine.ApplicationProperties;
import be.xtnd.cantine.Options;
import be.xtnd.commons.ShowGnu;
import be.xtnd.commons.db.Database;
import be.xtnd.commons.db.SqlErrors;
import be.xtnd.commons.db.hsql.HsqldbErrors;
import be.xtnd.commons.db.mysql.MysqlErrors;
import be.xtnd.commons.gui.MainGui;


/**
 * Cantine's main GUI
 *
 * @author Johan Cwiklinski
 * @since 2005-08-08
 */
public class Launcher extends MainGui {
	private static final long serialVersionUID = 4981548291124372064L;
	static Menus menus = new Menus();
	/** Application options */
	public static Options options;
	/** New icon */
	public static final ImageIcon NEW_IMAGE = new ImageIcon(ClassLoader.getSystemResource("be/xtnd/cantine/icons/new.png")); 
	/** Edit icon */
	public static final ImageIcon MODIF_IMAGE = new ImageIcon(ClassLoader.getSystemResource("be/xtnd/cantine/icons/open.png")); 
	/** Delete icon */
	public static final ImageIcon DELETE_IMAGE = new ImageIcon(ClassLoader.getSystemResource("be/xtnd/cantine/icons/delete.png")); 
	/** Search icon */
	public static final ImageIcon SEARCH_IMAGE = new ImageIcon(ClassLoader.getSystemResource("be/xtnd/cantine/icons/search.png")); 
	/** List icon */
	public static final ImageIcon LIST_IMAGE = new ImageIcon(ClassLoader.getSystemResource("be/xtnd/cantine/icons/list.png"));
	public static Launcher launch;
	/** */
	public static SqlErrors sql_errors;

	/**
	 * Main method
	 * @param args
	 */
    public static void main(String[] args) {
    	String application_date = ApplicationProperties.getString("appli.date");

		SimpleDateFormat dfp = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat df = new SimpleDateFormat("MMMM yyyy");

		String date = null;
		try {
			date = df.format(dfp.parse(application_date));
		} catch (ParseException e) {
			//in case of date parsing failure, we switch back to the text value
			date = application_date;
		}

    	System.out.println(
    			"***** "+
    			ApplicationProperties.getString("appli.name") +
				" - " +
				ApplicationProperties.getString("appli.author") +
    			" - Gnu GPL - V" + ApplicationProperties.getString("appli.version") +
				" " + date +
				" *****");

    	new ShowGnu(
    			ApplicationProperties.getString("appli.name") + " " + ApplicationProperties.getString("appli.version"), 
    			date,
    			ApplicationProperties.getString("appli.author"));
    	launch = new Launcher();
	    new MainMenu();
    }
    
    /**
     * Runs the graphical application
     */
    public Launcher() {
    	super(
    			ApplicationProperties.getString("appli.name") + " " + ApplicationProperties.getString("appli.version"),
				new ImageIcon(ClassLoader.getSystemResource(ApplicationProperties.getString("appli.logo"))),
				menus.BuildMenuBar(),
				menus.buildToolBar(),
				"cantine.xml",
				new Dimension(1024, 768),
				ApplicationProperties.getBundle()
			);
    	super.setLocationRelativeTo(this.getParent());
    }
    
    /**
     * Load database
     */
    @Override
    public void loadDb(){
		sql_errors = (conf.getProvider_name().equalsIgnoreCase("hsqldb"))?new HsqldbErrors():new MysqlErrors();
    	if(conf.getProvider_name().equalsIgnoreCase("hsqldb")){//hsqldb case
        	db = new Database();
        	db.setConnectionParameters(ResultSet.TYPE_SCROLL_INSENSITIVE,
    				ResultSet.CONCUR_UPDATABLE);
    		db.connect();
    		if(db.getErrCode() != 0) HsqldbErrors.showError(db.getErrCode(), launch, conf);
    	}else if(conf.getProvider_name().equalsIgnoreCase("mysql")){//mysql 5 case
    		db = new Database(true);
    		if(db.getErrCode()!=0) MysqlErrors.showError(db.getErrCode(), launch, conf);
    	}    	
    	options = new Options();
    }
    
    protected void finalize(){
    	db.disconnect();
    }
}
