/*
 * Menus.java, 2005-08-08
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005-2010 Johan Cwiklinski
 *
 * File :               Menus.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://cantine.ulysses.fr
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine.gui;

import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;

import be.xtnd.cantine.ApplicationProperties;
import be.xtnd.cantine.CantineI18n;
import be.xtnd.commons.GuiCommons;
import be.xtnd.commons.utils.Browser;

import com.jgoodies.looks.BorderStyle;
import com.jgoodies.looks.HeaderStyle;
import com.jgoodies.looks.Options;
import com.jgoodies.looks.plastic.PlasticXPLookAndFeel;

/**
 * Builds menus and tool bar, handle events on these components 
 *
 * @author Johan Cwiklinski
 * @since 2005-08-08
 */
public class Menus {
	private GuiCommons commons = new GuiCommons();
	
	/**
	 * Builds complete menu bar
	 * @return JMenuBar
	 */
    public JMenuBar BuildMenuBar(){
    	JMenuBar menu = new JMenuBar();
		menu.putClientProperty(Options.HEADER_STYLE_KEY, HeaderStyle.BOTH);
		menu.putClientProperty(PlasticXPLookAndFeel.BORDER_STYLE_KEY, BorderStyle.SEPARATOR);
    	
		JMenu file = new JMenu();
		commons.submenuEntry(file, null, CantineI18n.tr("File"), CantineI18n.trc("File menu mnemonic", "f"));
		
		file.add(
				commons.configMenuEntry(
						ApplicationProperties.getBundle(),
						Launcher.launch,
						new ImageIcon(ClassLoader.getSystemResource(ApplicationProperties.getString("menus.file.preferences.icon")))
				)
		);
		
		JMenuItem options = new JMenuItem();
		commons.submenuEntry(
				options,
				new ImageIcon(ClassLoader.getSystemResource(ApplicationProperties.getString("menus.file.options.icon"))),
				CantineI18n.tr("Options..."),
				CantineI18n.tr("{0} options...", ApplicationProperties.getString("appli.name")),
				CantineI18n.trc("Options menu mnemonic", "o"),
				null
		);
		options.addActionListener(new OptionsAction());
		file.add(options);

		file.addSeparator();

		JMenuItem save = new JMenuItem();
		commons.submenuEntry(
				save,
				new ImageIcon(ClassLoader.getSystemResource(ApplicationProperties.getString("menus.file.save.icon"))),
				CantineI18n.tr("Backups"),
				CantineI18n.tr("Database backup and restore"),
				CantineI18n.trc("Backups menu mnemonic", "b"),
				null
		);
		save.addActionListener(new SaveAction());
		file.add(save);
		
		file.addSeparator();
		
		file.add(commons.quitMenuEntry(ApplicationProperties.getBundle()));
		
		menu.add(file);
		
		JMenu datas = new JMenu();
		commons.submenuEntry(datas, null, CantineI18n.tr("Datas"), CantineI18n.trc("Datas menu mnemonic", "d"));
		
		JMenu families = new JMenu();
		commons.submenuEntry(families, new ImageIcon(ClassLoader.getSystemResource(ApplicationProperties.getString("menus.datas.families.icon"))), CantineI18n.tr("Families"), CantineI18n.trc("Families menu mnemonic", "f"));
		
		JMenuItem familiesList = new JMenuItem();
		commons.submenuEntry(
				familiesList,
				new ImageIcon(ClassLoader.getSystemResource(ApplicationProperties.getString("menus.datas.families.list.icon"))),
				CantineI18n.tr("List"),
				CantineI18n.tr("Families list"),
				CantineI18n.trc("List mnemonic", "l"),
				null
		);
		familiesList.addActionListener(new FamilleListMenuAction());
		
		JMenuItem familiesNew = new JMenuItem();
		commons.submenuEntry(
				familiesNew,
				new ImageIcon(ClassLoader.getSystemResource(ApplicationProperties.getString("menus.datas.families.new.icon"))),
				CantineI18n.tr("New family card"),
				CantineI18n.tr("Create a new family card"),
				CantineI18n.trc("New mnemonic", "n"),
				KeyStroke.getKeyStroke(KeyEvent.VK_N,Event.CTRL_MASK)
		);
		familiesNew.addActionListener(new FamilleNewMenuAction());
		
		JMenuItem roles = new JMenuItem();
		commons.submenuEntry(
				roles,
				null,
				CantineI18n.tr("Roles"),
				CantineI18n.tr("Third persons roles"),
				CantineI18n.trc("Roles mnemonic", "r"),
				null
		);
		roles.addActionListener(new RolesMenuAction());
		
		families.add(familiesList);
		families.add(familiesNew);
		families.addSeparator();
		families.add(roles);
		datas.add(families);
		
		JMenu adults = new JMenu();
		commons.submenuEntry(adults, new ImageIcon(ClassLoader.getSystemResource(ApplicationProperties.getString("menus.datas.adults.icon"))), CantineI18n.tr("Adults"), CantineI18n.trc("Adults menu mnemonic", "a"));
		
		JMenuItem adultsList = new JMenuItem();
		commons.submenuEntry(
				adultsList,
				new ImageIcon(ClassLoader.getSystemResource(ApplicationProperties.getString("menus.datas.adults.list.icon"))),
				CantineI18n.tr("List"),
				CantineI18n.tr("Adults list"),
				CantineI18n.trc("List mnemonic", "l"),
				null
		);
		adultsList.addActionListener(new AdulteListMenuAction());
		
		JMenuItem adultsNew = new JMenuItem();
		commons.submenuEntry(
				adultsNew,
				new ImageIcon(ClassLoader.getSystemResource(ApplicationProperties.getString("menus.datas.adults.new.icon"))),
				CantineI18n.tr("New adult card"),
				CantineI18n.tr("Create a new adult card"),
				CantineI18n.trc("New mnemonic", "n"),
				null
		);
		adultsNew.addActionListener(new AdulteNewMenuAction());
		
		adults.add(adultsList);
		adults.add(adultsNew);
		datas.add(adults);
		
		datas.addSeparator();

		JMenuItem invoices = new JMenuItem();
		commons.submenuEntry(
				invoices,
				new ImageIcon(ClassLoader.getSystemResource(ApplicationProperties.getString("menus.datas.invoices.icon"))),
				CantineI18n.tr("Invoices"),
				CantineI18n.tr("Invoices management"),
				CantineI18n.trc("Invoices mnemonic", "i"),
				null
		);
		invoices.addActionListener(new FactureAction());
		
		datas.add(invoices);
		
		datas.addSeparator();

		JMenuItem doctors = new JMenuItem();
		commons.submenuEntry(
				doctors,
				new ImageIcon(ClassLoader.getSystemResource(ApplicationProperties.getString("menus.datas.doctors.icon"))),
				CantineI18n.tr("Doctors"),
				CantineI18n.tr("Doctors management"),
				CantineI18n.trc("Doctors mnemonic", "d"),
				null
		);
		doctors.addActionListener(new MedecinsMenuAction());
		
		datas.add(doctors);
		
		JMenuItem hospitals = new JMenuItem();
		commons.submenuEntry(
				hospitals,
				new ImageIcon(ClassLoader.getSystemResource(ApplicationProperties.getString("menus.datas.hospitals.icon"))),
				CantineI18n.tr("Hospitals"),
				CantineI18n.tr("Hospitals management"),
				CantineI18n.trc("Hospitals mnemonic", "h"),
				null
		);
		hospitals.addActionListener(new HopitauxMenuAction());
		
		datas.add(hospitals);
		datas.addSeparator();
		
		JMenuItem planning = new JMenuItem();
		commons.submenuEntry(
				planning,
				new ImageIcon(ClassLoader.getSystemResource(ApplicationProperties.getString("menus.datas.planning.icon"))),
				CantineI18n.tr("Planning"),
				CantineI18n.tr("Planning management"),
				CantineI18n.trc("Planning mnemonic", "p"),
				null
		);
		planning.addActionListener(new PlanningMenuAction());
		datas.add(planning);
		
		menu.add(datas);
		
		JMenu help = new JMenu();
		commons.menuEntry(help, CantineI18n.tr("Help"), CantineI18n.trc("Help menu mnemonic", "h"), null);
		
		JMenuItem contents = new JMenuItem();
		commons.submenuEntry(
				contents,
				new ImageIcon(ClassLoader.getSystemResource(ApplicationProperties.getString("menus.help.contents.icon"))),
				CantineI18n.tr("Online help"),
				CantineI18n.tr("{0} documentation (wiki)", ApplicationProperties.getString("appli.name")),
				CantineI18n.trc("Help mnemonic", "h"),
				KeyStroke.getKeyStroke("F1")
		);
		contents.addActionListener(new HelpAction());
		help.add(contents);
		
		JMenuItem website = new JMenuItem();
		commons.submenuEntry(
				website,
				null,
				CantineI18n.tr("Website"),
				CantineI18n.tr("{0} website", ApplicationProperties.getString("appli.name")),
				CantineI18n.trc("Website mnemonic", "w"),
				null
		);
		website.addActionListener(new OpenWebsite());
		help.add(website);
		
		help.addSeparator();

		help.add(commons.aboutMenuEntry(ApplicationProperties.getBundle()));
		
		menu.add(help);
		
		return menu;
    }
       
	/**
	 * Build main toolbar
	 * @return JToolBar
	 */
    public JToolBar buildToolBar(){
    	JToolBar bar = new JToolBar();
		bar.putClientProperty(Options.HEADER_STYLE_KEY, HeaderStyle.BOTH);
		bar.putClientProperty(PlasticXPLookAndFeel.BORDER_STYLE_KEY, BorderStyle.EMPTY);
		bar.setRollover(true);
		
		JButton mainMenuBtn = new JButton();
		mainMenuBtn.setIcon(new ImageIcon(ClassLoader.getSystemResource(ApplicationProperties.getString("appli.logo"))));
		mainMenuBtn.setToolTipText(CantineI18n.tr("{0} main menu", ApplicationProperties.getString("appli.name")));
		mainMenuBtn.addActionListener(new MainMenuAction());
		bar.add(mainMenuBtn);
		
		bar.addSeparator();
		
		JButton famillesMenuButton = new JButton();
		famillesMenuButton.setIcon(new ImageIcon(ClassLoader.getSystemResource("be/xtnd/cantine/icons/famille.png")));
		famillesMenuButton.setToolTipText(CantineI18n.tr("Families management"));
		famillesMenuButton.addActionListener(new FamilleListMenuAction());
		bar.add(famillesMenuButton);
		
		JButton adultsMenuButton = new JButton();
		adultsMenuButton.setIcon(new ImageIcon(ClassLoader.getSystemResource("be/xtnd/cantine/icons/adultes.png")));
		adultsMenuButton.setToolTipText(CantineI18n.tr("Adults management"));
		adultsMenuButton.addActionListener(new AdulteListMenuAction());
		bar.add(adultsMenuButton);

		JButton calMenuButton = new JButton();
		calMenuButton.setIcon(new ImageIcon(ClassLoader.getSystemResource("be/xtnd/cantine/icons/pointages.png")));
		calMenuButton.setToolTipText(CantineI18n.tr("Planning management"));
		calMenuButton.addActionListener(new PlanningMenuAction());
		bar.add(calMenuButton);
		
		bar.addSeparator();
		
		JButton doctorMenuButton = new JButton();
		doctorMenuButton.setIcon(new ImageIcon(ClassLoader.getSystemResource("be/xtnd/cantine/icons/medecins.png")));
		doctorMenuButton.setToolTipText(CantineI18n.tr("Doctors management"));
		doctorMenuButton.addActionListener(new MedecinsMenuAction());
		bar.add(doctorMenuButton);
		
		JButton hospitalMenuButton = new JButton();
		hospitalMenuButton.setIcon(new ImageIcon(ClassLoader.getSystemResource("be/xtnd/cantine/icons/hopitaux.png")));
		hospitalMenuButton.setToolTipText(CantineI18n.tr("Hospitals management"));
		hospitalMenuButton.addActionListener(new HopitauxMenuAction());
		bar.add(hospitalMenuButton);

		bar.addSeparator();
		
		JButton invoiceMenuButton = new JButton();
		invoiceMenuButton.setIcon(new ImageIcon(ClassLoader.getSystemResource("be/xtnd/cantine/icons/factures.png")));
		invoiceMenuButton.setToolTipText(CantineI18n.tr("Invoices management"));
		invoiceMenuButton.addActionListener(new FactureAction());
		bar.add(invoiceMenuButton);

		bar.addSeparator();
		
		JButton printMenuButton = new JButton();
		printMenuButton.setIcon(new ImageIcon(ClassLoader.getSystemResource("be/xtnd/cantine/icons/print.png")));
		printMenuButton.setToolTipText(CantineI18n.tr("Print reports"));
		printMenuButton.addActionListener(new PrintMenuAction());
		bar.add(printMenuButton);
		
    	return bar;
    }
    
    /**
     * Action on help
     * @author trasher
     * @since 2005-08-08
     */
	class HelpAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Browser.displayURL(ApplicationProperties.getString("appli.doc.url"));
		}
	}
	
	/**
	 * Action on website
	 * @author trasher
	 * @since 2005-08-08
	 */
	class OpenWebsite implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Browser.displayURL(ApplicationProperties.getString("appli.url"));
		}
	}
	
	/**
	 * Action on options
	 * @author trasher
	 * @since 2005-08-08
	 */
	class OptionsAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(Launcher.verifUnicite(OptionsGui.NAME, null)){
				new OptionsGui();
			}
		}
	}
	
	/**
	 * Action on backup
	 * @author trasher
	 * @since 2005-08-08
	 */
	class SaveAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(Launcher.verifUnicite(Sauvegarde.NAME, null)){
				new Sauvegarde();
			}
		}
	}
	
	/**
	 * Action on main menu
	 * @author trasher
	 * @since 2005-08-08
	 */
	class MainMenuAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(Launcher.verifUnicite(MainMenu.NAME, null)){		
				new MainMenu();
			}
		}
	}
	
	/**
	 * Action on invoices
	 * @author trasher
	 * @since 2005-08-08
	 */
	class FactureAction implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if(Launcher.verifUnicite(Factures.LIST_NAME, null)){		
				new Factures();
			}
		}		
	}
	
	/**
	 * Action on families list
	 * @author trasher
	 * @since 2005-08-08
	 */
	class FamilleListMenuAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(Launcher.verifUnicite(Familles.LIST_NAME, null)){		
				new Familles().afficheListe();
			}
		}
	}
	
	/**
	 * Action on new family
	 * @author trasher
	 * @since 2005-08-08
	 */
	class FamilleNewMenuAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(Launcher.verifUnicite(Familles.NEW_NAME, null)){		
				new Familles().nouvelleFamille();
			}
		}
	}
	
	/**
	 * Action on roles
	 * @author trasher
	 * @since 2005-08-08
	 */
	class RolesMenuAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(Launcher.verifUnicite(Roles.LIST_NAME, null)){		
				new Roles().afficheListe();
			}
		}
	}
	
	/**
	 * Action on adults list
	 * @author trasher
	 * @since 2005-08-08
	 */
	class AdulteListMenuAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(Launcher.verifUnicite(Adultes.LIST_NAME, null)){		
				new Adultes().afficheListe();
			}
		}
	}
	
	/**
	 * Action on new adult
	 * @author trasher
	 * @since 2005-08-08
	 */
	class AdulteNewMenuAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(Launcher.verifUnicite(Adultes.NEW_NAME, null)){		
				new Adultes().nouvelAdulte();
			}
		}
	}
	
	/**
	 * Action on doctors
	 * @author trasher
	 * @since 2005-08-08
	 */
	class MedecinsMenuAction implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if(Launcher.verifUnicite(Urgences.DOCTORS_LIST_NAME, null)){		
				new Urgences("medecins").getList();
			}
		}		
	}
	
	/**
	 * Action on hospitals
	 * @author trasher
	 * @since 2005-08-08
	 */
	class HopitauxMenuAction implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if(Launcher.verifUnicite(Urgences.HOSPITALS_LIST_NAME, null)){		
				new Urgences("hopitaux").getList();
			}
		}		
	}
	
	/**
	 * Action on planning
	 * @author trasher
	 * @since 2005-08-08
	 */
	class PlanningMenuAction implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if(Launcher.verifUnicite(Plannings.NAME, null)){		
				new Plannings();
			}
		}		
	}
	
	/**
	 * Action on print
	 * @author trasher
	 * @since 2005-08-08
	 */
	class PrintMenuAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(Launcher.verifUnicite(PrintMenu.NAME, null)){		
				new PrintMenu();
			}
		}
	}
}
