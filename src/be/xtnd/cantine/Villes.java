/*
 * Villes.java, 19 juin 2006
 * 
 * This file is part of cantine-2.0.
 *
 * Copyright © 2006 Johan Cwiklinski
 *
 * File :               Villes.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://www.x-tnd.be/
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

/**
 * 
 */
package be.xtnd.cantine;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import be.xtnd.cantine.gui.Launcher;
import be.xtnd.commons.db.Database;

/**
 * @author Johan Cwiklinski
 *
 */
public class Villes {
	static Logger logger = Logger.getLogger(Villes.class.getName());

	/**
	 * @param adultes
	 * @return Liste des noms de villes
	 */
	public static ArrayList<String> liste(boolean adultes){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/cantine/log4j.properties"));
		ArrayList<String> array = new ArrayList<String>(500);
		Database db = Launcher.db;
		
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT DISTINCTROW ville FROM " + Famille.TABLE + " WHERE adulte=");
		sb.append((adultes)?"true":"false");
		sb.append(" ORDER BY ville ASC");
		
		try {
			ResultSet rs = db.execQuery(sb.toString());
			rs.beforeFirst();
			while(rs.next())
				array.add(rs.getString("ville"));
		} catch (SQLException e) {
			logger.fatal("SQLException while listing towns " + e.getMessage() + " (code was " + e.getErrorCode() + ")");			
			e.printStackTrace();
		}
		
		array.trimToSize();
		return array;
	}
}
