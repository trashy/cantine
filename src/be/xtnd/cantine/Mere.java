/*
 * Mere.java, 8 août 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               Mere.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://cantine.sourceforge.net
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */
package be.xtnd.cantine;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import be.xtnd.cantine.gui.Launcher;
import be.xtnd.commons.db.Database;

/**
 * 
 * 
 * @author Johan Cwiklinski
 */
public class Mere extends Tiers {
    private Database connexion;
	static Logger logger = Logger.getLogger(Mere.class.getName());
    
    /**
     * Initialise une mère
     *
     */
    public Mere() {
    	super();
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/cantine/log4j.properties"));
    	super.setRole(Role.MERE);
    }
    
    /**
     * Cherche le nom de la mère de la famille
     * @param id_famille id de la famille de recherche
     * @return nom de la mère de la famille
     * 
     * @see Famille#setNom(String)
     */
    public String nomMere(Integer id_famille){
    	String nom = new String();;
    	String strQuery = "SELECT nom FROM "+TABLE+" WHERE "+Role.PK+"="+Role.MERE+" AND "+Famille.PK+"="+id_famille;
    	connexion = Launcher.db;
    	try {
			ResultSet rs = connexion.execQuery(strQuery);
			while(rs.next()){
				nom = rs.getString("nom");
			}
		} catch (SQLException e) {
    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode());
			e.printStackTrace();
		}
    	return nom;
    }

}
