/*
 * Planning.java, 18 août 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               			Planning.java
 * Author's email :     	johan@x-tnd.be
 * Author's Website :   	http://cantine.sourceforge.net
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import be.xtnd.cantine.gui.Launcher;
import be.xtnd.commons.db.Database;

/**
 * Planning de la cantine. Gère les absences et les reports sur le mois suivant. 
 * 
 * @author Johan Cwiklinski
 *
 */
public class Planning {
	private Database database;
	private Integer id_eleve;
	/** Nom de la table des plannings */
	public static final String TABLE = "pointages";
	static Logger logger = Logger.getLogger(Planning.class.getName());

	/**
	 * Initialise un planning pour un élève selon un type de présence
	 * 
	 * @param id_eleve id de l'élève à initialiser
	 */
	public Planning(Integer id_eleve){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/cantine/log4j.properties"));
		this.id_eleve = id_eleve;
	}
	
	/**
	 * Charge les valeurs de présence pour l'élève
	 * @param month mois d'initialisation
	 * @param year année d'initialisation
	 * présents ne seront pas sélectionnés.
	 * @return ArrayList des pointages de l'élève 
	 */
	public ArrayList<Object[]> getValues(int month, int year){
		database = Launcher.db;
		ArrayList<Object[]> values = new ArrayList<Object[]>(20);
		
		DateFormat searchDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		String firstDate = searchDateFormat.format(cal.getTime());
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		String lastDate = searchDateFormat.format(cal.getTime());
		
		String strSql = "SELECT * FROM "+TABLE+" WHERE id_eleve="+this.id_eleve+" AND "+
			"date BETWEEN '"+firstDate+"' AND '"+lastDate+"'";
		
		try {
			ResultSet rs_planning = database.execQuery(strSql);
			DateFormat dateFormat = new SimpleDateFormat("d");
			while(rs_planning.next()){
				String strdate = dateFormat.format(rs_planning.getDate("date"));
				Object[] o = {
						strdate,
						rs_planning.getDate("date"),
						new Integer(rs_planning.getInt("id_type"))
				};
				values.add(o);
			}
		} catch (SQLException e) {
    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode());
			e.printStackTrace();
		}
    	values.trimToSize();
    	return values;
	}
	
	/**
	 * Enregistre les valeurs des pointages
	 * @param values tableau des présences de l'élève
	 */
	public void storeValues(ArrayList<Object> values){
		database = Launcher.db;
		
		for(int i = 0 ; i < values.size() ; i++){
			Object[] o = (Object[]) values.get(i);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String strdate = dateFormat.format(o[0]);

			String strSql = "INSERT INTO "+TABLE+" (date, id_eleve, id_type) VALUES ('"+
				strdate+"', "+this.id_eleve+", "+o[1]+")";
			try {database.execUpdate(strSql);} catch (SQLException e) {
				if(Launcher.sql_errors.isDuplicate(e.getErrorCode())){
					logger.debug("Record identified by "+strdate+","+id_eleve+" already exists, trying to update");
					String strModif = "UPDATE "+TABLE+" SET id_type="+o[1]+" WHERE date='"+strdate+"' AND "+
						"id_eleve="+this.id_eleve;
					try {database.execUpdate(strModif);} catch (SQLException e2) {
						logger.error("SQLError updating planning entry "+e2.getMessage()+" (code "+e2.getErrorCode()+") "+e2);
						e2.printStackTrace();
					}
				}else{
		    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode()+") "+e);
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Supprime une valeur à une date donnée
	 * @param date date à supprimer
	 */
	public void deleteValue(Date date){
	    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	    String dateToDel = df.format(date); 
	    String delQry = "DELETE FROM "+TABLE+" WHERE date='"+dateToDel+"' AND id_eleve="+this.id_eleve;
	    database = Launcher.db;
	    try {
            database.execUpdate(delQry);
        } catch (SQLException e) {
    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode());
        	e.printStackTrace();
        }
	}
}
