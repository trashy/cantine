/*
 * Adulte.java, 26 août 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               	Adulte.java
 * Author's email :     	johan@x-tnd.be
 * Author's Website :   	http://cantine.sourceforge.net
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * 
 * 
 * @author Johan Cwiklinski
 */
public class Adulte extends Famille {
	private Eleve adulte;
	/** Requête de liste des adultes */
	public static final String LISTQUERY = "SELECT id_famille, nom, adresse, cp, ville FROM familles WHERE adulte=true ORDER BY nom ASC";
	/** Monsieur ('G') */
	public static final char MONSIEUR = 'G';
	/** Madame ('F') */
	public static final char MADAME = 'F';
	/** Mademoiselle ('F') */
	public static final char MADEMOISELLE = 'H';
	
	/**
	 * Constructeur par défaut
	 *
	 */
	public Adulte(){
		super();
		adulte = new Eleve();
		adulte.setClasse(new Classe(new Integer(Classe.ADULTE)));
		adulte.setLieu_naissance(new String());
		adulte.setDate_naissance(Calendar.getInstance().getTime());
		adulte.setAllergies(new String());
		ArrayList<Eleve> childs = new ArrayList<Eleve>(1);
		childs.add(adulte);
		super.setEnfants(childs);
		super.setAdulte(true);
		super.setId_hopital(null);
		super.setId_medecin(null);
		super.setAssurance(new String());
		super.setNum_police(new String());
		super.setTelephone(new String());
		super.setTel_portable(new String());
	}
	
	/**
     * Renseigne les champs de classe en fonction
     * du contenu de la base de données
     * 
	 * @param id identifiant de l'adulte
	 */
	public Adulte(Integer id){
		super(id);
		adulte = super.getEnfants().get(0);
		adulte.setLieu_naissance(new String());
		adulte.setDate_naissance(Calendar.getInstance().getTime());
		adulte.setAllergies(new String());
		super.setAssurance(new String());
		super.setNum_police(new String());	
		super.setId_hopital(null);
		super.setId_medecin(null);
	}
	
	/**
	 * @return Renvoie adulte.
	 */
	public Eleve getAdulte() {
		return adulte;
	}

	/**
	 * 
	 * @param e
	 */
	public void setEnfants(Eleve e){
		ArrayList<Eleve> el = new ArrayList<Eleve>(1);
		el.add(e);
		super.setEnfants(el);
	}
	
	/**
	 * 
	 * @return Renvoie commentaires
	 */
	public String getCommentaires(){
		return adulte.getObservations();
	}
	/** 
	 * 
	 * @param commentaires
	 */
	public void setCommentaires(String commentaires){
		this.adulte.setObservations(commentaires);
	}
}
