/*
 * Presence.java, 30 août 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               	Presence.java
 * Author's email :     	johan@x-tnd.be
 * Author's Website :   	http://cantine.sourceforge.net
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import be.xtnd.cantine.gui.Launcher;
import be.xtnd.commons.db.Database;

/**
 * 
 * 
 * @author Johan Cwiklinski
 *
 */
public class Presence {
	/** Nom de la talbe des présences */
    public static final String TABLE = "presence";
	/* CONSTANTES DE TYPE */
    /** Présence cantine */
	//public final static char CANTINE = 'C';
	/** Présence centre aéré */
	//public final static char CENTRE = 'E';
	/* CONSTANTES DES VALEURS DE PRESENCE */
	/** Wee End */
	public static final int WEEKEND = -1;
	/** Non présent */
	public final static int NON = 0;
	/** Présent */
	public final static int PRESENT = 1;
	/** Déjà déduit*/
	public final static int DEDUIT = 2;
	/** A rembourser */
	public final static int REMBOURSER = 3;
	/** Absence non justifiée */
	public final static int NON_JUSTIFIE = 4;
	/** En attente */
	public final static int ATTENTE = 5;
	/** Présence imprévue */
	public final static int IMPREVU = 6;
	/** Présence prépayée */
	public static final int PREPAYE = 7;
	/* VARIABLES DE CLASSE */
	private Database connexion;
	private Integer id_eleve;
	//private char type;
	
	private boolean lundi, mardi, mercredi, jeudi, vendredi, samedi, dimanche, atall=false;
	static Logger logger = Logger.getLogger(Presence.class.getName());

	/**
	 * 
	 */
	public Presence(){
		this.id_eleve = null;		
		this.lundi = false;
		this.mardi = false;
		this.mercredi = false;
		this.jeudi = false;
		this.vendredi = false;
		this.samedi = false;
		this.dimanche = false;
		this.atall = true;
		connexion = Launcher.db;	
	}
	
	/**
	 * Renseigne les variables de classe en fonction du contenu de la base
	 * 
	 * @param id_eleve identifiant de l'élève
	 */
	public Presence(Integer id_eleve){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/cantine/log4j.properties"));
		this.id_eleve = id_eleve;
		String strSql="SELECT * FROM presence WHERE id_eleve="+id_eleve;
		connexion = Launcher.db;	
		try {
			ResultSet rs_presence = connexion.execQuery(strSql);
			while(rs_presence.next()){
				this.lundi = rs_presence.getBoolean("lundi");
				this.mardi = rs_presence.getBoolean("mardi");
				this.mercredi = rs_presence.getBoolean("mercredi");
				this.jeudi = rs_presence.getBoolean("jeudi");
				this.vendredi = rs_presence.getBoolean("vendredi");
				this.samedi = rs_presence.getBoolean("samedi");
				this.dimanche = rs_presence.getBoolean("dimanche");
				if(!lundi && !mardi && !mercredi && !jeudi && !jeudi && !vendredi && !samedi && !dimanche) atall=true;
			}
		} catch (SQLException e) {
    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode());
    		e.printStackTrace();
    	}
	}
	
	/**
	 * Crée ou enregistre les donnée de présence
	 * 
	 * @return int resultat de la requête
	 */
	public int setPresence(){
		int result = 0;
		int compte = 0;
		String strCount = "SELECT COUNT(id_eleve) As compte FROM presence WHERE id_eleve="+this.id_eleve;
		String qryInsert = "INSERT INTO presence(id_eleve, lundi, mardi, mercredi, jeudi, vendredi, samedi, dimanche) " +
			"VALUES ("+this.id_eleve+", "+this.lundi+", "+this.mardi+", "+this.mercredi+", "+
			this.jeudi+", "+this.vendredi+", "+this.samedi + ", "+this.dimanche+")";
		String qryUpdate = "UPDATE presence SET lundi="+this.lundi+", mardi="+this.mardi+
		", mercredi="+this.mercredi+", jeudi="+this.jeudi+", vendredi="+this.vendredi+
		", samedi="+this.samedi+", dimanche="+this.dimanche+
		" WHERE id_eleve="+this.id_eleve;
		connexion = Launcher.db;
		try {
			ResultSet rs = connexion.execQuery(strCount);
			while(rs.next()){compte = rs.getInt("compte");}
			if(compte==0){
				result = connexion.execUpdate(qryInsert);
			}else{
				result = connexion.execUpdate(qryUpdate);
			}
		} catch (SQLException e) {
    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode());
			e.printStackTrace();
		}
		return result;
	}
		
	/**
	 * @return Renvoie id_eleve.
	 */
	public Integer getId_eleve() {
		return id_eleve;
	}
	/**
	 * @param id_eleve id_eleve à définir.
	 */
	public void setId_eleve(Integer id_eleve) {
		this.id_eleve = id_eleve;
	}
	/**
	 * @return Renvoie jeudi.
	 */
	public boolean isJeudi() {
		return jeudi;
	}
	/**
	 * @param jeudi jeudi à définir.
	 */
	public void setJeudi(boolean jeudi) {
		this.jeudi = jeudi;
	}
	/**
	 * @return Renvoie lundi.
	 */
	public boolean isLundi() {
		return lundi;
	}
	/**
	 * @param lundi lundi à définir.
	 */
	public void setLundi(boolean lundi) {
		this.lundi = lundi;
	}
	/**
	 * @return Renvoie mardi.
	 */
	public boolean isMardi() {
		return mardi;
	}
	/**
	 * @param mardi mardi à définir.
	 */
	public void setMardi(boolean mardi) {
		this.mardi = mardi;
	}
	/**
	 * @return Renvoie mercredi.
	 */
	public boolean isMercredi() {
		return mercredi;
	}
	/**
	 * @param mercredi mercredi à définir.
	 */
	public void setMercredi(boolean mercredi) {
		this.mercredi = mercredi;
	}
	/**
	 * @return Renvoie samedi.
	 */
	public boolean isSamedi() {
		return samedi;
	}
	/**
	 * @param samedi samedi à définir.
	 */
	public void setSamedi(boolean samedi) {
		this.samedi = samedi;
	}
	/**
	 * @return Renvoie vendredi.
	 */
	public boolean isVendredi() {
		return vendredi;
	}
	/**
	 * @param vendredi vendredi à définir.
	 */
	public void setVendredi(boolean vendredi) {
		this.vendredi = vendredi;
	}
	/**
	 * @return Renvoie dimanche.
	 */
	public boolean isDimanche() {
		return dimanche;
	}
	/**
	 * @param dimanche dimanche à définir.
	 */
	public void setDimanche(boolean dimanche) {
		this.dimanche = dimanche;
	}

	/**
	 * @return Renvoie atall.
	 */
	public boolean isAtall() {
		return atall;
	}
}
