/*
 * Facture.java, 30 sept. 2005
 * 
 * This file is part of cantine.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               	Facture.java
 * Author's email :     	johan@x-tnd.be
 * Author's Website :   	http://cantine.sourceforge.net
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.cantine;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import be.xtnd.cantine.gui.Launcher;
import be.xtnd.commons.db.Database;

/**
 * 
 * 
 * @author Johan Cwiklinski
 *
 */
public class Facture {
	private Integer id_famille, id_facture;
	private String nom;
	private Date date;
	private float ht, net, acompte, deduire;
	private boolean regle;
	private Database db;
	/** Nom de la table factures */
	public static final String TABLE = "factures";
	/** Nom de la table des détails de factures */
	public static final String DETAILS_TABLE = "details_facture";
	/** Clé primaire de la table factures */
	public static final String PK = "id_facture";
	/** Requête de liste des factures sans clause order by*/
	public static final String QUERY_NOT_ORDERED = "SELECT id_facture As Num, date As Date, nom As Nom, ht As Total, " +
	"(ht-acompte-deduire) As Net, regle As Paye FROM factures INNER JOIN familles ON " +
	"(factures.id_famille=familles.id_famille)";
	/** Requête de liste des factures */
	public static final String ORDER_CLAUSE = " ORDER BY date, nom";
	/** Requête de liste des factures */
	public static final String QUERY_LIST = QUERY_NOT_ORDERED + ORDER_CLAUSE; 
	static Logger logger = Logger.getLogger(Facture.class.getName());
	
	/**
	 * Initialise une facture
	 *
	 */
	public Facture(){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/cantine/log4j.properties"));
	}
	
	/**
	 * Initialise une facture selon l'id spécifiée
	 * @param id id de la facture à initialiser
	 */
	public Facture(Integer id){
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/cantine/log4j.properties"));
		db =  Launcher.db;
		String sqlSelectInvoice = "SELECT * FROM "+TABLE+" INNER JOIN "+Famille.TABLE+
			" ON("+Famille.TABLE+"."+Famille.PK+"="+TABLE+"."+Famille.PK+")"+
			" WHERE "+PK+"="+id;
		try {
			ResultSet rs = db.execQuery(sqlSelectInvoice);
			while(rs.next()){
				this.id_facture = id;
				this.id_famille = new Integer(rs.getInt(Famille.PK));
				this.nom = rs.getString("nom");
				this.date = rs.getDate("date");
				this.regle = rs.getBoolean("regle");
				this.ht = rs.getFloat("ht");
				this.acompte = rs.getFloat("acompte");
				this.deduire = rs.getFloat("deduire");
				this.net = this.ht - this.acompte - this.deduire;
			}
		} catch (SQLException e) {
			logger.error("SQLException Facture(Integer id) : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			e.printStackTrace();
		}
	}
	
	/**
	 * Marque la facture comme réglée
	 * @param id id de la facture à modifier
	 * @param settled boolean réglé / non réglé
	 * @return entier résultat
	 */
	public int settleInvoice(Integer id, boolean settled){
		int response = 0;
		String sqlUpdate = "UPDATE "+TABLE+" SET regle="+settled+" WHERE "+PK+"="+id;
		try {
			response = db.execUpdate(sqlUpdate);
		} catch (SQLException e) {
			logger.error("SQLException settleInvoice(...) : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			e.printStackTrace();
		}
		return response;
	}
	
	/**
	 * Enregistre les modification d'une facture
	 * @param id id de la facture à modifier
	 */
	public void enregistreMontants(Integer id){
		String sqlUpdate = "UPDATE "+TABLE+" SET acompte="+this.acompte+", deduire="+this.deduire+" WHERE "+PK+"="+id;
		try {
			db.execUpdate(sqlUpdate);
		} catch (SQLException e) {
			logger.error("SQLException enregistreMontants(Integer id) : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			e.printStackTrace();
		}
	}
		
	/**
	 * Calcul des factures selon les paramètres passés
	 * @param mois mois d'édition. Les présences sont calculées sur le mois suivant,
	 * les pointages sur le mois d'édition. 
	 * @param annee année d'édition
	 * @param factDate date de facture
	 */
	public void calculFactures(int mois, int annee, Date factDate){
		String sqlFamilles = "SELECT DISTINCT "+Famille.PK+" FROM "+Eleve.TABLE+" WHERE cantine=true";

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		/* dates mois en cours */
		Calendar calendar = new GregorianCalendar(annee, mois, 1);
		String beginDate = df.format(calendar.getTime());
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		String endDate = df.format(calendar.getTime());
		/* dates mois précédent (recherche facture) */
		calendar.add(Calendar.MONTH, -1);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		String pEndDate = df.format(calendar.getTime());
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		String pBeginDate = df.format(calendar.getTime());
		/* dates mois suivant (présences prévues) */
		calendar.add(Calendar.MONTH, 2);
		String bDate = df.format(calendar.getTime());
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		String eDate = df.format(calendar.getTime());

		db =  Launcher.db;

    	try{    		
    		Statement stmt = db.getConnection().createStatement();
    		ResultSet rs = db.execQuery(sqlFamilles, stmt);
    		while(rs.next()){
    			Integer id_facture = new Integer(-1);
    			Integer id_famille = new Integer(rs.getInt(Famille.PK));
    			String date_facture = new String();

    			String sqlFactExists = "SELECT * FROM "+TABLE+" WHERE id_famille="+id_famille+
					" AND date BETWEEN '"+beginDate+"' AND '"+endDate+"'";
    			ResultSet rsExists = db.execQuery(sqlFactExists);
    			rsExists.last();
    			boolean fact_exists = (rsExists.getRow()<1)?false:true;
    			if(fact_exists){
    				id_facture = new Integer(rsExists.getInt(PK));
    				date_facture = rsExists.getString("date");
    			}
    			System.err.println("Does fact exists ? " + fact_exists);


    			String sqlVerif = "SELECT * FROM "+Planning.TABLE+
					" INNER JOIN "+Eleve.TABLE+" ON("+Eleve.TABLE+"."+Eleve.PK+"="+Planning.TABLE+"."+Eleve.PK+")"+
					" INNER JOIN "+Famille.TABLE+" ON("+Famille.TABLE+"."+Famille.PK+"="+Eleve.TABLE+"."+Famille.PK+")"+
					" WHERE ((id_type="+Presence.PRESENT+" AND date BETWEEN '"+bDate+"' AND '"+eDate+"')" +
					" OR ((id_type!="+Presence.PRESENT+" AND id_type!="+Presence.PREPAYE+") AND date BETWEEN '"+beginDate+"' AND '"+endDate+"'))"+
					" AND "+Famille.TABLE+"."+Famille.PK+"="+id_famille;
        		ResultSet rsVerif = db.execQuery(sqlVerif);
        		rsVerif.last();
        		if(rsVerif.getRow()<1){ //aucun pointage existant
        			System.err.println("Pas de pointages");
        			if(fact_exists){ //facture existante, la supprimer
        			//if(rsExists.getRow()>1){ //facture existante, la supprimer
            			System.err.println("Une facture existe déjà. Elle doit être supprimée.");
            			String delFactExists = "DELETE FROM "+TABLE+" WHERE id_famille="+id_famille+
            				" AND date BETWEEN '"+beginDate+"' AND '"+endDate+"'";
            			db.execUpdate(delFactExists);
        			}
        		}else{
	    			/* infos facture famille */
	    			if(!fact_exists){ //cas nouvelle facture
	    			//if(rsExists.getRow()<1){ //cas nouvelle facture
	    				String sqlInsertFamille = "INSERT INTO "+TABLE+" VALUES(null,'"+df.format(factDate)+
							"', "+id_famille+", 0, 0, 0, false)";
						db.execUpdate(sqlInsertFamille);
						//FIXME sortir de l'application
						if(Launcher.conf.getSql_provider().equalsIgnoreCase("hsqldb")){
							
						}
			    		//ResultSet rsId = db.execQuery("SELECT LAST_INSERT_ID()");
			    		//ResultSet rsId = db.execQuery("CALL IDENTITY()");
						ResultSet rsId = db.execQuery(
								(Launcher.conf.getSql_provider().equalsIgnoreCase("hsqldb"))?
										"CALL IDENTITY()":
											"SELECT LAST_INSERT_ID()");

			    		rsId.first();
			    		id_facture = new Integer(rsId.getInt(1));
			    		System.err.println("id facture = " + id_facture);
			    		date_facture = df.format(factDate);
	    			}/*else{ //cas facture existante
	    				id_facture = new Integer(rsExists.getInt(PK));
	    				date_facture = rsExists.getString("date");
	    			}*/
	
	    			/* date facture précédente */
	    			String precedingInvoice = "SELECT date FROM "+TABLE+" WHERE id_famille="+id_famille+
						" AND date BETWEEN '"+pBeginDate+"' AND '"+pEndDate+"'";
	    			ResultSet pInvoice = db.execQuery(precedingInvoice);
	    			pInvoice.last();
	    			String date_facture_precedente = (pInvoice.getRow()==1)?pInvoice.getString("date"):pBeginDate; 
	    			
	    			String sqlEleves = "SELECT * FROM "+Eleve.TABLE+" WHERE "+Famille.PK+"="+id_famille;
	    			Statement stmtEl = db.getConnection().createStatement();
	    			ResultSet rsEleves = db.execQuery(sqlEleves, stmtEl);
	
	    			rsEleves.last();
	    			
	    			rsEleves.beforeFirst();
	    			int eleveEnCours = 0;
	    			while(rsEleves.next()){
	    				eleveEnCours++;
	    				int present = 0;
	    				int imprevu = 0;
	    				int rembours = 0;
	    				int deduit = 0;
	    				int compte = 0;
	    				
	    				int id_eleve = rsEleves.getInt(Eleve.PK);
						    				
	    				String sqlPresences = "SELECT COUNT("+Eleve.PK+") FROM "+Planning.TABLE+" WHERE "
							+Eleve.PK+"="+id_eleve+" AND id_type="+Presence.PRESENT+" AND date BETWEEN '"
							+bDate+"' AND '"+eDate+"'";
	    				
	    				ResultSet rsPresences = db.execQuery(sqlPresences);
	    				rsPresences.last();
	    				present = rsPresences.getInt(1);
	    				
	    				String sqlPointages = "SELECT * FROM "+Planning.TABLE+" WHERE "+
							Eleve.PK+"="+id_eleve+" AND id_type!="+Presence.PRESENT+" AND id_type!="+Presence.PREPAYE+" AND date BETWEEN '"+
							date_facture_precedente+"' AND '"+date_facture+"'";
	    				ResultSet rsPointages = db.execQuery(sqlPointages);
	    				while(rsPointages.next()){
	    				    System.err.println(rsPointages.getInt("id_type"));
	    					switch(rsPointages.getInt("id_type")){
	    						case 2:
	    							deduit++;
	    							break;
	    						case 3:
	    							rembours++;
	    							break;
	    						case 6:
	    							imprevu++;
	    							break;
	    					}
	    				}
	    				
	    				compte = present + imprevu - rembours - deduit;
	    				if(compte>0){
	            			String sqlDetailFact = "INSERT INTO "+DETAILS_TABLE+" VALUES("+id_facture+", "+id_eleve+
							", "+compte+", 0)";
	            			try{db.execUpdate(sqlDetailFact);}catch(SQLException esql){
	            				if(Launcher.sql_errors.isDuplicate(esql.getErrorCode())){
	            					String sqlUpdateDetails = "UPDATE "+DETAILS_TABLE+" SET quantite="+compte+
										" WHERE id_facture="+id_facture+" AND id_eleve="+id_eleve;
	            					db.execUpdate(sqlUpdateDetails);
	            				}else{
	            					logger.error("SQLException sqlDetailFact : "+esql.getMessage()+" (code "+esql.getErrorCode()+")");
	            				}
	            			}
	    				}else{
	    					logger.info("Compte nul. Vérifie si une ligne existe déjà");
	    				    String sqlVerifDetail = "SELECT * FROM "+DETAILS_TABLE+" WHERE "+PK+"="+id_facture+
	    				    	" AND "+Eleve.PK+"="+id_eleve;
	    				    ResultSet rsVerifDetail = db.execQuery(sqlVerifDetail);
	    				    rsVerifDetail.last();
	    				    if(rsVerifDetail.getRow()>=1){
	    				    	logger.info("Suppression détail");
	    				    	String sqlDelDetail = "DELETE FROM "+DETAILS_TABLE+" WHERE "+PK+"="+id_facture+
	    				    		" AND "+Eleve.PK+"="+id_eleve;
	    				    	db.execUpdate(sqlDelDetail);
	    				    }
	    				}
	    			}
	    			stmtEl.close();
	    			
	        		String sqlRecupDetails = "SELECT * FROM "+DETAILS_TABLE+" WHERE "+PK+"="+id_facture+" ORDER BY quantite DESC";
	        		Statement stmtDetails = db.getConnection().createStatement();
	    			ResultSet rsDetails = db.execQuery(sqlRecupDetails, stmtDetails);
	    			int cpteDetail = 1;
	    			float tarifApplicable = 0;
	    			Object[][] tarifs = Launcher.options.getTarifs();
	    			float totalHt = 0;
	    			while (rsDetails.next()){
	    				float price = 0;
	    				for(int i = 0 ; i < tarifs.length ; i++){
	    					if(new Integer(tarifs[i][1].toString()).intValue()<=cpteDetail){
		    					price = new Float(tarifs[i][2].toString()).floatValue()*rsDetails.getInt("quantite");
		    					tarifApplicable = new Float(tarifs[i][2].toString()).floatValue();
	    					}
	    				}
	    				if(cpteDetail>tarifs.length){
	    					price = new Float(tarifs[tarifs.length-1][2].toString()).floatValue()*rsDetails.getInt("quantite");
    					    tarifApplicable = new Float(tarifs[tarifs.length-1][2].toString()).floatValue();
	    				}
	    				String sqlUpdateDetails = "UPDATE "+DETAILS_TABLE+" SET prix="+tarifApplicable+
							" WHERE "+Eleve.PK+"="+rsDetails.getInt("id_eleve")+" AND "+PK+"="+id_facture;
	    				db.execUpdate(sqlUpdateDetails);
	    				totalHt+= price;
	    				cpteDetail++;
	    			}
	    			stmtDetails.close();
	    			if(cpteDetail==1){ //aucun détail, supprimer la facture
	    				logger.info("La facture "+id_facture+" ne possède pas de détail. La supprimer");
	    			    String sqlDelInvoice = "DELETE FROM "+TABLE+" WHERE "+PK+"="+id_facture;
	    			    db.execUpdate(sqlDelInvoice);
	    			}else{
	    			    //	calcul prix facture
	    			    String sqlUpdateInvoice = "UPDATE "+TABLE+" SET ht="+totalHt+" WHERE "+PK+"="+id_facture;
	    			    db.execUpdate(sqlUpdateInvoice);
	    			}
        		}
    		}    		
    	}catch(SQLException e){
    		logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode()+")");
    		e.printStackTrace();
    	}
	}
	
	/**
	 * Supprime la facture de la base
	 *
	 */
	public void supprimeFacture(){
	    db =  Launcher.db;
	    String delQry = "DELETE FROM "+TABLE+" WHERE "+PK+"="+id_facture;
	    try {
            db.execUpdate(delQry);
        } catch (SQLException e) {
        	logger.error("SQLException : "+e.getMessage()+" (code "+e.getErrorCode()+")");
        	e.printStackTrace();
        }
	}
	
	/**
	 * @return Renvoie date.
	 */
	public Date getDate() {
		return date;
	}
	/**
	 * @return Renvoie ht.
	 */
	public float getHt() {
		return ht;
	}
	/**
	 * @return Renvoie id_facture.
	 */
	public Integer getId_facture() {
		return id_facture;
	}
	/**
	 * @return Renvoie id_famille.
	 */
	public Integer getId_famille() {
		return id_famille;
	}
	/**
	 * @return Renvoie nom.
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @return Renvoie regle.
	 */
	public boolean isRegle() {
		return regle;
	}

	/**
	 * 
	 * @return Renvoie le montant à déduire
	 */
	public float getDeduire() {
		return deduire;
	}

	/**
	 * 
	 * @param deduire Fixe le montant à déduire
	 */
	public void setDeduire(float deduire) {
		this.deduire = deduire;
	}

	/**
	 * 
	 * @return Renvoie le net à payer
	 */
	public float getNet() {
		return net;
	}

	/**
	 * 
	 * @return Renvoie l'acompte
	 */
	public float getAcompte() {
		return acompte;
	}

	/**
	 * 
	 * @param acompte Fixe l'acompte
	 */
	public void setAcompte(float acompte) {
		this.acompte = acompte;
	}
}
