#!/bin/bash

#set -x
if [ $# -gt 0 ]
then
	VERSION=$1
else
	echo "Usage: make_distrib_package.sh version "
	exit 1
fi
#set +x

VERSION=$1

mkdir -p tmp/cantine
cp dist/installers/cantine-$VERSION.jar install.sh readme.txt license.txt tmp/cantine
cd tmp
tar jcvf cantine-$VERSION.tar.bz2 cantine/ 
mv cantine-$VERSION.tar.bz2 ../
cd ..
rm -rf tmp
