INSTRUCTIONS D'INSTALLATION

0-CONTENU DE L'ARCHIVE

- install-cantine.jar : fichier java éxécutable pour l'installation
- install.sh : script exécutable de lancement de l'installation
- readme.txt : ce fichier
*****- cantine.sql : script d'initialisation de la base de données mysql

1-BASE DE DONNÉES MYSQL

- connectez-vous à votre serveur MySQL 
	$ mysql -u root [-p]
Le flag -p est requis si votre utilisateur root possède un mot de passe.
Notez qu'un utilisateur root sans mot de passe rend votre base de données insécure.
- créez la base de données qui recevra les données :
	mysql> CREATE DATABASE cantine;
Vous êtes libres de choisir le nom de base qui vous conviendra.
- insérez le contenu du fichier cantine.sql dans votre nouvelle base
	mysql>USE cantine;
	mysql>SOURCE cantine.sql;
- créez (pour l'utilisation) un utilisateur pour cette base 
	mysql>GRANT USAGE ON cantine.* TO 'cantine'@'%' IDENTIFIED BY 'cantine';

2-INSTALLATION DU PROGRAMME
- décompresser le package
        $ tar jxvf cantine.tar.bz2
- éxécuter le script install.sh
        $ chmod +x install.sh
        $ ./install.sh
- suivre la procédure (acceptation de la license, choix du chemin d'installation et des packages, créations des raccourcis).

